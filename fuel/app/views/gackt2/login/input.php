<div class="description">
	<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
	<p>ログインID（メールアドレス）とパスワードを入力してください。</p>
</div>

<div class="panel login">
	<h4 class="subheadline">会員ログイン</h4>
	<div class="description" id="cookie_error"></div>

	<div class="panel-body">
		<?php echo Form::open(array('id' => 'login','class' => 'form-horizontal'));?>
		<div class="form-group">
			<?php echo Form::label('メールアドレス', 'email',array("class" => "")); ?>
			<div class="form-container">
				<div class="inner">
					<?php echo Form::input('email', Input::post('email', ''),
						array('class' => 'validate[required] form-control','placeholder' =>'XXXXXXX@XXXXX.COM','type' => 'email')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<?php echo Form::label('パスワード', 'password',array("class" => "col-sm-2 col-md-offset-1 col-xs-4 control-label")); ?>
			<div class="form-container">
				<div class="inner">
					<?php echo Form::password('password', Input::post('password', ''),
						array('class' => 'validate[required,minSize[3],maxSize[12]] form-control col-sm-4','placeholder' =>'3〜12文字の英数字')); ?>
				</div>
			</div>
		</div>
		<div class="buttons">
			<?php echo Form::submit('exec', 'ログイン', array('class' => 'button rect key size-L')); ?>
		</div>
		<?php echo Form::close();?>
		<div class="buttons count-2">
			<?php echo Html::anchor('/regist', '新規会員登録はこちら',array('class' => 'button text'),true) ?>
			<?php echo Html::anchor('/forget', 'パスワードを忘れた方はこちら',array('class' => 'button text'),true) ?>
		</div>
	</div>
	<div class="description notice">
		既存通販サイト「GACKT STORE」やファンクラブ「G&LOVERS」のアカウントとは別になりますので、別途新規会員登録をお願いいたします。(2015年9-11月「神威♂楽園 」(氣志團万博2015 / HALLOWEEN PARTY 2015) グッズ通販サイトとは同アカウントとなります)<br>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#login").validationEngine({promptPosition : "topLeft"});
		// クッキー の有効/無効を取得
		if (navigator.cookieEnabled) {
			if ($('#alert').is('*')) {
				$('#alert').remove();
			}
		} else {
			$('#cookie_error').append('<div id="alert"><font color="red">続行するには、Webブラウザのcookieを有効にしてください<br>Cookieの設定方法については、ご利用のWebブラウザのツールバーにある「ヘルプ」をご覧ください。</font></div>');
		}
	});
</script>



