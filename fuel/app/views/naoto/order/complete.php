<div class="panel complete">
	<h4 class="subheadline">注文完了</h4>
	<h5 class="heading">ご注文いただき誠にありがとうございました。<br><span>受注番号：<?php echo $order->order_id;?></span></h5>

	<div class="description">
		<p>
			ご注文内容を確認する場合は、「マイページ」より上記受注番号からご確認ください。（会員登録された方のみ）<br /><br />
			また、注文確認のメールを「<?php echo $order->order_email;?>」宛に送信しておりますので、会員登録されてない方はこちらで注文内容をご確認いただけます。<br /><br />

			<?php if ($order->payment == 1) : ?>
				※請求明細の請求屋号は「official-goods-store.jp」になります。<br /><br />
			<?php endif;?>
			<?php if ($order->payment == 4) : ?>
				※商品発送後、別途請求書が郵送されます。請求書到着後14日以内にお支払いください。<br /><br />
			<?php endif;?>

			<?php if ($order->payment == 2) : ?>
				オンライン決済番号または、受付番号を紙などに控えてご指定いただいたコンビニエンスストアにて、下記の支払い期限までにお支払いください。<br><br>
				お支払い先コンビニ：<?php echo Config::get("cvs.".$order->cvs_type);?><br>
				<?php if (substr($order->cvs_type ,0,-1) == "econ") : ?>
					受付番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php elseif (substr($order->cvs_type ,0,-1) == "sej") : ?>
					オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php elseif (substr($order->cvs_type ,0,-1) == "other") : ?>
					オンライン決済番号：<?php echo $order->cvs_receipt_no;?><br>
				<?php endif;?>
				<?php if ($order->cvs_haraikomi_url) : ?>
					払込票URL：<a href="<?php echo $order->cvs_haraikomi_url;?>" target="_blank"><?php echo $order->cvs_haraikomi_url;?></a><br>
				<?php endif;?>
				支払期限：<?php echo $order->cvs_limit_date;?><br>
			<?php endif;?>
		</p>
	</div>

	<div class="notice">
		<strong>※注文確認メールが届かない場合</strong>
		<p class="description">
			注文確認メールが届かない場合でも、正常にご注文をお受けさせていただいております。<br />
			表示された受注番号にてマイページより内容をご確認ください。<br /><br />
		</p>
		<strong>※注文確認メールが届かない原因</strong>
		<ul>
			<li>( ! ) メールアドレス間違い</li>
			<li>( ! ) 迷惑メールボックスに注文確認メールが入っている</li>
			<li>( ! ) スマートフォンやフィーチャーフォンをご利用の場合、「PCからのメールを受信しない」「なりすまし設定を有効にしている」等のメール受信設定に引っかかっている（ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。）</li>
		</ul>
		<?php if ($order->payment == 4) : ?>
			<br />
			<strong>※代金のお支払いについて</strong>
			<ul>
				<li>・当店にかわり、後払い.com運営会社の(株)キャッチボールより請求書が送られます。</li>
				<li>・商品到着と請求書の到着は別になります。</li>
				<li>・請求書発行から14日後までにお支払い下さい。</li>
				<li>・銀行 / 郵便局 / コンビニでお支払いいただけます。</li>
				<?php if ($order->result != 1) : ?>
					<li>・お届け先の住所 / 電話番号を間違って登録してしまった場合、「後払い.com」の審査が通らないことがあります。その場合は別途メールにてご連絡いたしますので、メール内容ご確認ください。</li>
				<?php endif;?>
			</ul>
		<?php endif;?>
		<br />
		<!--        <p class="caption">※数分たっても届かない場合には上記をご確認の上、--><?php //echo Html::anchor('/inquiry','お問い合わせ') ?><!--よりご連絡ください。</p>-->
	</div>
</div>


