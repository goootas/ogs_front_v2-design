<div>
	<strong>配送方法 / 送料について</strong><br>
	<br>
	■配送<br>
	佐川急便での配送になります｡(日本国内のみ)<br><br>
	■送料<br>
	全国一律¥550(+tax / 1回のご注文につき)<br>
	<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
		※1回のご注文合計金額が¥<?php echo number_format($shop_data["free_deliver_price"]);?>(tax in)以上のお客様は送料無料とさせていただきます<br>
	<?php endif; ?>
</div>
<br>
<?php echo Form::open("/order/input3");?>
<div>
	<h4>お届け先</h4>
	<hr>
	お名前※必須
	<div>
		<?php echo Form::input('deliver_username_sei', Session::get("order_data.data2.deliver_username_sei") ? Session::get("order_data.data2.deliver_username_sei") : Session::get("order_data.data1.order_username_sei"),
			array("size"=>"10")); ?>
		<?php echo Form::input('deliver_username_mei', Session::get("order_data.data2.deliver_username_mei") ? Session::get("order_data.data2.deliver_username_mei") : Session::get("order_data.data1.order_username_mei"),
			array("size"=>"10")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
	<br>
	郵便番号※必須
	<div class="col-sm-3 col-xs-8">
		<?php echo Form::input('deliver_zip', Session::get("order_data.data2.deliver_zip") ? Session::get("order_data.data2.deliver_zip") : Session::get("order_data.data1.order_zip"),
			array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		ハイフン(-)は抜いてください
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_zip')); ?></font>
	<br>

	都道府県※必須
	<div class="col-sm-2 col-xs-8">
		<?php echo Form::select('deliver_state', Session::get("order_data.data2.deliver_state") ? Session::get("order_data.data2.deliver_state") : Session::get("order_data.data1.order_state"),$prefecture_data);?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_state')); ?></font>
	<br>

	住所１※必須
	<div>
		<?php echo Form::input('deliver_address1', Session::get("order_data.data2.deliver_address1") ? Session::get("order_data.data2.deliver_address1") : Session::get("order_data.data1.order_address1")); ?><br>
		市区町村
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_addr1')); ?></font>
	<br>

	住所２※必須
	<div>
		<?php echo Form::input('deliver_address2', Session::get("order_data.data2.deliver_address2") ? Session::get("order_data.data2.deliver_address2") : Session::get("order_data.data1.order_address2")); ?><br>
		番地 / 建物名など
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_addr2')); ?></font>
	<br>

	電話番号※必須
	<div>
		<?php echo Form::input('deliver_tel1', Session::get("order_data.data2.deliver_tel1") ? Session::get("order_data.data2.deliver_tel1") : Session::get("order_data.data1.order_tel1"),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('deliver_tel2', Session::get("order_data.data2.deliver_tel2") ? Session::get("order_data.data2.deliver_tel2") : Session::get("order_data.data1.order_tel2"),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('deliver_tel3', Session::get("order_data.data2.deliver_tel3") ? Session::get("order_data.data2.deliver_tel3") : Session::get("order_data.data1.order_tel3"),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_tel')); ?></font>
	<br>

	お届け日
	<div>
		<?php
		if(isset($deliver_term_date)){
			echo $deliver_term_date->deliver_start_text;
		}else{
			echo Form::select('delivery_date', Session::get("order_data.data2.delivery_date"),$delivery_date);
			echo '<br>注文状況の混み具合により、お届け日が異なる場合がございますことを予めご了承ください。';
		}
		?>
	</div>
	<br>

	お届け希望時間帯<br>
	<div>
		<?php
		if(isset($deliver_term_date)){
			echo "指定不可";
		}else {
			?>
			<?php echo Form::select('delivery_time', Session::get("order_data.data2.delivery_time"), Config::get("delivery_time"));
		}?>
	</div>
	<br>

	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>
	<br>
	<div>
		<?php echo Html::anchor('/order/input1'.$session_get_param, '<font color="#000000">前に戻る</font>'); ?>
	</div>

</div>
<?php echo Form::hidden('delivery_comment',""); ?>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
