<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<!-- 購入商品情報 -->
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<div class="cart-list">
	<table>
		<?php
		$total = 0;
		$total_tax = 0;
		?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="total">小計<br />&nbsp;</th>
			<th class="delete"></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			$anchor_txt = $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
			?>
			<tr class="list">
				<td>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td><?php echo number_format($data["num"]);?>&nbsp;&nbsp;</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
				<td></td>
			</tr>
		<?php endforeach; ?>
		<tr class="fixed">
			<td></td>
			<td></td>
			<td>商品合計</td>
			<td>¥<?php echo number_format(intval($total_tax));?></td>
			<td></td>
		</tr>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="description">
		<p>
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
					送料無料です<br>
				<?php else :?>
					あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
				<?php endif; ?>
			<?php endif; ?>
		</p>
		<p style="color: #FF0000">
			<?php if(Session::get("payment_ctl_cnt") < 3 && Session::get("payment_ctl_cnt") > 0):?>
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			<?php endif; ?>
		</p>
	</div>
</div>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">

	<?php $ses_name = Session::get("order_data.dataname.names_value");?>
	<h4 class="headline">ネームプリント文字入力</h4>
	<?php $counter = 0;?>
	<?php foreach ($cart as $key => $data): ?>
		<?php if(array_search($data["id"], Config::get("naire_product_ids")) !== false): ?>
			<?php
			$max_length = Config::get("naire_max_length.".$data["id"]);
			?>
			<?php for ($i = 0; $i < $data["num"]; $i++) { ?>
				<div class="form-group">
					<?php echo Form::label($data["title"].' '.$data["op1"].' '.$data["op2"], 'names',array("style" => "line-height:15px")); ?>
					<div class="form-container">
						<div class="inner">
							<?php echo Form::input('names_value[]',$ses_name[$counter],
							array('id' => 'names_value_'.$i."_".$data["id"] , 'class' => 'validate[required,custom[naire],maxSize['.$max_length.']] form-control','inputmode' => "verbatim")); ?>
							<?php echo Form::hidden('names_view[]',$data["title"].' '.$data["op1"].' '.$data["op2"]); ?>
							<?php echo Form::hidden('names_did[]',$data["did"]); ?>
						</div>
					</div>
				</div>
				<?php $counter++;?>
			<?php } ?>
		<?php endif;?>
	<?php endforeach; ?>

	<div class="buttons count-2">
		<img src="/gackt/assets/img/gackt/GC039_06.jpg" alt="" style="display: block;margin-left: auto; margin-right: auto; width: 60%"/>
	</div>

	<div class="buttons count-2">
		<div class="description notice">
			※プリント可能文字種<br>
			・半角英数字(大文字/小文字)<br>
			・半角記号（!”#$%&’()=~|`{+}<>?_-^¥@[:];,./_）<br>
			・半角スペース(空白)<br>
			(文字フォント / カラー指定不可)<br>
			<br>
			※プリント可能最大文字数(空白含む)<br>
			・LVL GUEST PASS Tシャツ：17文字<br>
			・タンクトップ (loves GACKT)：15文字<br>
			<br>
			※カート画面に戻ると入力文字はリセットされます<br>
			<br>
			※必ずご注文全該当商品分の入力をお願いします<br>
			<br>
			※ご注文確定後のプリント内容変更はできません<br>
		</div>
	</div>

	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input1");
			$("#order").submit();
		});
	});

	$(function(){
		$("#login").validationEngine({promptPosition : "topLeft"});
		$("#order").validationEngine({promptPosition : "topLeft"});
		$("input").keydown(function(e) {
			if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
				return false;
			} else {
				return true;
			}
		});
	});
</script>