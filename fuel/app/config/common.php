<?php
return array(
    "aws" => array(
        "key"		=> "AKIAJ3BTO5BEOOJQXHMA",
        "secret"	=> "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",
        "region"	=> "ap-northeast-1",
        "image_path" => "product/",
    ),
//    "zip_api_server" => array(
//        "http://zip.cgis.biz/xml/zip.php?zn=",
//        "http://zip2.cgis.biz/xml/zip.php?zn=",
//    ),
//    "free_deliver_price" => 5400,
//    "company_title" => "MN CO.,LTD.",
//    "site_title" => "TOKYO ELECTRIC MODE",
//    "site_email" => "info@electricmode.com",
    "cypt_key" => "flat",
//    "sync_root_dir" => APPPATH.'../../sync/',
//    "sync_img_product_dir" => 'product/',
    'role' => array(
        0 => "店舗",
        1 => "管理者",
    ),

    // ステータス
    'status_search' => array(
        "" => "全て",
        "1" => "有効",
        "0" => "無効",
        "-1" => "削除",
    ),

    'status_regist' => array(
        "1" => "有効",
        "0" => "無効",
//      "-1" => "削除",
    ),

    'status_value' => array(
        "enable" => 1,
        "disable" => 0,
        "deleted" => -1,
    ),
    // 在庫
    'stock_search' => array(
        "" => "全て",
        "0" => "しない",
        "1" => "する",
    ),

    'stock' => array(
        0 => "しない",
        1 => "する",
    ),

    'stock_value' => array(
        "enable" => 1,
        "disable" => 0,
    ),

    // デバイス
//    'device_types' => array(
//        0 => "共通",
//        1 => "PC/SP",
//        2 => "FP",
//    ),

    // セール価格
    'sale_regist' => array(
        0 => "設定する",
        1 => "設定しない",
    ),
    // 会員価格
    'member_regist' => array(
        0 => "設定する",
        1 => "設定しない",
    ),
    // その他
    'other_regist' => array(
        0 => "無効",
        1 => "有効",
    ),
    // その他
    'discount_regist' => array(
        1 => "%",
        2 => "円",
    ),
    'member' => array(
        'status' => array(
            0 => "無料会員",
            1 => "有料会員",
        ),
        'value' => array(
            "free"		=> 0,
            "paid"		=> 1,
        ),
    ),
    'inquiry' => array(
        'status' => array(
            "" => "全て",
            0 => "問合せ",
            1 => "返信済",
        ),
        'value' => array(
            "all"		=> "",
            "send"		=> 0,
            "answer"	=> 1,
        ),
    ),
    'mail' => array(
        'type' => array(
            0 => "問い合わせ",
            1 => "仮登録",
            2 => "本登録完了",
            3 => "退会完了",
            4 => "パスワード忘れ",
            5 => "注文完了",
            6 => "配送完了",
            7 => "リマインド",
            8 => "入金確認",
            9 => "メルマガ",
            10 => "受注キャンセル",
        ),
        'value' => array(
            "inquiry"	=> 0,
            "temp"		=> 1,
            "regist"	=> 2,
            "leave"		=> 3,
            "forget"	=> 4,
            "order"		=> 5,
            "deliver"	=> 6,
            "remind"	=> 7,
            "receipt"	=> 8,
            "magazine"	=> 9,
            "cancel"	=> 10,
        ),
    ),
    'order' => array(
        'status' => array(
            'status' => array(
                1 => "与信&売上",
                2 => "再取引",
                3 => "売上",
                4 => "キャンセル",
                5 => "発行",
                6 => "代引き",
            ),
            'value' => array(
                "authorize"		=> 1,
                "reauthorize"	=> 2,
                "capture"		=> 3,
                "cancel"		=> 4,
                'publish'		=> 5, // コンビニ決済用
                'cod'           => 6, // 代引き用
                'ato'           => 7, // 後払い用
            ),
        ),
        'result' => array(
            'status' => array(
                0 => "失敗",
                1 => "成功",
                9 => "保留",
            ),
            'value' => array(
                "failure"	=> 0,
                "success"	=> 1,
                "pending"	=> 9,
            ),
        ),
        'result_ato' => array(
            'status' => array(
                0 => "与信中",
                1 => "与信OK",
                2 => "与信NG",
            ),
            'value' => array(
                "check"	=> 0,
                "success"	=> 1,
                "failure"	=> 2,
            ),
        ),
    ),
    'payment' => array(
        'status' => array(
            "" => "指定なし",
            1 => "クレカ",
            2 => "コンビニ",
            3 => "代引き",
            4 => "後払い",
			5 => "NP後払い",
			6 => "楽天ID決済",
        ),
        'status_user' => array(
            "" => "指定なし",
            1 => "クレジットカード",
            2 => "コンビニ決済",
            3 => "代金引換",
            4 => "後払い",
            5 => "NP後払い",
			6 => "楽天ID決済",
        ),
        'value' => array(
            "nothing"   => "",
            "card"      => 1,
            "cvs"       => 2,
            "cod"       => 3,
            "ato"       => 4,
            "npato"     => 5,
            "rakuten"   => 6,
        ),
    ),
    'delivery_time' => array(
        '' => '指定無し',
        'AM' => '午前中',
        '12-14' => '12時～14時',
        '14-16' => '14時～16時',
        '16-18' => '16時～18時',
        '18-20' => '18時～20時',
        '19-21' => '19時～21時',
    ),

    'event_status' => array(
        0 => "指定なし",
        1 => "１日目",
        2 => "２日目",
        9 => "その他",
    ),
);
