
<?php echo Form::open("/order/confirm");?>
<div>
	<h4>お支払方法</h4>
	<hr>
	<br>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_payment')); ?></font><br>
	■<a href="#card"><font color='#FFFFFF'>クレジットカード</font></a><br>
	■<a href="#cvs"><font color='#FFFFFF'>コンビニ決済</font></a><br>
	■<a href="#ato"><font color='#FFFFFF'>後払い</font></a><br>
	<br>
	<hr>

	<?php if(Session::get("payment_ctl_card")):?>
		<a id="card" name="card"></a><input type="radio" name="payment" value="1" <?php echo (Session::get("order_data.data3.payment") == 1) ? "checked" : "";?>>クレジットカード
		<br>
		<p>
			ご利用可能なカードの種類：VISA/MasterCard/DINERS/JCB/AMEX<br>
			<br>
			お支払総額：商品代金＋送料<br>
			<br>
			＊カード会社からの請求書には､｢official-goods-store.jp｣と表記されます。<br>
		</p>

		カード番号<br>
		<?php echo Form::input('card_no', Session::get("order_data.data3.card_no") ? Session::get("order_data.data3.card_no") : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		「-(ハイフン)」は含まず入力してください<br>
		<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_cardno')); ?></font>
		<br>
		セキュリティコード<br>
		<?php echo Form::password('secure_cd', Session::get("order_data.data3.secure_cd") ? Session::get("order_data.data3.secure_cd") : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_secureno')); ?></font>
		<p>
			セキュリティコードについて<br>
			VISA/MASTER/DINERS/JCBの場合<br>
			クレジットカード背面に記載されている3桁の番号(メインのカード番号のすぐ右側)<br>
			<br>
			AMEXの場合<br>
			クレジットカード前面に記載されている4桁の番号(メインのカード番号の右上)<br>
		</p>
		有効期限<br>
		<select name="expire_m">
			<?php for ($i = 1; $i <= 12; $i++) {?>
				<option value="<?php echo sprintf("%02d", $i);?>"
					<?php echo (Session::get("order_data.data3.expire_m")==sprintf("%02d", $i)) ? " selected" : "";?>><?php echo sprintf("%02d", $i);?>月</option>
			<?php }?>
		</select>
		<select name="expire_y">
			<?php for ($i = 0; $i <= 25; $i++) {?>
				<option value="<?php echo date("y",strtotime($i." year" ));?>"
					<?php echo (Session::get("order_data.data3.expire_y")==date("y",strtotime($i." year" ))) ? " selected" : "";?>><?php echo date("y",strtotime($i." year" ));?>年</option>
			<?php }?>
		</select><br>
		例)カード表記が「04/2018」の場合は、「04月18年」と選択してください。<br>
		<br>

		<input type="hidden" name="credit_jpo" value="10">
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#FFFFFF'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>

	<?php if(Session::get("payment_ctl_cvs")):?>
		<a id="cvs" name="cvs"></a><input type="radio" name="payment" value="2" <?php echo (Session::get("order_data.data3.payment") == 2) ? "checked" : "";?>>コンビニ決済(前払い)
		<br>
		<p>
			お支払総額：商品代金＋送料＋決済手数料<br>
			<br>
			※ご注文完了画面・注文確認メールにて、お支払い方法(受付番号等)をお伝えいたします。<br>
			<br>

			【決済手数料】(tax in)<br>
			¥3,000未満：¥146<br>
			¥10,000未満：¥178<br>
			¥30,000未満：¥254<br>
			¥100,000未満：¥545<br>
			¥300,000未満：¥567<br>
			※30万円以上の決済不可<br>
			※1回のご注文につき

			<br>
			お支払い先コンビニ<br>
			<?php echo Form::select('cvs', '',Config::get("cvs"));?><br>
			コンビニ支払期限：<?php echo date("Y/m/d",$cvs_close_time);?> 23:59
		</p><br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#FFFFFF'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>

	<?php if(Session::get("payment_ctl_ato")):?>
		<a id="ato" name="ato"></a><input type="radio" name="payment" value="4" <?php echo (Session::get("order_data.data3.payment") == 4) ? "checked" : "";?>>後払い(コンビニ / 郵便振替 / 銀行振込)
		<br>
		<p>
			お支払い総額：商品代金＋送料＋決済手数料 ¥205(tax in)<br>
			<br>
			＊当サービスは(株)キャッチボールの運営する「後払い.com(ドットコム)」により提供されます。 下記注意事項を確認、同意の上、ご利用下さい。<br>
			<font color="red">(2016年5月30日ご注文分より、(株)ネットプロテクションズ(NP後払い)から決済会社が変更されております)</font><br />
			＊銀行 / 郵便局 / コンビニでお支払いいただけます。<br>
			＊当店にかわり、後払い.com運営会社の(株)キャッチボールより請求書が送られます。<br>
			＊商品到着と請求書の到着は別になります。<br>
			＊請求書発行から14日後までにお支払い下さい。<br>
			＊支払期限を過ぎた場合、再度の請求毎に¥300の再請求書発行手数料がかかりますのでご注意下さい。<br>
			＊ギフト注文やプレゼント注文(商品配送先が注文者住所と異なる場合)でもご利用いただけます。<br>
			＊ご本人様確認や後払い.comサービス利用にあたって、お電話 / メールにてご連絡させていただく場合がございます。<br>
			＊お客様が当サイトにおいて登録された個人情報および発注内容は、後払い.comのサービスに必要な範囲のみで(株)キャッチボールに提供させていただきます。<br>
			＊与信結果によっては当サービスをご利用いただけない場合がございます。その場合は、他の決済方法にご変更いただくことになります。<br>
			＊商品の配送先を配送業者の営業所止め(営業所来店引取り)また転送依頼することはできません。<br>
			＊サービスをご利用いただいた場合は、上記注意事項にご同意いただいたものとみなさせていただきます。<br>
		</p><br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#FFFFFF'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_npato")):?>
		<a id="ato" name="ato"></a><input type="radio" name="payment" value="5" <?php echo (Session::get("order_data.data3.payment") == 5) ? "checked" : "";?>>後払い(コンビニ / 郵便振替 / 銀行振込)
		<br>
		<p>
			お支払い総額：商品代金＋送料＋決済手数料 ¥205(tax in)<br>
			<br>
			○このお支払方法の詳細<br>
			商品の到着を確認してから、「コンビニ」「郵便局」「銀行」で後払いできる安心 / 簡単な決済方法です。請求書は、商品到着後別途郵送されますので、発行から14日以内にお支払いをお願いします。<br>
			<br>
			○ご注意<br>
			後払い手数料：¥205（tax in）<br>
			後払いのご注文には、株式会社ネットプロテクションズの提供するNP後払いサービスが適用され、サービスの範囲内で個人情報を提供し、代金債権を譲渡します。ご利用限度額は累計残高で¥54,000（tax in）迄です。<br>
			<br>
			◯与信審査<br />
			ご注文確定後、後払い決済与信審査を実施いたしますが、登録ご住所 / 電話番号等の不備、限度額超過、請求期限切れ未払い取引があるなどの理由により後払い決済与信審査がNGとなる場合がございます。その場合別途メールにてご連絡いたしますが、ご注文はキャンセル扱いとなりますので、予めご了承ください。(ご注文された商品は販売用在庫に戻ります)<br />
		</p><br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#FFFFFF'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_cod")):?>
		<a id="cod" name="cod"></a><input type="radio" name="payment" value="3" <?php echo (Session::get("order_data.data3.payment") == 3) ? "checked" : "";?>>代金引換
		<br>
		<p>
			配送業者：佐川急便<br>
			お支払総額：商品代金＋送料＋代引き手数料<br>
			<br>
			＊商品配送時に配送員に「現金」でお支払い下さい。<br>
			<br>
			【代引き手数料】(tax in)<br>
			¥9,999以下：¥324<br>
			¥29,999以下：¥432<br>
			¥99,999以下：¥648<br>
			¥299,999以下：¥1,080<br>
			¥499,999以下：¥2,160<br>
		</p><br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#FFFFFF'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>

	<div>
		<?php echo Html::anchor('/order/input2'.$session_get_param, '<font color="#FFFFFF">前に戻る</font>'); ?>
	</div>

</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
