<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<!-- 購入商品情報 -->
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<div class="cart-list">
	<table>

		<?php
		$total = 0;
		$total_tax = 0;
		?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="total">小計<br />&nbsp;</th>
			<th class="delete"></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (round($data["price"] * $tax) * $data["num"]);
			?>
			<tr class="list">
				<td>
					<?php
					$anchor_txt = "";
					$anchor_txt .= $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
					?>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format($data["price"] * $tax);?></td>
				<td>
					<?php echo number_format($data["num"]);?>&nbsp;&nbsp;
				</td>
				<td><?php echo number_format(round($data["price"] * $tax )* $data["num"]);?>円</td>
				<td></td>
			</tr>
		<?php endforeach; ?>
		<tr class="fixed">
			<td></td>
			<td></td>
			<td>商品合計</td>
			<td>¥<?php echo number_format(intval($total_tax));?></td>
			<td></td>
		</tr>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="description">
		<p>
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
					送料無料です<br>
				<?php else :?>
					あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
				<?php endif; ?>
			<?php endif; ?>
		</p>
		<p style="color: #FF0000">
			<?php if(Session::get("payment_ctl_cnt") < 3 && Session::get("payment_ctl_cnt") > 0):?>
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			<?php endif; ?>
		</p>
	</div>
</div>


<?php if(!isset($user_data)) : ?>
	<!-- ログイン -->
	<?php echo Form::open(array('action' => '/login' ,'id' => "login" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
	<div class="login-panel">
		<div class="panel-body">
			<p class="description">会員の方は、こちらからログインすると<br>お客様情報が自動入力されます。</p>
			<div class="form-group">
				<label class="col-sm-4 col-xs-5 control-label">メールアドレス</label>
				<div class="form-container">
					<div class="inner">
						<?php echo Form::input('email', Input::post('email', ''),
							array('class' => 'validate[required] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM')); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-xs-5 control-label" for="form_password">パスワード</label>
				<div class="form-container">
					<div class="inner">
						<?php echo Form::password('password', Input::post('password', ''),
							array('class' => 'validate[required,minSize[3],maxSize[12]] form-control col-sm-4','placeholder' =>'3〜12文字の英数字')); ?>
					</div>
				</div>
			</div>
			<div class="buttons">
				<input class="button rect key size-L" name="exec" value="ログイン" type="submit" />
				<a class="button text" href="/forget">パスワードを忘れた方はこちら</a>
			</div>
		</div>
		<div class="description notice">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</div>

	</div>
	<?php echo Form::close();?>
<?php endif;?>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<h4 class="headline">お客様情報</h4>
	<?php if(isset($user_data)):?>
		入力されているお客様情報確認後、変更がある場合は入力してください。<br><br>
	<?php else : ?>
		初めてお買い物をされるお客様、会員登録をされていないお客様は、こちらからお客様情報を入力してください。<br><br>
	<?php endif ;?>
	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須</span>', 'order_username',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_username_sei', Session::get("order_data.data1.order_username_sei") ? Session::get("order_data.data1.order_username_sei") : (isset($user_data) ? $user_data->username_sei : ""),
					array('class' => 'validate[required] form-control col-sm-4 col-xs-4',"placeholder" => "姓")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_username_mei', Session::get("order_data.data1.order_username_sei") ? Session::get("order_data.data1.order_username_mei") : (isset($user_data) ? $user_data->username_mei : ""),
					array('class' => 'validate[required] form-control col-sm-4 col-xs-4',"placeholder" => "名")); ?>
			</div>
		</div>
	</div>
	<div class="form-group count-2">
		<?php echo Form::label('ふりがな<span class="caption">※必須</span>', 'order_username_kana',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_username_sei_kana', Session::get("order_data.data1.order_username_sei_kana") ? Session::get("order_data.data1.order_username_sei_kana") : (isset($user_data) ? $user_data->username_sei_kana : ""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "せい")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_username_mei_kana', Session::get("order_data.data1.order_username_mei_kana") ? Session::get("order_data.data1.order_username_mei_kana") : (isset($user_data) ? $user_data->username_mei_kana : ""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "めい")); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('メールアドレス<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_email',Session::get("order_data.data1.order_email") ? Session::get("order_data.data1.order_email") : (isset($user_data) ? $user_data->email : ""),
					array('class' => 'validate[required,custom[email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM' ,'id' => 'order_email','type'=>'email')); ?>
			</div>
			<div class="description notice">携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、注文確認等のメールが届かない場合があります。（ドメイン指定受信設定 / URL付きメール受信不可 / PCからのメール受信不可）</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('メールアドレス確認<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_email2',Session::get("order_data.data1.order_email2") ? Session::get("order_data.data1.order_email2") : (isset($user_data) ? $user_data->email : ""),
					array('class' => 'validate[required,equals[order_email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM','type'=>'email')); ?>
			</div>
		</div>
	</div>
	<div class="form-group type-postal">
		<?php echo Form::label('郵便番号<span class="caption">※必須</span>', 'order_zip',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="buttons">
				<input type="button" class="button rect" id="order_zipsearch" value="郵便番号から住所自動入力">
			</div>

			<div class="inner">
				<?php echo Form::input('order_zip',Session::get("order_data.data1.order_zip") ? Session::get("order_data.data1.order_zip") : (isset($user_data) ? $user_data->zip : ""),
					array('id' => 'order_zip','class' => 'validate[required,custom[onlyNumberSp],minSize[7],maxSize[7]] form-control col-sm-4',"placeholder" => "1234567")); ?>
			</div>
			<div class="description">※ハイフン(-)は抜いてください </div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 col-xs-4 control-label">都道府県<span class="caption">※必須</span></label>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php echo Form::select('order_state', Session::get("order_data.data1.order_state") ? Session::get("order_data.data1.order_state") :
							(isset($user_data) ? $user_data->prefecture : ""),$prefecture_data,
							array('class' => 'validate[required] form-control col-md-5',));
						?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所１<span class="caption">※必須</span>', 'order_address1',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_address1', Session::get("order_data.data1.order_address1") ? Session::get("order_data.data1.order_address1") :
					(isset($user_data) ? mb_convert_kana($user_data->address1, "ASV") : ""),
					array('id' => 'order_address1' ,
						'class' => 'validate[required,custom[onlyLetterSpFull],maxSize[16]] form-control','placeholder' => '市区町村※全角のみ 24文字まで')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('住所２', 'order_address2',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_address2',  Session::get("order_data.data1.order_address2") ? Session::get("order_data.data1.order_address2") :
					(isset($user_data) ? mb_convert_kana($user_data->address2, "ASV") : ""),
					array('class' => 'validate[required,custom[onlyLetterSpFull],maxSize[16]] form-control',"placeholder" => "番地、建物名など※全角のみ 24文字まで")); ?>
			</div>
		</div>
	</div>
	<div class="form-group count-3">
		<?php echo Form::label('電話番号<span class="caption">※必須</span>', 'order_tel',array("class" => "col-sm-3 col-xs-12 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_tel1', Session::get("order_data.data1.order_tel1") ? Session::get("order_data.data1.order_tel1") :
					(isset($user_data) ? $user_data->tel1 : ""),
					array('class' => 'validate[required,custom[onlyNumberSp]] form-control',"placeholder" => "半角数字")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_tel2', Session::get("order_data.data1.order_tel2") ? Session::get("order_data.data1.order_tel2") :
					(isset($user_data) ? $user_data->tel2 : ""),
					array('class' => 'validate[required,custom[onlyNumberSp]] form-control',"placeholder" => "半角数字")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_tel3', Session::get("order_data.data1.order_tel3") ? Session::get("order_data.data1.order_tel3") :
					(isset($user_data) ? $user_data->tel3 : ""),
					array('class' => 'validate[required,custom[onlyNumberSp]] form-control',"placeholder" => "半角数字")); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('性別', 'order_sex',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<td><?php echo Form::radio('order_sex', '1',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>男性</td>
					<td><?php echo Form::radio('order_sex', '2',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>女性</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('生年月日', 'order_birthday',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_birthday', Session::get("order_data.data1.order_birthday") ? Session::get("order_data.data1.order_birthday") :
					(isset($user_data) ? $user_data->birthday : ""),
					array('class' => 'validate[custom[onlyNumberSp],minSize[8],maxSize[8]] form-control col-sm-4',"placeholder" => "19790929")); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('お知らせメール', 'mailmagazine_flg',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<td><?php echo Form::radio('mailmagazine_flg', '1',Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>受信する</td>
					<td><?php echo Form::radio('mailmagazine_flg', '0',Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>受信しない</td>
				</tr>
			</table>
		</div>
	</div>

	<?php if (!isset($user_data)) :?>

		<div class="form-group">
			<?php echo Form::label('会員登録', 'regist',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<table class="radio">
					<tr>
						<td><?php echo Form::radio('regist_flg', "1",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : "1",
								array('id'=> 'regist_on','class' => 'validate[required]')); ?></td>
						<td>登録する</td>
						<td><?php echo Form::radio('regist_flg', "0",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : "",
								array('id'=> 'regist_off','class' => 'validate[required]')); ?></td>
						<td>登録しない</td>
					</tr>
				</table>
				<div class="description" style="margin-top:-5px;">
					※会員登録されますと、次回ご利用になる際に、住所などの情報が自動的に入力されます。
				</div>
				<div class="description notice">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</div>
			</div>
		</div>
		<div class="form-group" id="regist_pass">
			<?php echo Form::label('パスワード', 'regist',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<div class="inner">
					<?php echo Form::password('order_password', Session::get("order_data.data1.order_password") ? Session::get("order_data.data1.order_password") : "",
						array('class' => 'validate[required,custom[onlyLetterNumber],minSize[3],maxSize[12]] form-control',"placeholder" => "3～12文字の英数字")); ?>
				</div>
			</div>
		</div>
	<?php endif;?>

	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		// 郵便番号検索　注文者
		$("#order_zipsearch").click(function(){
			data = get_address($("#order_zip").val());
			data.done(function(res) {
//				console.log(res);
				$("select[name='order_state']").val(res.state);
				$("#order_address1").val(res.city + res.address);
			}).fail(function(XMLHttpRequest, textStatus, errorThrown){
//                alert("正確に郵便番号を入力ください。住所検索が出来ない場合は、お問い合わせください。");
//                console.log("error!!!!");
			});
		});
		// 次へ
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input2");
			$("#order").submit();
		});
	});

	function get_address(zip){
		var param = { "zip": zip};
		return $.ajax({
			type: "get",
			url: "/<?php echo $shop_data["dir"];?>/api/postage/address.json?zip="+zip,
			async: false,
			dataType : "json",
			scriptCharset: 'utf-8'
		})
	}
	$(function(){
		$("#login").validationEngine({promptPosition : "topLeft"});
		$("#order").validationEngine({promptPosition : "topLeft"});
		regist_status = $("input[name='regist_flg']:checked").val();
		$("#regist_pass").hide();
		if(regist_status == 1){
//			console.log(regist_status);
			$("#regist_pass").show();
		}
		$('#regist_on').click(function() {
			$("#regist_pass").show();
		});
		$('#regist_off').click(function() {
			$("#regist_pass").hide();
		});
	});
</script>

