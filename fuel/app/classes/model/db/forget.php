<?php
class Model_Db_Forget extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'key',
		'value',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_forget';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	/**
	 * Key検索
	 *
	 * @param $key
	 * @return mixed
	 */
	public static function delExpired() {

		$del_query = DB::query("DELETE FROM tbl_forget WHERE insert_date < CURRENT_TIMESTAMP - INTERVAL 1 HOUR");
		$del_query->execute();
		return;
	}

	/**
	 * Key検索
	 *
	 * @param $key
	 * @return mixed
	 */
	public static function findForgetKey($key) {

		$query = self::query()
		->select('id')
		->where('insert_date',">",  date("Y-m-d H:i:s",strtotime("-1 hour")))
		->where('key',  $key);
		$result = array();
		if($query->count()){
			$result = $query->get_one();
		}
		return $result;
	}
}
