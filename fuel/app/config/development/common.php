<?php
return array(
	"rakuten" => array(
		"public_key"		=> "test_public_7a2c252205d9719ea47a3f48df9a68aab7e949506004b1989334b5107430860141ba0729521371410128626cf31d438c",
		"private_key"		=> "test_private_bb1c00ebd2a16d2c20145e6010f38bf7b7e949506004b1989334b5107430860141ba0729521371410128626cf31d438c",
		"js_url"			=> 'https://lite.checkout.rakuten.co.jp/s/js/checkout-lite-v1.js',
		"api_url"			=> "https://api.lite.checkout.rakuten.co.jp",
		"api_action"		=> array(
			"capture"			=> "/v1/charges/%%CHARGE_ID%%/capture",
			"cancel"			=> "/v1/charges/%%CHARGE_ID%%/refund",
		),
	),
	's3base_url' => "https://reni-ec-dev.s3-ap-northeast-1.amazonaws.com/",
	'assets_url' => "//reni-ec-dev.s3-ap-northeast-1.amazonaws.com/assets/",
	'banner_url' => "//reni-ec-dev.s3-ap-northeast-1.amazonaws.com/banner/",
	"basic" => array(
		'front_mode' => 1,
		'tool_mode' => 1,
		'user' => "admin",
		'pass' => "pass",
	),
	"atobarai_maintenance" => array(
//		"start"		=> 20151101000000,
//		"finish"	=> 20151201110000,
		"start"		=> 20160926000000,
		"finish"	=> 20160927120000,
	),
	// 写真集名入れ商品ID
	"print_product_ids" => array(
	),
	// 写真集名入れ商品詳細ID
	"print_product_detail_ids" => array(
		1634
	),
	"naire_product_ids" => array(
		1191,1232
	),
	"naire_max_length" => array(
		1191 => 17,
		1232 => 15,
	),
	// 購入制限5
	"order_limit" => array(
		1486,
		1594,1595,1596,1597,
		1626
	),
	// 購入制限1
	"order_limit1" => array(
	),
	// 購入制限2
	"order_limit2" => array(
		1540,1541
	),
	"apbankfes2016_category1" => 110,
	"apbankfes2016_category1old" => 111,

);
