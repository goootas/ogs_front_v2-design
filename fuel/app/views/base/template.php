<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta charset="utf-8">
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
<?php
	echo Asset::css(
			array(
				'reset.css',
				'common.css',
				$shop_data["dir"].'/'.$shop_data["dir"].'.css',
				'validationEngine.jquery.min.css',
			)
	);
?>
<?php
if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	echo Asset::css(
			array(
				'common-ie.css'
			)
	);
};?>
<?php
echo Asset::js(
		array(
			'jquery.1.8.2.min.js',
			'common.js',
			'jquery.validationEngine-ja.min.js',
			'jquery.validationEngine.min.js',
		)
);
if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	echo Asset::js(
			array(
				'jquery.innerfade.js',
				'masonry.pkgd.min.js',
				'jquery.autopager-1.0.0.js'
			)
	);
}
?>


<?php //TODO:OGタグの内容確認お願いします。 ?>
<?php if(Request::active()->controller == 'Controller_Product' and in_array(Request::active()->action, array('detail'))) :?>
		<meta property="fb:app_id" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:title" content="<?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'] . "?" . $_SERVER['QUERY_STRING']; ?>"/>
		<meta property="og:image" content="<?php echo "https://" . $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/" . Config::get("aws.image_path") . $product->id . "/" . $product->id . "_000.jpg"; ?>"/>
		<meta property="og:site_name" content="<?php echo $shop_data["name"] ?>"/>
<?php endif; ?>

</head>
<body>
<?php //TODO:OGタグの内容確認お願いします。 ?>
<!--<div id="fb-root"></div>-->
<!--<script>(function (d, s, id) {-->
<!--		var js, fjs = d.getElementsByTagName(s)[0];-->
<!--		if (d.getElementById(id)) return;-->
<!--		js = d.createElement(s);-->
<!--		js.id = id;-->
<!--		js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=166405993529647&version=v2.0";-->
<!--		fjs.parentNode.insertBefore(js, fjs);-->
<!--	}(document, 'script', 'facebook-jssdk'));</script>-->

<div id="TOP" class="WRAPPER">
	<div class="HEADER">
		<div class="TOURTITLE">
			<div class="CONTAINER">
				<div class="TITLE">
					<h2><?php echo Html::anchor('http://www.tokyoska.net', Asset::img($shop_data["dir"].'/title.png', array('id' => 'logo')), array("target" => "_blank")); ?></h2>
				</div>
			</div>
		</div>
		<div class="NAVIGATION">
			<div class="GLOBAL">
				<div class="CONTAINER">
					
					<div class="TITLE"><?php echo Html::anchor('/', 'STORE', array("class" => "text"), true); ?></div>
					<div class="buttons">
						<?php if(Session::get('user.id')) : ?>
						<?php echo Html::anchor('mypage', '<span>マイページ</span>', array("class" => "button text with-icon mypage"), true); ?>
						<?php echo Html::anchor('logout', '<span>ログアウト</span>', array("class" => "button text with-icon logout"), true); ?>
						<?php else: ?>
						<?php echo Html::anchor('login', '<span>ログイン</span>', array("class" => "button text with-icon login"), true); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="CATE">
				<div class="CONTAINER">
					<div class="buttons"><?php echo Html::anchor('/', '<span>ALL</span>', array("class" => "button text all"), true); ?></div>
					<div class="buttons"> <a class="button text categories" href="#"><span>カテゴリー一覧</span></a> </div>
					<div class="navigation">
						<ul >
							<?php if($category1): ?>
								<?php foreach($category1 as $c1): ?>
									<li><?php echo Html::anchor('/product/list?c1='.$c1->id, "<span>".$c1->title."</span>") ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
					<?php echo Html::anchor('cart', '<span>マイカート</span>',array("class" => "button key rect with-icon cart")); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="CONTENTS">
		<div class="CONTAINER">
		<?php echo $content; ?>
<!--		--><?php //if(Request::active()->controller !== 'Controller_Top' and
//			!(Request::active()->controller == 'Controller_Product' and Request::active()->action == 'list')) :?>
<!--			<div class="col-md-12 col-xs-12">-->
<!--				--><?php //echo Html::anchor('/', 'ショップへ戻る') ?>
<!--			</div>-->
<!--		--><?php //endif; ?>
		</div>
	</div>
	<div class="FOOTER">

	<div class="INFORMATION">
            <div class="inner">
                <div class="SCHEDULE">
                    <div class="head">
                    <h5>販売スケジュール</h5>
                    </div>
                    <div class="body">
						<p>2015/7/10(金) 0:00～2015/7/26(日) 23:59 <br />→8/6(金)までのお届け</p>
						<p>2015/7/27(月) 0:00～2015/8/7(金) 23:59 <br />→8/10(金)頃より順次発送予定</p>
						<p>2015/8/8(土) 0：00～ <br />→ご注文日から3〜5営業日以内で発送</p>
					</div>
                </div>
                <div class="NOTICE">
                    <p>
						※商品代金に加えて、全国一律（北海道・沖縄含む）配送料540円（tax in）を頂きます。なお、<?php echo number_format($shop_data["free_deliver_price"]);?>円以上のお買い上げで送料が無料となります。<br />
						※決済方法により、別途、決済手数料が発生する場合がございます。ご購入の際にご確認ください。<br />
						※ご注文後の変更、キャンセルは一切お受けできません。<br />
						※別々にいただいたご注文は、一つにまとめて出荷（同梱出荷）することはできません。<br />
						※商品には限りがあります。お申し込み多数の際は、受付期間内でも品切れとなる場合がございます。<br />
						※注文確認メールが届かない場合は、マイページ注文履歴をご確認ください。<br />
						※画像と実際の商品は色合い等、若干異なる場合がございますので、ご了承ください。<br />
						※各商品表示サイズをお確かめの上、お申し込みください。<br />
						※衣料商品は、特製上表示サイズと若干の誤差が生じる場合がございますので、あらかじめご了承ください。<br />
						※悪天候や交通事情等でお届けが遅れる場合がございます。<br />
						※通販会社（株）RENIにて販売代行しております。<br />
					</p>
                </div>
            </div>
		</div>
		<div class="LINKS">
			<div class="CONTAINER">
				<div class="navigation">
					<ul>
						<li><?php echo Html::anchor('http://www.tokyoska.net', '<span>TOP</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/news/', '<span>NEWS</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/schedule/', '<span>MEDIA</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/live/', '<span>LIVE</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/schedule/calendar.php', '<span>CALENDAR</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/profile/', '<span>PROFILE</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/discography/', '<span>MUSIC</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoska.net/movie/', '<span>VIDEO</span>', array("target" => "_blank")); ?></li>
						<li><?php echo Html::anchor('/', '<span>STORE</span>'); ?></li>
						<li><?php echo Html::anchor('http://www.tokyoskaqms.net/', '<span>FAN CLUB</span>', array("target" => "_blank")); ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="CREDIT">
			<div class="CONTAINER">
				<div class="navigation">
					<ul>
						<li><?php echo Html::anchor('/info/guide', '<span>ショッピングガイド</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/privacy', '<span>プライバシーポリシー</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/legal', '<span>特定商取引法に基づく表記</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/inquiry', '<span>お問い合わせ</span>', "", true) ?></li>
					</ul>
				</div>
				<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI";?> All Rights Reserved</p>
			</div>
		</div>
	</div>
</div>

<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', '<?php echo $shop_data["ga_tag"]?>', 'auto');
	ga('require', 'linkid', 'linkid.js');
	ga('send', 'pageview');
</script>


</body>
</html>

