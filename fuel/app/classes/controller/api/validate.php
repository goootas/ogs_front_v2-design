<?php
class Controller_Api_Validate extends Controller_Baseapi
{
	/*
	 * 郵便番号チェック
	 * 郵便番号マスタを参照し、存在チェックを行う。
	 * ※存在しない場合はエラーとする
	 */
	public function get_zip()
	{
		$status = \Model\lib\Validate::checkZip(Input::get("zip"));
		return $this->response(array(Input::get("fieldId"),$status));
	}

	/*
	 * 郵便番号チェック
	 * 郵便番号マスタを参照し、入力された住所と比較チェックを行う。
	 */
	public function get_zipaddress()
	{
		$status = \Model\lib\Validate::checkZipAddress(Input::get("zip"),Input::get("state"),Input::get("address1"),Input::get("address2"));
		return $this->response(array(Input::get("fieldId"),$status));
	}

	/*
	 * 電話番号チェック
	 * 携帯番号の時、設置電話の時で桁数チェックを行う
	 * 購入者電話番号は、先頭の3桁が「020」、「050」、「060」、「070」、「080」、「090」の場合、「-」を含めて13桁で設定して下さい
	 */
	public function get_tel()
	{
		$status = \Model\lib\Validate::checkTel(Input::get("tel1").Input::get("tel2").Input::get("tel3"));
		return $this->response(array(Input::get("fieldId"),$status));
	}
}
