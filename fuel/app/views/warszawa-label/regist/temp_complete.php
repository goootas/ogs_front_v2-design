<div class="panel regist">
    <h4 class="subheadline">仮登録完了</h4>
    <div class="description">
        新規会員登録には本登録処理が必要です。<br>
        入力していただいたメールアドレス宛に、本登録用URLをお送りしております。<br>
        メール記載のURLへアクセス後、本登録を行ってください。<br>
    </div>
</div>
