<?php
class Controller_Baseapi extends Controller_Rest
{
	public $template = 'tool/template';

	public function before()
	{
		parent::before();
		Config::load("common");
	}
    /**
     * $response をパラメータとして追加し、after() を Controller_Template 互換にする
     */
    public function after($response)
    {
        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。

		return $response; // after() は確実に Response オブジェクトを返すように
    }
}
