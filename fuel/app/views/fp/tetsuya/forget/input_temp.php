<h4>パスワード再設定</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<p>
	パスワード再設定用URLを送付いたします。<br>
	お客様のメールアドレスを入力して、送信ボタンを押してください。<br>
</p>

<?php echo Form::open();?>
<div>
	<p><b>メールアドレス</b></p>
</div>
<div>
	<?php echo Form::input('email', Input::post('email', ''), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<br>
<font color="red">
	携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、注文確認等のメールが届かない場合があります。（ドメイン指定受信設定/URL付きメール受信不可/PCからのメール受信不可）<br><br>
	ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。<br>
</font>
<br>
<div>
	<?php echo Form::submit('exec', '送信'); ?>
</div>

<?php echo Form::close();?>
