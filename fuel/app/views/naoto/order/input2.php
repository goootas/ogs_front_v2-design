<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<!-- 購入商品情報 -->
<div class="cart-list">
	<table>
		<?php
		$total = 0;
		$total_tax = 0;
		?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="total">小計<br />&nbsp;</th>
			<th class="delete"></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			?>
			<tr class="list">
				<td>
					<?php
					$anchor_txt = "";
					$anchor_txt .= $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
					?>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td>
					<?php echo number_format($data["num"]);?>&nbsp;&nbsp;
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
				<td></td>
			</tr>
		<?php endforeach; ?>
		<tr class="fixed">
			<td></td>
			<td></td>
			<td>商品合計</td>
			<td>¥<?php echo number_format(intval($total_tax));?></td>
			<td></td>
		</tr>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="description">
		<p>
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
					送料無料です<br>
				<?php else :?>
					あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
				<?php endif; ?>
			<?php endif; ?>
		</p>
		<p style="color: #FF0000">
			<?php if(Session::get("payment_ctl_cnt") < 3 && Session::get("payment_ctl_cnt") > 0):?>
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			<?php endif; ?>
		</p>
	</div>
</div>


<div class="login-panel">
	<div class="panel-body">
		<p class="heading">配送方法 / 送料について</p>
		<p>■配送<br>
			佐川急便での配送になります。（日本国内のみ）</p>
		<br>
		<p>■送料<br>
			全国一律¥550（+tax / 1回のご注文につき）<br />
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				※１回のご注文合計金額が¥<?php echo number_format($shop_data["free_deliver_price"]);?>(tax in)以上のお客様は送料無料とさせていただきます
			<?php endif; ?>
		</p>
	</div>
</div>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<h4 class="headline">お届け先情報</h4>
	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須</span>', 'deliver_username',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_username_sei', Session::get("order_data.data2.deliver_username_sei") ? Session::get("order_data.data2.deliver_username_sei") : Session::get("order_data.data1.order_username_sei"),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "姓")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_username_mei', Session::get("order_data.data2.deliver_username_mei") ? Session::get("order_data.data2.deliver_username_mei") : Session::get("order_data.data1.order_username_mei"),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "名")); ?>
			</div>
		</div>
	</div>

	<div class="form-group type-postal">
		<?php echo Form::label('郵便番号<span class="caption">※必須</span>', 'deliver_zip',array("class" => "col-sm-3 col-xs-4 control-label")); ?>

		<div class="form-container">
			<div class="buttons">
				<input type="button" class="button button rect" id="deliver_zipsearch" value="郵便番号から住所自動入力">
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_zip', Session::get("order_data.data2.deliver_zip") ? Session::get("order_data.data2.deliver_zip") : Session::get("order_data.data1.order_zip"),
					array('id' => 'deliver_zip','class' => 'validate[required,custom[onlyNumberSp]] form-control col-sm-4',"placeholder" => "1234567",'type'=>'tel')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 col-xs-4 control-label">都道府県<span class="caption">※必須</span></label>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php echo Form::select('deliver_state', Session::get("order_data.data2.deliver_state") ? Session::get("order_data.data2.deliver_state") : Session::get("order_data.data1.order_state"),$prefecture_data,
							array('class' => 'validate[required] form-control col-md-5',)); ?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所１<span class="caption">※必須</span>', 'deliver_address1',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_address1', Session::get("order_data.data2.deliver_address1") ? Session::get("order_data.data2.deliver_address1") : Session::get("order_data.data1.order_address1"),
					array('id' => 'deliver_address1' ,
						'class' => 'validate[required,maxSize[24]] form-control','placeholder' => '市区町村')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所２', 'deliver_address2',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_address2', Session::get("order_data.data2.deliver_address2") ? Session::get("order_data.data2.deliver_address2") : Session::get("order_data.data1.order_address2"),
					array('class' => 'validate[maxSize[24]] form-control',"placeholder" => "番地、建物名など")); ?>
			</div>
		</div>
	</div>

	<div class="form-group count-3">
		<?php echo Form::label('電話番号<span class="caption">※必須</span>', 'deliver_tel',array("class" => "col-sm-3 col-xs-12 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_tel1', Session::get("order_data.data2.deliver_tel1") ? Session::get("order_data.data2.deliver_tel1") : Session::get("order_data.data1.order_tel1"),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_tel2', Session::get("order_data.data2.deliver_tel2") ? Session::get("order_data.data2.deliver_tel2") : Session::get("order_data.data1.order_tel2"),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_tel3', Session::get("order_data.data2.deliver_tel3") ? Session::get("order_data.data2.deliver_tel3") : Session::get("order_data.data1.order_tel3"),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>

		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('お届け日', 'delivery_date',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<?php
			if(isset($deliver_term_date)){
				echo $deliver_term_date->deliver_start_text;
			}else{
				echo Form::select('delivery_date', Session::get("order_data.data2.delivery_date"),$delivery_date, array('class' => 'form-control col-md-5',));
//				echo "受注日翌日から2日以内で発送（土日祝日除く / 発送からお届けは1〜3日）";
				echo '<div class="description">※注文状況の混み具合により、お届け日が異なる場合がございますことを予めご了承ください。</div>';
			}
			?>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('お届け希望時間帯', 'delivery_time',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php
						if(isset($deliver_term_date)){
							echo "指定不可";
						}else {
							?>
							<?php echo Form::select('delivery_time', Session::get("order_data.data2.delivery_time"), Config::get("delivery_time"),
								array('class' => 'form-control col-md-5',));
						}?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="buttons count-2">
		<?php echo Html::anchor('/order/input1', '前に戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else :?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>

</div>
<?php echo Form::hidden('delivery_comment',""); ?>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("#order").validationEngine({promptPosition : "topLeft"});
		// 郵便番号検索　お届け先
		$("#deliver_zipsearch").click(function(){
			data = get_address($("#deliver_zip").val());
			data.done(function(res) {
				$("select[name='deliver_state']").val(res.state);
				$("#deliver_address1").val(res.city + res.address);

			}).fail(function(XMLHttpRequest, textStatus, errorThrown){
//                alert("正確に郵便番号を入力ください。住所検索が出来ない場合は、お問い合わせください。");
//                console.log("error!!!!");
			});
		});
		// 次へ
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input3");
			$("#order").submit();
		});
	});

	function get_address(zip){
		var param = { "zip": zip};
		return $.ajax({
			type: "get",
			url: "/<?php echo $shop_data["dir"];?>/api/postage/address.json?zip="+zip,
			async: false,
			dataType : "json",
			scriptCharset: 'utf-8'
		})
	}
</script>


