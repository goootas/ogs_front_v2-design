<?php
/*
 * FP用インフォメーション
 */
class Controller_fpinfo extends Controller_Basefront
{
	/*
	 * info/
	 */
	public function action_index()
	{
		throw new HttpNotFoundException();
	}

	/*
	 * プライバシーポリシー
	 */
	public function action_privacy()
	{
		$data["text"] = $this->shop_desc_data->fp_privacy;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["privacy"];
		}

		$this->template->title = "プライバシーポリシー";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * 特定商取引法に基づく表示
	 */
	public function action_legal()
	{
		$data["text"] = $this->shop_desc_data->fp_legal;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["legal"];
		}

		$this->template->title = "特定商取引法に基づく表示";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * ガイド
	 */
	public function action_guide()
	{
		$data["text"] = $this->shop_desc_data->fp_guide;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["guide"];
		}

		$this->template->title = "ガイド";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}

	/*
	 * FAQ
	 */
	public function action_faq()
	{
		$data["text"] = $this->shop_desc_data->fp_faq;
		if(!$data["text"]){
			$data["text"] = $this->shop_data["faq"];
		}
		$this->template->title = "FAQ";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/info/index', $data, false);
	}
}

