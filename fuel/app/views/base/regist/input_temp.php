<div class="panel regist">
    <h4 class="subheadline">新規会員仮登録</h4>

	<div class="description">
	    <p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
	    <p>新規会員仮登録を行います。
        <br>会員登録をするメールアドレスを入力し、送信ボタンをクリックしてください。<br>
        入力したメールアドレス宛に本登録用URLを送信いたします。<br>
        メール記載のURLへアクセス後、本登録を行ってください。
        </p>
	</div>

	<div class="panel-body">
			<?php echo Form::open(array('id' => "regist" ,'class' => 'form-inline','autocomplete'=>'off'));?>
            <div class="form-group">
                <label>メールアドレス</label>
               <div class="form-container">
                    <div class="inner">
                     <?php echo Form::input('email', Input::post('email', ''),
				array('class' => 'validate[required,custom[email]] form-control' ,'placeholder' =>'XXXXXXX@XXXXX.COM')); ?>
                    </div>
                </div>
            </div>
            <div class="buttons">
            <?php echo Html::anchor('javascript:void(0);', '送信',array("id" => "kari",'class' => 'button rect key size-L'),true); ?>
            </div>
			<?php echo Form::close();?>
	</div>
</div>
<script>
	$(function(){
		$("#regist").validationEngine({promptPosition : "topLeft"});
		$("#kari").click(function() {
			if(window.confirm('仮登録を行います。よろしいですか？')){
				$("#regist").submit();
			}
		});
	});
</script>
