<?php
/**
 * ログアウト
 */
class Controller_Logout extends Controller_Basefront
{
    public function action_index()
    {
        Session::delete('user');
        Session::delete('order');
        Session::delete('order_data');
        $this->template->title = "ログアウト";
        $this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/logout/index');
    }
}
