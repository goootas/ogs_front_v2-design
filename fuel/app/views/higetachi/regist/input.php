<div class="forms">
	<h4 class="headline">お客様情報を入力</h4>
	<div class="description">
		<p>お客様情報登録を行います。登録情報を入力してください。</p>
	</div>
	<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

	<?php echo Form::open(array('id' => "regist" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>

	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須<span>', 'username',array("class" => "col-sm-3 col-xs-4 control-label")); ?>

		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('username_sei', Input::post("username_sei",""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "姓")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('username_mei', Input::post("username_mei",""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "名")); ?>
			</div>
		</div>

	</div>

	<div class="form-group count-2">
		<?php echo Form::label('ふりがな<span class="caption">※必須</span>', 'username_kana',array("class" => "col-sm-3 col-xs-4 control-label")); ?>

		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('username_sei_kana', Input::post("username_sei_kana",""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "せい", "autocomplete"=>"off")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('username_mei_kana', Input::post("username_mei_kana",""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "めい" , "autocomplete"=>"off")); ?>
			</div>
		</div>
	</div>


	<div class="form-group">
		<?php echo Form::label('メールアドレス<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('email',Input::post("email",""),
					array('class' => 'validate[required,custom[email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM' ,'id' => 'email','type' => 'email')); ?>
			</div>
			<div class="description notice">
				携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、注文確認等のメールが届かない場合があります。（ドメイン指定受信設定 / URL付きメール受信不可 / PCからのメール受信不可）<br>
				ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('メールアドレス確認<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('email2',Input::post("email2",""),
					array('class' => 'validate[required,equals[email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM','type' => 'email')); ?>
			</div>
		</div>
	</div>

	<!--	<div class="form-group">-->
	<!--		--><?php //echo Form::label('メールアドレス', 'email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
	<!--		<div class="form-container">-->
	<!--			<span style="height:40px; line-height:40px;">--><?php //echo Session::get('regist.email'); ?><!--</span>-->
	<!--		</div>-->
	<!--	</div>-->
	<!---->

	<div class="form-group">
		<?php echo Form::label('パスワード<span class="caption">※必須</span>', 'password',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::password('password',"",
					array('class' => 'validate[required,custom[onlyLetterNumber],minSize[3],maxSize[12]] form-control col-sm-4')); ?>
			</div>
		</div>
	</div>

	<div class="form-group  type-postal">
		<?php echo Form::label('郵便番号<span class="caption">※必須</span>', 'zip',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="buttons">
				<input type="button" class="button rect" id="zipsearch" value="郵便番号から住所自動入力">
			</div>
			<div class="inner">
				<?php echo Form::input('zip',Input::post("zip",""),
					array('id' => 'zip','class' => 'validate[required,custom[onlyNumberSp],minSize[7],maxSize[7]] form-control col-sm-4',"placeholder" => "1234567","type" => "tel")); ?>
			</div>
			<div class="description">※ハイフン(-)は抜いてください</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 col-xs-4 control-label">都道府県<span class="caption">※必須</span></label>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php echo Form::select('state', Input::post("state",""),$prefecture_data,
							array('class' => 'validate[required] form-control col-md-5',));
						?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所１<span class="caption">※必須</span>', 'address1',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('address1', Input::post("address1",""),
					array('id' => 'address1' , 'class' => 'validate[required,maxSize[24]] form-control','placeholder' => '市区町村')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所２', 'address2',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('address2',Input::post("address2",""),
					array('class' => 'validate[maxSize[24]] form-control',"placeholder" => "番地、建物名など")); ?>
			</div>
			<div class="">※「字」「大字」は省略してください。</div>
		</div>
	</div>


	<div class="form-group count-3">
		<?php echo Form::label('電話番号<span class="caption">※必須</span>', 'tel',array("class" => "col-sm-3 col-xs-12 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('tel1', Input::post("tel1",""),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字","type" => "tel")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('tel2', Input::post("tel2",""),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字","type" => "tel")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('tel3', Input::post("tel3",""),
					array('class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字","type" => "tel")); ?>
			</div>
		</div>
	</div>


	<div class="form-group">
		<?php echo Form::label('性別', 'order_sex',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<td><?php echo Form::radio('sex', '1',Input::post("sex","")); ?></td>
					<td>男性</td>
					<td><?php echo Form::radio('sex', '2',Input::post("sex","")); ?></td>
					<td>女性</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('お知らせメール<span class="caption">※必須</span>', 'mailmagazine',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<td><?php echo Form::radio('mailmagazine', '1',"1", array('class' => 'validate[required]')); ?></td>
					<td>受信する</td>
					<td><?php echo Form::radio('mailmagazine', '0',"", array('class' => 'validate[required]')); ?></td>
					<td>受信しない</td>
				</tr>
			</table>
			<div class="description" style="margin-top:-5px;">
				※メールにて新商品のお知らせなどをさせていただく場合があります。
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('生年月日', 'birthday',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('birthday', Input::post("birthday",""),
					array('class' => 'validate[custom[onlyNumberSp],minSize[8],maxSize[8]] form-control col-sm-4',"placeholder" => "19790929","type" => "tel")); ?>
			</div>
		</div>
	</div>

	<div class="buttons">
		<?php echo Form::submit('exec', '登録', array('class' => 'button rect key size-L')); ?>
	</div>
	<?php echo Form::close();?>


</div>

<script>
	$(function(){
		$("#regist").validationEngine({promptPosition : "topLeft"});
		// 郵便番号検索　注文者
		$("#zipsearch").click(function(){
			data = get_address($("#zip").val());
			data.done(function(res) {
				$("select[name='state']").val(res.state);
				$("#address1").val(res.city + res.address);
			}).fail(function(XMLHttpRequest, textStatus, errorThrown){
			});
		});
	});

	function get_address(zip){
		var param = { "zip": zip};
		return $.ajax({
			type: "get",
			url: "/<?php echo $shop_data["dir"];?>/api/postage/address.json?zip="+zip,
			async: false,
			dataType : "json",
			scriptCharset: 'utf-8'
		})
	}
</script>


