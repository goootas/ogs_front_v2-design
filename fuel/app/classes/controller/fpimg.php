<?php
/*
 * FP用画像コンバート
 */
class Controller_fpimg extends Controller_Rest
{
    /*
     *
     */
    public function action_index()
    {
		if(Input::get('t')){
			$resize = self::resizeImage(Input::get('img'));
		}else{
			$resize = self::resizeImage(Input::get('img'),1);
		}
		$img = file_get_contents($resize);
		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		header('Expires: 0');
		header('Content-type: image/jpeg');
		echo $img;
		unlink($resize);
		exit;
    }

	//--------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------
	/*
	 * ファイルをリサイズ
	 */
	public static function resizeImage($file,$size = 0.5)
	{
		// 目的の画像から幅、高さを取得し,半分の値を計算します。
		list($width, $height) = getimagesize($file);
		$new_width = $width * $size;
		$new_height = $height * $size;

		$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromjpeg($file);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		// 出力
		$temp_file = tempnam(sys_get_temp_dir(), 'resize_');
//		imagejpeg($image_p, $temp_file, 100);
		imagejpeg($image_p, $temp_file);

		return $temp_file;

	}
}
