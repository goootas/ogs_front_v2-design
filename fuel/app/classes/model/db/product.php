<?php
class Model_Db_Product extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'group_id',
		'category1_id',
		'category2_id',
		'code',
		'title',
		'title2',
		'unit',
		'price_cost',
		'price_sale',
		'new',
		'soldout',
		'deliver_mail_flg',
		'reserve_date',
		'payment_card_flg',
		'payment_cvs_flg',
		'payment_cod_flg',
		'payment_ato_flg',
		'payment_npato_flg',
		'payment_rakuten_flg',
		'publish_flg',
		'term_start_date',
		'term_end_date',
		'term_stock_flg',
		'comment',
		'body_list',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_product';

	protected static $_has_many = array(
		'detail' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Db_Product_Detail',
			'key_to' => 'product_id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
	);

	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
		'group' => array(
			'key_from' => 'group_id',
			'model_to' => 'Model_Db_Group',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
		'category1' => array(
			'key_from' => 'category1_id',
			'model_to' => 'Model_Db_Category1',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
		'category2' => array(
			'key_from' => 'category2_id',
			'model_to' => 'Model_Db_Category2',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		switch ($factory) {
			case 'create':
				$val->add('code', '商品コード')
				->add_rule('required')
				->add_rule(function ($val) {
					$result = DB::select()->where('code', $val)
					->from(self::$_table_name)->execute();
					if($result->count() > 0){
						Validation::active()->set_message('closure', ':labelが重複しています。');
						return FALSE;
					}
					return TRUE;
				});
			default:
				$val->add('shop_id', '店舗ID')->add_rule('required');
//                $val->add('code', '商品コード')->add_rule('required');
				$val->add('title', '商品名')->add_rule('required');
				$val->add('status', '状態')->add_rule('required');
				break;
		}


		return $val;
	}

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
				'related'	=> array('detail','shop','category1','category2'),
				'where'		=> $where,
				'order_by'	=> $sort,
			)
		);
		return $data;
	}
	public static function findDetailTool($where="",$sort="")
	{
		$data = static::find('first', array(
				'related' => array('detail'),
				'where' => $where,
			)
		);
		return $data;
	}
	public static function findListFront($where="",$sort="",$paginations="")
	{
		$settings = array(
			'where' => $where,
			'order_by' => $sort,
		);
		if($paginations){
			$settings = array_merge($settings,array(
				'rows_offset' => $paginations->offset,
				'rows_limit' => $paginations->per_page,
			));
		}
		$data = static::find('all',$settings);
		return $data;
	}
	public static function findDetailFront($where="")
	{
		$data = static::find('first', array(
				'related' => array('detail'),
				'where' => $where,
			)
		);
		return $data;
	}

	public static function findDetail($shop_id , $id)
	{
		$sql = "";
		$sql .= "select id,shop_id,category1_id,code,title,title2,price_sale,deliver_mail_flg,reserve_date,publish_flg,term_start_date,term_end_date,comment ";
		$sql .= "from " . self::$_table_name . " where 1=1 ";
		$sql .= " and shop_id = :shop_id ";
		$sql .= " and id = :id ";
		$sql .= " and price_sale > 0 ";
		$sql .= " and status = 1 ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'id'		=> $id,
				'shop_id'	=> $shop_id,
			)
		);
		$data = $query->execute()->current();
		return $data;
	}


	public static function findProductList($where,$paginations="")
	{
		$sql = "";
		$sql .= "select * from (";
		$sql .= "select ";
		$sql .= " tbl_product.id, ";
		$sql .= " tbl_product.title, ";
		$sql .= " tbl_product.code, ";
		$sql .= " tbl_product.price_sale, ";
		$sql .= " tbl_product.new, ";
		$sql .= " tbl_product.term_stock_flg, ";
		$sql .= " tbl_product.publish_flg, ";
		$sql .= " tbl_product.body_list, ";
		$sql .= " tbl_product_recommend.sort, ";
		$sql .= " tbl_product.insert_date ";
		$sql .= "from ";
		$sql .= " (tbl_product left join tbl_product_recommend on tbl_product.id = tbl_product_recommend.product_id and tbl_product_recommend.shop_id = :shop_id) ";
		if($where["term_ids"]){
			$sql .= " left join tbl_product_term_relation on tbl_product.id = tbl_product_term_relation.product_id ";
			$sql .= " and tbl_product_term_relation.term_id in ({$where["term_ids"]}) ";
		}
		$sql .= "where 1=1 ";
		$sql .= " and tbl_product.price_sale > 0 ";
		$sql .= " and tbl_product.status = 1 ";
		$sql .= " and tbl_product.shop_id = :shop_id ";
		$sql .= " and ( ";
		$sql .= " tbl_product.publish_flg = 1 or ";
		$sql .= " (tbl_product.term_start_date <= now() and tbl_product.term_end_date >= now()) ";
		$sql .= " ) ";

		if(isset($where["c1"])) $sql .= " and tbl_product.category1_id = ".$where["c1"];
		//if(isset($where["g"])) $sql .= " and tbl_product.group_id = ".$where["g"];
		//if(isset($where["c2"])) $sql .= " and tbl_product.category2_id = ".$where["c2"];
		$sql .= " ) as prodcut_list ";
		$sql .= " order by ";
		$sql .= " prodcut_list.sort is null asc, ";
		$sql .= " prodcut_list.sort asc , ";
		$sql .= " prodcut_list.insert_date desc ";

		if($paginations){
			$sql .= " limit ".$paginations->per_page . " offset " . $paginations->offset;
		}
		$query = DB::query($sql);
		//Log::debug(print_r($sql_select.$sql_from.$sql_where.$sql_other.$offset_limit,true));

		$query->parameters(
			array(
				'shop_id'	=> $where["shop_id"],
			)
		);
		$data = $query->execute()->as_array();
		return $data;
	}

	public static function findProductList_old($where,$paginations="")
	{
		$sql_select =  <<< EOF
select
	tbl_product.id,
	tbl_product.title,
	tbl_product.code,
	tbl_product.price_sale,
	tbl_product.new,
	tbl_product.term_stock_flg,
	tbl_product.publish_flg,
	tbl_product.body_list,
	tbl_product_recommend.sort,
	sum(tbl_product_detail.stock) as stock
EOF;
		$sql_from =  <<< EOF

	from
		((tbl_product
			left join
				tbl_product_detail
			on  tbl_product.id = tbl_product_detail.product_id
		)left join
			tbl_product_recommend
		on  tbl_product.id = tbl_product_recommend.product_id and tbl_product_recommend.shop_id = {$where["shop_id"]}
		)left join
			tbl_product_term_relation
		on  tbl_product.id = tbl_product_term_relation.product_id
EOF;
		$sql_where =  <<< EOF

	where 1=1
		and tbl_product.price_sale > 0
		and tbl_product.status = 1
		and tbl_product_detail.status = 1
		and tbl_product.shop_id = {$where["shop_id"]}
EOF;

		if($where["term_ids"]){
			$sql_where .=  <<< EOF

		and (
			tbl_product_term_relation.term_id in ({$where["term_ids"]}) or
			tbl_product.publish_flg = 1 or
			(tbl_product.term_start_date <= now() and tbl_product.term_end_date >= now())
		)
EOF;
		}else{
			$sql_where .=  <<< EOF

		and (
			tbl_product.publish_flg = 1 or
			(tbl_product.term_start_date <= now() and tbl_product.term_end_date >= now())
		)
EOF;
		}

		if(isset($where["g"])) $sql_where .= " and tbl_product.group_id = ".$where["g"];
		if(isset($where["c1"])) $sql_where .= " and tbl_product.category1_id = ".$where["c1"];
		if(isset($where["c2"])) $sql_where .= " and tbl_product.category2_id = ".$where["c2"];

		$sql_other =  <<< EOF

	group by
		tbl_product.id,
		tbl_product.title,
		tbl_product.code,
		tbl_product.price_sale,
		tbl_product.new,
		tbl_product.term_stock_flg,
		tbl_product.publish_flg,
		tbl_product.body_list,
		tbl_product_recommend.sort
	order by
		tbl_product_recommend.sort is null asc,
		tbl_product_recommend.sort asc ,
		tbl_product.insert_date desc
EOF;

		$offset_limit = "";
		if($paginations){
			$offset_limit = " limit ".$paginations->per_page . " offset " . $paginations->offset;
		}
		$query = DB::query($sql_select.$sql_from.$sql_where.$sql_other.$offset_limit);
		//Log::debug(print_r($sql_select.$sql_from.$sql_where.$sql_other.$offset_limit,true));
		$data = $query->execute()->as_array();
		return $data;
	}

	public static function findProductLineupList($where,$paginations="")
	{
		$sql_select =  <<< EOF
select
	tbl_product.id,
	tbl_product.title,
	tbl_product.code,
	tbl_product.price_sale,
	tbl_product.comment,
	tbl_product.new,
	tbl_product.term_stock_flg,
	tbl_product.publish_flg,
	tbl_product.body_list,
	tbl_product_recommend.sort,
	sum(tbl_product_detail.stock) as stock
EOF;
		$sql_from =  <<< EOF

	from
		((tbl_product
			left join
				tbl_product_detail
			on  tbl_product.id = tbl_product_detail.product_id
		)left join
			tbl_product_recommend
		on  tbl_product.id = tbl_product_recommend.product_id and tbl_product_recommend.shop_id = {$where["shop_id"]}
		)left join
			tbl_product_term_relation
		on  tbl_product.id = tbl_product_term_relation.product_id
EOF;
		$sql_where =  <<< EOF

	where 1=1
		and tbl_product.price_sale > 0
		and tbl_product.status = 1
		and tbl_product_detail.status = 1
		and tbl_product.shop_id = {$where["shop_id"]}
EOF;

		if($where["term_ids"]){
			$sql_where .=  <<< EOF

		and (
			tbl_product_term_relation.term_id in ({$where["term_ids"]}) or
			tbl_product.publish_flg = 1 or
			(tbl_product.term_start_date <= now() and tbl_product.term_end_date >= now())
		)
EOF;
		}else{
			$sql_where .=  <<< EOF

		and (
			tbl_product.publish_flg = 1 or
			(tbl_product.term_start_date <= now() and tbl_product.term_end_date >= now())
		)
EOF;
		}

		if(isset($where["c1"])) $sql_where .= " and tbl_product.category1_id = ".$where["c1"];

		$sql_other =  <<< EOF

	group by
		tbl_product.id,
		tbl_product.title,
		tbl_product.code,
		tbl_product.price_sale,
		tbl_product.comment,
		tbl_product.new,
		tbl_product.term_stock_flg,
		tbl_product.publish_flg,
		tbl_product.body_list,
		tbl_product_recommend.sort
	order by
		tbl_product_recommend.sort is null asc,
		tbl_product_recommend.sort asc ,
		tbl_product.insert_date desc
EOF;

		$offset_limit = "";
		if($paginations){
			$offset_limit = " limit ".$paginations->per_page . " offset " . $paginations->offset;
		}
		$query = DB::query($sql_select.$sql_from.$sql_where.$sql_other.$offset_limit);
		//Log::debug(print_r($sql_select.$sql_from.$sql_where.$sql_other.$offset_limit,true));
		$data = $query->execute()->as_array();
		return $data;
	}

}
