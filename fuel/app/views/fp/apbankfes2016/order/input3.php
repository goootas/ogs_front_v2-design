
<?php echo Form::open("/order/confirm");?>
<div>
	<h4>お支払方法</h4>
	<hr>
	<br>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_payment')); ?></font><br>
	<?php if(Session::get("payment_ctl_card")):?>
	■<a href="#card"><font color='#000000'>クレジットカード</font></a><br>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_ato")):?>
	■<a href="#ato"><font color='#000000'>後払い</font></a><br>
	<?php endif; ?>
	<br>
	<hr>

	<?php if(Session::get("payment_ctl_card")):?>
		<a id="card" name="card"></a><input type="radio" name="payment" value="1" <?php echo (Session::get("order_data.data3.payment") == 1) ? "checked" : "";?>>クレジットカード
		<br>
		<p>
			ご利用可能なカードの種類：VISA/MasterCard/DINERS/JCB/AMEX<br>
			<br>
			お支払総額：商品代金合計＋送料<br>
			<br>
			＊カード会社からの請求書には､｢official-goods-store.jp｣と表記されます。<br>
		</p>

		カード番号<br>
		<?php echo Form::input('card_no', Session::get("order_data.data3.card_no") ? Session::get("order_data.data3.card_no") : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		「-(ハイフン)」は含まず入力してください<br>
		<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_cardno')); ?></font>
		<br>
		セキュリティコード<br>
		<?php echo Form::password('secure_cd', Session::get("order_data.data3.secure_cd") ? Session::get("order_data.data3.secure_cd") : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_secureno')); ?></font>
		<p>
			セキュリティコードについて<br>
			VISA/MASTER/DINERS/JCBの場合<br>
			クレジットカード背面に記載されている3桁の番号(メインのカード番号のすぐ右側)<br>
			<br>
			AMEXの場合<br>
			クレジットカード前面に記載されている4桁の番号(メインのカード番号の右上)<br>
		</p>
		有効期限<br>
		<select name="expire_m">
			<?php for ($i = 1; $i <= 12; $i++) {?>
				<option value="<?php echo sprintf("%02d", $i);?>"
					<?php echo (Session::get("order_data.data3.expire_m")==sprintf("%02d", $i)) ? " selected" : "";?>><?php echo sprintf("%02d", $i);?>月</option>
			<?php }?>
		</select>
		<select name="expire_y">
			<?php for ($i = 0; $i <= 25; $i++) {?>
				<option value="<?php echo date("y",strtotime($i." year" ));?>"
					<?php echo (Session::get("order_data.data3.expire_y")==date("y",strtotime($i." year" ))) ? " selected" : "";?>><?php echo date("y",strtotime($i." year" ));?>年</option>
			<?php }?>
		</select><br>
		例)カード表記が「04/2018」の場合は、「04月18年」と選択してください。<br>
		<input type="hidden" name="credit_jpo" value="10">
		<br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#000000'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_ato")):?>
		<a id="ato" name="ato"></a><input type="radio" name="payment" value="4" <?php echo (Session::get("order_data.data3.payment") == 4) ? "checked" : "";?>>後払い(コンビニ / 郵便振替 / 銀行振込)
		<br>
		<p>
			お支払い総額：商品代金＋送料＋決済手数料 ¥205(tax in)<br>
			<br>
			＊当サービスは(株)キャッチボールの運営する「後払い.com(ドットコム)」により提供されます。 下記注意事項を確認、同意の上、ご利用下さい。<br>
			＊銀行 / 郵便局 / コンビニでお支払いいただけます。<br>
			＊当店にかわり、後払い.com運営会社の(株)キャッチボールより請求書が送られます。<br>
			＊商品到着と請求書の到着は別になります。<br>
			＊請求書発行から14日後までにお支払い下さい。<br>
			＊支払期限を過ぎた場合、再度の請求毎に¥300の再請求書発行手数料がかかりますのでご注意下さい。<br>
			＊ギフト注文やプレゼント注文(商品配送先が注文者住所と異なる場合)でもご利用いただけます。<br>
			＊ご本人様確認や後払い.comサービス利用にあたって、お電話 / メールにてご連絡させていただく場合がございます。<br>
			＊お客様が当サイトにおいて登録された個人情報および発注内容は、後払い.comのサービスに必要な範囲のみで(株)キャッチボールに提供させていただきます。<br>
			＊与信結果によっては当サービスをご利用いただけない場合がございます。その場合は、他の決済方法にご変更いただくことになります。<br>
			＊商品の配送先を配送業者の営業所止め(営業所来店引取り)また転送依頼することはできません。<br>
			＊サービスをご利用いただいた場合は、上記注意事項にご同意いただいたものとみなさせていただきます。<br>
		</p><br>
		<div>
			<?php if( $change == 1) :?>
				<?php echo Form::submit('next', '変更'); ?>
			<?php else:?>
				<?php echo Form::submit('next', '次へ進む'); ?>
			<?php endif;?>
		</div>
		<br>
		■<a href="#top"><font color='#000000'>ページの先頭へ戻る</font></a><br>
		<hr>
		<br>
	<?php endif; ?>

	<div>
		<?php echo Html::anchor('/order/input2'.$session_get_param, '<font color="#000000">前に戻る</font>'); ?>
	</div>

</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
