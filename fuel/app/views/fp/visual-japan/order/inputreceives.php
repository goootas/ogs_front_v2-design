<?php echo Form::open("/order/input1");?>
<div>
	<h4>商品受取日時選択</h4>
	<hr>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_receives')); ?></font><br>
	<?php $ses_name = Session::get("order_data.datareceives.receives");?>

	<?php foreach ($shop_receives as $receives): ?>

		<?php
		if ($receives["stock"] <= 0 ){
			echo "受付終了";
		}else{ ?>
			<input type="radio" name="receives" value="<?php echo $receives["id"];?>">
		<?php } ?>
		<?php echo $receives["title1"].$receives["title2"]; ?><br>

	<?php endforeach; ?>
	<font style="color: #FF0000">
		■ご注文についての注意事項<br>
		・ご注文は、お1人様1回のみとなります。<br>
		・支払い方法：クレジットカード決済(Vプリカ等プリペイド式カード利用可) / コンビニ支払い<br>
		・各時間帯先着順定員制となります。定員数に達した時間帯は選択できません。<br>
		・注文確定後の受取日時変更はできません。<br>
		<br>
		■商品受取方法<br>
		下記をご用意いただき、選択受取日時に「VISUAL JAPAN SUMMIT 2016」会場受取窓口で提示してください。<br>
		下記をお持ちでない場合、商品をお渡しすることはできません。忘れずに必ずお持ちください。<br>
		<br>
		①ご本人確認書類(運転免許証、健康保険証、学生証など)<br>
		②注文確定後に発行される受注番号下5桁(注文確認画面・注文確認メール・マイページ注文履歴で確認できます)<br>
		※商品受取に「VISUAL JAPAN SUMMIT 2016」入場チケットは必要ありません。<br>
		<br>
		・会場内受取窓口：受取当日会場にて告知予定<br>
		・選択受取日時以外での商品受取はできません。必ず選択した時間内にお受け取りください。<br>
		・会場でのお受け取りは、ご注文されたご本人様のみに限らせていただきます。<br>
		・会場受取の方は、グッズ販売列に並ぶ必要はありません。選択時間内に会場受取窓口に直接お越しください。<br>
		<br>
	</font>
	<br>

	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>

	<br>

	<div>
		<?php echo Html::anchor('/cart'.$session_get_param, '<font color="#FFFFFF">カートに戻る</font>'); ?>
	</div>

</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
