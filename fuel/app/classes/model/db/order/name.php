<?php
class Model_Db_Order_Name extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'order_id',
		'did',
		'name',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_order_name';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	public static function findMypageOrderList($order_id)
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= "tbl_product.title, ";
		$sql .= "tbl_product_detail.option1_name, ";
		$sql .= "tbl_product_detail.option2_name, ";
		$sql .= "tbl_order_name.name  ";
		$sql .= "from ";
		$sql .= "(tbl_order_name left join tbl_product_detail on tbl_order_name.did = tbl_product_detail.id) ";
		$sql .= "left join tbl_product on tbl_product_detail.product_id = tbl_product.id ";
		$sql .= "where 1 = 1 ";
		$sql .= "AND tbl_product.status = 1 ";
		$sql .= "AND tbl_order_name.order_id = '".$order_id."'";
		$query = DB::query($sql);
		$data = $query->execute()->as_array();
		return $data;

	}
}

