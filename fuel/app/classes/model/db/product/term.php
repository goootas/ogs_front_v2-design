<?php
class Model_Db_Product_Term extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'shop_id',
        'title',
        'start_date',
        'finish_date',
        'sale_type',
        'deliver_start_date',
        'deliver_start_text',
        'status',
        'insert_id',
        'insert_date',
        'update_id',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_product_term';

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

    public static function findList($where="",$sort="")
    {
        $data = static::find('all', array(
                'where'		=> $where,
                'order_by'	=> $sort,
            )
        );
        return $data;
    }
    public static function findDetail($where="")
    {
        $data = static::find('first', array(
                'where'		=> $where,
            )
        );
        return $data;
    }

}
