<?php echo Form::open('/cart/fpchange');?>
<div>
	<p><?php echo implode('</p><p>', (array) Session::get_flash('error')); ?></p>
	<hr>
	<?php
	$total = 0;
	$total_tax = 0;
	?>
	<?php if(count($cart) > 0): ?>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			$anchor_txt = "";
			$anchor_txt .= "<font color='#FFFFFF'>".$data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"]."</font>";
			?>
			<?php echo Html::anchor('/product/detail/'.$data["id"].$session_get_param,$anchor_txt) ?><br>
			価格:¥<?php echo number_format(floor($data["price"] * $tax));?><br>
			数量:<select name="num_<?php echo $data["did"];?>">
				<option value="1" <?php if($data["num"]==1){?>selected="selected"<?php }?>>1</option>
				<option value="2" <?php if($data["num"]==2){?>selected="selected"<?php }?>>2</option>
				<option value="3" <?php if($data["num"]==3){?>selected="selected"<?php }?>>3</option>
				<option value="4" <?php if($data["num"]==4){?>selected="selected"<?php }?>>4</option>
				<option value="5" <?php if($data["num"]==5){?>selected="selected"<?php }?>>5</option>
				<option value="6" <?php if($data["num"]==6){?>selected="selected"<?php }?>>6</option>
				<option value="7" <?php if($data["num"]==7){?>selected="selected"<?php }?>>7</option>
				<option value="8" <?php if($data["num"]==8){?>selected="selected"<?php }?>>8</option>
				<option value="9" <?php if($data["num"]==9){?>selected="selected"<?php }?>>9</option>
				<option value="10" <?php if($data["num"]==10){?>selected="selected"<?php }?>>10</option>
				<option value="11" <?php if($data["num"]==11){?>selected="selected"<?php }?>>11</option>
				<option value="12" <?php if($data["num"]==12){?>selected="selected"<?php }?>>12</option>
				<option value="13" <?php if($data["num"]==13){?>selected="selected"<?php }?>>13</option>
				<option value="14" <?php if($data["num"]==14){?>selected="selected"<?php }?>>14</option>
				<option value="15" <?php if($data["num"]==15){?>selected="selected"<?php }?>>15</option>
				<option value="16" <?php if($data["num"]==16){?>selected="selected"<?php }?>>16</option>
				<option value="17" <?php if($data["num"]==17){?>selected="selected"<?php }?>>17</option>
				<option value="18" <?php if($data["num"]==18){?>selected="selected"<?php }?>>18</option>
				<option value="19" <?php if($data["num"]==19){?>selected="selected"<?php }?>>19</option>
				<option value="20" <?php if($data["num"]==20){?>selected="selected"<?php }?>>20</option>
			</select><br />
			削除:<?php echo Form::checkbox("del[]", $data["did"]); ?><br />
			小計:¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?><br>
			<hr>

		<?php endforeach; ?>

		合計:¥<?php echo number_format(intval($total_tax));?><br>

	<?php else: ?>
		現在、カートには商品がございません。<br>
		商品をお選びください。<br>
	<?php endif; ?>
	<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
		<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
			送料無料です<br>
		<?php else: ?>
			あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
		<?php endif; ?>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_cnt") < 3 && Session::get("payment_ctl_cnt") > 0):?>
		決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
	<?php endif; ?>
</div>
<?php if($total > 0 ): ?>
	<br>
	<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
	<div align="center">
		<?php echo Form::submit('change', '注文内容を更新する'); ?>
	</div>
	<?php echo Form::close();?>

	<?php if( $change == 1) :?>
		<?php echo Form::open(array("action" => '/order/confirm' , "method" => "get"));?>
		<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
		<div align="center">
			<?php echo Form::submit('order', '注文する'); ?>
		</div>
		<?php echo Form::close();?>
	<?php else:?>
<!--		--><?php //echo Form::open(array("action" => '/order/inputname' , "method" => "get"));?>
		<?php echo Form::open(array("action" => '/order/print/member' , "method" => "get"));?>
		<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
		<div align="center">
			<?php echo Form::submit('order', '注文する'); ?>
		</div>
		<?php echo Form::close();?>
	<?php endif;?>

	<br>
	※ご注文確定するまでは、カートに商品が入っていても在庫は確保されていません。予め以下ご了承ください。<br>
	・在庫の少ない商品は、ご注文手続き中に在庫切れとなる場合があります。<br>
	・ご注文手続き中に、ご注文商品の販売期間が終了してしまった場合、ご注文はできません。<br>
	<br>
	<div>
		<p><?php echo Html::anchor('/'.$session_get_param, "<font color='#FFFFFF'>←前に戻る</font>"); ?></p>
	</div>
<?php endif; ?>
