<style>
	.CONTENTS {
		margin: 10px 0 0;
	}
</style>
<?php if ($products): ?>
	<div class="marsonry">
		<ul>
			<?php foreach ($products as $key => $line): ?>
				<li class="box">
					<?php
					$html = '<div class="inner imgchange">';
					$html .= '<div class="alpha"></div>';
					$html .= '<div class="thumbnail">';

					if(isset($line["imgs"][0])){
						$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
						$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0]."?".time();
					}else{
						$path_front = "";
						$path_back = "";
					}
					if(isset($line["imgs"][1]) && (strpos($line["title"],"Tシャツ") !== FALSE || strpos($line["title"],"キャラT") !== FALSE)){
						$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][1]."?".time();
					}
					$img_tag = <<< EOM
<img src="//{$path_front}">
<input type="hidden" id="hover" name="hover" value="//{$path_front}" >
<input type="hidden" id="out" name="out" value="//{$path_back}">
EOM;
					$html .= $img_tag;

					$html .= '</div>';
					$html .= '<div class="text">';
					if($agent == "sp"){
						$html .= '<dl style="background: linear-gradient(to bottom, rgba(229,0,17,0) 0%,rgba(0, 0, 0, 0.59) 100%);">';
					}else{
						$html .= '<dl>';
					}
					$html .= '<dt>'.$line["title"].'</dt>';
					$html .= '<dd>¥'.number_format(floor($line["price_sale"] * $tax)).'</dd>';
					$html .= '</dl>';
					$html .= '</div>';
					$html .= '</div>';
					?>
					<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>
		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>
<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>
<?php if($banner): ?>
	<?php foreach($banner as $b): ?>
		<?php
		$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
		$array = get_headers($img_path);
		if(strpos($array[0],'OK')){
			?>
			<div class="SPECIAL">
				<?php if($b->url = ""){?>
					<?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title));?>
				<?php }else{?>
					<a href="<?php echo $b->url;?>" target="_blank"><?php echo Asset::img($img_path, array('class' => 'banner','alt' => $b->title));?></a>
				<?php }?>
			</div>
		<?php } ?>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
	$(function($){
		$('.HEADER').removeClass('MIN');
	});
	$(document).on({
		"mouseenter": function(){
			$(this).find("img").attr("src",$(this).find("#out").val());
		},
		"mouseleave": function(){
			$(this).find("img").attr("src",$(this).find("#hover").val());
		}
	}, ".imgchange");

</script>
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	?>
	<script type="text/javascript">
		$(function($){
			//タイルレイアウト実行
			function marsonryDo(){
				var windowW = $(window).width()
				var parentW = $(".CONTENTS .marsonry").width()
				if(windowW < 728){
					var boxW = (parentW) / 2
//					$('.box:nth-child(5n+1)').addClass("pickup");
				} else {
					var rand = Math.floor(Math.random()*2 + 1) ;
					var boxW = (parentW) / 3
//					$('.box:nth-child(' + rand + ')').addClass("pickup");
				}
				$('.box').each(function(i){
					if($(this).hasClass("pickup")){
						$(this).css({
							"width" : boxW * 2+ "px",
							"height" : boxW * 2 + "px"
						});
					} else {
						$(this).css({
							"width" : boxW + "px",
							"height" : boxW + "px"
						});
					}
					var thumbNailSize = $(this).width()
					$(this).find(".inner").css({
						"width" : (thumbNailSize - 10) + "px",
						"height" : (thumbNailSize - 10) + "px"
					});
				});
//				clossFadeDo()
				setTimeout(function(){
					$('.marsonry ul').masonry({
						itemSelector: '.box',
						columnWidth: boxW,
						isFitWidth: false
					});
				},100);
			}
			//クロスフェード
			function clossFadeDo(){
				setTimeout(function(){
					$('.box:odd .thumbnail').innerfade({
						speed: 500,
						timeout: 9000,
						type: 'random_start'
					});
				},6000);
				$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
					speed: 500,
					timeout: 9000,
					type: 'random_start'
				});
			}

			function autoLoad(){
				var maxpage = <?php echo $total_pages ?>;
				$('#loading').css('display', 'none');
				$.autopager({
					content: '.marsonry ul',
					link: '#NEXT a',
					autoLoad: true,

					start: function(current, next){
						$('#loading').css('display', 'block');
						$('#NEXT a').css('display', 'none');
					},
					load: function(current, next){
						$('#loading').css('display', 'none');
						$('#NEXT a').css('display', 'block');
						if( current.page >= maxpage ){
							marsonryDo()//パネル実行
							$('#NEXT a').hide();
							return false;
						}else{
							marsonryDo()//パネル実行
						}
					}
				});
				$('#NEXT a').click(function(){
					$.autopager('load');
					return false;
				});
			}


			//読み込み直後実行
			$(window).load(function () {
				marsonryDo()
				autoLoad()
			});
			//リサイズ時実行
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					marsonryDo()
				}, 1000);
			});});
	</script>
<?php } ?>
