<?php
namespace Model\lib;

use Model_Db_Group;
use Fuel\Core\Config;
use Fuel\Core\Log;

class Group extends \Model
{
	/*
	 * ID,TITLEを返す
	 * 該当レコードがなければNULLを返す。
	 */
	public static function getGroup($shop_id)
	{
		$search_wheres = array();
		$search_wheres[] = array("shop_id", $shop_id);
		$search_wheres[] = array("status", "!=",Config::get('status_value.deleted'));
		$search_order = array();
		$search_order[] = array("sort","asc");
		$group = Model_Db_Group::findListTool($search_wheres,$search_order);
		$data = array();
		$data = array("" => "未設定");
		if($group){
			foreach ($group as $value) {
				$data = $data + array($value->id => $value->title);
			}
		}
		Log::warning(print_r($data,true));
		return $data;
	}
}