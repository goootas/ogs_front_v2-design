<?php
class Model_Db_Category2 extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'category1_id',
		'title',
		'sort',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_category2';

	protected static $_belongs_to = array(
		'base' => array(
			'key_from' => 'category1_id',
			'model_to' => 'Model_Db_Category1',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		)
	);
	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		'category1' => array(
			'key_from' => 'category1_id',
			'model_to' => 'Model_Db_Category1',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		);

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('shop','category1'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
	public static function findDetailTool($where="")
	{
		$data = static::find('first', array(
			'related'	=> array('shop','category1'),
			'where'		=> $where,
			)
		);
		return $data;
	}
	public static function findListFront($where="",$sort="")
	{
		$data = static::find('all', array(
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
	public static function findDetailFront($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('category1'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
}
