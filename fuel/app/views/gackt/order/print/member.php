
<style>
	.CONTENTS {
		 margin-top: 0px;
	}
</style>

<p style="color: #FF0000;text-align: center"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php echo Form::open(array('id' => "print" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
<!--	<h4 class="headline">お客様情報</h4>-->
	<div style="text-align: center">
		「LAST VISUALIVE ドキュメンタリー写真集」(刻印入り)をご注文のお客様は、G&LOVERS(ファンクラブ)登録情報をご入力ください。
	</div>

	<div class="form-group">
		<?php echo Form::label('G&LOVERS会員番号<span class="caption">※必須</span>'); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('club_no',isset($club_data["club_no"]) ? $club_data["club_no"] : "",
					array('class' => 'validate[required] form-control','type'=>'tel')); ?>
			</div>
		</div>
	</div>

<!--	<div class="form-group count-2">-->
<!--		--><?php //echo Form::label('お名前<span class="caption">※必須</span>'); ?>
<!--		<div class="form-container">-->
<!--			<div class="inner">-->
<!--				--><?php //echo Form::input('name_sei', isset($club_data["name_sei"]) ? $club_data["name_sei"] : "",
//					array('class' => 'validate[required] form-control',"placeholder" => "姓")); ?>
<!--			</div>-->
<!--			<div class="inner">-->
<!--				--><?php //echo Form::input('name_mei', isset($club_data["name_mei"]) ? $club_data["name_mei"] : "",
//					array('class' => 'validate[required] form-control',"placeholder" => "名")); ?>
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!---->
	<div class="form-group">
		<?php echo Form::label('G&LOVERS登録電話番号<span class="caption">※必須</span>'); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('tel',isset($club_data["tel"]) ? $club_data["tel"] : "",
					array('class' => 'validate[required] form-control','type'=>'tel')); ?>
			</div>
			<div>
				※ハイフン/スペース等は省略してください。
			</div>
		</div>
	</div>

	<div class="description notice" style="margin-top:-5px;">
		G&LOVERSで「LAST VISUALIVE」のチケットを購入された方以外はご注文できません。
	</div>

	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("#print").submit();
		});
		$("#print").validationEngine({
			promptPosition : "topLeft",
			scroll: false,
		});
	});
</script>
