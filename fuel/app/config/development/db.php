<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=ogs-instance.cwppwbyiawdq.ap-northeast-1.rds.amazonaws.com;dbname=ogsdb_t',
			'username'   => 'reniuser',
			'password'   => 'Renipass',
		),
		'profiling'    => true,
	),
);
