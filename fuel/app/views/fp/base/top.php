<?php if ($products): ?>
	<?php foreach ($products as $line): ?>
		<?php
		$img_html = '<img src="/'.$shop_data["dir"].'/fpimg?t=1&img=http://'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".Config::get("aws.image_path")."fp/".$line["id"]."/".$line["id"]."_000.jpg?".time().'" align="left"  width="100" height="100">';
		$new_html = "";
		if($line["new"]) {
			$new_html =  "<span>NEW</span>";
		}
		$stock_html = "";
		if($line["stock"] == 0) {
			$stock_html = "<span>SOLD OUT</span>";
		}
		$caption_html = '';
		$caption_html .= '<br>'.$line["code"].'<br>¥'.number_format($line["price_sale"] * $tax).'(tax in)';
		?>
		<table width="100%">
			<tr>
				<td width="100">
					<?php echo $img_html;?>
				</td>
				<td valign="top">
					<?php echo Html::anchor('/product/detail/'.$line["id"].$session_get_param, "<font color='#000000'>".$line["title"]."</font>") ?>
					<?php echo $caption_html;?>
				</td>
			</tr>
		</table>

	<?php endforeach; ?>

	<?php if(Pagination::instance('list')->total_pages): ?>
		<br>
		<div align="center">
			<?php if($paginations["previous"]["uri"] !="#"): ?>
				<a href="<?php echo $paginations["previous"]["uri"].$session_get_param;?>"><font color='#EA6900'>←戻る</font></a>
			<?php else : ?>
				&nbsp;&nbsp;
			<?php endif ; ?>
			<?php if($paginations["next"]["uri"] !="#"): ?>
				<a href="<?php echo $paginations["next"]["uri"].$session_get_param;?>"><font color='#EA6900'>次へ→</font></a>
			<?php else : ?>
				&nbsp;&nbsp;
			<?php endif ; ?>
		</div>
	<?php endif; ?>

<?php endif; ?>


