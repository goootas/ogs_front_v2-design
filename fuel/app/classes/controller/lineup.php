<?php
use Model\lib\Image;

class Controller_Lineup extends Controller_Basefront
{
	/*
	 * 商品TOP
	 */
	public function action_index()
	{
		Response::redirect("/lineup/list/".$this->session_get_param);
	}

	/*
	 * 商品一覧
	 */
	public function action_list()
	{
		//----------------------------------
		// 販売期間情報取得
		//----------------------------------
		$term_ids = array();
		foreach($this->term_data as $val){
			$term_ids[] = $val->id;
		}

		//----------------------------------
		// 商品情報取得
		//----------------------------------
		$wheres["shop_id"] = $this->shop_data["id"];
		$wheres["term_ids"] = implode(",",$term_ids);
		$page = Input::get("p") ? Input::get("p") : 1;
		if(Input::get("c1")) $wheres["c1"] = Input::get("c1");

		$param = Input::server("QUERY_STRING") ? "?".Input::server("QUERY_STRING") : "";

		$product_total = Model_Db_Product::findProductLineupList($wheres);
		$paginations = Pagination::forge('list',
			array(
				'pagination_url'    => ($this->shop_data["type"] ? "/".$this->shop_data["dir"] : "").'/lineup/list/'.$param,
				'total_items'       => count($product_total),
				'per_page'          => 50,
				'uri_segment'       => "p",
				'current_page'      => $page,
				'num_links'         => 0,
			)
		);

		$product_list = Model_Db_Product::findProductLineupList($wheres,$paginations);

		$this->template->set_global('paginations', $paginations->render(true), false);
		$this->template->set_global('total_pages', Pagination::instance('list')->total_pages, false);

		foreach($product_list as $key => $product){
			$imgs = Image::getImage($product["id"]);
			$product_list[$key]["imgs"] = $imgs;
		}

		$this->template->set_global('products', $product_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/lineup/list');
	}

}

