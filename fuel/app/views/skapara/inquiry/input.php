<?php echo isset($shop_desc->pcsp_faq) ? $shop_desc->pcsp_faq : "";?>

<h4 class="headline">お問い合わせ</h4>
<div class="forms">
	<?php echo Form::open(array('id' => 'inquiry' ,'autocomplete'=>'off'));?>
	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須</span>', 'username',array("class" => "col-sm-3 control-label text-right")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('lastname', Input::post('lastname', isset($user_data) ? $user_data->username_sei : ""),
					array('class' => 'validate[required] form-control col-sm-1')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('firstname', Input::post('firstname', isset($user_data) ? $user_data->username_mei : ""),
					array('class' => 'validate[required] form-control col-sm-4')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('メールアドレス<span class="caption">※必須</span>', 'email',array("class" => "col-sm-3 control-label text-right")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('email', Input::post('email', isset($user_data)  ? $user_data->email : ""),
					array('class' => 'validate[required,custom[email]] form-control col-sm-4','type'=>'email')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('お問い合わせ内容<span class="caption">※必須</span>', 'message',array("class" => "col-sm-3 control-label text-right")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::textarea('message', Input::post('message', ''),
					array('class' => 'validate[required] form-control','rows' =>'10')); ?>
			</div>
		</div>
	</div>

	<div class="description notice" style="text-align: center">
		メールによるお問い合わせの回答は原則、祝日 / 年末年始を除く月～金曜 12:00～17:00の間となります。<br><br>
		携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、メールが届かない場合があります。<br>
		ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。<br>
		<?php if(date("Ymd") <= "20160103"):?>
			<br>
			<p>
				年末年始のお問い合わせ窓口について<br>
				12/25(金)～1/3(日)の間、お問い合わせ窓口を休業いたします。<br>
				※メールでの受付は可能ですが、返信は1/4(月)以降となる可能性があります。
			</p>
		<?php endif; ?>

	</div>

	<div class="buttons count-2">
		<button id="send" class="button rect key size-L">送信</button>
	</div>

	<?php echo Form::close();?>
</div>
<script type="text/javascript">
	$(function(){
		$('.faq-container').find('.body').hide();
		$('.faq-container').click(function() {
			if (!$(this).hasClass("active")){
				$('.faq-container').removeClass("active");
				$('.faq-container').find('.body').slideUp(200);
				$(this).find('.body').slideDown(200);
				$(this).addClass("active");
			}else{
				$('.faq-container').removeClass("active");
				$('.faq-container').find('.body').slideUp(200);
			}
		});
	});
</script>

<script>
	$(function(){
		$("#inquiry").validationEngine({promptPosition : "topLeft"});
		$("#send").click(function() {
			if(window.confirm('お問い合わせを送信します。よろしいですか？')){
				if ($("#inquiry").validationEngine('validate')) {
					$("#inquiry").submit();
					$('#inquiry').attr('disabled',true);
					$('#send').attr('disabled',true);
				}
			}
		});
	});
</script>
