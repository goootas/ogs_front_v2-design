<?php
namespace Model\lib;

use Fuel\Core\Log;
use Fuel\Core\Upload;

use Model_Lib_S3image;

class Request extends \Model
{
	public static function Encode(&$req)
	{
		//どのエンコーディングか判定
		$default_enc = "UTF-8";

		$detect_order = 'ASCII,JIS,UTF-8,CP51932,SJIS,SJIS-win';
		$enc = mb_detect_encoding(print_r($req,true), $detect_order, true);

//		$enc = mb_detect_encoding($req);
//		$enc = "SJIS-win";
		foreach ($req as &$value) {
			self::Core($value,$default_enc,$enc);
		}
		unset($value);
	}
	public static function Core( &$value , $default_enc , $enc)
	{
		Log::debug(print_r($value,true));
		if( is_array($value)){
			//配列の場合は再帰処理
			foreach ($value as &$value2) {
				self::Core($value2 , $default_enc , $enc);
			}
		}else if( $enc != $default_enc){
			//文字コード変換
			$value = mb_convert_encoding( $value , $default_enc , $enc ) ;
		}
	}
}