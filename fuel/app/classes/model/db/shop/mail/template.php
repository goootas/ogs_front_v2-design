<?php
class Model_Db_Shop_Mail_Template extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'type',
		'mail_sender',
		'title',
		'message',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_mail_template';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }

	public static function findType($shop_id,$type)
	{
		$data = static::find('first', array(
			'where' => array(
                array("shop_id", $shop_id),
                array("type", $type),
			))
		);
		return $data;
	}
    public static function findListTool($where="",$sort="")
    {
        $data = static::find('all', array(
                'where'		=> $where,
                'order_by'	=> $sort,
            )
        );
        return $data;
    }
}
