<?php
namespace Model\lib;
class Postal extends \Model
{
	public static function getPostalZip($zip)
	{
		if(!$postal = \Model_Db_Postalfull::getData($zip)){
			return "";
		}
		return $postal;
	}
}
