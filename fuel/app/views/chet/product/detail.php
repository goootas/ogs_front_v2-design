<?php echo Asset::css(array(Config::get("assets_url").'css/fotorama.css')); ?>
<?php echo Asset::js(array(Config::get("assets_url").'js/fotorama.js')); ?>

<div class="DETAIL">
	<div class="CONTAINER">
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<!--Photo-->
		<div class="product-photo">
			<div class="fotorama" data-width="100%" data-ratio="4/4" data-nav="thumbs" data-thumbheight="70">
				<?php if(isset($images) && count($images) > 0): ?>
					<?php foreach ($images as $image): ?>
						<?php echo Asset::img("//".$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$image."?".time());?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<!--Text-->
		<div class="product-texts">

			<fieldset>
				<!-- 商品基本 -->
				<ul class="list">
					<li class="name"><h3><?php echo isset($product) ? $product->title : '';?></h3></li>
					<li class="price"><span>¥<?php echo isset($product) ? number_format(floor($product->price_sale)).'(+tax)' : '';?></span></li>
				</ul>

				<div class="product-social">
<!--					<div class="social-title">Share with:</div>-->
					<div class="twitter">
						<a href="//twitter.com/share" data-url="https://<?php echo Input::server("SERVER_NAME").Input::server("REQUEST_URI");?>" data-text="<?php echo $product->title; ?>" data-lang="ja" data-hashtags="<?php echo Config::get("tw.hashtag");?>"><img src="/assets/img/chet/IMG_icon_twitter.png"></a>
					</div>
					<div class="facebook">
						<a href="http://www.facebook.com/share.php?u=https://<?php echo Input::server("SERVER_NAME")."/".Input::server("REQUEST_URI");?>" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;"><img src="/assets/img/chet/IMG_icon_facebook.png"></a>
					</div>
				</div>

				<?php if($kikangai_flg) :?>

					<div class="description notice">
						現在販売しておりません
<!--						12月中旬発売予定-->
					</div>
					<br>

				<?php else :?>

				<form>
					<?php if(isset($product)): ?>
						<?php // 全SKU在庫確認　0の場合は、selectフォームは表示しない ?>
						<?php $stock_cnt = 0;?>
						<?php foreach ($product->detail as $detail): ?>
							<?php $stock_cnt = $stock_cnt + $detail->stock;?>
						<?php endforeach; ?>
						<?php if($stock_cnt <= 0 ): ?>

							<div class="description notice" style="text-align: center ; font-weight: bold;font-size: x-large;">
								<b>SOLD OUT</b>
							</div>
							<br>


						<?php else: ?>
							<select name="select" id="cart_num">
								<?php if(count($product->detail) > 1): ?>
									<option value="">選択してください</option>
								<?php endif; ?>
								<?php foreach ($product->detail as $detail): ?>
									<?php if($detail->option1_name || $detail->option2_name): ?>
										<option value="<?php echo $detail->id;?>" <?php if ($detail->stock <= 0){echo "disabled";}?>>
											<?php
											echo $detail->option1_name ."  ".$detail->option2_name.":";
											if ($detail->stock > 0 ){
												if ($detail->stock <= 5 ){
													echo "残りわずか";
												}else{
													echo "在庫あり";
												}
											}else{
												echo "在庫なし";
											}
											?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
							<div class="description notice" style="text-align: left">
								※ご注文数量の増減は「購入する」ボタン押下後、カート内にて変更できます。
							</div>

							<div class="buttons">
								<a href="javascript:void(0)" class="button key rect size-L" onclick="selectitem();  return false;" >購入する</a>
							</div>

						<?php endif; ?>
					<?php endif; ?>

				<?php echo Form::close();?>

				<?php endif; ?>

				<?php echo isset($product) ? $product->comment : '';?>
<!--				<ul class="freebox">-->
<!--					<li>-->
<!--					<p class="text-point">-->
<!--					<span><img src="/assets/img/chet/IMG_icon_checkmark.png">ハードタイプ</span>-->
<!--					<span><img src="/assets/img/chet/IMG_icon_checkmark.png">天然シチリア産レモン精油配合</span>-->
<!--					<span><img src="/assets/img/chet/IMG_icon_checkmark.png">優れた速乾性と程よいホールド力</span>-->
<!--					</p>-->
<!--					<strong class="subitem-name">CHET デップジェル</strong>-->
<!--					<p class="text-caption">香り：レモン  /  内容量 : (NET.)500g</p>-->
<!--					<br>-->
<!--					<figcaption><p class="text-caption">■用法<br>-->
<!--					少量を手のひらでよくのばしてから、髪になじませてください。<br>-->
<!--					<br>-->
<!--					■ご注意<br>-->
<!--					お肌に異常が生じていないかよく注意して使用して下さい。お肌に合わないとき、または使用中に赤み、腫れ、刺激、かゆみ、色抜け（白斑等）や黒ずみ等の異常が現れたときは使用を中止し、皮膚科専門医等へのご相談をおすすめします。目に入らないようにご注意ください。目に入った場合は、こすらず、すぐに洗い流してください。極端に高温又は低温の場所、直射日光の当たる場所には保管しないで下さい。お子さまやペットの届かない場所に保管して下さい。<br>-->
<!--					<br>-->
<!--					■全成分<br>-->
<!--					水・PVP・PEG-20ソルビタンココエート・BG・レモン果実油・グレープフルーツ果皮油・AMP・カルボマー・EDTA-2Na・フェノキシエタノール・メチルパラベン</p>-->
<!--					</figcaption>-->
<!--					</li>-->
<!--				</ul>-->
			</fieldset>
		</div>
	</div>
</div>
<?php echo Form::open(array('action' => '/cart/add', 'id' => "cart"));?>
<?php echo Form::hidden('did', '',array('id' =>"did")); ?>
<?php echo Form::hidden('num', '1',array('id' =>"num")); ?>
<?php echo Form::close();?>
<script>
	function selectitem(){
		if($("#cart_num").val() != ""){
			$("#did").val($("select[name='select']").val());
			$("#num").val(1);
			$("#cart").submit();
		}else{
			alert("購入アイテムを選択してください");
		}
	}
</script>




