<?php
header('Content-Type: application/xhtml+xml');
echo '<?xml version="1.0" encoding="Shift_JIS" ?>';

if (strripos(Input::user_agent(),"docomo")){
	echo '<!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/1.1) 1.0//EN" "i-xhtml_4ja_10.dtd">';
}
if (strripos(Input::user_agent(),"kddi")){
	echo '<!DOCTYPE html PUBLIC "-//OPENWAVE//DTD XHTML 1.0//EN" "http://www.openwave.com/DTD/xhtml-basic.dtd">';
}
if ((strripos(Input::user_agent(),"vodafone")) || (strripos(Input::user_agent(),"softbank"))){
	echo '<!DOCTYPE html PUBLIC "-//J-PHONE//DTD XHTML Basic 1.0 Plus//EN""xhtml-basic10-plus.dtd">';
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-Type" content="application/xhtml+xml; charset=Shift_JIS" />
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Cache-Control" content="max-age=0" />
	<meta http-equiv="Expires" content="0">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
</head>
<body style="background-color:#FFFFFF; color:#000000;" link="#000000" vlink="#000000" alink="#000000">
<style type="text/css">
	<![CDATA[
	a:link{color:#999999;}
	a:visited{color:#999999;}
	a:focus{color:#999999;}
	]]>
</style>
<div style="font-size:x-small;"><a id="top" name="top"></a>
	<div align="center">
		<?php echo '<img src="/'.$shop_data["dir"].'/fpimg?img=https://'.$shop_data["domain"]."/".$shop_data["dir"].'/assets/img/'.$shop_data["dir"].'/fp/header.jpg" width="100%">';?>
	</div>
	<hr>
	<div align="center">STORE</div>
	<hr>

	<?php if(Request::active()->controller !== 'Controller_Order') :?>
		<div align="center">

			<?php echo Form::open(array("action" => '/cart' , "method" => "get"));?>
			<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
			<div align="center">
				<?php echo Form::submit('cart', 'カートを見る'); ?>
			</div>
			<?php echo Form::close();?>
			<br>
			<?php echo Html::anchor('/'.$session_get_param, '<span><font color="#000000">ALL</font></span>', array("class" => "button text all"), true); ?>
			<?php if($category1): ?>
				<?php foreach($category1 as $c1): ?>
					<?php echo Html::anchor('/product/list/'.$session_get_param.'&c1='.$c1->id, "<span><font color='#000000'>".$c1->title."</font></span>") ?>
				<?php endforeach; ?>
			<?php endif; ?>

		</div>
		<br>

		<div align="center" style="background-color: #DFDFDF">
			<?php if(Session::get('user.id')) : ?>
				<?php echo Html::anchor('mypage/'.$session_get_param,Asset::img($shop_data["dir"].'/fp/button2.gif',array("width"=>"80"))); ?>
				<?php echo Html::anchor('logout/'.$session_get_param,Asset::img($shop_data["dir"].'/fp/button3.gif',array("width"=>"80"))); ?>
			<?php else: ?>
				<?php echo Html::anchor('login'.$session_get_param, '<font color="#000000">ログイン</font>'); ?>
			<?php endif; ?>
		</div>

	<?php endif; ?>

	<div>
		<?php echo $content; ?>
	</div>
	<br>
	<?php echo isset($shop_desc->fp_top1) ? $shop_desc->fp_top1 : "";?>
	<div>
		<br>
		■<a href="#top"><font color='#000000'>ページの先頭へ戻る</font></a><br>
		■<?php echo Html::anchor('/'.$session_get_param, "<font color='#000000'>ホームに戻る</font>") ?><br>
		■<?php echo Html::anchor('/cart'.$session_get_param, "<font color='#000000'>カートを見る</font>") ?><br>
		■<?php echo Html::anchor('/mypage'.$session_get_param, "<font color='#000000'>マイページ</font>") ?><br>
		■<?php echo Html::anchor('/fpinfo/guide/'.$session_get_param, '<font color="#000000">ショッピングガイド</font>') ?><br />
		■<?php echo Html::anchor('/fpinfo/privacy/'.$session_get_param, '<font color="#000000">プライバシーポリシー</font>') ?><br />
		■<?php echo Html::anchor('/fpinfo/legal/'.$session_get_param, '<font color="#000000">特定商取引法に基づく表記</font>') ?><br />
		■<?php echo Html::anchor('/inquiry/'.$session_get_param, '<font color="#000000">お問い合わせ</font>') ?><br />
		<br>
	</div>

	<?php echo isset($shop_desc->fp_top2) ? $shop_desc->fp_top2 : "";?>

	<div align="center">
		<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI Co.,Ltd.";?> All Rights Reserved</p>
	</div>
</div>
</body>
</html>

