$(function(){
function clickAction(){
	$(".TOPICS.clone").click(function(event) {
		$(this).toggleClass("on");
		$(this).next().slideToggle("fast");
	});

	$(".menubutton").toggle(
	  function () {
		$(this).toggleClass("on");
		$(this).siblings(".GLOBALNAVI").fadeIn();
		$(this).siblings(".GLOBALNAVI").toggleClass("on");
		$("body").css("overflow","hidden");
	  },
	  function () {
		$(this).toggleClass("on");
		$(this).siblings(".GLOBALNAVI").fadeOut();
		$(this).siblings(".GLOBALNAVI").toggleClass("on");
		$("body").css("overflow","auto");
	  }
	);
}


function floatingHeader(){
	var header = $('.HEADER');
	$(window).scroll(function () {
	if($(window).scrollTop() > 400) {
		if(!header.hasClass("on")){
		header.clone(true).addClass("clone").insertAfter("body");
		setTimeout(function(){
			$('.HEADER.clone').addClass('show')
		},200);
		header.addClass("on");
		}
	}else{
		$('.HEADER.clone').removeClass('show')
		setTimeout(function(){
		$('.HEADER.clone').remove();
		header.removeClass("on");
		},200);
	}
	});
}

function mainImege(){
	var winW = $(window).width();
	if(winW < 1024){
		$('.titlelogo').attr('src', 'img/IMG_logo_white.png');
		$('.mainvisual').attr('src', 'img/IMG_main_sp.jpg');
	}else{
		$('.titlelogo').attr('src', 'img/IMG_logo.png');
		$('.mainvisual').attr('src', 'img/IMG_main.jpg');
	}
}

function rotateTopics(){
	var winW = $(window).width();
	var winH = $(window).height();
	var mainvisualH = $(".mainvisual").height();
	var topics = $('.TOPICS');
	if(winW < 1024){
		if(!$('.TOPICS.clone').length){
			topics.clone(true).addClass("clone flexslider").insertAfter(".mainvisual");
			$('.flexslider').flexslider({
				animation: "slide",
				animationSpeed: 300,
				directionNav: false,
				controlNav: false
			});
			$('.TOPICS.clone .btn').remove();

		}
	}else{
		$('.TOPICS.clone').remove();
	}
	$('.TOPICS.clone').css({
		top: mainvisualH - 65
	});
}

//Onload
$(window).load(function() {
	floatingHeader();
	mainImege();
	rotateTopics();
	clickAction();
});


//Resize
$(window).resize(function() {
	mainImege();
	rotateTopics();
});


});


