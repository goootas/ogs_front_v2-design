<?php
namespace Model\lib;

use Fuel\Core\Config;
use Fuel\Core\DB;
use Fuel\Core\Input;
use Fuel\Core\Log;
use Fuel\Core\Response;
use Fuel\Core\Session;

/*
 * ガラ向け入力チェック
 */
class FpValidate extends \Model
{
	/*
	 * 名入れ入力チェック
	 */
	public static function naire($names,$session_get_param)
	{
		$err_cnt = 0;
		$e_name = array();
//		$names = Input::post("names_value");
		foreach ($names as $val){

			$error_msg = "";
			if(!$val){
				$e_name[] = '必須項目です';
				$err_cnt++;
			}else{
				//if (preg_match("/^[a-zA-Z0-9]+$/", $val)) {
				if (preg_match("/^[0-9a-zA-Z!\"#\$%& \'\(\)=~\|`\{\+\}<>\?_\-\^¥@\[:\];,\.\/]+$/", $val)) {
				}else{
					$error_msg = '入力文字種・文字数を確認してください';
					$err_cnt++;
				}

				if(strlen($val) > 17){
					$error_msg = '入力文字種・文字数を確認してください';
					$err_cnt++;
				}
				$e_name[] = $error_msg;
			}
		}
		if($err_cnt){
			Session::set_flash('e_name', $e_name);
			Response::redirect('/order/inputname'.$session_get_param);
		}
		return;
	}
	/*
	 * 注文者情報入力チェック
	 */
	public static function input1($name_sei,$name_mei,$name_sei_kana,$name_mei_kana,$email,$email2,
								  $zip,$state,$addr1,$addr2,$tel1,$tel2,$tel3,$reg_flg,$passwd,$session_get_param)
	{
		$e_name		= array();
		$e_kana		= array();
		$e_email1	= array();
		$e_email2	= array();
		$e_zip		= array();
		$e_state	= array();
		$e_addr1	= array();
		$e_addr2	= array();
		$e_tel		= array();
		$e_pass		= array();
//				$e_event = array();

		//必須チェック
		$err_cnt = 0;
		if(!($name_sei && $name_mei)){
			$e_name[] = '必須項目です';
			$err_cnt++;
		}
		if(!($name_sei_kana && $name_mei_kana)){
			$e_kana[] = '必須項目です';
			$err_cnt++;
		}
		if(!$email){
			$e_email1[] = '必須項目です';
			$err_cnt++;
		}
		if(!$email2){
			$e_email2[] = '必須項目です';
			$err_cnt++;
		}
		if($email != $email2){
			$e_email2[] = '入力された値が一致しません';
			$err_cnt++;
		}else{
			if(preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD', $email)){
			}else{
				$e_email2[] = 'メールアドレスが正しくありません';
				$err_cnt++;
			}
		}

		if(!$zip){
			$e_zip[] = '必須項目です';
			$err_cnt++;
		}else{
			if(strlen($zip) != 7){
				$e_zip[] = '7桁で入力してください';
				$err_cnt++;
			}
			if(!\Model\lib\Validate::checkZip($zip)){
				$e_zip[] = '郵便番号が正しくありません';
				$err_cnt++;
			}
		}
		if(!$state){
			$e_state[] = '必須項目です';
			$err_cnt++;
		}
		if(!$addr1){
			$e_addr1[] = '必須項目です';
			$err_cnt++;
		}
		if(!$addr2){
			$e_addr2[] = '必須項目です';
			$err_cnt++;
		}
		if($zip && $state && $addr1) {
			if(!\Model\lib\Validate::checkZipAddress($zip,$state,$addr1,$addr2)){
				$e_addr1[] = '郵便番号と住所が一致していません';
				$err_cnt++;
			}
		}
		if(!($tel1 && $tel2 && $tel3)){
			$e_tel[] = '必須項目です';
			$err_cnt++;
		}else{
			if(!\Model\lib\Validate::checkTel($tel1.$tel2.$tel3)){
				$e_tel[] = '電話番号が正しくありません';
				$err_cnt++;
			}
		}
		if($reg_flg){
			if(!$passwd){
				$e_pass[] = '必須項目です';
				$err_cnt++;
			}
		}

		if($err_cnt){
			Session::set_flash('e_name', $e_name);
			Session::set_flash('e_kana', $e_kana);
			Session::set_flash('e_email1', $e_email1);
			Session::set_flash('e_email2', $e_email2);
			Session::set_flash('e_zip', $e_zip);
			Session::set_flash('e_state', $e_state);
			Session::set_flash('e_addr1', $e_addr1);
			Session::set_flash('e_addr2', $e_addr2);
			Session::set_flash('e_tel', $e_tel);
			Session::set_flash('e_pass', $e_pass);
			Response::redirect('/order/input1'.$session_get_param);
		}
		return;
	}

	/*
	 * お届け先情報入力チェック
	 */
	public static function input2($name_sei,$name_mei,$zip,$state,$addr1,$addr2,$tel1,$tel2,$tel3,$session_get_param)
	{
		$e_name = array();
		$e_zip = array();
		$e_state = array();
		$e_addr1 = array();
		$e_addr2 = array();
		$e_tel = array();

		//必須チェック
		$err_cnt = 0;
		if(!($name_sei && $name_mei)){
			$e_name[] = '必須項目です';
			$err_cnt++;
		}
		if(!$zip){
			$e_zip[] = '必須項目です';
			$err_cnt++;
		}else{
			if(strlen($zip) != 7){
				$e_zip[] = '7桁で入力してください';
				$err_cnt++;
			}
		}
		if(!$state){
			$e_state[] = '必須項目です';
			$err_cnt++;
		}
		if(!$addr1){
			$e_addr1[] = '必須項目です';
			$err_cnt++;
		}
		if(!$addr2){
			$e_addr2[] = '必須項目です';
			$err_cnt++;
		}
		if($zip && $state && $addr1) {
			if(!\Model\lib\Validate::checkZipAddress($zip,$state,$addr1,$addr2)){
				$e_addr1[] = '郵便番号と住所が一致していません';
				$err_cnt++;
			}
		}
		if(!($tel1 && $tel2 && $tel3)){
			$e_tel[] = '必須項目です';
			$err_cnt++;
		}else{
			if(!\Model\lib\Validate::checkTel($tel1.$tel2.$tel3)){
				$e_tel[] = '電話番号が正しくありません';
				$err_cnt++;
			}
		}
		if($err_cnt){
			Session::set_flash('e_name', $e_name);
			Session::set_flash('e_zip', $e_zip);
			Session::set_flash('e_state', $e_state);
			Session::set_flash('e_addr1', $e_addr1);
			Session::set_flash('e_addr2', $e_addr2);
			Session::set_flash('e_tel', $e_tel);
			Response::redirect('/order/input2'.$session_get_param);
		}
		return;
	}


	/*
	 * お支払情報入力チェック
	 */
	public static function input3($payment,$card_no,$secure_cd,$session_get_param)
	{

		$e_payment = array();
		$e_cardno = array();
		$e_secureno = array();

		//必須チェック
		$err_cnt = 0;
		if(!($payment)){
			$e_payment[] = '必須項目です';
			$err_cnt++;
		}else{
			if($payment == Config::get("payment.value.card")){
				if(!($card_no)){
					$e_cardno[] = '必須項目です';
					$err_cnt++;
				}

//				if($this->shop_data["dir"] == "keyakizaka46") {
//					if(date("Ymd") >= "20160721" && date("Ymd") <= "20160728"){
//						if(preg_match('/^516527.*$/', $card_no)){
//						}else{
//							$e_cardno[] = 'マネパカードのクレジットカード番号を入力してください';
//							$err_cnt++;
//						}
//					}
//				}

				if(!($secure_cd)){
					$e_secureno[] = '必須項目です';
					$err_cnt++;
				}
			}
		}
		if($err_cnt){
			Session::set_flash('e_payment', $e_payment);
			Session::set_flash('e_cardno', $e_cardno);
			Session::set_flash('e_secureno', $e_secureno);
			Response::redirect('/order/input3'.$session_get_param);
		}

		return;
	}
}
