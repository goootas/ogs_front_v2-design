<?php
class Model_Db_Order_Print extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'order_id',
		'product_id',
		'product_detail_id',
		'print_id',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_order_print';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	public static function findDetail($order_id )
	{
		$sql = "";
		$sql .= "SELECT * FROM " . self::$_table_name . " where 1 = 1 ";
		$sql .= " AND order_id = :order_id ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'order_id'	=> $order_id,
			)
		);
		$data = $query->execute()->as_array();
		return $data;
	}

}

