<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<h4>ご注文内容詳細</h4>
<div>
	<?php $total_tax = 0; ?>
	<?php if(count($order["detail"]) > 0): ?>
		<?php foreach ($order["detail"] as $data): ?>
			<?php $product = $data["product"];?>
			<?php $total_tax = $total_tax + $data["price"];?>
			<?php
			$anchor_txt = "";
			$anchor_txt .= "<font color='#000000'>".$product["base"]["title"] ."&nbsp;". $product["option1_name"] ."&nbsp;". $product["option2_name"]."</font>";
			?>
			<?php echo Html::anchor('/product/detail/'.$data["id"].$session_get_param,$anchor_txt) ?><br>
			価格:¥<?php echo number_format(floor($data["price"]/$data["num"]));?><br>
			数量:<?php echo $data["num"];?><br />
			小計:¥<?php echo number_format($data["price"]);?><br>
			<hr>
		<?php endforeach; ?>

		商品合計:¥<?php echo number_format(intval($total_tax));?><br>
		送料:¥<?php echo number_format(intval($order["postage"]));?><br>
		決済手数料:¥<?php echo number_format(intval($order["fee"]));?><br>
		総合計:¥<?php echo number_format(intval(floor($total_tax) + $order["postage"] + $order["fee"]));?><br>

	<?php endif; ?>
</div>
<br>
<div>
	<h4>お客様情報</h4>
	お名前<br>
	<?php echo $order["order_username_sei"].$order["order_username_mei"]?> 様<br>
	<br>
	ふりがな<br>
	<?php echo $order["order_username_sei_kana"].$order["order_username_mei_kana"]?> 様<br>
	<br>
	郵便番号<br>
	<?php echo substr($order["order_zip"], 0,3);?>-<?php echo substr($order["order_zip"], 3,4);?><br>
	<br>
	都道府県<br>
	<?php echo $order["order_prefecture"];?><br>
	<br>
	住所１<br>
	<?php echo $order["order_address1"];?><br>
	<br>
	住所２<br>
	<?php echo $order["order_address2"];?><br>
	<br>
	メールアドレス<br>
	<?php echo $order["order_email"];?><br>
	<br>
	電話番号<br>
	<?php echo $order["order_tel1"];?>-<?php echo $order["order_tel2"];?>-<?php echo $order["order_tel3"];?><br>
	<br>
	性別<br>
	<?php if ($order["order_sex"]=="1"):?>男性
	<?php elseif ($order["order_sex"]=="2"):?>女性
	<?php else:?>指定なし
	<?php endif;?><br>
	<br>
	生年月日<br>
	<?php if ($order["order_birthday"]):?>
		<?php echo substr($order["order_birthday"], 0,4);?>年<?php echo substr($order["order_birthday"], 4,2);?>月<?php echo substr($order["order_birthday"], 6,2);?>日
	<?php else:?>指定なし
	<?php endif;?><br>
	<br>
	<h4>お届け先情報</h4>
	お名前<br>
	<?php echo $order["deliver_username_sei"];?> <?php echo $order["deliver_username_mei"];?> 様<br>
	<br>
	郵便番号<br>
	<?php echo substr($order["deliver_zip"], 0,3);?>-<?php echo substr($order["deliver_zip"], 3,4);?><br>
	<br>
	都道府県<br>
	<?php echo $order["deliver_prefecture"];?><br>
	<br>
	住所１<br>
	<?php echo $order["deliver_address1"];?><br>
	<br>
	住所２<br>
	<?php echo $order["deliver_address2"];?><br>
	<br>
	電話番号<br>
	<?php echo $order["deliver_tel1"];?>-<?php echo $order["deliver_tel2"];?>-<?php echo $order["deliver_tel3"];?><br>
	<br>
	お届け日<br>
	<?php if(!$order["deliver_day"]) : ?>
		指定無し
	<?php else : ?>
		<?php echo sprintf("%04d年%02d月%02d日",
			substr($order["deliver_day"],0,4),
			substr($order["deliver_day"],4,2),
			substr($order["deliver_day"],6,2));?>
	<?php endif; ?><br>
	<br>
	お届け希望時間帯<br>
	<?php if(!$order["deliver_time"]) : ?>
		指定無し<br>
	<?php else : ?>
		<?php if($order["deliver_time"] == "AM") : ?>
			<?php echo "午前中";?>
		<?php else : ?>
			<?php $dt = explode("-",$order["deliver_time"]);?>
			<?php echo sprintf("%d時〜%d時",$dt[0],$dt[1]);?>
		<?php endif; ?>
	<?php endif; ?><br>
<!--	<br>-->
<!--	備考<br>-->
<!--	--><?php //echo nl2br($order["comment"]);?><!--<br>-->
	<br>
	<h4>お支払情報</h4>
	支払方法<br>
	<?php echo Config::get("payment.status_user.".$order["payment"]);?>
	<?php if($order["payment"] == 2) echo Config::get("cvs.".$order["cvs_type"]);?><br>
	<br>
	受付番号<br>
	<?php echo $order["order_id"];?><br>
	<br>
	注文日<br>
	<?php echo $order["insert_date"];?><br>
	<br>
<!--	入金状況<br>-->
<!--	--><?php //if($order["capture_date"]):?>
<!--		入金済み-->
<!--	--><?php //else:?>
<!--		--><?php //if($order["payment"] == 3 || $order["payment"] == 4):?>
<!--		--><?php //else:?>
<!--			入金待ち-->
<!--		--><?php //endif;?>
<!--	--><?php //endif;?><!--<br>-->
<!--	<br>-->
	配送状況<br>
	<?php if($order["delivery_date"]):?>
		出荷済み(佐川急便お問い合わせNo:<a href="http://k2k.sagawa-exp.co.jp/p/web/okurijosearch.do?okurijoNo=<?php echo $order["tracking_number"];?>" target="_blank" style="text-decoration: underline;"><?php echo $order["tracking_number"];?></a>)
	<?php else:?>
		<?php if($order["cancel_date"]):?>
			出荷取消
		<?php else :?>
			出荷準備中
		<?php endif;?>
	<?php endif;?>
	<br>
	<br>
	その他<br>
	<?php if($order["cancel_date"]):?>注文キャンセル<?php endif;?><br>
	<div>
		<p><?php echo Html::anchor('/mypage'.$session_get_param, '<font color="#000000">戻る</font>'); ?></p>
	</div>
</div>

