<?php
class Model_Db_Shop_Tax extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'tax',
		'start_date',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_tax';

	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		);

	public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('shop'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
	public static function findTaxNow($shop_id)
	{
		$data = static::find('first', array(
			'where'		=> array(
				array("shop_id" ,$shop_id),
				array("start_date" ,"<",date("Y-m-d H:i:s")),
			),
			'order_by'	=> array(
				array("start_date" ,"desc"),
			),
			)
		);
		return isset($data->tax) ? ($data->tax + 100) / 100 : 0;
	}

}
