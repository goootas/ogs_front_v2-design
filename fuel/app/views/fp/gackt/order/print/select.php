<p style="color: #FF0000;text-align: center"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php echo Form::open(array('id' => "print" ,'autocomplete'=>'off'));?>
<div>
	<div style="text-align: center">
		刻印される内容をご確認ください。
	</div>
	<br>
	G&LOVERS会員番号
	<div class="form-group">
		<?php echo $club_data["club_no"]?>
	</div>
	<br>
	お名前
	<div class="form-group">
		<?php echo $form_name_data;?><br>
		<p style="color: #FF0000;">
		※アルファベットの綴りに誤りがある場合は、大変お手数ですがご注文確定後、CONTACTページより正しい綴りをご連絡ください。(刻印するお名前の変更はできません)
		</p>
	</div>

	<br>
	<?php for ($i=0;$i < $target_cnt;$i++):?>
		<?php if($target_cnt > 1):?>
			<div class="form-group">
				商品<?PHP echo $i+1;?><br>
			</div>
		<?php endif;?>
		参加公演/席番号※選択必須
		<div class="form-group">
			<?php echo Form::select('select_ids[]', isset($select_ids[$i]) ? $select_ids[$i] : "",
				$form_data, array('class' => 'validate[required] form-control')); ?>
			<div>
				※商品1点に複数公演名プリント不可<br>
				※注文確定後変更不可
			</div>
		</div>
		<br>
	<?php endfor;?>

	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>
	<br>
	<div>
		<?php echo Html::anchor('/cart'.$session_get_param, '<font color="#FFFFFF">カートに戻る</font>'); ?>
	</div>

</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
