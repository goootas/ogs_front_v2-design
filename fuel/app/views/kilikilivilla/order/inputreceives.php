<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<!-- 購入商品情報 -->
<?php echo render('apbankfes2016/order/_cart_view'); ?>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">

	<?php $ses_name = Session::get("order_data.datareceives.receives");?>

	<div style="text-align: center;">
		<h4 class="headline">商品受取日時選択</h4>
		<select name="receives" class="validate[required]">
			<option value="">受取日時を選択</option>
			<?php foreach ($shop_receives as $receives): ?>
				<option value="<?php echo $receives["id"];?>" <?php if ($receives["stock"] <= 0){echo "disabled";}?>>
					<?php
					echo $receives["title1"].$receives["title2"].":";
					if ($receives["stock"] > 0 ){
						if ($receives["stock"] <= 10 ){
							echo "残りわずか";
						}else{
							echo "受付中";
						}
					}else{
						echo "規定数終了";
					}
					?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="description notice">
		■ご注文<br>
		・会場受取によるご注文は、お1人様1回のみとなります。<br>
		・クレジットカード決済のみご利用可能です。(<a href="http://vpc.lifecard.co.jp" style="text-decoration: underline"  target="_blank">Vプリカ</a>等プリペイド式カード利用可)<br>
		・各時間帯先着順定員制となります。定員数に達した時間帯は選択できません。<br>
		・注文確定後の受取日時変更はできません。<br>
		<br>
		■商品お受け取り<br>
		下記をご用意いただき、選択受取日時に「Reborn-Art Festival × ap bank fes 2016」開催会場内受取窓口で提示してください。<br>
		下記をお持ちでない場合、商品をお渡しすることはできません。忘れずに必ずお持ちください。<br>
		<br>
		①ご本人確認書類(運転免許証、健康保険証、学生証など)<br>
		②注文確定後に発行される受注番号下5桁(注文確認画面・注文確認メール・マイページ注文履歴で確認できます)<br>
		<br>
		・会場内受取窓口：グッズ販売窓口に併設(会場内にて告知予定)<br>
		・選択受取日時以外での商品受取はできません。必ず選択した時間内にお受け取りください。お受け取りにならなかった場合は、後日配送でのお届けとなりますので、予めご了承ください。(その場合は、別途送料を頂く場合がございます)<br>
		・会場でのお受け取りは、ご注文されたご本人様のみに限らせていただきます。<br>
		・会場受取の方は、グッズ販売列に並ぶ必要はありません。選択時間内に会場内受取窓口に直接お越し下さい。<br>
	</div>

	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>

</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("#order").validationEngine({promptPosition : "topLeft"});
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input1");
			$("#order").submit();
		});
	});
</script>
