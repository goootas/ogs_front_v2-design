<!--<div style="color: #FF0000;text-align: center;font-size: large">--><?php //echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error_center')); ?><!--</div>-->
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php if(!Session::get_flash('receives_duplicate')){?>
<div class="confirm-panel">
	<div class="panel-body">
		<div class="description">
			この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
		</div>
		<div class="buttons count-2">
			<?php echo Html::anchor('/order/input3', '前に戻る',array('class' => 'button rect size-L'),true); ?>
			<div class="button rect key size-L"><?php echo Form::button('final','注文する',array('id' => 'next','class' => '')); ?></div>
		</div>
		<div class="description">
			<br>
			<p style="color: #FF0000">「注文する」押下後のキャンセル、お支払い方法 / ご注文商品の変更は承ることができません。</p>
		</div>
	</div>
</div>
<?php }else{ ?>
	<div class="description notice" style="color: #FF0000;text-align: center;font-size: large ;margin-bottom: 50px;line-height: 1.5em;">
		会場受取によるご注文は、受付期間(1会場)ごとにお1人様1回のみとなっております。<br>
		配送受取でのご注文に変更いただくか、次回受付期間にご注文ください。
	</div>
<?php } ?>

<div class="cart-list">
	<h4 class="headline">カート</h4>
	<table>
		<?php $total_tax = 0; ?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="delete"></th>
			<th class="total">小計<br />&nbsp;</th>
		</tr>
		</thead>
		<tbody id="table-tbody">
		<?php foreach ($cart as $data): ?>
			<?php $total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);?>

			<tr class="list">
				<td>
					<?php
					$anchor_txt = "";
					$anchor_txt .= $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
					?>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td>
					<?php echo number_format($data["num"]);?>&nbsp;&nbsp;
				</td>
				<td></td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
			</tr>
		<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="totals">
		<ul>
			<li>
				<dl><dt>商品合計</dt><dd>¥<?php echo number_format(floor($total_tax));?></dd></dl>
			</li>
			<li>
				<dl><dt>送料</dt><dd>¥<?php echo number_format(intval($postage));?></dd></dl>
			</li>
			<li>
				<dl><dt>決済手数料</dt><dd>¥<?php echo number_format(intval($fee));?></dd></dl>
			</li>
			<li class="all">
				<dl><dt>総合計</dt><dd>¥<?php echo number_format(intval($total));?></dd></dl>
			</li>
		</ul>
	</div>

	<div class="buttons change">
		<?php echo Html::anchor('/cart/list/1','カート内容変更',array('class' => 'button rect key')) ?>
	</div>
	<?php if(Session::get("payment_ctl_cnt") < 3):?>
		<div class="description">
			<p style="color: #FF0000">
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			</p>
		</div>
	<?php endif; ?>

</div>

<?php if( $receives_count > 0) :?>
	<div class="fixed-data">
		<h4 class="headline">商品受取日時</h4>
		<?php foreach ($shop_receives as $receives): ?>
			<?php if( $receives["id"] == Session::get("order_data.datareceives.receives")) :?>
				<table>
					<tr>
						<td class="head">受取日時</td>
						<td class="body"><?php echo $receives["title1"].$receives["title2"];?></td>
					</tr>
				</table>
			<?php endif;?>
		<?php endforeach; ?>
		<div class="buttons change">
			<?php echo Html::anchor('/order/inputreceives/1','商品受取日時変更',array('class' => 'button rect key')) ?>
		</div>
	</div>
<?php endif;?>


<div class="fixed-data">
	<h4 class="headline">お客様情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_username_sei");?> <?php echo Session::get("order_data.data1.order_username_mei");?> 様</td>
		</tr>
		<tr>
			<td class="head">ふりがな</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_username_sei_kana");?> <?php echo Session::get("order_data.data1.order_username_mei_kana");?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr(Session::get("order_data.data1.order_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data1.order_zip"), 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_state");?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_address1");?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_address2");?></td>
		</tr>
		<tr>
			<td class="head">メールアドレス</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_email");?><br></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_tel1");?>-<?php echo Session::get("order_data.data1.order_tel2");?>-<?php echo Session::get("order_data.data1.order_tel3");?></td>
		</tr>
		<tr>
			<td class="head">性別</td>
			<td class="body">
				<?php if (Session::get("order_data.data1.order_sex")=="1"):?>男性
				<?php elseif (Session::get("order_data.data1.order_sex")=="2"):?>女性
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
		<tr>
			<td class="head">生年月日</td>
			<td class="body">
				<?php if (Session::get("order_data.data1.order_birthday")):?>
					<?php echo substr(Session::get("order_data.data1.order_birthday"), 0,4);?>年<?php echo substr(Session::get("order_data.data1.order_birthday"), 4,2);?>月<?php echo substr(Session::get("order_data.data1.order_birthday"), 6,2);?>日
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input1/1','お客様情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>


<div class="fixed-data">
	<h4 class="headline">お届け先情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_username_sei");?> <?php echo Session::get("order_data.data2.deliver_username_mei");?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr(Session::get("order_data.data2.deliver_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data2.deliver_zip"), 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_state");?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_address1");?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_address2");?></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_tel1");?>-<?php echo Session::get("order_data.data2.deliver_tel2");?>-<?php echo Session::get("order_data.data2.deliver_tel3");?></td>
		</tr>

		<?php if( $receives_count == 0) :?>
			<tr>
				<td class="head">お届け日</td>
				<td class="body">
					<?php
					if(isset($deliver_term_date)){
						echo $deliver_term_date->deliver_start_text;
					}else{
						?>
						<?php if(!Session::get("order_data.data2.delivery_date")) : ?>
							<?php if(date("Ymd") >= "20151228" && date("Ymd") <= "20160103"):?>
								1/5(火)以降順次発送
							<?php else : ?>
								受注日翌日から2日以内で発送（土日祝日除く / 発送からお届けは1〜3日）
							<?php endif; ?>
						<?php else : ?>
							<?php echo sprintf("%04d年%02d月%02d日",
								substr(Session::get("order_data.data2.delivery_date"),0,4),
								substr(Session::get("order_data.data2.delivery_date"),4,2),
								substr(Session::get("order_data.data2.delivery_date"),6,2));?>
						<?php endif; ?>
						<?php
					}
					?>
				</td>
			</tr>
			<tr>
				<td class="head">お届け希望時間帯</td>
				<td class="body">
					<?php
					if(isset($deliver_term_date)){
						echo "指定不可";
					}else{
						?>
						<?php if(!Session::get("order_data.data2.delivery_time")) : ?>
							指定無し
						<?php else : ?>
							<?php if(Session::get("order_data.data2.delivery_time") == "AM") : ?>
								<?php echo "午前中";?>
							<?php else : ?>
								<?php $dt = explode("-",Session::get("order_data.data2.delivery_time"));?>
								<?php echo sprintf("%d時〜%d時",$dt[0],$dt[1]);?>
							<?php endif; ?>
						<?php endif; ?>
					<?php }?>
				</td>
			</tr>
		<?php endif;?>
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input2/1','お届け先情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>

<div class="fixed-data">
	<h4 class="headline">お支払情報</h4>
	<table>
		<tr>
			<td class="head">支払方法</td>
			<td class="body">
				<?php echo Config::get("payment.".Session::get("order_data.data3.payment"));?>
				<?php if(Session::get("order_data.data3.payment") == 1) echo Config::get("jpo.".Session::get("order_data.data3.credit_jpo"));?>
				<?php if(Session::get("order_data.data3.payment") == 2) echo Config::get("cvs.".Session::get("order_data.data3.cvs"));?>
			</td>
		</tr>
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input3/1','お支払情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>

<?php if(!Session::get_flash('receives_duplicate')){?>
	<div class="confirm-panel">
		<div class="panel-body">
			<div class="description">
				この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
			</div>
			<div class="buttons count-2">
				<?php echo Html::anchor('/order/input3', '前に戻る',array('class' => 'button rect size-L'),true); ?>
				<div class="button rect key size-L"><?php echo Form::button('final','注文する',array('id' => 'next','class' => '')); ?></div>
			</div>
			<div class="description">
				<br>
				<p style="color: #FF0000">「注文する」押下後のキャンセル、お支払い方法 / ご注文商品の変更は承ることができません。</p>
			</div>
		</div>
	</div>
<?php } ?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("[id=next]").attr('disabled',true);
			$('#order_final').submit();
		});
	});
</script>

<?php echo Form::open(array('action' => '/order/exec', 'id' => "order_final" ,'method' => 'get'));?>
<?php echo Form::close();?>