<?php
class Model_Db_User_Delivery extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'shop_id',
        'user_id',
        'zip',
        'prefectures',
        'address1',
        'address2',
        'tel1',
        'tel2',
        'tel3',
        'status',
        'insert_date',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_user_delivery';

    public static function findListFront($where="",$sort="")
    {
        $data = static::find('all', array(
                'where'		=> $where,
                'order_by'	=> $sort,
            )
        );
        return $data;
    }

}
