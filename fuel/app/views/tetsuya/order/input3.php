<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<!-- 購入商品情報 -->
<?php echo render($shop_data["dir"].'/order/_cart_view'); ?>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="col-md-12">

	<h4 class="headline">お支払い方法</h4>
	<?php if(Session::get("payment_ctl_card")):?>
		<div class="payments">

			<div id="card" class="radio">
				<input type="radio" name="payment" value="1" class="validate[required]" id="collapse_card"<?php echo (Session::get("order_data.data3.payment") == 1) ? "checked" : "";?>>クレジットカード
			</div>
			<div class="forms">
				<div class="form-group">
					<div class="fixed-data">
						<table>
							<td class="head">お支払い総額</td>
							<td class="body">お支払総額：商品代金合計＋送料</td>
						</table>
					</div>
					<div class="discription">
						VISA / MasterCard / DINERS / JCB / AMEXのクレジットカードがご利用いただけます。
					</div>
				</div>
				<div class="form-group">
					<label>カード番号</label>
					<div class="form-container">
						<div class="inner">
							<?php echo Form::input('card_no', Session::get("order_data.data3.card_no") ? Session::get("order_data.data3.card_no") : "",
								array('class' => 'validate[required,custom[onlyNumberSp],minSize[14],maxSize[16]] form-control',"placeholder" => "半角数字","type" => "tel")); ?>
						</div>
						<div class="description">※「-（ハイフン）」は含まず入力してください</div>
					</div>
				</div>

				<div class="form-group">
					<label>セキュリティコード</label>
					<div class="form-container">
						<div class="inner secucord">
							<?php echo Form::input('secure_cd', Session::get("order_data.data3.secure_cd") ? Session::get("order_data.data3.secure_cd") : "",
								array('class' => 'validate[required,custom[onlyNumberSp],minSize[3],maxSize[4]] form-control',"placeholder" => "半角数字","type" => "tel")); ?>
						</div>
						<div class="description">
							<strong>セキュリティコードについて</strong>
							<br />
							VISA / MASTER / DINERS / JCBの場合<br />
							クレジットカード背面に記載されている3桁の番号 (メインのカード番号のすぐ右側)<br />
							<br />
							AMEXの場合<br />
							クレジットカード前面に記載されている4桁の番号 (メインのカード番号の右上)
						</div>
					</div>
				</div>

				<div class="form-group">
					<label>有効期限</label>
					<div class="form-container">
						<table class="select">
							<tr>
								<td>
									<select class="form-control" name="expire_m">
										<?php for ($i = 1; $i <= 12; $i++) {?>
											<!--                                        <option value="--><?php //echo date("m",strtotime($i." months" ));?><!--"-->
											<!--                                            --><?php //echo (Session::get("order_data.data3.expire_m")==date("m",strtotime($i." months" ))) ? " selected" : "";?>
											<!--                                            >--><?php //echo date("m",strtotime($i." months" ));?><!--月</option>-->
											<option value="<?php echo sprintf("%02d", $i);?>"
												<?php echo (Session::get("order_data.data3.expire_m")==sprintf("%02d", $i)) ? " selected" : "";?>
												><?php echo sprintf("%02d", $i);?>月</option>
										<?php }?>
									</select>
								</td>
								<td>
									<select class="form-control" name="expire_y">
										<?php for ($i = 0; $i <= 25; $i++) {?>
											<option value="<?php echo date("y",strtotime($i." year" ));?>"
												<?php echo (Session::get("order_data.data3.expire_y")==date("y",strtotime($i." year" ))) ? " selected" : "";?>
												><?php echo date("y",strtotime($i." year" ));?>年</option>
										<?php }?>
									</select>
								</td>
							</tr>
						</table>
						<div class="description">例）カード表記が「04/2018」の場合は、「04月18年」と選択してください。</div>
					</div>
				</div>

				<div class="form-group">
					<label>お支払方法</label>
					<div class="form-container">
						<table class="select">
							<tr>
								<td>
									<?php echo Form::select('credit_jpo', Session::get("order_data.data3.credit_jpo") ? Session::get("order_data.data3.credit_jpo") : "",
										Config::get("jpo"),array('class' => 'form-control col-md-5'));?>
								</td>
							</tr>
						</table>
						<div class="description">＊DINERS：「一括払い」/「リボ払い」のみ対応<br />＊AMEX：「リボ払い」非対応 </div>
					</div>
				</div>
			</div>

		</div>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_ato")): ?>
		<div id="dfp" class="payments">
			<div class="radio">
				<input type="radio" name="payment" value="4" class="validate[required]" id="collapse_ato"<?php echo (Session::get("order_data.data3.payment") == 4) ? "checked" : "";?>>後払い（コンビニ/郵便振替/銀行振込）
			</div>
			<div class="forms">
				<div class="form-group">
					<div class="fixed-data">
						<table>
							<tr>
								<td class="head">お支払い総額</td>
								<td class="body">商品代金合計+送料＋決済手数料</td>
							</tr>
							<tr>
								<td class="head">決済手数料(税込)</td>
								<td class="body">¥172(1回のご注文につき)</td>
							</tr>
							<tr>
								<td class="head">お支払い方法</td>
								<td class="body">請求書は商品に同封されております。<br />請求書記載事項に従って、請求書到着後14日以内にお支払いください。</td>
							</tr>
						</table>
					</div>
					<div class="description">

						<a style="border: 1px solid #a0a0a0; display: block; margin: 10px 0; width: 187px;" href="http://www.ato-barai.com/r_annai.html" target="_blank"><img src="http://www.ato-barai.com/images/annai/kessai_banner_003.gif" alt="後払い.com【後払いドットコム】コンビニ・銀行・郵便局で後払い決済" width="187" height="40" border="0"></a>
						※当サービスは(株)キャッチボールの運営する「後払い.com(ドットコム)」により提供されます。 下記注意事項を確認、同意の上、ご利用下さい。<br />
						※銀行・郵便局・コンビニでお支払いいただけます。<br />
						※請求書は商品に同封されておりますので、請求書到着後14日以内にお支払いください。<br />
						※支払期限を過ぎた場合、再度の請求毎に300円の再請求書発行手数料がかかりますのでご注意下さい。<br />
						※ご本人様確認や後払い.comサービス利用にあたって、お電話・メールにてご連絡させていただく場合がございます。<br />
						※お客様が当サイトにおいて登録された個人情報および発注内容は、後払い.comのサービスに必要な範囲のみで(株)キャッチボールに提供させていただきます。<br />
						※与信結果によっては当サービスをご利用いただけない場合がございます。その場合は、他の決済方法にご変更いただくことになります。<br />
						※商品の配送先を配送業者の営業所止め(営業所来店引取り)また転送依頼することはできません。<br />
						※サービスをご利用いただいた場合は、上記注意事項にご同意いただいたものとみなさせていただきます。<br /><br />

					</div>

				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_cvs")):?>
		<div id="cvs" class="payments">
			<div class="radio">
				<input type="radio" name="payment" value="2" class="validate[required]" id="collapse_cvs"
					<?php echo (Session::get("order_data.data3.payment") == 2) ? "checked" : "";?>>コンビニ決済（前払い）
			</div>
			<div class="forms">
				<div class="form-group">
					<div class="fixed-data">
						<table>
							<tr>
								<td class="head">お支払い総額</td>
								<td class="body">商品代金＋送料＋決済手数料</td>
							</tr>
							<tr>
								<td class="head">【決済手数料】（tax in）</td>
								<td class="body">¥9,999以下：¥216<br>
									¥29,999以下：¥270<br>
									¥99,999以下：¥594<br>
									¥299,999以下：¥648<br>
									※¥300,000以上の決済不可
								</td>
							</tr>
						</table>
					</div>
					<div class="discription">
						※ご注文後にメールでお支払い方法(受付番号等)をお送り致します。ご入金が確認でき次第、商品を発送致します。<br />
						※5日以内にご入金のない場合、ご注文はキャンセルとさせていただきます。
					</div>
				</div>
				<div class="form-group">
					<label>お支払い先コンビニ</label>
					<div class="form-container">
						<table class="select">
							<tr>
								<td>
									<?php echo Form::select('cvs', '',Config::get("cvs"),
										array('class' => 'form-control col-md-5',));?>
								</td>
							</tr>
						</table>
						<div class="description" style="margin-top:-5px;">コンビニ支払期限：<?php echo date("Y/m/d",strtotime("1 week"));?> 23:59 </div>
					</div>
				</div>



			</div>
		</div>
	<?php endif; ?>
	<?php if(Session::get("payment_ctl_cod")):?>
		<div id="cod" class="payments">
			<div class="radio">
				<input type="radio" name="payment" value="3" class="validate[required]" id="collapse_cod"<?php echo (Session::get("order_data.data3.payment") == 3) ? "checked" : "";?>>代金引換
			</div>
			<div class="forms">
				<div class="form-group">
					<div class="fixed-data">
						<table>
							<tr>
								<td class="head">お支払い総額</td>
								<td class="body">商品代金＋送料＋決済手数料</td>
							</tr>
							<tr>
								<td class="head">配送業者</td>
								<td class="body">佐川急便</td>
							</tr>
							<tr>
								<td class="head">【代引き手数料】（tax in）</td>
								<td class="body">¥9,999以下：¥324<br>
									¥29,999以下：¥432<br>
									¥99,999以下：¥648<br>
									¥299,999以下：¥1,080<br>
									¥499,999以下：¥2,160
								</td>
							</tr>
						</table>
					</div>
					<div class="description">
						※商品配送時に配送員に「現金」でお支払い下さい。
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="buttons count-2">
		<?php echo Html::anchor('/order/input2', '前に戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else :?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button key rect size-L'),true); ?>
		<?php endif;?>

	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("#order").validationEngine({promptPosition : "topLeft"});
		$('.payments').find('.forms').hide();
		<?php echo (Session::get("order_data.data3.payment") == 1) ? '$("#card").children(".forms").show(); $("#card").find("input").prop("checked", "checked");' : '';?>
		<?php echo (Session::get("order_data.data3.payment") == 2) ? '$("#cvs").children(".forms").show(); $("#cvs").find("input").prop("checked", "checked");' : '';?>
		<?php echo (Session::get("order_data.data3.payment") == 3) ? '$("#cod").children(".forms").show(); $("#cod").find("input").prop("checked", "checked");' : '';?>
		<?php echo (Session::get("order_data.data3.payment") == 4) ? '$("#dfp").children(".forms").show(); $("#dfp").find("input").prop("checked", "checked");' : '';?>
		$('.payments .radio').click(function() {
			if (!$(this).hasClass("active")){
				$('.payments .radio').removeClass("active");
				$('.payments').find('.forms').slideUp();
				$('.payments').find('input').prop("checked", "false");
				$(this).next('.forms').slideDown();
				$(this).find('input').prop("checked", "checked");
				$(this).addClass("active");

				$("html,body").animate({
					scrollTop : 300
				}, {
					queue : false
				});

			}else{
				$('.payments .radio').removeClass("active");
				$('.payments').find('.forms').slideUp();
			}
		});

		$( "#datepicker" ).datepicker();
		// 次へ
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/confirm");
			$("#order").submit();
		});
	});
</script>


