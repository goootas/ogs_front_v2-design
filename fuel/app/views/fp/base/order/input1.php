<?php if(!isset($user_data)) : ?>
	<!-- ログイン -->
	<?php echo Form::open(array('action' => '/login'));?>
	<div>
		会員の方は、こちらからログインすると<br>お客様情報が自動入力されます。<br>
		メールアドレス
		<div><?php echo Form::input('email', Input::post('email', ''),array( "istyle"=>"3","format"=>"*x","MODE"=>"alphabet")); ?></div>
		パスワード
		<div><?php echo Form::password('password', Input::post('password', '')); ?></div>
		<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
		<?php echo Form::submit('exec', 'ログイン'); ?><br><br>
<!--		<font color="red">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</font><br>-->
		<?php echo Html::anchor('forget'.$session_get_param, '<font color="#000000">パスワードを忘れた方はこちら</font>',array('id' => 'next')); ?><br>
	</div>
	<?php echo Form::close();?>
	<br>
<?php endif;?>

<?php echo Form::open("/order/input2");?>
<div>
	<h4>お客様情報</h4>
	<hr>
	<?php if(isset($user_data)) : ?>
		<div>会員ログイン済みです</div>
	<?php endif;?>

	<?php if(isset($user_data)):?>
		入力されているお客様情報確認後、変更がある場合は入力してください。<br><br>
	<?php else : ?>
		初めてお買い物をされるお客様、会員登録をされていないお客様は、こちらからお客様情報を入力してください。<br><br>
	<?php endif ;?>


	お名前※必須
	<div class="form-group">
		<?php echo Form::input('order_username_sei', Session::get("order_data.data1.order_username_sei") != "" ? Session::get("order_data.data1.order_username_sei") : (isset($user_data) ? $user_data->username_sei : ""),
			array("size"=>"10")); ?>
		<?php echo Form::input('order_username_mei', Session::get("order_data.data1.order_username_mei") != "" ? Session::get("order_data.data1.order_username_mei") : (isset($user_data) ? $user_data->username_mei : ""),
			array("size"=>"10")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
	<br>
	ふりがな※必須
	<div>
		<?php echo Form::input('order_username_sei_kana', Session::get("order_data.data1.order_username_sei_kana") ? Session::get("order_data.data1.order_username_sei_kana") : (isset($user_data) ? $user_data->username_sei_kana : ""),
			array("size"=>"10")); ?>
		<?php echo Form::input('order_username_mei_kana', Session::get("order_data.data1.order_username_mei_kana") ? Session::get("order_data.data1.order_username_mei_kana") : (isset($user_data) ? $user_data->username_mei_kana : ""),
			array("size"=>"10")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_kana')); ?></font>
	<br>

	メールアドレス※必須
	<div>
		<?php echo Form::input('order_email',Session::get("order_data.data1.order_email") ? Session::get("order_data.data1.order_email") : (isset($user_data) ? $user_data->email : ""),array( "istyle"=>"3","format"=>"*x","MODE"=>"alphabet")); ?><br>
		携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く､予約確認メールが届かない場合があります｡(ドメイン指定受信設定 / URL付きメール受信不可 / PCからのメール受信不可)
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_email1')); ?></font>
	<br>

	メールアドレス確認※必須
	<div>
		<?php echo Form::input('order_email2',Session::get("order_data.data1.order_email2") ? Session::get("order_data.data1.order_email2") : (isset($user_data) ? $user_data->email : ""),array( "istyle"=>"3","format"=>"*x","MODE"=>"alphabet")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_email2')); ?></font>
	<br>

	郵便番号※必須
	<div>
		<?php echo Form::input('order_zip',Session::get("order_data.data1.order_zip") ? Session::get("order_data.data1.order_zip") : (isset($user_data) ? $user_data->zip : ""),
			array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		ハイフン(-)は抜いてください
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_zip')); ?></font>
	<br>
	都道府県※必須
	<div>
		<?php echo Form::select('order_state', Session::get("order_data.data1.order_state") ? Session::get("order_data.data1.order_state") :
			(isset($user_data) ? $user_data->prefecture : ""),$prefecture_data);?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_state')); ?></font>
	<br>
	住所１※必須
	<div>
		<?php echo Form::input('order_address1', Session::get("order_data.data1.order_address1") ? Session::get("order_data.data1.order_address1") :
			(isset($user_data) ? mb_convert_kana($user_data->address1, "ASV") : "")); ?><br>
		市区町村
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_addr1')); ?></font>

	<br>
	住所２※必須
	<div>
		<?php echo Form::input('order_address2',  Session::get("order_data.data1.order_address2") ? Session::get("order_data.data1.order_address2") :
			(isset($user_data) ? mb_convert_kana($user_data->address2, "ASV") : "")); ?><br>
		番地 / 建物名など
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_addr2')); ?></font>

	<br>
	電話番号※必須
	<div>
		<?php echo Form::input('order_tel1', Session::get("order_data.data1.order_tel1") ? Session::get("order_data.data1.order_tel1") : (isset($user_data) ? $user_data->tel1 : ""),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('order_tel2', Session::get("order_data.data1.order_tel2") ? Session::get("order_data.data1.order_tel2") : (isset($user_data) ? $user_data->tel2 : ""),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('order_tel3', Session::get("order_data.data1.order_tel3") ? Session::get("order_data.data1.order_tel3") : (isset($user_data) ? $user_data->tel3 : ""),
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_tel')); ?></font>
	<br>

	性別
	<div>
		<?php echo Form::radio('order_sex', '1',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
			(isset($user_data) ? $user_data->sex : "")); ?>男性
		<?php echo Form::radio('order_sex', '2',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
			(isset($user_data) ? $user_data->sex : "")); ?>女性
	</div>
	<br>

	生年月日
	<div>
		<?php echo Form::input('order_birthday', Session::get("order_data.data1.order_birthday") ? Session::get("order_data.data1.order_birthday") :
			(isset($user_data) ? $user_data->birthday : ""),
			array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>


	<?php if (!isset($user_data) || (isset($user_data) && !$user_data->mailmagazine)) :?>
		<br>
		お知らせメール
		<div>
			<?php echo Form::radio('mailmagazine_flg', "1",Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") : "1"); ?>受信する
			<?php echo Form::radio('mailmagazine_flg', "0",Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") : ""); ?>受信しない<br>
			メールにて新商品のお知らせなどをさせていただく場合があります
		</div>
	<?php endif;?>
	<?php if (!isset($user_data)) :?>
		<br>
		<div>
			会員登録されますと、次回ご利用になる際に、住所などの情報が自動的に入力されます。
		</div>
		<br>
		会員登録
		<div>
			<?php echo Form::radio('regist_flg', "1",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : "1"); ?>登録する
			<?php echo Form::radio('regist_flg', "0",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : ""); ?>登録しない
		</div>
		<font color="red">通販サイトリニューアルに伴い、2015年6月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</font><br>

		<br>
		パスワード
		<div>
			<?php echo Form::password('order_password', Session::get("order_data.data1.order_password") ? Session::get("order_data.data1.order_password") : ""); ?>
		</div>
		<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_pass')); ?></font>
	<?php endif;?>

	<br>
	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>
	<br>
	<div>
		<?php echo Html::anchor('/cart'.$session_get_param, '<font color="#000000">カートに戻る</font>'); ?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
