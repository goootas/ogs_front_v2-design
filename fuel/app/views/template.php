<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Official Goods Store</title>
	<style type="text/css">

		body {
			background-color: #000000;
			color: #FFFFFF;
		}
		a{
			color:#FFFFFF;
		}
	</style>
</head>
<body>

<div align="center">
	<img src="ogs-logo.jpg" >
	<h2><a href="/base"><font color="white">TOKYO SKA PARADISE ORCHESTRA</font></a></h2>
	<a href="/base/info/guide">ショッピングガイド</a>|<a href="/base/info/privacy">プライバシーポリシー</a>|<a href="/base/info/legal">特定商取引法に基づく表記</a>
</div>

</body>
</html>