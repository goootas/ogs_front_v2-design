<?php
/*
 * CMS
 */
class Controller_Page extends Controller_Basefront
{
	/*
	 * ページ表示
	 */
	public function action_index($pid="")
	{
		is_null($pid) and Response::redirect('https://'.Input::server('SERVER_NAME').$this->session_get_param);

		$target_pre		= Input::get("pre","");
		$target_date	= Input::get("date",date("YmdHis"));
		$target_device	= Input::get("device","");

		// ページ情報取得
		$page_data = Model_Db_Cms_Page::find($pid);

		if($page_data->shop_id != $this->shop_data["id"]){
			Response::redirect();
		}

		// 入力チェック
		if($target_pre != "" && mb_strlen($target_date) < 8 ){
			Response::redirect('https://'.Input::server('SERVER_NAME').$this->session_get_param);
			exit;
		}

		// 右0埋め
		$target_date = str_pad($target_date, 14, 0, STR_PAD_RIGHT);

		// 掲載期間チェック
		if($page_data->view_date_from >= date("Y-m-d H:i:s",strtotime($target_date)) &&
			$page_data->view_date_to <= date("Y-m-d H:i:s",strtotime($target_date))){
			Response::redirect('https://'.Input::server('SERVER_NAME').$this->session_get_param);
			exit;
		}

		// 状態チェック
		if($target_pre == "" && $page_data->status < 1) {
			Response::redirect('https://'.Input::server('SERVER_NAME').$this->session_get_param);
			exit;
		}

		// セル情報取得
		$wheres = array();
		$wheres["page_id"] = $pid;
		$wheres["device_type"] = 1;
		if($this->agent_dir == "fp"){
			$wheres["device_type"] = 2;
		}

		if($target_device){
			$wheres["device_type"] = $target_device;
		}

		$wheres["view_date"] = date("Y-m-d H:i:s",strtotime($target_date));
		$cms_data = Model_Db_Cms_Cell::findList($wheres);
		$body = "";
		foreach ($cms_data as $key => $val) {
			$body = $body . $val["body"];
		}

		// カスタムタグ置換
		// 該当タグを検知し、素材テーブルよりURLを取得
		$pattern= '/%%(.*?)%%/s';
		preg_match_all($pattern, $body , $match);

		// 素材取得
		foreach ($match[1] as $key => $val) {
			list($type , $obj_id) = explode("_",$val);
			$object_data = Model_Db_Object::find((int)$obj_id);
			if($object_data){
				$body = str_replace($match[0][$key], $object_data->image_url, $body);
			}else{
				$body = str_replace($match[0][$key], "", $body);
			}
		}

		$this->template->set_global('cms', $body, false);

		$this->template->title = $page_data->title;
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/page/index');
	}

}
