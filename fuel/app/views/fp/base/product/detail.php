<div>
	<h3><?php echo isset($product) ? $product->title : '';?></h3>
</div>
<?php if(isset($images) && count($images) > 0): ?>
	<?php foreach ($images as $image): ?>
		<center><?php echo '<img src="/'.$shop_data["dir"].'/fpimg?img=http://'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$image."?".time().'" width="100%">';?></center>
	<?php endforeach; ?>
<?php endif; ?>

<div>
	<!-- 商品基本 -->
	商品コード:<br><?php echo isset($product) ? $product->code : '';?><br /><br />
	価格:<br><?php echo isset($product) ? number_format($product->price_sale * $tax).'円(tax in)' : '';?>
	<br>
	<br>
	<?php echo Form::open(array('action' => '/cart/add'));?>
	<?php if(isset($product)): ?>
		<?php foreach ($product->detail as $detail): ?>
			<?php
			if ($detail->stock == 0 ){
				echo "在庫切れ";
			}else{
//				if ($product->publish_flg){?>
				<input type="radio" name="did" value="<?php echo $detail->id;?>">
			<?php
//				}
			}?>
			<?php echo $detail->option1_name ." ".$detail->option2_name; ?><br>
		<?php endforeach; ?>
	<?php endif; ?>
	<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
		<br>
		<?php echo Form::submit('buy', 'カートに入れる'); ?>
		<?php echo Form::hidden('num', '1'); ?>
		<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

	<?php echo Form::close();?>
	<br>
	<?php echo isset($product) ? nl2br($product->comment) : '';?>

</div>
