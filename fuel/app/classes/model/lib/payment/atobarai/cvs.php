<?php
class Model_Lib_Payment_Atobarai_Cvs extends \Model
{
	/*
	 * 与信
	 */
	public static function authorize_exec($shop,$order)
	{
		$url = "https://www.atobarai.jp/api/order/rest";

		$curl = curl_init($url);
		$params = array();
		// 基本情報
		$params["O_ReceiptOrderDate"]	= date("Y-m-d");
		$params["O_EnterpriseId"]		= isset($shop["atobarai_enterprise_id"]) ? $shop["atobarai_enterprise_id"] : "7339";
		$params["O_SiteId"]				= isset($shop["atobarai_site_id"]) ? $shop["atobarai_site_id"] : "9014";
		$params["O_ApiUserId"]			= isset($shop["atobarai_api_user_id"]) ? $shop["atobarai_api_user_id"] : "100";
		$params["O_Ent_OrderId"]		= $order["order_id"];
		$params["O_Ent_Note"]			= $shop["name"];
		$params["O_UseAmount"]			= intval($order["total"]);
		$params["O_RtOrderStatus"]		= 1;

		// 請求先情報※注文者情報
		$params["C_PostalCode"]			= $order["order_zip"];
		$params["C_UnitingAddress"]		= $order["order_state"].$order["order_address1"].$order["order_address2"];
		$params["C_NameKj"]				= $order["order_username_sei"]." ".$order["order_username_mei"];
		$params["C_NameKn"]				= $order["order_username_sei_kana"]." ".$order["order_username_mei_kana"];
		$params["C_Phone"]				= $order["order_tel1"].$order["order_tel2"].$order["order_tel3"];
		$params["C_MailAddress"]		= $order["order_email"];
		$params["C_Occupation"]			= "";//職業
//		$params["C_SeparateShipment"]	= "0";//同梱請求書の加盟店で請求書を別送依頼する場合は1、以外は0、または未設定

		// 配送先情報
		// 住所及び氏名が異なる場合は別送扱いのパラメータを設定
		if(
			$order["order_zip"] != $order["deliver_zip"] ||
			$order["order_state"] != $order["deliver_state"] ||
			$order["order_address1"] != $order["deliver_address1"] ||
			$order["order_address2"] != $order["deliver_address2"] ||
			$order["order_username_sei"] != $order["deliver_username_sei"] ||
			$order["order_username_mei"] != $order["deliver_username_mei"]
		){
			$params["O_AnotherDeliFlg"]		= "1";
			$params["D_PostalCode"]			= $order["deliver_zip"];
			$params["D_UnitingAddress"]		= $order["deliver_state"].$order["deliver_address1"].$order["deliver_address2"];
			$params["D_DestNameKj"]			= $order["deliver_username_sei"].$order["deliver_username_mei"];
			$params["D_DestNameKn"]			= "";//フリガナ
			$params["D_Phone"]				= $order["deliver_tel1"].$order["deliver_tel2"].$order["deliver_tel3"];
		}

		// 商品情報
		$counter = 1 ;
		foreach ($order["products"] as $value) {
			$params["I_ItemNameKj_".$counter]	= $value["data"]->base->title. " " .$value["data"]->option1_name." ".$value["data"]->option2_name;
			$params["I_UnitPrice_".$counter]	= intval($value["price"]/$value["num"]);
			$params["I_ItemNum_".$counter]		= $value["num"];
			$counter++;
		}

		$params["I_ItemCarriage"]		= $order["postage"];
		$params["I_ItemCharge"]			= $order["fee"];

		$options = array(
//			CURLOPT_HEADER => true,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => http_build_query($params),
		);
		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);
		$info   = curl_getinfo($curl);

		$result = strstr($result, "<");

		// XMLパース
		$xml = simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);//LIBXML_NOCDATA LIBXML_NOWARNING

		//$response_status	= (string)$xml->status;
		//$order_id_ato		= (string)$xml->orderId;
		//$ordre_id			= (string)$xml->systemOrderId;
		//$yoshin_status		= (string)$xml->orderStatus->attributes()->cd;

		$response_status	= (string)$xml->status;
		$order_id			= (string)$xml->systemOrderId;
		$ordre_id_sys		= (string)$xml->orderId;
		$yoshin_status		= (string)$xml->orderStatus->attributes()->cd;

		$status = "error";
		if($response_status == "success" && $yoshin_status != 2 ){
			$status = "success";
		}

		$response = array();
		$response["res_data"] = $result;
		$response["req_data"] = $params;

		/**
		 * 取引ID取得
		 */
		$response["order_id"] = $order_id;
		/**
		 * 取引ID取得
		 */
		$response["order_id_sys"] = $ordre_id_sys;
		/**
		 * 結果コード取得
		 */
		//$response["status"] = $status;
		$response["status"] = $yoshin_status;
		/**
		 * 詳細コード取得
		 */
		//$response["result_code"] = $yoshin_status;

		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r("ATOBATAI @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r($response,true));
		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));

		return $response;

	}
	/*
	 * 与信手動用
	 */
	public static function authorize_exec2($shop,$order)
	{
		$url = "https://www.atobarai.jp/api/order/rest";

		$curl = curl_init($url);
		$params = array();
		// 基本情報
		$params["O_ReceiptOrderDate"]	= date("Y-m-d");
		$params["O_EnterpriseId"]		= isset($shop["atobarai_enterprise_id"]) ? $shop["atobarai_enterprise_id"] : "7339";
		$params["O_SiteId"]				= isset($shop["atobarai_site_id"]) ? $shop["atobarai_site_id"] : "9014";
		$params["O_ApiUserId"]			= isset($shop["atobarai_api_user_id"]) ? $shop["atobarai_api_user_id"] : "100";
		$params["O_Ent_OrderId"]		= $order["order_id"];
		$params["O_Ent_Note"]			= $shop["name"];
		$params["O_UseAmount"]			= intval($order["total"]);
		$params["O_RtOrderStatus"]		= 1;

		// 請求先情報※注文者情報
		$params["C_PostalCode"]			= $order["order_zip"];
		$params["C_UnitingAddress"]		= $order["order_prefecture"].$order["order_address1"].$order["order_address2"];
		$params["C_NameKj"]				= $order["order_username_sei"]." ".$order["order_username_mei"];
		$params["C_NameKn"]				= $order["order_username_sei_kana"]." ".$order["order_username_mei_kana"];
		$params["C_Phone"]				= $order["order_tel1"].$order["order_tel2"].$order["order_tel3"];
		$params["C_MailAddress"]		= $order["order_email"];
		$params["C_Occupation"]			= "";//職業
//		$params["C_SeparateShipment"]	= "0";//同梱請求書の加盟店で請求書を別送依頼する場合は1、以外は0、または未設定

		// 配送先情報
		// 住所及び氏名が異なる場合は別送扱いのパラメータを設定
		if(
			$order["order_zip"] != $order["deliver_zip"] ||
			$order["order_prefecture"] != $order["deliver_prefecture"] ||
			$order["order_address1"] != $order["deliver_address1"] ||
			$order["order_address2"] != $order["deliver_address2"] ||
			$order["order_username_sei"] != $order["deliver_username_sei"] ||
			$order["order_username_mei"] != $order["deliver_username_mei"]
		){
			$params["O_AnotherDeliFlg"]		= "1";
			$params["D_PostalCode"]			= $order["deliver_zip"];
			$params["D_UnitingAddress"]		= $order["deliver_prefecture"].$order["deliver_address1"].$order["deliver_address2"];
			$params["D_DestNameKj"]			= $order["deliver_username_sei"].$order["deliver_username_mei"];
			$params["D_DestNameKn"]			= "";//フリガナ
			$params["D_Phone"]				= $order["deliver_tel1"].$order["deliver_tel2"].$order["deliver_tel3"];
		}

		// 商品情報
		$counter = 1 ;
		foreach ($order["products"] as $value) {
			$params["I_ItemNameKj_".$counter]	= $value["data"]->base->title. " " .$value["data"]->option1_name." ".$value["data"]->option2_name;
			$params["I_UnitPrice_".$counter]	= intval($value["price"]/$value["num"]);
			$params["I_ItemNum_".$counter]		= $value["num"];
			$counter++;
		}

		$params["I_ItemCarriage"]		= $order["postage"];
		$params["I_ItemCharge"]			= $order["fee"];

		$options = array(
//			CURLOPT_HEADER => true,
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => http_build_query($params),
		);
		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);
		$info   = curl_getinfo($curl);

		$result = strstr($result, "<");

		// XMLパース
		$xml = simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);//LIBXML_NOCDATA LIBXML_NOWARNING

		//$response_status	= (string)$xml->status;
		//$order_id_ato		= (string)$xml->orderId;
		//$ordre_id			= (string)$xml->systemOrderId;
		//$yoshin_status		= (string)$xml->orderStatus->attributes()->cd;

		$response_status	= (string)$xml->status;
		$order_id			= (string)$xml->systemOrderId;
		$ordre_id_sys		= (string)$xml->orderId;
		$yoshin_status		= (string)$xml->orderStatus->attributes()->cd;

		$status = "error";
		if($response_status == "success" && $yoshin_status != 2 ){
			$status = "success";
		}

		$response = array();
		$response["res_data"] = $result;
		$response["req_data"] = $params;

		/**
		 * 取引ID取得
		 */
		$response["order_id"] = $order_id;
		/**
		 * 取引ID取得
		 */
		$response["order_id_sys"] = $ordre_id_sys;
		/**
		 * 結果コード取得
		 */
		//$response["status"] = $status;
		$response["status"] = $yoshin_status;
		/**
		 * 詳細コード取得
		 */
		//$response["result_code"] = $yoshin_status;

		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r("ATOBATAI @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));
		Log::debug(print_r($response,true));
		Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ",true));

		return $response;

	}
}

