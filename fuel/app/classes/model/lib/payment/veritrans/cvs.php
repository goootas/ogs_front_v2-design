<?php
require_once APPPATH.'vendor/tgMdkPHP/tgMdk/3GPSMDK.php';

class Model_Lib_Payment_Veritrans_Cvs extends \Model
{
    /*
     * 決済
     */
    public static function authorize_exec(
            $order_id,$payment_amount,$cvs_type,$name1,$name2,$tel,$limit_date,$veritrans_conf="")
    {
        /**
        * 要求電文パラメータ値の指定
        */
        $request_data = new CvsAuthorizeRequestDto();

        $request_data->setServiceOptionType(trim(substr($cvs_type, 0, -1)));
//        $request_data->setServiceOptionType("econ");
        $request_data->setOrderId($order_id);
        $request_data->setAmount(trim($payment_amount));
        $request_data->setName1(trim($name1));
        $request_data->setName2(trim($name2));
        $request_data->setTelNo(trim($tel));
        $request_data->setPayLimit(trim($limit_date));
        $request_data->setPaymentType("0");

       /**
        * 実施
        */
        $transaction = new TGMDK_Transaction();
        $response_data = $transaction->execute($request_data,$veritrans_conf);

        $response = array();
        $response["res_data"] = $response_data->__toString();
        $response["req_data"] = $request_data->__toString();

        //予期しない例外
        if (!isset($response_data)) {
            return $response;
        //想定応答の取得
        } else {

            /**
             * 取引ID取得
             */
            $response["order_id"] = $response_data->getOrderId();
            /**
             * 受付番号
             */
            $response["receipt_no"] = $response_data->getReceiptNo();
            /**
             * 払込票URL
             */
            $response["haraikomi_url"] = $response_data->getHaraikomiUrl();
            /**
             * 結果コード取得
             */
            $response["status"] = $response_data->getMStatus();
            /**
             * 詳細コード取得
             */
            $response["result_code"] = $response_data->getVResultCode();
            /**
             * エラーメッセージ取得
             */
            $response["error_msg"] = $response_data->getMerrMsg();

            return $response;

        }
    }
    public static function cancel_exec($order_id,$cvs_type,$veritrans_conf="")
    {// キャンセル処理
        /**
         * 要求電文パラメータ値の指定
         */
        $request_data = new CvsCancelRequestDto();

        $request_data->setServiceOptionType(trim(substr($cvs_type, 0, -1)));
        $request_data->setOrderId($order_id);

        /**
         * 実施
         */
        $transaction = new TGMDK_Transaction();
        $response_data = $transaction->execute($request_data,$veritrans_conf);

        $response = array();
        $response["res_data"] = $response_data->__toString();
        $response["req_data"] = $request_data->__toString();

        //予期しない例外
        if (!isset($response_data)) {
            return $response;
            //想定応答の取得
        } else {

            /**
             * 取引ID取得
             */
            $response["order_id"] = $response_data->getOrderId();
            /**
             * 結果コード取得
             */
            $response["status"] = $response_data->getMStatus();
            /**
             * 詳細コード取得
             */
            $response["result_code"] = $response_data->getVResultCode();
            /**
             * エラーメッセージ取得
             */
            $response["error_msg"] = $response_data->getMerrMsg();

            return $response;

        }

    }

}
