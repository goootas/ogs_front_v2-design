<?php
class Model_Db_Receives_Order extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'order_id',
		'timetable_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_local_receives_order';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findMypageOrderList($order_id)
	{
		$sql = "";
		$sql .= "SELECT ";
		$sql .= " tbl_local_receives_timetable.title1, ";
		$sql .= " tbl_local_receives_timetable.title2 ";
		$sql .= "from ";
		$sql .= " tbl_local_receives_order left join tbl_local_receives_timetable on tbl_local_receives_order.timetable_id = tbl_local_receives_timetable.id ";
		$sql .= "where 1 = 1 ";
		$sql .= "AND tbl_local_receives_order.order_id = '".$order_id."'";
		$query = DB::query($sql);
		$data = $query->execute()->current();
//		$data = $query->execute()->as_array()->();
		return $data;

	}
	//注文履歴を取得
	public static function findDetailFront($where="")
	{
		$data = static::find('first', array(
				'where' => $where,
			)
		);
		return $data;
	}
}
