<?php
class Model_Db_Print extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'club_no',
		'name_sei',
		'name_mei',
		'tel',
		'param1',
		'param2',
		'status',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_print';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

//	public static function findDetail($shop_id ,$club_no, $name_sei ,$name_mei , $tel )
	public static function findDetail($shop_id ,$club_no, $tel )
	{
		$sql = "";
		$sql .= "SELECT * FROM " . self::$_table_name . " where 1 = 1 ";
		$sql .= " AND shop_id = :shop_id ";
		$sql .= " AND club_no = :club_no ";
//		$sql .= " AND name_sei = :name_sei ";
//		$sql .= " AND name_mei = :name_mei ";
		$sql .= " AND tel = :tel ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'shop_id'	=> $shop_id,
				'club_no'	=> $club_no,
//				'name_sei'	=> $name_sei,
//				'name_mei'	=> $name_mei,
				'tel'	=> $tel,
			)
		);
		$data = $query->execute()->as_array();
		return $data;
	}
	public static function findDetail2($shop_id ,$club_no)
	{
		$sql = "";
		$sql .= "SELECT * FROM " . self::$_table_name . " where 1 = 1 ";
		$sql .= " AND shop_id = :shop_id ";
		$sql .= " AND club_no = :club_no ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'shop_id'	=> $shop_id,
				'club_no'	=> $club_no,
			)
		);
		$data = $query->execute()->as_array();
		return $data;
	}
}

