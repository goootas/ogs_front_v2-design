<?php // TODO: JQueryを使わないで処理する方法に変更　?>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<p style="color: #000000"><?php echo implode('</p><p style="color: #000000">', (array) Session::get_flash('success')); ?></p>

<div>
	<?php echo Html::anchor('/mypage/profile'.$session_get_param,'<font color="#000000">登録情報変更</font>') ?><br>
	<br>
	お名前<br>
	<?php echo $user->username_sei.$user->username_mei; ?><br>
	<br>
	ふりがな<br>
	<?php echo $user->username_sei_kana.$user->username_mei_kana; ?><br>
	<br>
	郵便番号<br>
	<?php echo $user->zip; ?><br>
	<br>
	都道府県<br>
	<?php echo $user->prefecture; ?><br>
	<br>
	住所１<br>
	<?php echo $user->address1; ?><br>
	<br>
	住所２<br>
	<?php echo $user->address2; ?><br>
	<br>
	メールアドレス<br>
	<?php echo $user->email; ?><br>
	<br>
	電話番号<br>
	<?php echo $user->tel1."-".$user->tel2."-".$user->tel3; ?><br>
	<br>
	性別<br>
	<?php if($user->sex == 1 ) :?>男性
	<?php elseif($user->sex == 2 ) :?>女性
	<?php else :?>未設定
	<?php endif;?><br>
	<br>
	生年月日<br>
	<?php if ($user->birthday):?>
		<?php echo substr($user->birthday,0,4)."年".substr($user->birthday,4,2)."月".substr($user->birthday,6,2)."日"; ?>
	<?php else:?>指定なし
	<?php endif;?><br>
	<br>
	お知らせメール<br>
	<?php if($user->mailmagazine > 0 ) :?>受信する
	<?php else :?>受信しない
	<?php endif;?><br>
	<br>
	<div>
		<?php echo Form::open('/mypage/leave');?>
		<?php echo Form::submit('submit', '退会する'); ?>
		<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
		<?php echo Form::close();?>
	</div>
</div>
<br>
<div>
	<h4>注文履歴</h4>
	<hr>
	<?php if(isset($order)):?>
		<?php foreach($order as $data):?>

			受注番号:<?php echo Html::anchor('/mypage/order/detail/'.$data["order_id"].$session_get_param,"<font color='#000000'>".$data["order_id"]."</font>") ?><br>
			注文日:<?php echo $data["insert_date"];?><br>
			金額:¥<?php echo number_format($data["d_total"] + $data["fee"] + $data["postage"]);?><br>
			<hr>

		<?php endforeach;?>
	<?php endif;?>
</div>
