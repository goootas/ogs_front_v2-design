<div class="panel forget">
    <h4 class="subheadline">パスワード再設定</h4>
	<div class="description">
        <p><?php echo implode('</p><p>', (array) Session::get_flash('error')); ?></p>
        <p>
        <?php if($type ==1):?>
        新しいパスワードの設定が完了いたしました。</br>
        ログイン画面より、新しいパスワードでログインしてください。</br>
        <?php else :?>
        入力していただいたメールアドレス宛に、パスワード再設定用URLをお送りしております。</br>
        メール記載のURLへアクセス後、パスワード再設定を行ってください。</br></br>
        なお、パスワード再設定用URLの有効期限は発行から1時間となっております。</br>
        <?php endif;?>
        </p>
	</div>
</div>





