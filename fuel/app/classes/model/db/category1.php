<?php
class Model_Db_Category1 extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title',
		'sort',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_category1';

	protected static $_has_many = array(
		'category2' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Db_Category2',
			'key_to' => 'category1_id',
			'cascade_save' => true,
			'cascade_delete' => false,
			),
		);
	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		);

	public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('shop'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}

	public static function findListToolSelect($where="",$sort="")
	{
		$data = static::find('all', array(
			'select' => array(
				"id",
				"title",
				),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);

		$results = array();
		$results[""] = "未指定";
		foreach ($data as $value) {
			$results[$value->id] = $value->title;
		}
		return $results;
	}

	public static function findListFront($where="",$sort="")
	{
		$data = static::find('all', array(
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}
}
