<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta charset="UTF-8">
	<?php if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<?php } else { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php } ?>
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="/assets/icon/chet/chet.ico">
	<?php
	echo Asset::css(
		array(
			$shop_data["dir"].'/'.'common.css',
			$shop_data["dir"].'/'.'reset.css',
			$shop_data["dir"].'/'.'base.css',
			$shop_data["dir"].'/'.'style.css',
			'validationEngine.jquery.min.css',
		)
	);
	echo Asset::js(
		array(
			$shop_data["dir"].'/'.'jquery.1.8.2.min.js',
			$shop_data["dir"].'/'.'common.js',
			$shop_data["dir"].'/'.'jquery.flexslider.js',
			$shop_data["dir"].'/'.'jquery.waypoints.min.js',
			$shop_data["dir"].'/'.'jquery.validationEngine-ja.min.js',
			'jquery.validationEngine.min.js',
		)
	);
	if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::css(
			array(
				Config::get("assets_url").'css/common-ie.css',
			)
		);
	} ?>
	<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,700' rel='stylesheet' type='text/css'>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $shop_data["ga_tag"]?>', 'auto');
		ga('require', 'linkid', 'linkid.js');
		ga('send', 'pageview');

	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1078253998891421');
		fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=1078253998891421&ev=PageView&noscript=1"
			/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<?php if(Request::active()->controller == 'Controller_Top') :?><body class="TOP">
<?php else : ?><body class="SUB">
<?php endif; ?>
<div class="WRAPPER">
	<div class="HEADER">
		<div class="TITLE">
			<a href="/"><img class="titlelogo" src="/assets/img/chet/IMG_logo.png"></a>
		</div>
		<div class="GLOBALNAVI">
			<div class="HEAD">
				<ul>
					<li class="title">
						<?php echo Html::anchor('/', '<img src="/assets/img/chet/IMG_logo_white.png">') ?>
					</li>
					<li><a class="button link LOGIN" href="/cart/"><span><img class="" src="/assets/img/chet/IMG_icon_cart.png"></span></a></li>
					<?php if(Session::get('user.id')) : ?>
						<li><a class="button link LOGIN" href="/mypage/"><span><img class="" src="/assets/img/chet/IMG_icon_mypage.png"></span></a></li>
						<li><a class="button link LOGIN" href="/logout/"><span><img class="" src="/assets/img/chet/ICON_logout.png"></span></a></li>
					<?php else: ?>
						<li><a class="button link LOGIN" href="/login/"><span><img class="" src="/assets/img/chet/ICON_login.png"></span></a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="NAVIGATION">
				<ul>

					<?php if(Request::active()->controller == 'Controller_Top') :?>
						<li><?php echo Html::anchor('/', "<span>HOME</span><hr>" ,array("class" => "active")) ?></li>
					<?php else : ?>
						<li><?php echo Html::anchor('/', "<span>HOME</span><hr>") ?></li>
					<?php endif; ?>
					<?php if(Request::active()->controller == 'Controller_Topic') :?>
						<li><?php echo Html::anchor('/topic', "<span>TOPICS</span><hr>" ,array("class" => "active")) ?></li>
					<?php else : ?>
						<li><?php echo Html::anchor('/topic', "<span>TOPICS</span><hr>") ?></li>
					<?php endif; ?>
					<?php if(Request::active()->controller == 'Controller_Lineup') :?>
						<li><?php echo Html::anchor('/lineup', "<span>LINE UP</span><hr>" ,array("class" => "active")) ?></li>
					<?php else : ?>
						<li><?php echo Html::anchor('/lineup', "<span>LINE UP</span><hr>") ?></li>
					<?php endif; ?>
					<?php if(Request::active()->controller == 'Controller_Concept') :?>
						<li><?php echo Html::anchor('/concept', "<span>CONCEPT</span><hr>" ,array("class" => "active")) ?></li>
					<?php else : ?>
						<li><?php echo Html::anchor('/concept', "<span>CONCEPT</span><hr>") ?></li>
					<?php endif; ?>

				</ul>
			</div>
		</div>
		<a class="menubutton" href="javascript:void(0)">MENU<hr></a>
	</div>

	<div class="CONTENTS">
		<?php echo $content; ?>
	</div>

	<div class="FOOTER">
		<div class="CONTAINER">
			<ul class="menu-01">
				<li><?php echo Html::anchor('/', "<span>HOME</span>") ?></li>
				<li><?php echo Html::anchor('/topic', "<span>TOPICS</span>") ?></li>
				<li><?php echo Html::anchor('/lineup', "<span>LINE UP</span>") ?></li>
				<li><?php echo Html::anchor('/concept', "<span>CONCEPT</span>") ?></li>
				<li><?php echo Html::anchor('/inquiry', "<span>CONTACT</span>") ?></li>
			</ul>
			<ul class="menu-02">
				<li><?php echo Html::anchor('/info/guide', "<span>ショッピングガイド</span>") ?></li>
				<li><?php echo Html::anchor('/info/privacy', "<span>プライバシーポリシー</span>") ?></li>
				<li><?php echo Html::anchor('/info/legal', "<span>特定商取引法に基づく表記</span>") ?></li>
			</ul>
			<ul class="menu-02">
				<li><a href="https://www.facebook.com/CHET-857632044335106/timeline/" target="_blank"><span>Facebook</span></a></li>
<!--				<li><a href="https://twitter.com/i_am_chet55" target="_blank"><span>Twitter</span></a></li>-->
				<li><a href="https://instagram.com/i_am_chet55/" target="_blank"><span>Instagram</span></a></li>
			</ul>
			<div class="CONTACT" id="mc_embed_signup">
				<h2 class="text-heading">MailMagazine</h2>
				<form action="//official-goods-store.us11.list-manage.com/subscribe/post?u=4b0ce536622e92ca5280864fb&amp;id=d5ae6b5859" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">

						<div class="mc-field-group email">
							<label class="text-caption" for="mce-EMAIL">Email Address </label>
							<input  type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						</div>
						<div class="mc-field-group fname">
							<label class="text-caption"  for="mce-FNAME">First Name </label>
							<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
						</div>
						<div class="mc-field-group lname">
							<label class="text-caption"  for="mce-LNAME">Last Name </label>
							<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
						</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>
						<div style="position: absolute; left: -5000px;"><input type="text" name="b_4b0ce536622e92ca5280864fb_d5ae6b5859" tabindex="-1" value=""></div>
						<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					</div>
				</form>
			</div>
			<div class="credit">
				<span class="text-caption">© <?php echo date("Y");?> MN All Rights Reserved.</span>
			</div>
		</div>
	</div>
</div>
</body>
</html>

