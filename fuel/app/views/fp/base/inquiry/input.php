<h4>FAQ</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<b>Q.注文内容を変更 / キャンセルしたい</b><br>
A.「注文する」ボタン押下後の取り消し、お支払い方法の変更、ご注文の追加等、内容の変更は承ることができません。ご注文の際は十分にご検討のうえお申し込みください。<br>
<hr>
<b>Q.商品を交換 / 返品したい</b><br>
A.不良品がお手元に届いてしまった場合、良品とお取り換えさせて頂きます。<br />
（実際の商品の色が、画面上の色と微妙に異なることがございます）<br />
返品は一切承っておりませんので、予めご了承下さい。<br />
お届け商品が不良品だった場合、商品到着後７日以内にご連絡をお願い致します。商品到着後７日を過ぎてしまいますと、不良品交換はお受けできませんので、商品到着後に、なるべく早く内容のご確認をお願い致します。<br />
お客様のもとで、破損、汚損が生じた場合には、お取り換え出来ません。<br />
交換時に該当商品が品切れになった場合には、返金させて頂きますので、予めご了承下さい。<br>
<hr>
Q.注文時に登録したメールアドレスを変更したい<br>
A.メールアドレスはお客様ご自身にてマイページより変更していただくことが可能です。<br>
<hr>
<b>Q.お届け先を変更したい</b><br>
A.ご注文後に住所等のご変更がございましたら、お問い合わせページよりご連絡ください。発送準備に入ってしまいますと、住所変更等を承ることができませんので、あらかじめご了承ください。<br>
<hr>
<b>Q.後払い決済について</b><br />
A.請求書は商品とは別で送付いたしますので、ご確認ください。<br />
<br />
商品代金のお支払いは「コンビニ」「郵便局」「銀行」どこでも可能です。<br />
請求書の記載事項に従って発行日から14日以内にお支払いください。<br />
<br />
○ご注意 後払い手数料：¥205<br />
後払いのご注文には、株式会社キャッチボールよりの提供する後払い.comサービスが適用され、サービスの範囲内で個人情報を提供し、代金債権を譲渡します。
<br />
商品到着後2週間経過してもお支払用紙が届かない場合はお支払用紙が 届かない旨をご連絡くださいませ。<br />
<hr>
<b>Q.注文したがメールが届かない</b><br />
A.注文完了画面に「受注番号」が表示されていれば、正常にご注文をお受けさせていただいております。 表示された受注番号にてマイページから内容をご確認ください。<br />
<hr>


<h4>お問い合わせ</h4>
<?php echo Form::open();?>
<div>
	<div>
		お名前(必須)<br>
		<?php echo Form::input('username', Input::post('username', isset($user_data)  ? $user_data->username_sei . $user_data->username_mei : "")); ?>
	</div>
	<div>
		メールアドレス(必須)<br>
		<?php echo Form::input('email', Input::post('email', isset($user_data)  ? $user_data->email : ""), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>
	<div>
		お問い合わせ内容(必須)<br>
		<?php echo Form::textarea('message', Input::post('message', ''),array('rows' =>'10')); ?>
	</div>

	<div>
		<?php echo Form::submit('exec', '送信'); ?><br>
	</div>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<?php echo Form::close();?>
