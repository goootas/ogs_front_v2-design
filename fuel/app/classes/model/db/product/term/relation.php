<?php
class Model_Db_Product_Term_Relation extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'term_id',
		'product_id',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_product_term_relation';
	protected static $_has_one = array(
		'term' => array(
			'key_from' => 'term_id',
			'model_to' => 'Model_Db_Product_Term',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		'product' => array(
			'key_from' => 'product_id',
			'model_to' => 'Model_Db_Product',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}

	public static function findDetailSimple($where="")
	{
		$data = static::find('first', array(
				'where'		=> $where,
			)
		);
		return $data;
	}

	public static function findList($where="",$sort="")
	{
		$data = static::find('all', array(
				'related'	=> array('product','term'),
				'where'		=> $where,
				'order_by'	=> $sort,
			)
		);
		return $data;
	}

	public static function delTerm($term_id = "")
	{
		if($term_id){
			$where = array(
				'term_id' => $term_id,
			);
			DB::delete(self::$_table_name)->
				where($where)->
				execute('default');
		}
		return ;
	}


}

