<div>
	<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
</div>

<?php echo Form::open(array('action' => '/order/exec','method' => 'get'));?>
<div>
	<center>
		この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
		<p><?php echo Form::submit('final','注文する'); ?></p>
	</center>
	<div>
		<p><?php echo Html::anchor('/order/input3'.$session_get_param, '<font color="#FFFFFF">前に戻る</font>'); ?></p>
	</div>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<?php echo Form::close();?>

<div>
	<?php echo Html::anchor('/cart/list/1'.$session_get_param,'<font color="#FFFFFF">カート内容変更</font>') ?><br>
	<br>
	<?php $total_tax = 0; ?>
	<?php if(count($cart) > 0): ?>
		<?php foreach ($cart as $data): ?>
			<?php
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			$anchor_txt = "";
			$anchor_txt .= "<font color='#FFFFFF'>".$data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"]."</font>";
			?>
			<?php echo Html::anchor('/product/detail/'.$data["id"].$session_get_param,$anchor_txt) ?><br>
			価格:¥<?php echo number_format(floor($data["price"] * $tax));?><br>
			数量:<?php echo $data["num"];?><br />
			小計:¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?><br>
			<hr>

		<?php endforeach; ?>

		商品合計:¥<?php echo number_format(intval($total_tax));?><br>
		送料:¥<?php echo number_format(intval($postage));?><br>
		決済手数料:¥<?php echo number_format(intval($fee));?><br>
		総合計:¥<?php echo number_format(intval($total));?><br>

	<?php endif; ?>

	<p>
		<?php if(Session::get("payment_ctl_cnt") < 3):?>
			決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
		<?php endif; ?>
	</p>

	<?php if( $name_count > 0) :?>
		<?php echo Html::anchor('/order/inputname/1'.$session_get_param,'<font color="#FFFFFF">ネームプリント文字変更</font>'); ?><br>
		<?php $ses_name_view	= Session::get("order_data.dataname.names_view");?>
		<?php $ses_name_value	= Session::get("order_data.dataname.names_value");?>
		<?php foreach ($ses_name_view as $key => $data): ?>
			<?php echo $ses_name_view[$key]?><br>
			<?php echo $ses_name_value[$key]?><br><br>
		<?php endforeach; ?>
	<?php endif;?>

	<br>
	<?php echo Html::anchor('/order/input1/1'.$session_get_param,'<font color="#FFFFFF">お客様情報変更</font>'); ?><br>
	<br>
	お名前<br>
	<?php echo Session::get("order_data.data1.order_username_sei");?> <?php echo Session::get("order_data.data1.order_username_mei");?> 様<br>
	<br>
	ふりがな<br>
	<?php echo Session::get("order_data.data1.order_username_sei_kana");?> <?php echo Session::get("order_data.data1.order_username_mei_kana");?> 様<br>
	<br>
	郵便番号<br>
	<?php echo substr(Session::get("order_data.data1.order_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data1.order_zip"), 3,4);?><br>
	<br>
	都道府県<br>
	<?php echo Session::get("order_data.data1.order_state");?><br>
	<br>
	住所１<br>
	<?php echo Session::get("order_data.data1.order_address1");?><br>
	<br>
	住所２<br>
	<?php echo Session::get("order_data.data1.order_address2");?><br>
	<br>
	メールアドレス<br>
	<?php echo Session::get("order_data.data1.order_email");?><br>
	<br>
	電話番号<br>
	<?php echo Session::get("order_data.data1.order_tel1");?>-<?php echo Session::get("order_data.data1.order_tel2");?>-<?php echo Session::get("order_data.data1.order_tel3");?><br>
	<br>
	性別<br>
	<?php if (Session::get("order_data.data1.order_sex")=="1"):?>男性
	<?php elseif (Session::get("order_data.data1.order_sex")=="2"):?>女性
	<?php else:?>指定なし
	<?php endif;?><br>
	<br>
	生年月日<br>
	<?php if (Session::get("order_data.data1.order_birthday")):?>
		<?php echo substr(Session::get("order_data.data1.order_birthday"), 0,4);?>年<?php echo substr(Session::get("order_data.data1.order_birthday"), 4,2);?>月<?php echo substr(Session::get("order_data.data1.order_birthday"), 6,2);?>日
	<?php else:?>指定なし
	<?php endif;?><br><br>
	ツアー参加予定公演<br>

	<?php if (date("Ymd") < "20160412") :?>
		<?php if (Session::get("order_data.data1.order_event_flg")=="1"):?>4/21 広島公演<br>
		<?php elseif (Session::get("order_data.data1.order_event_flg")=="2"):?>4/23 岡山公演<br>
		<?php else:?>その他公演 / 参加予定無し<br>
		<?php endif;?>
	<?php elseif (date("Ymd") >= "20160412" && date("Ymd") <= "20160418") :?>
		<?php if (Session::get("order_data.data1.order_event_flg")=="1"):?>4/29,30 札幌公演<br>
		<?php elseif (Session::get("order_data.data1.order_event_flg")=="2"):?>5/3,4 大阪公演<br>
		<?php else:?>その他公演 / 参加予定無し<br>
		<?php endif;?>
	<?php elseif (date("Ymd") >= "20160419" && date("Ymd") <= "20160425") :?>
		<?php if (Session::get("order_data.data1.order_event_flg")=="1"):?>5/3,4 大阪公演<br>
		<?php elseif (Session::get("order_data.data1.order_event_flg")=="2"):?>5/7,8 栃木公演<br>
		<?php else:?>その他公演 / 参加予定無し<br>
		<?php endif;?>
	<?php endif;?>

	<br>
	<?php echo Html::anchor('/order/input2/1'.$session_get_param,'<font color="#FFFFFF">お届け先情報変更</font>'); ?><br>
	<br>
	お名前<br>
	<?php echo Session::get("order_data.data2.deliver_username_sei");?> <?php echo Session::get("order_data.data2.deliver_username_mei");?> 様<br>
	<br>
	郵便番号<br>
	<?php echo substr(Session::get("order_data.data2.deliver_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data2.deliver_zip"), 3,4);?><br>
	<br>
	都道府県<br>
	<?php echo Session::get("order_data.data2.deliver_state");?><br>
	<br>
	住所１<br>
	<?php echo Session::get("order_data.data2.deliver_address1");?><br>
	<br>
	住所２<br>
	<?php echo Session::get("order_data.data2.deliver_address2");?><br>
	<br>
	電話番号<br>
	<?php echo Session::get("order_data.data2.deliver_tel1");?>-<?php echo Session::get("order_data.data2.deliver_tel2");?>-<?php echo Session::get("order_data.data2.deliver_tel3");?><br>
	<br>
	お届け日<br>
	<?php
	if(isset($deliver_term_date)){
		echo $deliver_term_date->deliver_start_text;
	}else{
		?>
		<?php if(!Session::get("order_data.data2.delivery_date")) : ?>
			受注日翌日から2日以内で発送（土日祝日除く / 発送からお届けは1〜3日）
		<?php else : ?>
			<?php echo sprintf("%04d年%02d月%02d日",
				substr(Session::get("order_data.data2.delivery_date"),0,4),
				substr(Session::get("order_data.data2.delivery_date"),4,2),
				substr(Session::get("order_data.data2.delivery_date"),6,2));?>
		<?php endif; ?>
	<?php }?>
	<br>
	お届け希望時間帯<br>
	<?php
	if(isset($deliver_term_date)){
		echo "指定不可<br>";
	}else{
		?>
		<?php if(!Session::get("order_data.data2.delivery_time")) : ?>
			指定無し<br>
		<?php else : ?>
			<?php if(Session::get("order_data.data2.delivery_time") == "AM") : ?>
				<?php echo "午前中<br>";?>
			<?php else : ?>
				<?php $dt = explode("-",Session::get("order_data.data2.delivery_time"));?>
				<?php echo sprintf("%d時〜%d時",$dt[0],$dt[1]);?><br>
			<?php endif; ?>
		<?php endif; ?>
	<?php }?>
	<br>
	<?php echo Html::anchor('/order/input3/1'.$session_get_param,'<font color="#FFFFFF">お支払情報変更</font>'); ?><br>
	<br>
	支払方法<br>
	<?php echo Config::get("payment.".Session::get("order_data.data3.payment"));?>
	<?php if(Session::get("order_data.data3.payment") == 1) echo Config::get("jpo.".Session::get("order_data.data3.credit_jpo"));?>
	<?php if(Session::get("order_data.data3.payment") == 2) echo Config::get("cvs.".Session::get("order_data.data3.cvs"));?>
</div>
<br>
<?php echo Form::open(array('action' => '/order/exec','method' => 'get'));?>
<div>
	<center>
		この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
		<p><?php echo Form::submit('final','注文する'); ?></p>
		<p style="color: #FF0000">「注文する」押下後の取り消し / お支払い方法変更 / ご注文商品変更等は承ることができません。</p>
	</center>
	<div>
		<p><?php echo Html::anchor('/order/input3'.$session_get_param, '<font color="#FFFFFF">前に戻る</font>'); ?></p>
	</div>
</div>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
<?php echo Form::close();?>
