<?php
require_once APPPATH.'vendor/tgMdkPHP/tgMdk/3GPSMDK.php';

class Model_Lib_Payment_Veritrans_Card extends \Model
{
    /*
     * 与信
     */
    public static function authorize_exec(
            $order_id,$payment_amount,$card_number,$card_expire,$jpo,$security_code,$veritrans_conf="")
    {
        /**
        * 要求電文パラメータ値の指定
        */
        $request_data = new CardAuthorizeRequestDto();

        $request_data->setOrderId($order_id);
        $request_data->setAmount(trim($payment_amount));
        $request_data->setCardNumber(trim($card_number));
        $request_data->setCardExpire(trim($card_expire));
        $request_data->setWithCapture("false");
        $request_data->setJpo(trim($jpo));
        $request_data->setSecurityCode(trim($security_code));

       /**
        * 実施
        */
        $transaction = new TGMDK_Transaction();
        $response_data = $transaction->execute($request_data,$veritrans_conf);

        $response = array();
        $response["res_data"] = $response_data->__toString();
        $response["req_data"] = $request_data->__toString();

        //予期しない例外
        if (!isset($response_data)) {
            return $response;
        //想定応答の取得
        } else {

            /**
             * 取引ID取得
             */
            $response["order_id"] = $response_data->getOrderId();
            /**
             * 結果コード取得
             */
            $response["status"] = $response_data->getMStatus();
            /**
             * 詳細コード取得
             */
            $response["result_code"] = $response_data->getVResultCode();
            /**
             * エラーメッセージ取得
             */
            $response["error_msg"] = $response_data->getMerrMsg();

            return $response;

        }
    }
    /*
     * 与信＆売上
     */
    public static function authorize_capture_exec(
        $order_id,$payment_amount,$card_number,$card_expire,$jpo,$security_code,$veritrans_conf="")
    {
        /**
         * 要求電文パラメータ値の指定
         */
        $request_data = new CardAuthorizeRequestDto();

        $request_data->setOrderId($order_id);
        $request_data->setAmount(trim($payment_amount));
        $request_data->setCardNumber(trim($card_number));
        $request_data->setCardExpire(trim($card_expire));
        $request_data->setWithCapture("true");
        $request_data->setJpo(trim($jpo));
        $request_data->setSecurityCode(trim($security_code));

        /**
         * 実施
         */
        $transaction = new TGMDK_Transaction();
        $response_data = $transaction->execute($request_data,$veritrans_conf);

        $response = array();
        $response["res_data"] = $response_data->__toString();
        $response["req_data"] = $request_data->__toString();

        //予期しない例外
        if (!isset($response_data)) {
            return $response;
            //想定応答の取得
        } else {

            /**
             * 取引ID取得
             */
            $response["order_id"] = $response_data->getOrderId();
            /**
             * 結果コード取得
             */
            $response["status"] = $response_data->getMStatus();
            /**
             * 詳細コード取得
             */
            $response["result_code"] = $response_data->getVResultCode();
            /**
             * エラーメッセージ取得
             */
            $response["error_msg"] = $response_data->getMerrMsg();

            return $response;

        }
    }
    public static function capture_exec()
    {// 売上処理

    }
    public static function cancel_exec($order_id,$veritrans_conf="")
    {// キャンセル処理
        /**
         * 要求電文パラメータ値の指定
         */
        $request_data = new CardCancelRequestDto();

        $request_data->setOrderId($order_id);

        /**
         * 実施
         */
        $transaction = new TGMDK_Transaction();
        $response_data = $transaction->execute($request_data,$veritrans_conf);

        $response = array();
        $response["res_data"] = $response_data->__toString();
        $response["req_data"] = $request_data->__toString();

        //予期しない例外
        if (!isset($response_data)) {
            return $response;
            //想定応答の取得
        } else {

            /**
             * 取引ID取得
             */
            $response["order_id"] = $response_data->getOrderId();
            /**
             * 結果コード取得
             */
            $response["status"] = $response_data->getMStatus();
            /**
             * 詳細コード取得
             */
            $response["result_code"] = $response_data->getVResultCode();
            /**
             * エラーメッセージ取得
             */
            $response["error_msg"] = $response_data->getMerrMsg();

            return $response;

        }

    }
    public static function reauthorize_exec()
    {// 再売上処理

    }

}
