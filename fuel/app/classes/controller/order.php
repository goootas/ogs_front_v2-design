<?php
/**
 * 決済
 */
use Fuel\Core\Config;
use Fuel\Core\Log;
use Fuel\Core\Session;
use Fuel\Core\Request;
use Fuel\Core\Response;
use Fuel\Core\Input;
use Fuel\Core\View;
use Model\lib\Postage;

class Controller_Order extends Controller_Basefront
{
	protected $deliver_term_date;
	protected $name_count;
//	protected $receives_count;

	public function before()
	{
		parent::before();

		Config::load("veritrans");// ベリトランス設定ファイル読み込み

		// カートが空の場合、カートにリダイレクト※注文完了ページ除く
		$cart_sess = Session::get('cart');
		if(!in_array(Request::active()->action, array('finish'))){

			if(! $cart_sess){
				Response::redirect('/cart'.$this->session_get_param);
			}

			// ---------------------------------------------------
			//カートに入っている商品の情報を取得
			// didに紐づく商品情報を取得する。
			// ---------------------------------------------------
			$stock_error = 0;
			// ---------------------------------------------------
			// 在庫判定処理
			// ---------------------------------------------------
			foreach ($cart_sess as $key => $val) {
				if(!$cart_sess[$key]["did"]){
					continue;
				}
				$product_detail = Model_Db_Product_Detail::find($cart_sess[$key]["did"]);
				if($product_detail){
					if($product_detail->stock < $cart_sess[$key]["num"] ){
						$cart_sess[$key]["num"] = $product_detail->stock;
						$item = Model_Db_Product_Detail::findProductDetailOne(array(array("id",$cart_sess[$key]["did"])));
						$stock_error++;
						if($cart_sess[$key]["num"] > 0){
							Session::set_flash("error", "大変申し訳ございません。現在【&nbsp;"
								.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
								."&nbsp;".($item->option2_name ? $item->option2_name :"")
								."&nbsp;】の在庫数が不足しております。");
						}else{
							Session::set_flash("error", "大変申し訳ございませんが、【&nbsp;"
								.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
								."&nbsp;".($item->option2_name ? $item->option2_name :"")
								."&nbsp;】お客様が注文手続きをされている間に品切れとなりました。");
						}
					}

					// ---------------------------------------------------
					// 現地受渡判定
					// ---------------------------------------------------
					if($product_detail->receives_flg){
						$this->receives_count++;
					}
				}
			}
			$this->template->set_global('receives_count', $this->receives_count, false);

			// ---------------------------------------------------
			// 現地受渡時間在庫判定処理
			// ---------------------------------------------------
			if($this->receives_count){
				// ---------------------------------------------------
				// 現地受渡情報
				// ---------------------------------------------------
				$this->shop_receives_data = array();
				$sql = "SELECT id,title1,title2,stock FROM tbl_local_receives_timetable WHERE 1 = 1 ";
				$sql .= "AND shop_id = ".$this->shop_data["id"] . " ";
				$sql .= "AND start_date <= now() AND finish_date >= now() ";
				$sql .= "order by id ";
				$this->shop_receives_data = DB::query($sql)->execute();
				$this->template->set_global('shop_receives', $this->shop_receives_data, false);

				// ---------------------------------------------------
				// 5000円以下はカートに戻す TODO あとでツール化
				// 握手会場受取は合計¥5,000以上のご注文のみ受付となります
				// ---------------------------------------------------
				$subtotal = 0;
				$wheres_base = array(
					array("base.status",Config::get('status_value.enable')),
					array("status",Config::get('status_value.enable')),
				);
				foreach ($cart_sess as $val) {
					$wheres = array_merge_recursive($wheres_base,array(array("id",$val["did"])));
					$data = Model_Db_Product_Detail::findProductDetailOne($wheres);
					if($data){
						$w_price = floor($data->base->price_sale * $this->tax) * $val["num"];
						$subtotal = $subtotal + $w_price;
					}
				}
				if($subtotal < 5000){
					Session::set_flash("error", "会場受取は合計¥5,000以上のご注文のみ受付となります");
					Response::redirect('/cart/list'.$this->session_get_param);
					exit();
				}

				$ses_receives = Session::get("order_data.datareceives.receives") ? Session::get("order_data.datareceives.receives") : "";
				if($ses_receives){
					$receives_stock = DB::query('SELECT stock,title1,title2 FROM tbl_local_receives_timetable WHERE id = '.$ses_receives)->execute()->current();
					if($receives_stock["stock"] <= 0 ){
						Session::set_flash("error", "大変申し訳ございませんが、注文手続きをされている間に【&nbsp;"
							.$receives_stock["title1"].$receives_stock["title2"] ."&nbsp;】の枠が規定数終了となりました。別の時間帯でご指定ください。");
						Session::delete('order_data.datareceives');
						Response::redirect('/order/inputreceives'.$this->session_get_param);
						exit();
					}
				}
			}

			// ---------------------------------------------------
			// カート不整合処理
			// ---------------------------------------------------
			\Model\lib\Cart::adjCartData($cart_sess,$stock_error);
			// セッション再登録
			Session::set('cart',$cart_sess);
			// 在庫不整合があったら、カートに戻す
			if($stock_error){

				// 楽天ID決済の与信をキャンセル ------------------------------>
				$rakuten_order_id = Input::post("rakuten_order_id");
				if(isset($rakuten_order_id) && $rakuten_order_id != ""){
					Model_Lib_Payment_Rakuten::cancel(Input::post("rakuten_order_id"));
				}
				// 楽天ID決済の与信をキャンセル ------------------------------>

				Response::redirect('/cart/list'.$this->session_get_param);
				exit();
			}

			// ---------------------------------------------------
			// カートに入っている商品の販売期間をチェック(異なる販売期間チェック)
			// ---------------------------------------------------
			$ccnt = 0;
			$term_id = "";
			\Model\lib\Term::chkCartProductTerm($cart_sess,$term_id,$ccnt);

			// ---------------------------------------------------
			// カートに入っている商品の販売期間をチェック(販売期間終了チェック)
			// ---------------------------------------------------
			// 販売期間が終了した商品がカートに入っていた場合は、カート内にある該当商品を削除する。
			$no_count = 0 ;
			foreach ($cart_sess as $key => $val) {
				$is_term = \Model\lib\Term::isTerm($val["did"]);
				if($is_term === FALSE){
					Arr::delete($cart_sess,$key);
					$no_count++;
				}
			}
			if($no_count){
				// 楽天ID決済の与信をキャンセル ------------------------------>
				$rakuten_order_id = Input::post("rakuten_order_id");
				if(isset($rakuten_order_id) && $rakuten_order_id != ""){
					Model_Lib_Payment_Rakuten::cancel(Input::post("rakuten_order_id"));
				}
				// 楽天ID決済の与信をキャンセル ------------------------------>
				Session::set('cart',$cart_sess);
				Response::redirect('/cart/list'.$this->session_get_param);
				exit;
			}

			if($term_id != ""){
				$this->deliver_term_date = Model_Db_Product_Term::find($term_id);
				$this->template->set_global('deliver_term_date', $this->deliver_term_date, false);
			}

			// ---------------------------------------------------
			// カート内容取得
			// ---------------------------------------------------
			$cart_data = \Model\lib\Cart::getCartData($cart_sess);
			$this->template->set_global('cart', $cart_data, false);

			// ---------------------------------------------------
			// 名入れ判定 TODO:対象商品ID固定記述
			// ---------------------------------------------------
			$this->name_count = 0;
			foreach ($cart_data as $key => $val) {
				if(array_search($val["id"], Config::get("naire_product_ids")) !== false){
					$this->name_count++;
				}
			}
			$this->template->set_global('name_count', $this->name_count, false);

			// ---------------------------------------------------
			// 配送指定日時
			// ---------------------------------------------------
//		$delivery_date = array("" => "土日祭日を除く7営業日以内でお届け");
			$delivery_date = array("" => "指定無し");
			for($i=8;$i<=22 ;$i++){
				$daydiff1=0;
				$k = date("Ymd", strtotime($i+$daydiff1." day"));
				// 年末年始対応（該当日付はスキップする）
				if($k >= 20151230 && $k <= 20160108){
					continue;
				}
				$v = date("Y年m月d日(D)", strtotime($i+$daydiff1." day"));
				$delivery_date[$k] = $v;
			}
			$this->template->set_global('delivery_date', $delivery_date, false);

			// ---------------------------------------------------
			// GACKT写真集名入れ対応
			// 該当商品数と同じ数だけ名入れ設定値がある確認
			// ない場合は、変更フラグ付きで選択画面へリダイレクトする。
			// ---------------------------------------------------
			// 該当商品がない場合は、注文者情報入力画面へリダイレクト
			$target_product_detail_ids = Config::get("print_product_detail_ids");
			$target_cnt = 0;// 名入れできる商品数
			foreach ($cart_sess as $cart) {
				if(isset($target_product_detail_ids)){
					foreach ($target_product_detail_ids as $target_product_detail_id) {
						if($cart["did"] == $target_product_detail_id){
							$target_cnt = $cart["num"];
						}
					}
				}
			}
			if($target_cnt > 0){
				if(count(Session::get("print.select_ids")) != $target_cnt){
					Response::redirect('/order/print/select/1'.$this->session_get_param);
				}
			}
		}
	}

	/*
	 * 注文者情報入力
	 */
	public function action_index()
	{
		$this->template->title = "注文者情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/input');
	}

	/*
	 * 注文者情報入力(名入れ)
	 */
	public function action_inputname($change=0)
	{
		if($this->name_count == 0){
			Response::redirect('/order/input1'.$this->session_get_param);
		}
		$this->template->set_global('change', $change, false);
		$this->template->set_global('order_dataname', Session::get("order_data.dataname"), false);

		$this->template->title = "名入れ情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/inputname');
	}

	/*
	 * 注文者情報入力(現地受渡)
	 */
	public function action_inputreceives($change=0)
	{
		if($this->receives_count == 0){
			Response::redirect('/order/input1'.$this->session_get_param);
		}
		$this->template->set_global('change', $change, false);
		$this->template->set_global('order_datareceives', Session::get("order_data.datareceives.receives"), false);

		$this->template->title = "会場受渡情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/inputreceives');
	}

	/*
	 * 注文者情報入力
	 */
	public function action_input1($change=0)
	{
		if (Input::method() == 'POST') {
			// 名入れパラメータをセッションに登録し、入力チェック
			if($this->name_count > 0){
				Session::set("order_data.dataname",Input::post());
				if($this->agent_dir){
					\Model\lib\FpValidate::naire(Input::post("names_value"),$this->session_get_param);
				}
			}
			// 現地受渡処理
			if($this->receives_count > 0){
				Session::set("order_data.datareceives",Input::post());
				if($this->agent_dir){
					$err_cnt = 0;
					$e_receives = array();
					$receives = Input::post("receives");
					if(!$receives){
						$e_receives[] = '必須項目です';
						$err_cnt++;
					}
					if($err_cnt){
						Session::set_flash('e_receives', $e_receives);
						Response::redirect('/order/inputreceives'.$this->session_get_param);
					}
				}
			}

			if(Input::post("change_flg")){
				Response::redirect('/order/confirm'.$this->session_get_param);
			}
		}
		$this->template->set_global('change', $change, false);
		$this->template->set_global('order_data1', Session::get("order_data.data1"), false);

		$this->template->title = "注文者情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/input1');
	}
	/*
	 * お届け先情報入力
	 */
	public function action_input2($change=0)
	{
		if (Input::method() == 'POST') {
			// 注文者情報をセッションに入れる
			Session::set("order_data.data1",Input::post());
			if(Session::get("order_data.data1.regist_flg")){
				if(Model_Db_User::findEmail2($this->shop_data["id"],Session::get("order_data.data1.order_email"))){
					Session::set_flash('error', '入力されているメールアドレスは既に登録されています。');
					Response::redirect('/order/input1'.$this->session_get_param);
				}
			}
			// ガラ向けバリデーション
			if($this->agent_dir){
				\Model\lib\FpValidate::input1(
					Input::post("order_username_sei"),
					Input::post("order_username_mei"),
					Input::post("order_username_sei_kana"),
					Input::post("order_username_mei_kana"),
					Input::post("order_email"),
					Input::post("order_email2"),
					Input::post("order_zip"),
					Input::post("order_state"),
					Input::post("order_address1"),
					Input::post("order_address2"),
					Input::post("order_tel1"),
					Input::post("order_tel2"),
					Input::post("order_tel3"),
					Input::post("regist_flg"),
					Input::post("order_password"),
					$this->session_get_param
				);
			}
			if(Input::post("change_flg")){
				Response::redirect('/order/confirm'.$this->session_get_param);
			}
		}
		$this->template->set_global('change', $change, false);

		$this->template->set_global('order_data2', Session::get("order_data.data2"), false);
		$this->template->title = "注文者情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/input2');
	}
	/*
	 * 配送・決済方法指定
	 */
	public function action_input3($change=0)
	{
		if (Input::method() == 'POST') {
			// お届け先情報をセッションに入れる
			Session::set("order_data.data2",Input::post());
			// ガラ向けバリデーション
			if($this->agent_dir){
				\Model\lib\FpValidate::input2(
					Input::post("deliver_username_sei"),
					Input::post("deliver_username_mei"),
					Input::post("deliver_zip"),
					Input::post("deliver_state"),
					Input::post("deliver_address1"),
					Input::post("deliver_address2"),
					Input::post("deliver_tel1"),
					Input::post("deliver_tel2"),
					Input::post("deliver_tel3"),
					$this->session_get_param
				);
			}
			if(Input::post("change_flg")){
				Response::redirect('/order/confirm'.$this->session_get_param);
			}
		}
		$this->template->set_global('change', $change, false);
		$this->template->set_global('order_data3', Session::get("order_data.data3"), false);
		$this->template->title = "注文者情報入力";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/input3');
	}

	/*
	 * 注文確認
	 */
	public function action_confirm()
	{
		if($this->name_count > 0){
			if(!Session::get("order_data.dataname")){
				Response::redirect('/order/inputname'.$this->session_get_param);
			}
		}
		if(!Session::get("order_data.data1")){
			Response::redirect('/order/input1'.$this->session_get_param);
		}
		if(!Session::get("order_data.data2")){
			Response::redirect('/order/input2'.$this->session_get_param);
		}

		// カート用の変更ボタン制御用のセッションを削除
		Session::delete('change');
		if (Input::method() == 'POST')
		{
			// 配送・決済方法情報をセッションに入れる
			Session::set("order_data.data3",Input::post());

			// ガラ向けバリデーション
			if($this->agent_dir){
				\Model\lib\FpValidate::input3(
					Input::post("payment"),
					Input::post("card_no"),
					Input::post("secure_cd"),
					$this->session_get_param
				);
			}
		}

		// 会場受渡時の購入チェック TODO:一旦共通機能とする。問題がある場合は店舗IDで分岐
		if($this->receives_count > 0){
			$w_error_cnt = \Model\lib\Validate::checkPersonalAuthAlreadyOrder($this->shop_data["id"],Session::get("order_data.data1"),Session::get("order_data.data2"));
			if($w_error_cnt > 0){
				Session::set_flash("receives_duplicate", 1);
			}
		}

		/*
		 * ラブライブ注文点数、注文数制限
		 */
		if($this->shop_data["dir"] == "lovelive"){
			// 購入回数制限
			$w_error_cnt = \Model\lib\Validate::checkPersonalAuthAlreadyOrder($this->shop_data["id"],Session::get("order_data.data1"),Session::get("order_data.data2"),1);
			if($w_error_cnt > 0){
				Session::set_flash("receives_duplicate", 1);
			}
		}
		$subtotal = 0;

		// ---------------------------------------------------
		// 商品情報取得
		// ---------------------------------------------------
		$wheres_base = array(
			array("base.status",Config::get('status_value.enable')),
			array("status",Config::get('status_value.enable')),
		);
		$products =array();
		foreach (Session::get('cart') as $val) {

			$wheres = array_merge_recursive($wheres_base,array(array("id",$val["did"])));
			$data = Model_Db_Product_Detail::findProductDetailOne($wheres);
			if($data){
				$tmp = $val;
				$tmp["data"] = $data;
				$tmp["price"] = floor($data->base->price_sale * $this->tax) * $val["num"];

				$products[] = $tmp;
				$subtotal = $subtotal + $tmp["price"];
			}
		}

		// ---------------------------------------------------
		// 商品小計
		// ---------------------------------------------------
		$this->template->set_global('subtotal', $subtotal, false);

		// ---------------------------------------------------
		// 商品情報
		// ---------------------------------------------------
		$this->template->set_global('products', $products, false);

		// ---------------------------------------------------
		// 都道府県情報と送料計算
		// ---------------------------------------------------
//		$order_postage = Postage::getPostageNoApi($this->shop_data["id"],
//			Session::get('order_data.data1.order_zip'),Session::get('order_data.data1.order_state'));
//		$this->template->set_global('order_postage', $order_postage, false);

//		$deliver_postage = Postage::getPostageNoApi($this->shop_data["id"],
//			Session::get('order_data.data2.deliver_zip'),Session::get('order_data.data2.deliver_state'));
//		$this->template->set_global('deliver_postage', $deliver_postage, false);

		$postage = 0;
		$deliver_postage = Postage::getPostageNoApi($this->shop_data["id"],
			Session::get('order_data.data2.deliver_zip'),Session::get('order_data.data2.deliver_state'));
		if($this->shop_data["free_deliver_price"] < 0 || $subtotal < $this->shop_data["free_deliver_price"]) {
			$postage = $deliver_postage["price"];
			// 現地受渡は送料無料
			if($this->receives_count > 0){
				$postage = 0;
			}
			$subtotal = $subtotal + $postage;
		}
		$this->template->set_global('postage', $postage, false);

		// ---------------------------------------------------
		// 決済手数料計算 コンビニ、代引き、後払い
		// 決済額に対して、手数料を算出する。
		// ---------------------------------------------------
		$fee = 0;
		// コンビニ決済の手数料計算
		if(Session::get('order_data.data3.payment') == Config::get("payment.value.cvs") && $subtotal > 0){
			foreach (Config::get("cvs_fee") as $val) {
				if($val["min"] <= ($subtotal) && $val["max"] >= ($subtotal)){
					$fee = intval($val["fee"]);
				}
			}
		}
		// 代引き決済の手数料計算
		if(Session::get('order_data.data3.payment') == Config::get("payment.value.cod") && $subtotal > 0){
			foreach (Config::get("cod_fee") as $val) {
				if($val["min"] <= ($subtotal) && $val["max"] >= ($subtotal)){
					$fee = intval($val["fee"]);
				}
			}
		}
		// 後払い決済の手数料計算(キャッチボール)
		if(Session::get('order_data.data3.payment') == Config::get("payment.value.ato") && $subtotal > 0){
			foreach (Config::get("ato_fee") as $val) {
				if($val["min"] <= ($subtotal) && $val["max"] >= ($subtotal)){
					$fee = intval($val["fee"]);
				}
			}
			if($this->shop_data["dir"] != "gackt") {
				$fee = 172;
			}
		}
		// 後払い決済の手数料計算(NP)
		if(Session::get('order_data.data3.payment') == Config::get("payment.value.npato") && $subtotal > 0){
			foreach (Config::get("ato_fee") as $val) {
				if($val["min"] <= ($subtotal) && $val["max"] >= ($subtotal)){
					$fee = intval($val["fee"]);
				}
			}
		}
		$this->template->set_global('fee', $fee, false);

		// ---------------------------------------------------
		//支払総額
		// ---------------------------------------------------
		$total = $subtotal + $fee;
		$this->template->set_global('total', $total, false);

		// ---------------------------------------------------
		//セッションに登録する。※決済情報
		// ---------------------------------------------------
		$amount = Session::get("order_data.data1")+Session::get("order_data.data2")+Session::get("order_data.data3");
		$amount["order_id"]			= $this->shop_data["prefix"] . date("YmdHis")."-".mt_rand(10000,99999);

		// コンビニ支払期日
		$limit_date = date("Y/m/d",$this->cvs_close_time);
//		$limit_date = date("Y/m/d");
		$amount["limit_date"]		= $limit_date;

		$amount["total"]			= $total;
//		$amount["order_postage"]	= $order_postage;
		$amount["deliver_postage"]	= $deliver_postage;
		$amount["fee"]				= $fee;
		$amount["postage"]			= $postage;
		$amount["products"]			= $products;
		Session::set('order', $amount);

		// 楽天ID決済対応 -------------------------------------------->
		$rakuten_conf = Config::get("rakuten");
		$this->template->set_global('rakuten_conf', $rakuten_conf, false);
		$this->template->set_global('rakuten_order_data', $amount, false);
		// 楽天ID決済対応 -------------------------------------------->

		// ---------------------------------------------------
		// GACKT写真集プリント対応
		// ---------------------------------------------------
		$print_product_name = "";
		foreach ($products as $value) {
			$target_product_detail_ids = Config::get("print_product_detail_ids");
			if(isset($target_product_detail_ids)){
				foreach ($target_product_detail_ids as $target_product_detail_id) {
					if($value["did"] == $target_product_detail_id){
						$print_product_name = $value["data"]["base"]["title"]." ".$value["data"]["option1_name"]." ".$value["data"]["option2_name"];
					}
				}
			}
		}
		$this->template->set_global('print_product_name', $print_product_name, false);


		$this->template->title = "注文内容確認";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/confirm');
	}

	/*
	 * 決済処理
	 */
	public function action_exec()
	{
		$res = "";
		if($this->name_count > 0){
			if(!Session::get("order_data.dataname")){
				Response::redirect('/order/inputname'.$this->session_get_param);
			}
		}
		if($this->receives_count > 0){
			if(!Session::get("order_data.datareceives")){
				Response::redirect('/order/inputreceives'.$this->session_get_param);
			}
		}
		if(!Session::get("order_data.data1")){
			Response::redirect('/order/input1'.$this->session_get_param);
		}
		if(!Session::get("order_data.data2")){
			Response::redirect('/order/input2'.$this->session_get_param);
		}
		if(!Session::get("order_data.data3")){
			Response::redirect('/order/input3'.$this->session_get_param);
		}

		// ---------------------------------------------------
		// 会場受渡時の購入チェック TODO:一旦共通機能とする。問題がある場合は店舗IDで分岐
		// ---------------------------------------------------
		if($this->receives_count > 0){
			$w_error_cnt = \Model\lib\Validate::checkPersonalAuthAlreadyOrder($this->shop_data["id"],Session::get("order_data.data1"),Session::get("order_data.data2"));
			if($w_error_cnt > 0){
				Session::set_flash("receives_duplicate", 1);
				Session::set_flash("error_center", "会場受取のご注文は、お1人様1会場につき1回までとなります。");
				Response::redirect('/order/confirm'.$this->session_get_param);
			}
		}
		/*
		 * ラブライブ注文点数、注文数制限
		 */
		if($this->shop_data["dir"] == "lovelive"){
			// 購入回数制限
			$w_error_cnt = \Model\lib\Validate::checkPersonalAuthAlreadyOrder($this->shop_data["id"],Session::get("order_data.data1"),Session::get("order_data.data2"),1);
			if($w_error_cnt > 0){
				Session::set_flash("receives_duplicate", 1);
				Session::set_flash("error_center", "会場受取のご注文は、お1人様1会場につき1回までとなります。");
				Response::redirect('/order/confirm'.$this->session_get_param);
			}
		}

		// ---------------------------------------------------
		//注文者情報(注文者情報＋お届け先＋配送日時＋決済情報)
		// ---------------------------------------------------
		$order = Session::get('order');

		// 在庫判定の行ロック順を統一するため、注文商品をdid順にソートする
		$sort_did = array();
		foreach ($order["products"] as $key => $value) {
			$sort_did[$key] = $value["did"];
		}
		array_multisort($order["products"], $sort_did);
		
		// ---------------------------------------------------
		//在庫切れチェック
		// ---------------------------------------------------
		foreach ($order["products"] as $value) {
			$stock_minus = DB::query('SELECT stock FROM tbl_product_detail WHERE id = '.$value["did"])->execute()->current();
			if($stock_minus["stock"] < $value["num"]){
				$item = Model_Db_Product_Detail::findProductDetailOne(array(array("id",$value["did"])));

				if($stock_minus["stock"] > 0){
					Session::set_flash("error", "大変申し訳ございません。現在【&nbsp;"
						.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
						."&nbsp;".($item->option2_name ? $item->option2_name :"")
						."&nbsp;】の在庫数が不足しております。");
				}else{
					Session::set_flash("error", "大変申し訳ございませんが、【&nbsp;"
						.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
						."&nbsp;".($item->option2_name ? $item->option2_name :"")
						."&nbsp;】お客様が注文手続きをされている間に品切れとなりました。");
				}

				// 楽天ID決済の与信をキャンセル ------------------------------>
				$rakuten_order_id = Input::post("rakuten_order_id");
				if(isset($rakuten_order_id) && $rakuten_order_id != ""){
					Model_Lib_Payment_Rakuten::cancel(Input::post("rakuten_order_id"));
				}
				// 楽天ID決済の与信をキャンセル ------------------------------>

				Response::redirect('/cart/list'.$this->session_get_param);
				exit;
			}
		}

		try {
			DB::start_transaction();

			// ---------------------------------------------------
			// 受注情報登録
			// ---------------------------------------------------
			$order_req = \Model\lib\Order::setOrderInsert($order,$this->shop_data["id"],Session::get("user.id"));

			// ---------------------------------------------------
			// 名入れ情報登録
			// ---------------------------------------------------
			$mail_names = "";
			if($this->name_count > 0){
				$ses_names = Session::get("order_data.dataname");
				$ses_names_view		= $ses_names["names_view"];
				$ses_names_value	= $ses_names["names_value"];
				\Model\lib\Order::setOrderNameInsert($order["order_id"],$ses_names);

				$mail_names .= "■ネームプリント文字\n";
				foreach ($ses_names_view as $key => $data){
					$mail_names .= "[".$ses_names_view[$key]."]".$ses_names_value[$key]."\n";
				}
			}

			// ---------------------------------------------------
			// GACKT写真集プリント対応
			// ---------------------------------------------------
			foreach ($order["products"] as $value) {
				$target_product_detail_ids = Config::get("print_product_detail_ids");
				if(isset($target_product_detail_ids)){
					foreach ($target_product_detail_ids as $target_product_detail_id) {
						if($value["did"] == $target_product_detail_id){
//							$target_cnt = $value["num"];
							$mail_names .= "■刻印内容\n";

							foreach (Session::get("print.select_ids") as $key => $val) {
								\Model\lib\Order::setOrderPrintInsert($order["order_id"],$value["data"]["base"]["id"],$value["did"],$val);
								if (count(Session::get("print.select_ids"))>1){
									$mail_names .=  "商品".($key+1) ."\n";
								}
								foreach (Session::get("print.print_data") as $printkey => $printval){
									if ($val == $printval["id"]){
										$mail_names .= "【商品名】";
										$mail_names .= $value["data"]["base"]["title"]." ".$value["data"]["option1_name"]." ".$value["data"]["option2_name"];
										$mail_names .= "\n";
										$mail_names .= "【G&LOVERS会員番号】".$printval["club_no"]."\n";
										$mail_names .= "【お名前】".$printval["name_mei"]." ".$printval["name_sei"]."\n";
										$mail_names .= "【参加公演/席番号】".$printval["param1"]." ". $printval["param2"]."\n\n";
										break;
									}
								}
							}
							$mail_names .= "※アルファベットの綴りに誤りがある場合は、大変お手数ですがご注文確定後、%%SHOP_EMAIL%%まで正しい綴りをご連絡ください。(刻印するお名前の変更はできません)\n";
						}
					}
				}
			}
			
			// ---------------------------------------------------
			// 現地受渡登録
			// ---------------------------------------------------
			$mail_receives = "";
			if($this->receives_count > 0){
				// ---------------------------------------------------
				// 現地受渡用受注レコード登録
				// ---------------------------------------------------
				$ses_receives = Session::get("order_data.datareceives.receives");
				if(!$ses_receives){
					throw new Exception("receives : NG !");
				}
				\Model\lib\Order::setOrderReceivesInsert($order["order_id"],$ses_receives);

				// ---------------------------------------------------
				// 現地受渡用在庫チェックと減算処理
				// ---------------------------------------------------
				$receives_stock_minus = DB::query('SELECT * FROM tbl_local_receives_timetable WHERE id = '.$ses_receives.' FOR UPDATE')->execute()->current();
				if($receives_stock_minus["stock"] <= 0 ){
					throw new Exception("STOCK : NG !");
				}
				DB::query('UPDATE tbl_local_receives_timetable SET stock = '.($receives_stock_minus["stock"] - 1).' WHERE id = '.$ses_receives)->execute();

				// ---------------------------------------------------
				// 注文確認メール文面作成用
				// ---------------------------------------------------
				$mail_receives .= "■会場受取日時\n";
				$mail_receives .= $receives_stock_minus["title1"] . $receives_stock_minus["title2"]."\n\n";
				$mail_receives .= "※下記配送先情報は、配送受取を選択された場合の情報となります。";

			}

			// ---------------------------------------------------
			// 在庫チェック＆在庫減算処理
			// ---------------------------------------------------
			foreach ($order["products"] as $value) {
				$stock_minus = DB::query('SELECT stock FROM tbl_product_detail WHERE id = '.$value["did"].' FOR UPDATE')->execute()->current();
				if($stock_minus["stock"] < $value["num"]){
					throw new Exception("STOCK : NG !");
				}
				DB::query('UPDATE tbl_product_detail SET stock = '.($stock_minus["stock"] - $value["num"]).' WHERE id = '.$value["did"])->execute();
			}

			// ---------------------------------------------------
			// 決済API実行
			// ---------------------------------------------------
			switch($order["payment"]){
				// ---------------------------------------------------
				// クレジットカード決済
				// ---------------------------------------------------
				case Config::get("payment.value.card"):
					$res = Model_Lib_Payment_Veritrans_Card::authorize_capture_exec($order["order_id"], intval($order["total"]), $order["card_no"],
						$order["expire_m"]."/".$order["expire_y"], $order["credit_jpo"], $order["secure_cd"], $this->veritrans_conf);
					if($res["status"] != "success")
					{
						Log::error(print_r($res,true));
						throw new Exception("Veritrans STATUS : NG !");
					}
					break;
				// ---------------------------------------------------
				// コンビニ決済
				// ---------------------------------------------------
				case Config::get("payment.value.cvs"):
					$res = Model_Lib_Payment_Veritrans_Cvs::authorize_exec($order["order_id"], intval($order["total"]), $order["cvs"],
						$order["order_username_sei"], $order["order_username_mei"], $order["order_tel1"] . $order["order_tel2"] . $order["order_tel3"],
						$order["limit_date"], $this->veritrans_conf);
					if($res["status"] != "success")
					{
						Log::error(print_r($res,true));
						throw new Exception("Veritrans STATUS : NG !");
					}
					break;
				// ---------------------------------------------------
				// 代引き決済
				// ---------------------------------------------------
				case Config::get("payment.value.cod"):
					break;
				// ---------------------------------------------------
				// 後払い決済（キャッチボール）
				// ---------------------------------------------------
				case Config::get("payment.value.ato"):
					// TODO 後払い決済メンテナンス時は強制的に与信中「0」、後払い注文ID「XXXXXXXXX」を設定する。
					if(date("YmdHis") >= Config::get("atobarai_maintenance.start") && date("YmdHis") <= Config::get("atobarai_maintenance.finish")){
						$res["order_id"] = "XXXXXXXXX";
						$res["status"] = 0;
					}else{
						$res = Model_Lib_Payment_Atobarai_Cvs::authorize_exec($this->shop_data,$order);
						if($res["status"] == "2"){
							throw new Exception("Atobarai STATUS : NG !");
						}
					}
					break;
				// ---------------------------------------------------
				// 後払い決済（NP）
				// ---------------------------------------------------
				case Config::get("payment.value.npato"):
					// NP後払いは、即時与信が無いため、受注時は全て与信中として扱う
					break;
				// ---------------------------------------------------
				// 楽天ID決済LITE
				// ---------------------------------------------------
				case Config::get("payment.value.rakuten"):

					$rakuten_status = Model_Db_Rakuten::forge();
					$rakuten_status->order_id = $order["order_id"];
					$rakuten_status->rakuten_order_id = Input::post("rakuten_order_id");
					$rakuten_status->status = 0;
					$rakuten_status and $rakuten_status->save();

					break;
			}

			// ---------------------------------------------------
			// 決済API実行結果更新
			// ---------------------------------------------------
			$order_sec = \Model\lib\Order::setOrderUpdate($order_req,$res);

			DB::commit_transaction();
			$error_flg = 0;

		}catch(\Database_exception $e){

			// 楽天ID決済の与信をキャンセル
			$rakuten_order_id = Input::post("rakuten_order_id");
			if(isset($rakuten_order_id) && $rakuten_order_id != ""){
				Model_Lib_Payment_Rakuten::cancel(Input::post("rakuten_order_id"));
			}

			Log::warning("DB ERROR ".print_r($e->getMessage(),true));
			DB::rollback_transaction();
			Session::set_flash('error', 'システムエラーが発生しました。しばらくしてから再度お試し下さい。D');
			$error_flg = 1;
			Response::redirect('/cart/list'.$this->session_get_param);

		}catch(Exception $e){

			// 楽天ID決済の与信をキャンセル
			$rakuten_order_id = Input::post("rakuten_order_id");
			if(isset($rakuten_order_id) && $rakuten_order_id != ""){
				Model_Lib_Payment_Rakuten::cancel(Input::post("rakuten_order_id"));
			}

			Log::warning("PAYMENT ERROR ".print_r($e->getMessage(),true));
			DB::rollback_transaction();
			if($order["payment"] == 1){
				Session::set_flash('error', 'クレジットカード決済に失敗しました。クレジットカード番号などをお確かめの上、再度、処理を行って下さい。');
			}elseif($order["payment"] == 4){
				Session::set_flash('error', '後払い決済に失敗しました。お名前・住所・電話番号などをお確かめください。');
			}else{
				Session::set_flash('error', '通信エラーが発生しました。再度「注文する」ボタンを押してください。（先ほどのご注文は確定しておりません）');
			}
			$error_flg = 1;
		}

		// ---------------------------------------------------
		// 決済ログ保存（決済に失敗してもログの登録は行う）
		// ---------------------------------------------------
		$log_payment = Model_Db_Log_Payment::forge(array(
			'shop_id'   => $this->shop_data["id"],
			'order_id'  => $order["order_id"],
			'payment'   => $order["payment"],
			'req_data'  => isset($res["req_data"]) ? print_r($res["req_data"],true) : "",
			'res_data'  => isset($res["res_data"]) ? print_r($res["res_data"],true) : "",
			'status'    => isset($res["status"]) ? $res["status"] : "",
		));
		$log_payment and $log_payment->save();

		// 何かしらのエラーが発生した場合は、注文確認画面へ
		if($error_flg){
			Response::redirect('/order/confirm'.$this->session_get_param);
		}

		// ---------------------------------------------------
		// メール送信
		// ---------------------------------------------------
		$shop_data = Model_Db_Shop::find($this->shop_data["id"]);
		Model_Lib_Mail::sendmail($shop_data,Config::get('mail.value.order'),$order["order_email"],
			array(
				'order_data' => $order,
				'order_data2' => $order_req,
				'username' => $order["order_username_sei"] . $order["order_username_mei"],
			),$this->deliver_term_date,$mail_names,$mail_receives
		);

		// ---------------------------------------------------
		// メルマガ登録
		// ---------------------------------------------------
		if(!empty($order["mailmagazine_flg"]) && $order["mailmagazine_flg"])
		{
			if(isset($this->shop_data["mailchimp_apikey"]) && isset($this->shop_data["mailchimp_listid"])) {

				//一度解約する。
				Model_Lib_MailChimp::unsubscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
					$order["order_email"],$order["order_username_mei"],$order["order_username_sei"]);

				if ( ! $mailmagazine = Model_Db_Mailmagazine::findEmail($order["order_email"])) {
					// 登録
					$mailmagazine = Model_Db_Mailmagazine::forge(array(
						'email'		=> $order["order_email"],
						'status'	=> Config::get('status_value.enable'),
					));
					$mailmagazine and $mailmagazine->save();
				}

				//メルマガ登録
				Model_Lib_MailChimp::subscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
					$order["order_email"],$order["order_username_mei"],$order["order_username_sei"]);
			}

			// 会員登録されていたらユーザー情報も更新する。
			if(isset($this->user_data->id)){
				if ($user_data_mm = Model_Db_User::find($this->user_data->id)) {
					$user_data_mm->mailmagazine = Config::get('status_value.enable');
					$user_data_mm and $user_data_mm->save();
				}
			}
		}

		// ---------------------------------------------------
		// 会員登録
		// ---------------------------------------------------
		if(!empty($order["regist_flg"]) && $order["regist_flg"]){
			$wk_address1 = str_replace("–","-",$order["order_address1"]);
			$wk_address1 = str_replace($order["order_state"],"",$wk_address1);

			$user = Model_Db_User::forge(array(
				'shop_id'           => $this->shop_data["id"],
				'password'          => $order["order_password"],
				'username_sei'      => $order["order_username_sei"],
				'username_mei'      => $order["order_username_mei"],
				'username_sei_kana' => $order["order_username_sei_kana"],
				'username_mei_kana' => $order["order_username_mei_kana"],
				'email'             => trim($order["order_email"]),
				'tel1'              => trim($order["order_tel1"]),
				'tel2'              => trim($order["order_tel2"]),
				'tel3'              => trim($order["order_tel3"]),
				'zip'               => $order["order_zip"],
				'mailmagazine'      => !empty($order["mailmagazine_flg"]) ? $order["mailmagazine_flg"] : 0,
				'prefecture'        => $order["order_state"],
				'address1'          => $wk_address1,
				'address2'          => $order["order_address2"] ? str_replace("–","-",$order["order_address2"]):"",
				'sex'               => !empty($order["order_sex"]) ? $order["order_sex"]:"",
				'birthday'          => $order["order_birthday"] ? $order["order_birthday"]:"",
				'status'            => Config::get("status_value.enable"),
				'funclub_flg'       => !empty($order["order_funclub_flg"]) ? $order["order_funclub_flg"] : NULL,
			));
			$user and $user->save();
			//ログイン状態にする。
			Session::set('user',array('id' => $user->id));

			// ---------------------------------------------------
			// 注文情報にユーザーIDを紐付ける
			// ---------------------------------------------------
//			\Model\lib\Order::setOrderUpdateUserID($order_req,$user->id);
			Model_Db_Order::updateUserID($order["order_id"],$user->id);
		}

		// ---------------------------------------------------
		// セッションクリア
		// ---------------------------------------------------
		Session::delete("print");		// プリント内容
		Session::delete("order");		// 注文情報
		Session::delete("order_data");	// 注文情報
		Session::delete("cart");		// カート情報

		Response::redirect('/order/finish/'.$order_req->order_id.$this->session_get_param);
	}

	/*
	 * 注文完了
	 */
	public function action_finish($id = NULL)
	{
		is_null($id) and Response::redirect("/".$this->session_get_param);

		$res_wheres = array();
		$res_wheres[] = array("order_id", $id);
		$res_wheres[] = array("update_date", ">", date("Y-m-d H:i:s",strtotime("-1 day")));
		$res_order = Model_Db_Receives_Order::findDetailFront($res_wheres);
		Log::debug(print_r($res_order,true));
		$res_flg = 1;
		if(empty($res_order)){
			$res_flg = 0;
		}
		$this->template->set_global('res_flg', $res_flg, false);

		$wheres = array();
		$wheres[] = array("order_id", $id);
		$wheres[] = array("shop_id", $this->shop_data["id"]);
		$wheres[] = array("insert_date", ">", date("Y-m-d H:i:s",strtotime("-1 day")));
		$order = Model_Db_Order::findDetailFront($wheres);
		Log::debug(print_r($order,true));
		empty($order) and Response::redirect("/".$this->session_get_param);

		$this->template->set_global('order', $order, false);

		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/order/complete');
		$this->template->title = "注文完了";
	}
}
