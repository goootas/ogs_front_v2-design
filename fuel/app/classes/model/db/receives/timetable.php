<?php
class Model_Db_Receives_Timetable extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title1',
		'title2',
		'start_date',
		'finish_date',
		'stock',
		'term_id',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_local_receives_timetable';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		return $val;
	}
	public static function findListFront($where="",$sort="")
	{
		$data = static::find('all', array(
				'where'		=> $where,
				'order_by'	=> $sort,
			)
		);
		return $data;
	}

}
