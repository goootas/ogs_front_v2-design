<?php echo Form::open("/order/input1");?>
<div>
	<h4>ネームプリント文字入力</h4>
	<hr>
	<?php $ses_name = Session::get("order_data.dataname.names_value");?>
	<?php $counter = 0;?>
	<?php foreach ($cart as $key => $data): ?>
		<?php if(array_search($data["id"], Config::get("naire_product_ids")) !== false): ?>
<!--		--><?php //if($data["id"] == 191 || $data["id"] == 1191): ?>
			<?php for ($i = 0; $i < $data["num"]; $i++) { ?>
				<?php echo $data["title"].' '.$data["op1"].' '.$data["op2"]; ?><br>
				<?php echo Form::input('names_value[]',$ses_name[$counter],array( "istyle"=>"3","format"=>"*x","MODE"=>"alphabet")); ?>
				<?php echo Form::hidden('names_view[]',$data["title"].' '.$data["op1"].' '.$data["op2"]); ?>
				<?php echo Form::hidden('names_did[]',$data["did"]); ?><br>
				<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name.'.$i)); ?></font><br>
				<br>
				<?php $counter++;?>
			<?php } ?>
		<?php endif;?>
	<?php endforeach; ?>

	<div align="center">
		<img src="/gackt/assets/img/gackt/GC039_06_fp.jpg" width="100%"/>
	</div>

	<font style="color: #FF0000">
		<b>
			※プリント可能文字種<br>
			・半角英数字(大文字/小文字)<br>
			・半角記号（!”#$%&’()=~|`{+}<>?_-^¥@[:];,./_）<br>
			・半角スペース(空白)<br>
			(文字フォント / カラー指定不可)<br>
			<br>
			※プリント可能最大文字数(空白含む)<br>
			・LVL GUEST PASS Tシャツ：17文字<br>
			・タンクトップ (loves GACKT)：15文字<br>
			<br>
			※カート画面に戻ると入力文字はリセットされます<br>
			<br>
			※必ずご注文全該当商品分の入力をお願いします<br>
			<br>
			※ご注文確定後のプリント内容変更はできません<br>
		</b>
	</font>

	<br>
	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>
	<br>
	<div>
		<?php echo Html::anchor('/cart'.$session_get_param, '<font color="#FFFFFF">カートに戻る</font>'); ?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
