<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta charset="utf-8">
	<?php if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<?php } else { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php } ?>
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
	<?php
	echo Asset::css(
		array(
			Config::get("assets_url").'css/reset.css',
			$shop_data["dir"].'/common.css',
			$shop_data["dir"].'/'.$shop_data["dir"].'.css',
			Config::get("assets_url").'css/validationEngine.jquery.min.css',
		)
	);
	if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::css(
			array(
				Config::get("assets_url").'css/common-ie.css'
			)
		);
	}
	echo Asset::js(
		array(
			Config::get("assets_url").'js/jquery.1.8.2.min.js',
			Config::get("assets_url").'js/common.js',
			Config::get("assets_url").'js/jquery.validationEngine-ja.min.js',
			Config::get("assets_url").'js/jquery.validationEngine.min.js',
		)
	);
	if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::js(
			array(
				Config::get("assets_url").'js/jquery.innerfade.js',
				Config::get("assets_url").'js/masonry.pkgd.min.js',
				Config::get("assets_url").'js/jquery.autopager-1.0.0.js'
			)
		);
	}
	?>
	<style>
		<!--
		.form-group input[type="tel"],.form-group input[type="email"],
		.form-group input[type="text"], .form-group input[type="password"], .form-group textarea {
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
			padding: 12px;
			width: 90%;
		}
		.SPECIAL {
			padding: 18px 0px 0px;
			margin: 0 auto;
			max-width: 960px;
		}
		@media screen and (max-width: 1024px) {
			.SPECIAL {
				padding: 18px 20px 0px;
			}
		}
		-->
	</style>
	<?php if(Request::active()->controller != 'Controller_Top') :?>
		<style>
			.scheduleimg {
				height: 58px;
			}
		</style>
	<?php endif; ?>
	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<meta property="og:title" content="<?php echo $shop_data["name"] ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'];?>"/>
		<meta property="og:image" content="<?php echo "https://" . $_SERVER["HTTP_HOST"]."/naoto/assets/img/naoto/naoto_1511_item.jpg"; ?>"/>
		<meta property="og:image" content="<?php echo "https://" . $_SERVER["HTTP_HOST"]."/naoto/assets/img/naoto/bunner_1170x500.jpg"; ?>"/>
		<meta property="og:site_name" content="<?php echo $shop_data["name"] ?>"/>
	<?php endif; ?>

	<?php //TODO:OGタグの内容確認お願いします。 ?>
	<?php if(Request::active()->controller == 'Controller_Product' and in_array(Request::active()->action, array('detail'))) :?>
		<meta property="fb:app_id" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:title" content="<?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'] . "?" . $_SERVER['QUERY_STRING']; ?>"/>
		<meta property="og:image" content="<?php echo "https://" . $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/" . Config::get("aws.image_path") . $product->id . "/" . $product->id . "_000.jpg"; ?>"/>
		<meta property="og:site_name" content="<?php echo $shop_data["name"] ?>"/>
	<?php endif; ?>

</head>
<body>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo $shop_data["ga_tag"]?>', 'auto');
	ga('require', 'linkid', 'linkid.js');
	ga('send', 'pageview');

</script>
<div id="" class="WRAPPER">
	<div class="HEADER MIN">
		<div class="MAINTITLE">
			<div class="TITLE">
				<?php if(Request::active()->controller == 'Controller_Top') :?>
					<style>
						@media screen and (max-width: 728px) {
							.BUYSCHEDULE {
								display: block;
								bottom: 0;
								padding: 0px;
								position: absolute;
								width: 100%;
								color: #408578;
								margin-bottom: 3px;
							}
						}
						@media screen and (max-width: 1024px) {
							.BUYSCHEDULE {
								display: block;
								bottom: 0;
								padding: 0px;
								position: absolute;
								width: 100%;
								color: #408578;
								margin-bottom: 3px;
							}
						}
					</style>
					<h2><?php echo Html::anchor('/', Asset::img($shop_data["dir"].'/tourtitle_dome.png', array('id' => 'logo'))); ?></h2>
				<?php else : ?>
					<h2><?php echo Html::anchor('/', Asset::img($shop_data["dir"].'/tourtitle_dome.png', array('id' => 'logo'))); ?></h2>
				<?php endif; ?>
			</div>
<!--			--><?php //echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
			<style>
				@media screen and (max-width: 728px) {
					.scheduleimg {
						width: 80%;
					}
				}
				@media screen and (max-width: 1024px) {
					.scheduleimg {
						width: 80%;
					}
				}
			</style>
			<div class="BUYSCHEDULE  stroke">
				<div class="inner stroke_text" style="text-align: center;margin: 0 auto;">
					<?php if(date("Ymd") <= 20160115){?>
						<img src="/naoto/assets/img/naoto/naoto_store_uketsukekikan-3.png" style="display: inline;" class="scheduleimg">
					<?php }elseif(date("Ymd") >= 20160116 && date("Ymd") < 20160201 ){?>
						<img src="/naoto/assets/img/naoto/naoto_store_uketsukekikan-5.png" style="display: inline;" class="scheduleimg">
					<?php }else{?>
						<img src="/naoto/assets/img/naoto/naoto_store_uketsukekikan-6.png" style="display: inline;" class="scheduleimg">
					<?php }?>
				</div>
			</div>
		</div>
		<div class="NAVIGATION">
			<div class="GLOBAL">
				<div class="CONTAINER">
					<div class="buttons">
						<?php if(Session::get('user.id')) : ?>
							<?php echo Html::anchor('mypage', '<span>マイページ</span>', array("class" => "button text with-icon mypage"), true); ?>
							<?php echo Html::anchor('logout', '<span>ログアウト</span>', array("class" => "button text with-icon logout"), true); ?>
						<?php else: ?>
							<?php echo Html::anchor('login', '<span>ログイン</span>', array("class" => "button text with-icon login"), true); ?>
						<?php endif; ?>
						<?php echo Html::anchor('cart', '<span>マイカート</span>',array("class" => "button key rect with-icon cart")); ?>
					</div>
				</div>
			</div>
			<div class="CATE">
				<div class="CONTAINER">
					<div class="buttons"><?php echo Html::anchor('/', "<span style='font-weight: bold;'>ALL</span>", array("class" => "button text all"), true); ?></div>
					<div class="buttons"> <a class="button text categories" href="#"><span style='font-weight: bold;'>カテゴリー一覧</span></a> </div>
					<div class="navigation">
						<ul >
							<?php if($category1): ?>
								<?php foreach($category1 as $c1): ?>
									<li><?php echo Html::anchor('/product/list?c1='.$c1->id, "<span style='font-weight: bold;'>".$c1->title."</span>") ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="SPECIAL">
			<img id="logo" src="/naoto/assets/img/naoto/bunner_poster_1000x300.png" alt="" />
		</div>
	<?php endif; ?>

	<div class="CONTENTS">
		<div class="CONTAINER">
			<?php echo $content; ?>
		</div>
	</div>
	<div class="FOOTER">
		<?php echo isset($shop_desc->pcsp_top2) ? $shop_desc->pcsp_top2 : "";?>
		<div class="LINKS">
			<div class="CONTAINER">
				<div class="navigation">
					<ul>
						<li><?php echo Html::anchor('/info/guide', '<span>ショッピングガイド</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/privacy', '<span>プライバシーポリシー</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/legal', '<span>特定商取引法に基づく表記</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/inquiry', '<span>お問い合わせ</span>', "", true) ?></li>
						<li><?php echo Html::anchor('http://www.nananaoto.com/', '<span>ナオト・インティライミ OFFICIAL WEB</span>',  array("target" => "_blank")) ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="CREDIT">
			<div class="CONTAINER">
				<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI Co.,Ltd.";?> All Rights Reserved</p>
			</div>
		</div>
	</div>
</div>
</body>
</html>