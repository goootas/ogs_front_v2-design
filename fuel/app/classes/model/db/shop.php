<?php
class Model_Db_Shop extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'domain',
		'mail',
		's3bucket',
		'ga_tag',
		'dir',
		'prefix',
		'keywords',
		'description',
		'copyright',
		'facebook_id',
		'type',
		'free_deliver_price',
		'atobarai_enterprise_id',
		'atobarai_site_id',
		'atobarai_api_user_id',
		'merchant_ccid',
		'merchant_secret_key',
		'dummy_request',
		'helpscout_apikey',
		'helpscout_mailboxno',
		'mailchimp_apikey',
		'mailchimp_listid',
		'basic_auth_flg',
		'basic_start_date',
		'basic_end_date',
		'basic_account',
		'basic_password',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add('name', '店舗名')->add_rule('required');
		$val->add('status', '状態')->add_rule('required');

		return $val;
	}

	/*
	 *
	 */
	public static function findShopInfoByDomain($domain) {
		$where = array(
			'domain' => $domain,
			'status' => 1,
		);

		return DB::select()->
		from(self::$_table_name)->
		where($where)->
		execute('default')->
		as_array();
	}

	/*
	 *
	 */
	public static function findShopList() {
		$where = array(
			'status' => 1,
		);

		$data = DB::select("id","name")->
		from(self::$_table_name)->
		where($where)->
		execute('default')->
		as_array();
		$list = array();
		foreach ($data as $val) {
			$list[$val["id"]] = $val["id"]. ":" . $val["name"];
		}
		return $list;
	}

	/*
	 *
	 */
	public static function findShopDetail($shop_id)
	{
		$sql = "SELECT * FROM ".self::$_table_name." WHERE id = ".$shop_id;
		return \Fuel\Core\DB::query($sql)->execute('default')->current();
	}

}

