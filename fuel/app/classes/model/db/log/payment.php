<?php
class Model_Db_Log_Payment extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'payment',
		'req_data',
		'res_data',
		'status',
		'insert_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
	);
	protected static $_table_name = 'log_payment';

}
