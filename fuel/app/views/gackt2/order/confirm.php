<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php if(Session::get("order_data.data3.payment") != 6 ) : ?>
	<div class="confirm-panel">
		<div class="panel-body">
			<div class="description">
				この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
			</div>
			<div class="buttons count-2">
				<?php echo Html::anchor('/order/input3', '前に戻る',array('class' => 'button rect size-L'),true); ?>
				<div class="button rect key size-L"><?php echo Form::button('final','注文する',array('id' => 'next','class' => '')); ?></div>
			</div>
			<div class="description">
				<br>
				<p style="color: #FF0000">「注文する」押下後の取り消し / お支払い方法変更 / ご注文商品変更等は承ることができません。</p>
			</div>
		</div>
	</div>
<?php else :?>


<?php endif;?>

<div class="cart-list">
	<h4 class="headline">カート</h4>
	<table>
		<?php $total_tax = 0; ?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="delete"></th>
			<th class="total">小計<br />&nbsp;</th>
		</tr>
		</thead>
		<tbody id="table-tbody">
		<?php foreach ($cart as $data): ?>
			<?php $total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);?>
			<tr class="list">
				<td>
					<?php
					$anchor_txt = "";
					$anchor_txt .= $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
					?>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td>
					<?php echo number_format($data["num"]);?>&nbsp;&nbsp;
				</td>
				<td></td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
			</tr>
		<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="totals">
		<ul>
			<li>
				<dl><dt>商品合計</dt><dd>¥<?php echo number_format(floor($total_tax));?></dd></dl>
			</li>
			<li>
				<dl><dt>送料</dt><dd>¥<?php echo number_format(intval($postage));?></dd></dl>
			</li>
			<li>
				<dl><dt>決済手数料</dt><dd>¥<?php echo number_format(intval($fee));?></dd></dl>
			</li>
			<li class="all">
				<dl><dt>総合計</dt><dd>¥<?php echo number_format(intval($total));?></dd></dl>
			</li>
		</ul>
	</div>

	<div class="buttons change">
		<?php echo Html::anchor('/cart/list/1','カート内容変更',array('class' => 'button rect key')) ?>
	</div>
	<?php if(Session::get("payment_ctl_cnt") < 3):?>
		<div class="description">
			<p style="color: #FF0000">
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			</p>
		</div>
	<?php endif; ?>

</div>

<?php if( $name_count > 0) :?>
	<div class="fixed-data">
		<h4 class="headline">ネームプリント文字</h4>

		<table>

			<?php $ses_name_view	= Session::get("order_data.dataname.names_view");?>
			<?php $ses_name_value	= Session::get("order_data.dataname.names_value");?>
			<?php foreach ($ses_name_view as $key => $data): ?>
				<tr>
					<td class="head"><?php echo $ses_name_view[$key]?></td>
					<td class="body"><?php echo $ses_name_value[$key]?></td>
				</tr>
			<?php endforeach; ?>

		</table>
		<div class="buttons change">
			<?php echo Html::anchor('/order/inputname/1','ネームプリント文字変更',array('class' => 'button rect key')) ?>
		</div>
	</div>
<?php endif;?>


<div class="fixed-data">
	<h4 class="headline">お客様情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_username_sei");?> <?php echo Session::get("order_data.data1.order_username_mei");?> 様</td>
		</tr>
		<tr>
			<td class="head">ふりがな</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_username_sei_kana");?> <?php echo Session::get("order_data.data1.order_username_mei_kana");?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr(Session::get("order_data.data1.order_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data1.order_zip"), 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_state");?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_address1");?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_address2");?></td>
		</tr>
		<tr>
			<td class="head">メールアドレス</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_email");?><br></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo Session::get("order_data.data1.order_tel1");?>-<?php echo Session::get("order_data.data1.order_tel2");?>-<?php echo Session::get("order_data.data1.order_tel3");?></td>
		</tr>
		<tr>
			<td class="head">性別</td>
			<td class="body">
				<?php if (Session::get("order_data.data1.order_sex")=="1"):?>男性
				<?php elseif (Session::get("order_data.data1.order_sex")=="2"):?>女性
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
		<tr>
			<td class="head">生年月日</td>
			<td class="body">
				<?php if (Session::get("order_data.data1.order_birthday")):?>
					<?php echo substr(Session::get("order_data.data1.order_birthday"), 0,4);?>年<?php echo substr(Session::get("order_data.data1.order_birthday"), 4,2);?>月<?php echo substr(Session::get("order_data.data1.order_birthday"), 6,2);?>日
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
<!--		<tr>-->
<!--			<td class="head">ツアー参加予定公演</td>-->
<!--			<td class="body">-->
<!--				--><?php //if (date("Ymd") < "20160412") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--4/21 広島公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--4/23 岡山公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160412" && date("Ymd") <= "20160418") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--4/29,30 札幌公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--5/3,4 大阪公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160419" && date("Ymd") <= "20160425") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--5/3,4 大阪公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--5/7,8 栃木公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160426" && date("Ymd") <= "20160502") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--5/12 東京公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--5/14 ,15 名古屋公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160503" && date("Ymd") <= "20160508") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--5/21 松山公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--5/22 高松公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160509" && date("Ymd") <= "20160515") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--5/27 東京公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--6/3 岩手公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160516" && date("Ymd") <= "20160522") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--6/7 大宮公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--6/10 広島公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160523" && date("Ymd") <= "20160529") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--6/10 広島公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--6/12 山口公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160530" && date("Ymd") <= "20160605") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--6/17 宮崎公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--6/19 福岡公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160606" && date("Ymd") <= "20160609") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--6/25 沖縄公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--6/26 沖縄公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //elseif (date("Ymd") >= "20160610" && date("Ymd") <= "20160619") :?>
<!--					--><?php //if (Session::get("order_data.data1.order_event_flg")=="1"):?><!--7/2 埼玉公演<br>-->
<!--					--><?php //elseif (Session::get("order_data.data1.order_event_flg")=="2"):?><!--7/3 埼玉公演<br>-->
<!--					--><?php //else:?><!--その他公演 / 参加予定無し<br>-->
<!--					--><?php //endif;?>
<!--				--><?php //endif;?>
<!--			</td>-->
<!--		</tr>-->
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input1/1','お客様情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>


<div class="fixed-data">
	<h4 class="headline">お届け先情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_username_sei");?> <?php echo Session::get("order_data.data2.deliver_username_mei");?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr(Session::get("order_data.data2.deliver_zip"), 0,3);?>-<?php echo substr(Session::get("order_data.data2.deliver_zip"), 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_state");?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_address1");?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_address2");?></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo Session::get("order_data.data2.deliver_tel1");?>-<?php echo Session::get("order_data.data2.deliver_tel2");?>-<?php echo Session::get("order_data.data2.deliver_tel3");?></td>
		</tr>
		<tr>
			<td class="head">お届け日</td>
			<td class="body">
				<?php
				if(isset($deliver_term_date)){
					echo $deliver_term_date->deliver_start_text;
				}else{
					?>
					<?php if(!Session::get("order_data.data2.delivery_date")) : ?>
						<?php if(date("Ymd") >= "20151228" && date("Ymd") <= "20160103"):?>
							1/5(火)以降順次発送
						<?php else : ?>
							受注日翌日から2日以内で発送（土日祝日除く / 発送からお届けは1〜3日）
						<?php endif; ?>
					<?php else : ?>
						<?php echo sprintf("%04d年%02d月%02d日",
							substr(Session::get("order_data.data2.delivery_date"),0,4),
							substr(Session::get("order_data.data2.delivery_date"),4,2),
							substr(Session::get("order_data.data2.delivery_date"),6,2));?>
					<?php endif; ?>
					<?php } ?>
			</td>
		</tr>
		<tr>
			<td class="head">お届け希望時間帯</td>
			<td class="body">
				<?php
				if(isset($deliver_term_date)){
					echo "指定不可";
				}else{
					?>
					<?php if(!Session::get("order_data.data2.delivery_time")) : ?>
						指定無し
					<?php else : ?>
						<?php if(Session::get("order_data.data2.delivery_time") == "AM") : ?>
							<?php echo "午前中";?>
						<?php else : ?>
							<?php $dt = explode("-",Session::get("order_data.data2.delivery_time"));?>
							<?php echo sprintf("%d時〜%d時",$dt[0],$dt[1]);?>
						<?php endif; ?>
					<?php endif; ?>
				<?php }?>
			</td>
		</tr>
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input2/1','お届け先情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>

<div class="fixed-data">
	<h4 class="headline">お支払情報</h4>
	<table>
		<tr>
			<td class="head">支払方法</td>
			<td class="body">
				<?php echo Config::get("payment.".Session::get("order_data.data3.payment"));?>
				<?php if(Session::get("order_data.data3.payment") == 1) echo Config::get("jpo.".Session::get("order_data.data3.credit_jpo"));?>
				<?php if(Session::get("order_data.data3.payment") == 2) echo Config::get("cvs.".Session::get("order_data.data3.cvs"));?>
			</td>
		</tr>
	</table>
	<div class="buttons change">
		<?php echo Html::anchor('/order/input3/1','お支払情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>

<?php if(Session::get("order_data.data3.payment") != 6 ) : ?>
	<div class="confirm-panel">
		<div class="panel-body">
			<div class="description">
				この内容でよろしければ<br>「注文する」ボタンを押してください。<br>
			</div>
			<div class="buttons count-2">
				<?php echo Html::anchor('/order/input3', '前に戻る',array('class' => 'button rect size-L'),true); ?>
				<div class="button rect key size-L"><?php echo Form::button('final','注文する',array('id' => 'next','class' => '')); ?></div>
			</div>
			<div class="description">
				<br>
				<p style="color: #FF0000">「注文する」押下後の取り消し / お支払い方法変更 / ご注文商品変更等は承ることができません。</p>
			</div>
		</div>
	</div>

<?php else :?>

	<div class="confirm-panel">
		<div class="panel-body">
			<div class="description">
				この内容でよろしければ<br>「お支払い」ボタンを押してください。<br>
				<br>
				<script
					src="<?php echo $rakuten_conf["js_url"]?>"
					data-key="<?php echo $rakuten_conf["public_key"]?>"
					class="checkout-lite-button"
					data-callback="callback"
					data-cart-id="<?php echo 1111 ;//$rakuten_order_data["order_id"]?>"
					<?php foreach ($rakuten_order_data["products"] as $key => $product):?>
						data-item-id-<?php echo $key + 1 ;?>="<?php echo $product["did"]?>"
						data-item-name-<?php echo $key + 1 ;?>="<?php echo $product["data"]["base"]["title"] . $product["data"]["option1_name"]. $product["data"]["option2_name"]?>"
						data-item-unit-price-<?php echo $key + 1 ;?>="<?php echo $product["price"] / $product["num"]?>"
						data-item-quantity-<?php echo $key + 1 ;?>="<?php echo $product["num"]?>"
					<?php endforeach;?>
					data-item-id-<?php echo $key + 2 ;?>="0000"
					data-item-name-<?php echo $key + 2 ;?>="送料"
					data-item-unit-price-<?php echo $key + 2 ;?>="<?php echo $rakuten_order_data["postage"]?>"
					data-item-quantity-<?php echo $key + 2 ;?>="1"
				></script>
<!--				<br>-->
<!--				--><?php //echo Html::anchor('/order/input3', '前に戻る',array('class' => 'button rect size-L'),true); ?>
				<br>
				<p style="color: #FF0000">「お支払い」押下後の取り消し / お支払い方法変更 / ご注文商品変更等は承ることができません。</p>
			</div>
		</div>
	</div>

<?php endif;?>

<?php echo Form::open(array('action' => '/order/exec', 'id' => "order_final" ,'method' => 'post'));?>
<input type="hidden" id="rakuten_order_id" name="rakuten_order_id" value="">
<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("[id=next]").attr('disabled',true);
			$('#order_final').submit();
		});
	});

	function callback(data){
//		console.log(data);
		$(".button").attr('disabled',true);
		$('#rakuten_order_id').val(data.id);
		$('#order_final').submit();
	}
</script>
