<div class="panel forget">
    <h4 class="subheadline">パスワード再設定</h4>
    <div class="description">
        <p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
        <p>新しいパスワードを入力して、設定ボタンを押してください。</p>
    </div>
    <div class="forms">
    
        <?php echo Form::open(array('id' => 'forget' ,'class'=>'form-horizontal' ,'autocomplete'=>'off' ));?>

                <div class="form-group">
                    <?php echo Form::label('メールアドレス', 'email',array("class" => "col-sm-2 col-md-offset-1 control-label")); ?>
					<div class="form-container">
						<span style="height:40px; line-height:40px;"><?php echo Session::get('forget.email'); ?>&nbsp;</span>
					</div>
                </div>

    
                <div class="form-group">
                    <?php echo Form::label('新しいパスワード', 'password',array("class" => "col-sm-2 col-md-offset-1 control-label")); ?>
			        <div class="form-container">
                        <div class="inner">
                            <?php echo Form::password('password', Input::post('password', ''),
                                array('id'=>'password','class' => 'validate[required,minSize[3],maxSize[12]] form-control','placeholder' =>'3〜12文字の英数字')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo Form::label('新しいパスワード(確認用)', 'password',array("class" => "col-sm-2 col-md-offset-1 control-label")); ?>
			        <div class="form-container">
                        <div class="inner">
                            <?php echo Form::password('password', Input::post('password2', ''),
                                array('class' => 'validate[required,equals[password],minSize[3],maxSize[12]] form-control','placeholder' =>'3〜12文字の英数字')); ?>
                        </div>
						<div class="description">※確認用に再度同じパスワードを入力してください。</div>
                    </div>
                </div>
    
    
                <div class="buttons">
                    <?php echo Form::submit('exec', '設定', array('class' => 'button rect key size-L')); ?><br>
                </div>

        <?php echo Form::close();?>
    </div>
</div>
<script>
	$(function(){
		$("#forget").validationEngine({promptPosition : "topLeft"});
	});
</script>

