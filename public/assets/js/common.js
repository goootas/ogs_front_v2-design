$(function($){
	//フローティングヘッダー
	function floatingHeader(){
		var nvH = $(".NAVIGATION").height();
		var hdH = $(".HEADER").height();
		var flag =  hdH;
		if ($(window).scrollTop() > flag) {
			$(".NAVIGATION").addClass("fixed")
		}else{
			$(".NAVIGATION").removeClass("fixed")
		}
	}
	function footerHeight(){
		var height = ($('.FOOTER').height()) + 40
		$('.CONTENTS').css("padding-bottom",height + "px");
	}
	//スクロール時実行
	$(window).scroll(function() {
		//floatingHeader()
	});

	//カテゴリーメニュー開閉
	$(".button.categories").click(function () {
		$(".NAVIGATION .CATE .navigation").slideToggle("fast");
	});
	$(".button.categories").click(function () {
		$(".NAVIGATION2 .CATE2 .navigation").slideToggle("fast");
	});
	$(".oac-menu").click(function () {
		$(".NAVIGATION").slideToggle("fast");
	});

	$(".FOOTERMENU .button").click(function () {
		$('.FOOTER').toggleClass("hidden");
	});

	$(window).load(function () {
		//footerHeight();
	});
	//リサイズ時実行
	var timer = false;
	$(window).resize(function() {
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function() {
			//footerHeight();
		}, 1000);
	});

});
