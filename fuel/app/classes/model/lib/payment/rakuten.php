<?php
/*
 * 楽天ID決済LITE用決済モジュール
 */
class Model_Lib_Payment_Rakuten extends \Model
{
	/*
	 * キャンセル
	 * 在庫不足、または何らかのエラーで処理が中断した場合、キャンセル処理を行う。
	 * ※キャンセル処理を行うと楽天側で購入者へキャンセルメールを送信
	 */
	public static function cancel($rakuten_order_id)
	{
		Config::load("common");
		$rakuten_conf = \Fuel\Core\Config::get("rakuten");

		// 置換
		$url = $rakuten_conf["api_url"].$rakuten_conf["api_action"]["cancel"];
		$url = str_replace("%%CHARGE_ID%%", $rakuten_order_id, $url);
		\Fuel\Core\Log::debug("URL : " . print_r($url,true));

		$json = self::_curl_exec($url,$rakuten_conf["private_key"]);

		$response = json_decode($json,true);
		\Fuel\Core\Log::debug(print_r($response,true));

		$response["res_data"] = json_encode($response);
		$response["req_data"] = "";
		if(isset($response["error"])){
			$response["order_id"] = "";
			$response["status"] = "error";
		}
//		$response["order_status"] = $order_status;
		return $response;

	}

	/*
	 * CURL通信
	 */
	private static function _curl_exec($url,$params)
	{
		$curl = curl_init($url);

		$options = array(
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT => 5,
			CURLOPT_USERNAME => $params
//			CURLOPT_POSTFIELDS => http_build_query($params),
		);

		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);
//		$info   = curl_getinfo($curl);

		return $result;
	}

}
