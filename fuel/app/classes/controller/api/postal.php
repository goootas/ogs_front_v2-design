<?php
class Controller_Api_Postal extends Controller_Baseapi
{
	public function get_address()
	{
		$data = \Model\lib\Postal::getPostalZip(Input::get("term"));
		return $this->response( $data );
	}
}
