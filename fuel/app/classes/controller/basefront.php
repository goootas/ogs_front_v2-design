<?php
use Fuel\Core\Cache;
use Fuel\Core\DB;
use Fuel\Core\Input;
use Fuel\Core\Log;
use Fuel\Core\Session;
use Model\lib\Request;

class Controller_Basefront extends Controller_Template
{
	protected $tax = 1.08;
	protected $term_data;
	protected $agent;
	protected $agent_dir;
	protected $shop_data;
	protected $shop_desc_data;
	protected $shop_receives_data;
	protected $user_data;
	protected $session_get_param;
	protected $veritrans_conf;
	protected $receives_count;
	protected $cvs_close_time;

	public function before()
	{
		parent::before();

		// 共通設定ファイル読み込み
		Config::load("common");

		// ---------------------------------------------------
		// 店舗情報取得
		// -------------------------------------------------
		$http_host = explode(":",Input::server("HTTP_HOST"));
//		$shop_domains = Model_Db_Shop::findShopInfoByDomain($http_host[0]);
		try
		{
			$shop_domains = Cache::get("shop_domain_".$http_host[0]);
		}
		catch (\CacheNotFoundException $e)
		{
			$shop_domains = Model_Db_Shop::findShopInfoByDomain($http_host[0]);
			Cache::set("shop_domain_".$http_host[0], $shop_domains, 60 * 2);
		}

		if(count($shop_domains)>1){
			$shop_dirs = explode("/",Input::server("REQUEST_URI"));
			$shop_dir = $shop_dirs[1];
			foreach($shop_domains as $val){
				if($val["dir"] == $shop_dir){
					$this->shop_data = $val;
					break;
				}
			}
		}else{
			$this->shop_data = $shop_domains[0];
		}

		if(!$this->shop_data){
			Response::redirect('top/index.html');
			exit;
		}
		$this->template->set_global('shop_data', $this->shop_data, false);

		// ---------------------------------------------------
		// セッション設定変更
		// ---------------------------------------------------
		Config::load('session', true, false, true);
		Config::set("session.".Config::get("session.driver").".cookie_name" ,
			$this->shop_data["id"]."_". Config::get("session.".Config::get("session.driver").".cookie_name"));
		Config::set("session.post_cookie_name" ,Config::get("session.".Config::get("session.driver").".cookie_name"));

		Session::instance();

		// ---------------------------------------------------
		// UserAgent※ガラ用テンプレート切替用※セッション引き回しの処理もここで行う
		// ---------------------------------------------------
		$this->agent_dir = "";
		$session_name	= "";
		$session_key	= "";
		if (strripos(Input::user_agent(),"docomo") !== false ||
			strripos(Input::user_agent(),"kddi") !== false ||
			strripos(Input::user_agent(),"vodafone") !== false ||
			strripos(Input::user_agent(),"softbank") !== false){

			Request::Encode($_POST);
			$this->agent_dir = "fp/";
			Config::load("session");
			$session_name = Config::get("session.".Config::get("session.driver").".cookie_name");
			$session_key	= Session::key();
			$this->session_get_param = "?".$session_name."=".$session_key;

			// FP用のGAタグ
			$fp_ga_tag = \Model\lib\Ga::getFeatureTag($this->shop_data["ga_tag"] , Input::server("REQUEST_URI"));
			$this->template->set_global('fp_ga_tag', $fp_ga_tag, false);

		}

		$this->template->set_global('session_key', $session_key, false);
		$this->template->set_global('session_name', $session_name, false);
		$this->template->set_global('session_get_param', $this->session_get_param, false);

		$this->template = View::forge($this->agent_dir.$this->shop_data["dir"].'/template');

		// ---------------------------------------------------
		// UserAgent
		// ---------------------------------------------------
		$this->agent = "pc";
		if(strripos(Input::user_agent(),"iphone") or
			strripos(Input::user_agent(),"android") !== false && strripos(Input::user_agent(),"mobile") !== false){
			$this->agent = "sp";
		}
		$this->template->set_global('agent', $this->agent, false);

		// ---------------------------------------------------
		// 強制SSL通信 TODO:必要な箇所のみSSL通信でもいいかも
		// ---------------------------------------------------
//		Log::debug(print_r($_SERVER,true));
//		Log::debug(print_r(Input::all(),true));
		if(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])){
			if($_SERVER['HTTP_X_FORWARDED_PROTO'] != 'https'){
				Response::redirect('https://'.Input::server('SERVER_NAME').Input::server("REQUEST_URI"));
			}
		}else{
			if ($_SERVER['SERVER_PORT'] != "443") {
				Response::redirect('https://'.Input::server('SERVER_NAME').Input::server("REQUEST_URI"));
			}
		}

		// ---------------------------------------------------
		// Basic認証設定
		// ---------------------------------------------------
		$basic_auth_flg = 0;
		$basic_account	= Config::get("basic.user");
		$basic_password	= Config::get("basic.pass");
		if(strstr($_SERVER["SERVER_NAME"],"dev.") || strstr($_SERVER["SERVER_NAME"],"test")){
			$basic_auth_flg = 1;
		}else{
			if($this->shop_data["basic_auth_flg"]){

				$start_dt	= $this->shop_data["basic_start_date"];
				if(!$start_dt){
					$start_dt = date("Y-m-d H:i:s",strtotime('1970-01-01 00:00:00'));
				}
				$end_dt		= $this->shop_data["basic_end_date"];
				if(!$end_dt){
					$end_dt = date("Y-m-d H:i:s",strtotime("1 minute"));
				}

				if(strtotime($start_dt) <= strtotime(date("Y-m-d H:i:s")) && strtotime($end_dt) >= strtotime(date("Y-m-d H:i:s")) ){
					$basic_auth_flg = 1;
				}
				$basic_account	= isset($this->shop_data["basic_account"]) ? $this->shop_data["basic_account"] : $basic_account;
				$basic_password	= isset($this->shop_data["basic_password"]) ? $this->shop_data["basic_password"] : $basic_password;
			}
		}
		if($basic_auth_flg){
			// basic認証
			if (!isset($_SERVER['PHP_AUTH_USER'])){
				header('WWW-Authenticate: Basic realm="Private Page"');
				header('HTTP/1.0 401 Unauthorized');
				die('このページを見るにはログインが必要です');
			}else{
				if ($_SERVER['PHP_AUTH_USER'] != $basic_account || $_SERVER['PHP_AUTH_PW'] != $basic_password){
					header('WWW-Authenticate: Basic realm="Private Page"');
					header('HTTP/1.0 401 Unauthorized');
					die('このページを見るにはログインが必要です');
				}
			}
		}

		// ---------------------------------------------------
		// ディレクトリ型の場合、ベースURLを変更する。
		// ---------------------------------------------------
		Config::set("base_url" ,"/");
		if($this->shop_data["type"]){
			Config::set("base_url" ,"/".$this->shop_data["dir"]."/");
		}

		// ---------------------------------------------------
		// ベリトランス情報取得
		// ---------------------------------------------------
		$this->veritrans_conf["merchant_ccid"] =
			isset($this->shop_data["merchant_ccid"]) ? $this->shop_data["merchant_ccid"] : "A100000000000001069951cc";
		$this->veritrans_conf["merchant_secret_key"] =
			isset($this->shop_data["merchant_secret_key"]) ? $this->shop_data["merchant_secret_key"] : "ca7174bea6c9a07102fa990cfba330d0dad579a7c13a974fa7c3ec0ff66c1d6f";
		$this->veritrans_conf["dummy_request"] =
			isset($this->shop_data["dummy_request"]) ? $this->shop_data["dummy_request"] : "1";

//		Log::debug(print_r("SHOP DATA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",true));
//		Log::debug(print_r( $this->shop_data,true));
//		Log::debug(print_r("VT DATA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",true));
//		Log::debug(print_r( $this->veritrans_conf,true));

		// ---------------------------------------------------
		// 商品詳細の画像一覧取得のためS3のバスケット名設定
		// ---------------------------------------------------
		Config::set("aws.buckets",$this->shop_data["s3bucket"]);

		// ---------------------------------------------------
		// カテゴリ情報取得
		// ---------------------------------------------------
		try
		{
			$category1 = Cache::get("shop_category1_".$this->shop_data["id"]);
		}
		catch (\CacheNotFoundException $e)
		{
			$wheres = array();
			$wheres[] = array("status",Config::get('status_value.enable'));
			$wheres[] = array("shop_id", $this->shop_data["id"]);
			$orders = array();
			$orders[] = array("sort","asc");
			$category1 = Model_Db_Category1::findListFront($wheres,$orders);
			Cache::set("shop_category1_".$this->shop_data["id"], $category1, 60 * 5);
		}
		$this->template->set_global('category1', $category1, false);

		// ---------------------------------------------------
		// 都道府県情報取得
		// ---------------------------------------------------
		try
		{
			$prefecture_data = Cache::get("prefecture_data");
		}
		catch (\CacheNotFoundException $e)
		{
			$prefecture_data = array("" => "");
			$prefecture = DB::query("SELECT name FROM mst_prefectures order by region_id ")->execute();
			foreach($prefecture as $val){
				$prefecture_data[$val["name"]] = $val["name"];
			}
			Cache::set("prefecture_data", $prefecture_data, 60 * 60 * 24); // 24時間キャッシュ
		}
		$this->template->set_global('prefecture_data', $prefecture_data, false);

		// ---------------------------------------------------
		// ユーザー情報
		// ---------------------------------------------------
		if(Session::get('user.id')){
			try
			{
				$this->user_data = Cache::get("user_data_".Session::get('user.id'));
			}
			catch (\CacheNotFoundException $e)
			{
				$this->user_data = Model_Db_User::find(Session::get('user.id'));
				Cache::set("user_data_".Session::get('user.id'), $this->user_data, 60 * 5); // 5分キャッシュ
			}
			$this->template->set_global('user_data', $this->user_data, false);
		}

		// ---------------------------------------------------
		//消費税
		// ---------------------------------------------------
		$this->template->set_global('tax', $this->tax, false);

		//----------------------------------
		// 販売期間情報取得
		//----------------------------------
		$wheres = array();
		$wheres[] = array("status", Config::get("status_value.enable"));
		$wheres[] = array("shop_id", $this->shop_data["id"]);
		$wheres[] = array("sale_type", 0);//
		$wheres[] = array("start_date", "<=", date("Y-m-d H:i:s"));
		$wheres[] = array("finish_date", ">=", date("Y-m-d H:i:s"));
		$orders = array();
		$orders[] = array("start_date","asc");
		$this->term_data = Model_Db_Product_Term::findList($wheres,$orders);
		$this->template->set_global('term', $this->term_data, false);

		// ---------------------------------------------------
		// バナー
		// ---------------------------------------------------
		try
		{
			$banner = Cache::get("shop_banner_".$this->shop_data["id"]);
		}
		catch (\CacheNotFoundException $e)
		{
			$wheres = array();
			$wheres[] = array("status",Config::get('status_value.enable'));
			$wheres[] = array("shop_id", $this->shop_data["id"]);
			$wheres[] = array("start_date", "<=", date("Y-m-d H:i:s"));
			$wheres[] = array("end_date", ">=", date("Y-m-d H:i:s"));
			$orders = array();
			$orders[] = array("sort","asc");
			$banner = Model_Db_Banner::findListFront($wheres,$orders);
			Cache::set("shop_banner_".$this->shop_data["id"], $banner, 60 * 5);
		}
		$this->template->set_global('banner', $banner, false);

		// ---------------------------------------------------
		// 店舗詳細情報
		// ---------------------------------------------------
		try
		{
			$this->shop_desc_data = Cache::get("shop_desc_".$this->shop_data["id"]);
		}
		catch (\CacheNotFoundException $e)
		{
			$wheres = array();
			$wheres[] = array("status",Config::get('status_value.enable'));
			$wheres[] = array("shop_id", $this->shop_data["id"]);
			$this->shop_desc_data = Model_Db_Shop_Description::findListFront($wheres);
			Cache::set("shop_desc_".$this->shop_data["id"], $this->shop_desc_data, 60 * 5); // 5分キャッシュ
		}
		$this->template->set_global('shop_desc', $this->shop_desc_data, false);

		// ---------------------------------------------------
		// コンビニ決済用支払期限 TODO : Visual Japan Summit Only
		// ---------------------------------------------------
		$this->cvs_close_time = strtotime("5 day");

		if($this->shop_data["dir"] == "visual-japan") {
			if($this->cvs_close_time > strtotime("2016-10-11")){
				$this->cvs_close_time = strtotime("2016-10-11");
			}
		}

		$this->template->set_global('cvs_close_time', $this->cvs_close_time, false);

//		// ---------------------------------------------------
//		// 現地受渡情報
//		// ---------------------------------------------------
//		try
//		{
//			$this->shop_receives_data = Cache::get("shop_receives_".$this->shop_data["id"]);
//		}
//		catch (\CacheNotFoundException $e)
//		{
//			$this->shop_receives_data = array();
//			$sql = "SELECT id,title1,title2,stock FROM tbl_local_receives_timetable WHERE 1 = 1 ";
//			$sql .= "AND shop_id = ".$this->shop_data["id"] . " ";
//			$sql .= "AND start_date <= now() AND finish_date >= now() ";
//			$sql .= "order by id ";
//			$work_receives_data = DB::query($sql)->execute();
//			$this->shop_receives_data = $work_receives_data;
//			Cache::set("shop_receives_".$this->shop_data["id"], $this->shop_receives_data, 5 * 1);
//		}
//		$this->template->set_global('shop_receives', $this->shop_receives_data, false);

	}

	/**
	 * $response をパラメータとして追加し、after() を Controller_Template 互換にする
	 */
	public function after($response)
	{
		// ガラからのアクセスの場合はテンプレートをSJISエンコードする。
		if($this->agent_dir !== ""){
			$this->template = mb_convert_encoding($this->template, "SJIS-win", "UTF-8");
		}
		$response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
		return $response; // after() は確実に Response オブジェクトを返すように
	}
}
