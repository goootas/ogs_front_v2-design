<?php
class Model_Db_Shop_Postage extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'prefectures_id',
		'price',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_postage';

	protected static $_has_one = array(
		'shop' => array(
			'key_from' => 'shop_id',
			'model_to' => 'Model_Db_Shop',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		'prefectures' => array(
			'key_from' => 'prefectures_id',
			'model_to' => 'Model_Db_Prefectures',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
			),
		);

	public static function validate($factory)
    {
        $val = Validation::forge($factory);
        return $val;
    }

//	public static function findListTool($where="",$sort="")
//	{
//		$data = static::find('all', array(
//			'related'	=> array('shop','prefectures'),
//			'where'		=> $where,
//			'order_by'	=> $sort,
//			)
//		);
//		return $data;
//	}
	public static function findPriceDetailTool($where="")
	{
		$data = static::find('first', array(
			'where'		=> $where,
			)
		);
		return $data;
	}
	public static function findPriceDetailApi($where="")
	{
	}

	public static function findListFront($where="",$sort="")
	{
		$data = static::find('all', array(
			'related'	=> array('prefectures'),
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}

}
