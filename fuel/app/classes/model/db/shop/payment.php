<?php
class Model_Db_Shop_Payment extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'title',
		'sort',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_payment';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }
}
