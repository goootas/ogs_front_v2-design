<?php
namespace Model\lib;

use Fuel\Core\Cache;
use Fuel\Core\DB;
use Fuel\Core\Log;

class Term extends \Model
{
	/*
	 * 販売期間判定
	 * 異なる販売期間の商品はカートに入れないようにする。
	 */
	public static function chkCartProductTerm($array_cart,&$term_id,&$count)
	{
		$count = 0;
		$term_id = "";
		foreach ($array_cart as $key => $val) {
			$term_product = self::getTerm($val["did"]);
			if($term_product){
				$temp_term_id = $term_product["term_id"];
			}else{
				//通常販売商品も判定対象にする。通常販売のものは「0」を設定する。
				$temp_term_id = 0;
			}
			if($term_id === ""){
				$term_id = $temp_term_id;
			}
			// 販売期間ID比較
			if($term_id != $temp_term_id){
				$count++;
			}
		}
		return;
	}

	/*
	 * 通常・予約販売確認
	 * 0:通常
	 * 1:予約
	 * false:エラー
	 */
	public static function chkCartType($array_cart)
	{
		$tsujyo_count = 0;
		$yoyaku_count = 0;

		foreach ($array_cart as $key => $val) {

			$product_detail = \Model_Db_Product_Detail::find($val["did"]);
			if(self::_isTermTsujyo($product_detail->product_id)){
				$tsujyo_count++;
			}
			if(self::_isTermYoyaku($product_detail->product_id)){
				$yoyaku_count++;
			}
		}

		if($tsujyo_count){
			return 0;
		}
		if($yoyaku_count){
			return 1;
		}

		return false;
	}

	/*
	 * 販売期間ID取得
	 */
	public static function getTerm($product_detail_id)
	{
//		$product_detail = \Model_Db_Product_Detail::find($product_detail_id);
		try
		{
			$product_detail = Cache::get("getTerm_product_detail_".$product_detail_id);
		}
		catch (\CacheNotFoundException $e)
		{
			$product_detail = \Model_Db_Product_Detail::find($product_detail_id);
			Cache::set("getTerm_product_detail_".$product_detail_id, $product_detail, 60 * 5); // 5分キャッシュ
		}

		$sql = "";
		$sql .= "select tbl_product_term_relation.term_id from tbl_product_term_relation ";
		$sql .= "left join tbl_product_term on tbl_product_term.id = tbl_product_term_relation.term_id ";
		$sql .= "where tbl_product_term.start_date <= now() and tbl_product_term.finish_date >= now() ";
		$sql .= "and tbl_product_term.status = 1 ";
		$sql .= "and tbl_product_term_relation.product_id = ".$product_detail->product_id;

		$query = DB::query($sql);
		$term_product = $query->execute()->current();
		return $term_product;
	}

	/*
	 * 商品が販売期間内か確認
	 */
	public static function isTerm($product_detail_id)
	{
//		$product_detail = \Model_Db_Product_Detail::find($product_detail_id);
		try
		{
			$product_detail = Cache::get("isTerm_product_detail_".$product_detail_id);
		}
		catch (\CacheNotFoundException $e)
		{
			$product_detail = \Model_Db_Product_Detail::find($product_detail_id);
			Cache::set("isTerm_product_detail_".$product_detail_id, $product_detail, 60 * 5); // 5分キャッシュ
		}

		if(self::_isTermYoyaku($product_detail->product_id)){
			return true;
		}
		if(self::_isTermTsujyo($product_detail->product_id)){
			return true;
		}
		return false;
	}

	/*
	 * 予約販売期間チェック
	 * 販売期間内か確認する。
	 */
	private static function _isTermYoyaku($product_id)
	{
		$sql = "";
		$sql .= "select tbl_product_term_relation.product_id from tbl_product_term_relation ";
		$sql .= "left join tbl_product_term on tbl_product_term.id = tbl_product_term_relation.term_id ";
		$sql .= "where tbl_product_term.start_date <= now() and tbl_product_term.finish_date >= now() ";
		$sql .= "and tbl_product_term.status = 1 ";
		$sql .= "and tbl_product_term_relation.product_id = ".$product_id;

		$query = DB::query($sql);
		$term_product = $query->execute()->current();

		if($term_product){
			return true;
		}
		return false;
	}
	/*
	 * 通常販売期間チェック
	 * 販売期間内か確認する。
	 */
	private static function _isTermTsujyo($product_id)
	{
		$sql = "";
		$sql .= "select id from tbl_product ";
		$sql .= "where term_start_date <= now() and term_end_date >= now() ";
		$sql .= "and term_start_date is not null ";
		$sql .= "and term_end_date is not null ";
		$sql .= "and status = 1 ";
		$sql .= "and price_sale > 0 ";
		$sql .= "and id = ".$product_id;

		$query = DB::query($sql);
		$term_product = $query->execute()->current();

		if($term_product){
			return true;
		}
		return false;
	}


}