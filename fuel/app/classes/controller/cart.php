<?php
use Fuel\Core\Arr;
use Fuel\Core\Log;

/**
 * カート
 */
class Controller_Cart extends Controller_Basefront
{
	public function before()
	{
		parent::before();
		Config::load("common");

		// ---------------------------------------------------
		// カートに入っている商品の販売期間をチェック(販売期間終了チェック)
		// ---------------------------------------------------
		$cart_sess = Session::get('cart');
		if($cart_sess){

			$no_count = 0 ;
			foreach ($cart_sess as $key => $val) {
				$is_term = \Model\lib\Term::isTerm($val["did"]);
				if($is_term === FALSE){
					Arr::delete($cart_sess,$key);
					$no_count++;
				}
			}

			if($no_count){
				Session::set('cart',$cart_sess);
				Response::redirect('/cart/list'.$this->session_get_param);
				exit;
			}
		}

		// ---------------------------------------------------
		// 現地受渡商品がカートに入っているか確認
		// ---------------------------------------------------
		$this->receives_count = 0;
		if($cart_sess){
			foreach ($cart_sess as $key => $val) {
				$product_detail = Model_Db_Product_Detail::find($val["did"]);
				if($product_detail->receives_flg){
					$this->receives_count++;
				}
			}
		}
		$this->template->set_global('receives_count', $this->receives_count, false);
	}

	/*
	 * index list redirect
	 */
	public function action_index()
	{
		Response::redirect('/cart/list'.$this->session_get_param);
	}
	/*
	 * カート内容一覧
	 */
	public function action_list($change=0)
	{
		// 変更ボタン制御用セッション
		if($change) Session::set('change',$change);

//		// 予約商品判定用
//		$reserve_date = "";
		// 決済指定判定用
		$payment_ctl_data = array();
		$payment_ctl_card = 1;
		$payment_ctl_cvs = 1;
		$payment_ctl_cod = 1;
		$payment_ctl_ato = 1;
		$payment_ctl_npato = 1;
		$payment_ctl_rakuten = 1;

		$cart_data = array();
		$cart_sess = Session::get('cart');
		if($cart_sess){
			// カート内容取得
			$cart_data = \Model\lib\Cart::getCartData($cart_sess);
			foreach ($cart_data as $key => $val) {

//				$item = Model_Db_Product_Detail::findProductDetailOne(array(array("id", $val["did"])));
////				// 予約商品判定
////				if($item->base->reserve_date && strtotime($item->base->reserve_date) >= strtotime(date('Y-m-d'))){
////					if($reserve_date == "" || ($reserve_date != "" && $reserve_date < $item->base->reserve_date)){
////						$reserve_date = $item->base->reserve_date;
////					}
////				}

				// 決済指定判定
				$item = Model_Db_Product_Detail::findDetailPayment($val["did"]);
				if($item["payment_card_flg"] != 1){
					$payment_ctl_card = 0;
				}
				if($item["payment_cvs_flg"] != 1){
					$payment_ctl_cvs = 0;
				}
				if($item["payment_cod_flg"] != 1){
					$payment_ctl_cod = 0;
				}
				if($item["payment_ato_flg"] != 1){
					$payment_ctl_ato = 0;
				}
				if($item["payment_npato_flg"] != 1){
					$payment_ctl_npato = 0;
				}
				if($item["payment_rakuten_flg"] != 1){
					$payment_ctl_rakuten = 0;
				}
			}
		}

		// 現地受渡商品の場合はカード以外は利用不可にする
		if($this->receives_count > 0){
			$payment_ctl_card = 1;
			$payment_ctl_cvs = 0;
			$payment_ctl_cod = 0;
			$payment_ctl_ato = 0;
			$payment_ctl_npato = 0;
			$payment_ctl_rakuten = 0;
			if($this->shop_data["dir"] == "visual-japan") {
				$payment_ctl_cvs = 1;
			}
		}

//		// 予約商品判定用
//		Session::set('reserve_date',$reserve_date);
		// 決済指定判定用
		if($payment_ctl_card){
			$payment_ctl_data[] = "クレジットカード";
		}
		if($payment_ctl_cvs){
			$payment_ctl_data[] = "コンビニ決済";
		}
		if($payment_ctl_cod){
			$payment_ctl_data[] = "代金引換";
		}
		if($payment_ctl_ato){
			$payment_ctl_data[] = "後払い";
		}
		if($payment_ctl_npato){
			$payment_ctl_data[] = "NP後払い";
		}
		if($payment_ctl_rakuten){
			$payment_ctl_data[] = "楽天ID決済LITE";
		}

		Session::set('payment_ctl_cnt',$payment_ctl_card + $payment_ctl_cvs + $payment_ctl_cod + $payment_ctl_ato + $payment_ctl_npato + $payment_ctl_rakuten);
		Session::set('payment_ctl_data',$payment_ctl_data);
		Session::set('payment_ctl_card',$payment_ctl_card);
		Session::set('payment_ctl_cvs',$payment_ctl_cvs);
		Session::set('payment_ctl_cod',$payment_ctl_cod);
		Session::set('payment_ctl_ato',$payment_ctl_ato);
		Session::set('payment_ctl_npato',$payment_ctl_npato);
		Session::set('payment_ctl_rakuten',$payment_ctl_rakuten);

		// カート追加、削除、数量変更が発生するため、カートでは名入れのセッション情報を削除
		Session::delete("order_data.dataname");

		$this->template->set_global('change', $change, false);
		$this->template->set_global('cart', $cart_data, false);

		$this->template->title = 'カート';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/cart/list');
	}

	/*
	 * カート追加
	 */
	public function action_add()
	{
		if(!Input::post('did')) {
			if($this->agent_dir != ""){
				Session::set_flash('error', '商品を選択してください');
				Response::redirect(Input::server("HTTP_REFERER"));
			}else{
				Response::redirect('/cart/list'.$this->session_get_param);
			}
		}
		// カートに追加するpostデータ取得
		$cart_data = array(
			array(
				'did'	=> Input::post('did'),
				'num'	=> Input::post('num'),
			));

		// セッションに登録されているカート情報取得
		$cart_sess = Session::get('cart');
		if($cart_sess){
			// 同一IDが存在するかチェックし、存在する場合は、数量を加算する
			foreach ($cart_sess as $key => $val) {
				if($val["did"] == Input::post('did')){
					$cart_sess[$key]["num"] = $val["num"] + Input::post('num');
					$cart_data = NULL;
				}
			}
			// 商品追加か数量加算判定
			if($cart_data){
				$cart_data = array_merge_recursive($cart_data,$cart_sess);
			}else{
				$cart_data = $cart_sess;
			}
		}

		// 在庫確認
		foreach ($cart_data as $key => $val) {
			$product_detail = Model_Db_Product_Detail::find($val["did"]);
			if($product_detail){

				// ---------------------------------------------------
				// 生写真は5セットまで対応 TODO:一時的な実装（後ほどツール化）
				// ---------------------------------------------------
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit")) !== false){
					if($this->shop_data["dir"] == "apbankfes2016"){
						if($cart_data[$key]["num"] > 2 ){
							$cart_data[$key]["num"] = 2;
							Session::set_flash("error", "お1人様1会計につき2点までご注文可能です");
						}
					}else{
						if($cart_data[$key]["num"] > 5 ){
							$cart_data[$key]["num"] = 5;
							Session::set_flash("error", "お1人様1会計につき5点までご注文可能です");
						}
					}
				}

				// 購入制限1
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit1")) !== false){
					if($cart_data[$key]["num"] > 1 ){
						$cart_data[$key]["num"] = 1;
						Session::set_flash("error", "お1人様1会計につき1点までご注文可能です");
					}
				}

				// 購入制限2
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit2")) !== false){
					if($cart_data[$key]["num"] > 2 ){
						$cart_data[$key]["num"] = 2;
						Session::set_flash("error", "お1人様1会計につき2点までご注文可能です");
					}
				}

				if($product_detail->stock < $val["num"] ){
					$cart_data[$key]["num"] = $product_detail->stock;
					$item = Model_Db_Product_Detail::findProductDetailOne(array(array("id",$val["did"])));

					if($cart_data[$key]["num"] > 0){
						Session::set_flash("error", "大変申し訳ございません。現在【&nbsp;"
							.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
							."&nbsp;".($item->option2_name ? $item->option2_name :"")
							."&nbsp;】の在庫数が不足しております。");
					}else{
						Session::set_flash("error", "大変申し訳ございませんが、【&nbsp;"
							.$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")
							."&nbsp;".($item->option2_name ? $item->option2_name :"")
							."&nbsp;】お客様が注文手続きをされている間に品切れとなりました。");
					}
				}
			}
		}
		// 不良データ整合
		\Model\lib\Cart::adjCartData($cart_data,$stock_error);

		// ---------------------------------------------------
		// カートに入っている商品の販売期間をチェック
		// ---------------------------------------------------
		$ccnt = 0;
		$term_id = "";
		\Model\lib\Term::chkCartProductTerm($cart_data,$term_id,$ccnt);

		// 比較して異なる場合は、カートに入れずにエラーメッセージを表示する
		if($ccnt){
			// IDが異なる商品がカートに入っていない前提でインデックス0を削除
			Arr::delete($cart_data,0);
			Session::set_flash("error", "発送予定日が異なる予約商品を同時にカートに入れることはできません。<br>別々にご注文くださるようお願いいたします。");
		}

		// ---------------------------------------------------
		// 配送商品、現地受渡商品のチェック
		// 　同じステータスの商品以外はカートに入れない
		// ---------------------------------------------------
		if(!\Model\lib\Cart::chkProductReceives($cart_data)){
			Arr::delete($cart_data,0);
			Session::set_flash("error", "会場受取商品と配送受取商品の同時注文はできません<br>別々にご注文くださるようお願いいたします。");
		}

		// ---------------------------------------------------
		// セッション登録
		// ---------------------------------------------------
		Session::set('cart',$cart_data);
		Response::redirect('/cart/list'.$this->session_get_param);
	}

	/*
	 * カート削除
	 */
	public function action_del()
	{
		$cart_sess = Session::get('cart');
		if($cart_sess){
			// 同一IDが存在するかチェックし、存在する場合は、数量を加算する
			foreach ($cart_sess as $key => $val) {
				if($val["did"] == Input::post('did')){
					Session::delete('cart.'.$key);
				}
			}
		}
		Response::redirect('/cart/list'.$this->session_get_param);
	}

	/*
	 * カート内容変更※ガラ用
	 */
	public function action_fpchange()
	{
		//削除から処理
		$del = Input::post('del');
		$cart_sess = Session::get('cart');
		if(isset($del)){
			if($cart_sess){
				// 同一IDが存在するかチェックし、存在する場合は、数量を加算する
				foreach ($cart_sess as $key => $val) {
					foreach (Input::post('del') as $dkey => $dval) {
						if($val["did"] == $dval){
							Log::debug("DELETE SESSION >> ".$key);
							Arr::delete($cart_sess,$key);
						}
					}
				}
			}
		}

		//数量取得しなおし
		if($cart_sess){
			foreach ($cart_sess as $key => $val) {
				$num = Input::post('num_'.$val["did"]);
				$cart_sess[$key]["num"] = $num;

				// ---------------------------------------------------
				// 生写真は5セットまで対応 TODO:一時的な実装（後ほどツール化）
				// ---------------------------------------------------
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit")) !== false){
					if($this->shop_data["dir"] == "apbankfes2016"){
						if($cart_sess[$key]["num"] > 2 ){
							$cart_sess[$key]["num"] = 2;
							Session::set_flash("error", "お1人様1会計につき2点までご注文可能です");
						}
					}else{
						if($cart_sess[$key]["num"] > 5 ){
							$cart_sess[$key]["num"] = 5;
							Session::set_flash("error", "お1人様1会計につき5点までご注文可能です");
						}
					}
				}
				// 購入制限1
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit1")) !== false){
					if($cart_sess[$key]["num"] > 1 ){
						$cart_sess[$key]["num"] = 1;
						Session::set_flash("error", "お1人様1会計につき1点までご注文可能です");
					}
				}
				// 購入制限2
				if(array_search($val["did"],\Fuel\Core\Config::get("order_limit2")) !== false){
					if($cart_sess[$key]["num"] > 2 ){
						$cart_sess[$key]["num"] = 2;
						Session::set_flash("error", "お1人様1会計につき2点までご注文可能です");
					}
				}

				$product_detail = Model_Db_Product_Detail::find($val["did"]);
				if($product_detail){
					if($product_detail->stock < $num ){
						$cart_sess[$key]["num"] = $product_detail->stock;
						$item = Model_Db_Product_Detail::findProductDetailOne(array(array("id",$val["did"])));
						Session::set_flash("error","申し訳ございませんが、【&nbsp;".$item->base->title."&nbsp;".($item->option1_name ? $item->option1_name :"")."&nbsp;".($item->option2_name ? $item->option2_name :"")."&nbsp;】は在庫数が不足しています。");
					}
				}
				// 在庫がないくカート内の数量が0のものは削除する
				if($cart_sess[$key]["num"] <= 0){
					Arr::delete($cart_sess,$key);
				}
			}

		}
		// セッション登録
		Session::set('cart',$cart_sess);
		Response::redirect('/cart/list'.$this->session_get_param);
	}

}
