<?php
class Model_Db_Product_Detail extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'product_id',
        'option1_name',
        'option2_name',
        'receives_flg',
        'stock',
        'status',
        'insert_id',
        'insert_date',
        'update_id',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_product_detail';

    protected static $_belongs_to = array(
        'base' => array(
            'key_from' => 'product_id',
            'model_to' => 'Model_Db_Product',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        //$val->add('shop_id', '店舗ID')->add_rule('required');
        //$val->add('product_id', '商品ID')->add_rule('required');
        //$val->add('limited_flg', '限定販売')->add_rule('required');
        //$val->add('stock_flg', '在庫管理')->add_rule('required');
        //$val->add('stock', '在庫数')->add_rule('required');
        //$val->add('price_cost', '原価')->add_rule('required');
        //$val->add('price_sale', '通常販売価格')->add_rule('required');
        //$val->add('status', '状態')->add_rule('required');

        return $val;
    }

    public static function findProductDetailOne($where="")
    {
        $data = static::find('first', array(
            'related' => array('base'),
            'where' => $where,
            )
        );
        return $data;
    }
//    public static function findProductDetailList($where="")
//    {
//        $data = static::find('all', array(
//            'related' => array('base'),
//            'where' => $where,
//            )
//        );
//        return $data;
//    }

	public static function findList($product_id)
	{
		$sql = "";
		$sql .= "select id,product_id,option1_name,option2_name,stock from " . self::$_table_name . " where 1=1 ";
		$sql .= " and product_id = :product_id ";
		$sql .= " and status = 1 ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'product_id'	=> $product_id,
			)
		);

		// 既存のテンプレートがオブジェクト変数から値を取得する形式になっているため。
		$data = $query->as_object()->execute();
		return $data;
	}

	public static function findDetail($product_detail_id)
	{
		try
		{
			$data = \Fuel\Core\Cache::get("model_product_detail_finddetail_".$product_detail_id);
		}
		catch (\Fuel\Core\CacheNotFoundException $e)
		{
			$sql = "";
			$sql .= "select " ;
			$sql .= " tbl_product.id ,";
			$sql .= " tbl_product.title ,";
			$sql .= " tbl_product.price_sale ,";
			$sql .= " tbl_product_detail.option1_name ,";
			$sql .= " tbl_product_detail.option2_name ";
			$sql .= " from tbl_product_detail left join tbl_product on tbl_product_detail.product_id = tbl_product.id ";
			$sql .= " where 1 = 1 ";
			$sql .= " and tbl_product_detail.id = :product_detail_id ";
			$query = DB::query($sql);
			$query->parameters(
				array(
					'product_detail_id'	=> $product_detail_id,
				)
			);
			$data = $query->execute()->current();
			\Fuel\Core\Cache::set("model_product_detail_finddetail_".$product_detail_id, $data, 60 * 5); // 5分キャッシュ
		}
		return $data;
	}
	public static function findDetailPayment($product_detail_id)
	{
		try
		{
			$data = \Fuel\Core\Cache::get("model_product_detail_finddetailpayment_".$product_detail_id);
		}
		catch (\Fuel\Core\CacheNotFoundException $e)
		{
			$sql = "";
			$sql .= "select " ;
			$sql .= " tbl_product.payment_card_flg ,";
			$sql .= " tbl_product.payment_cvs_flg ,";
			$sql .= " tbl_product.payment_cod_flg ,";
			$sql .= " tbl_product.payment_ato_flg ,";
			$sql .= " tbl_product.payment_npato_flg ,";
			$sql .= " tbl_product.payment_rakuten_flg ";
			$sql .= " from tbl_product_detail left join tbl_product on tbl_product_detail.product_id = tbl_product.id ";
			$sql .= " where 1 = 1 ";
			$sql .= " and tbl_product_detail.id = :product_detail_id ";
			$query = DB::query($sql);
			$query->parameters(
				array(
					'product_detail_id'	=> $product_detail_id,
				)
			);
			$data = $query->execute()->current();
			\Fuel\Core\Cache::set("model_product_detail_finddetailpayment_".$product_detail_id, $data, 60 * 5); // 5分キャッシュ
		}
		return $data;
	}
}
