<?php
class Model_Db_Order_Detail extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'order_id',
        'did',
        'price',
        'num',
        'status',
        'insert_date',
        'update_id',
        'update_date',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => true,
            'property' => 'insert_date',
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => true,
            'property' => 'update_date',
        ),
    );
    protected static $_table_name = 'tbl_order_detail';

    protected static $_has_one = array(
        'base' => array(
            'key_from' => 'order_id',
            'model_to' => 'Model_Db_Order',
            'key_to' => 'order_id',
            'cascade_save' => true,
            'cascade_delete' => false,
            ),
        'product' => array(
            'key_from' => 'did',
            'model_to' => 'Model_Db_Product',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
            ),
        );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }
}
