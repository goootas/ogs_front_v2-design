<?php
/**
 * 会員登録
 */
class Controller_Regist extends Controller_Basefront
{
	/*
	 * 仮登録
	 */
	public function action_index()
	{
		self::action_input();
//		if (Input::method() == 'POST')
//		{
//			//入力チェック
//			$val = Validation::forge();
//			$val->add('email', 'メールアドレス')->add_rule('required')
//				->add_rule('valid_email');
//			if ($val->run())
//			{
//				//メールアドレス存在チェック
//				$user = Model_Db_User::findEmail2($this->shop_data["id"],Input::post('email'));
//				if(!$user){
//					//===================================================
//					// メール送信
//					//===================================================
//					$shop_data = Model_Db_Shop::find($this->shop_data["id"]);
//					Model_Lib_Mail::sendmail($shop_data,Config::get('mail.value.temp'),
//						Input::post('email'),array('reg_param' => Crypt::encode(Input::post('email'),  Config::get('cypt_key'))));
//					Response::redirect("/regist/temp_complete".$this->session_get_param);
//				}else{
//					Session::set_flash('error', "入力されたメールアドレスは既に会員登録されています。<br><a href='/".$this->shop_data["dir"]."/forget'>パスワードを忘れた方はこちら</a>");
//				}
//			}else
//			{
//				Session::set_flash('error', $val->error());
//			}
//		}
//		$this->template->title = '会員登録';
//		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/regist/input_temp');
	}
	/*
	 * 本登録
	 */
	public function action_input($email = null)
	{
//		if($email)
//		{
//			Session::set("regist", array('email' => Crypt::decode($email,  Config::get('cypt_key'))));
//			Response::redirect("/regist/input".$this->session_get_param);
//		}
//		// 既に会員登録済みの場合は、TOPページへリダイレクト
//		!is_null(Model_Db_User::findEmail2($this->shop_data["id"],Session::get('regist.email'))) and Response::redirect("/".$this->session_get_param);
//
//		// パラメータ無しでアクセスした場合、登録ページへリダイレクト
//		is_null(Session::get('regist.email')) and Response::redirect('/regist'.$this->session_get_param);


		if (Input::method() == 'POST')
		{
			$val = Model_Db_User::validate("regist2");
			if ($val->run())
			{
				// 既に会員登録済みの場合は、TOPページへリダイレクト
				$user = Model_Db_User::findEmail2($this->shop_data["id"],Input::post('email'));
				if(!$user) {
					$user = Model_Db_User::forge(array(
						'shop_id'       => $this->shop_data["id"],
//					'email'         => Session::get('regist.email'),
						'email'         => Input::post('email'),
						'username_sei'  => Input::post('username_sei'),
						'username_mei'  => Input::post('username_mei'),
						'username_sei_kana'	=> Input::post('username_sei_kana'),
						'username_mei_kana'	=> Input::post('username_mei_kana'),
						'tel1'          => Input::post('tel1'),
						'tel2'          => Input::post('tel2'),
						'tel3'          => Input::post('tel3'),
						'zip'           => Input::post('zip'),
						'prefecture'    => Input::post('state'),
						'address1'      => Input::post('address1'),
						'address2'      => Input::post('address2'),
						'password'      => Input::post('password'),
						'sex'           => Input::post('sex'),
						'birthday'      => Input::post('birthday'),
						'mailmagazine'  => Input::post('mailmagazine'),
						'status'        => Config::get('status_value.enable'),
						'funclub_flg'	=> Input::post('funclub_flg',NULL),
					));

					if ($user and $user->save())
					{
						//===================================================
						// メール送信
						//===================================================
						$shop_data = Model_Db_Shop::find($this->shop_data["id"]);
						Model_Lib_Mail::sendmail($shop_data,Config::get('mail.value.regist'),
//						Session::get('regist.email'),array('username' => $user->username_sei ." ".$user->username_mei));
							$user->email,array('username' => $user->username_sei ." ".$user->username_mei));

						if($user->mailmagazine == "1")
						{
							if(isset($this->shop_data["mailchimp_apikey"]) && isset($this->shop_data["mailchimp_listid"]))
							{
								//メルマガ登録
								Model_Lib_MailChimp::subscribed($this->shop_data["mailchimp_apikey"],$this->shop_data["mailchimp_listid"],
									$user->email,$user->username_mei,$user->username_sei);
							}
						}

						//ログイン状態にする。
						Session::set('user',array('id' => $user->id));

						Session::delete('regist');//期限切れになるが、念のため、不要なセッションは削除する。
						Response::redirect('/regist/complete'.$this->session_get_param);
					}else
					{
						Session::set_flash('error', '登録できませんでした。');
					}
				}else{
					Session::set_flash('error', "入力されたメールアドレスは既に会員登録されています。<br><a href='/".$this->shop_data["dir"]."/forget'>パスワードを忘れた方はこちら</a>");
				}

			}else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$this->template->title = '会員登録';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/regist/input');
	}

	/*
	 * 本登録完了
	 */
	public function action_temp_complete()
	{
		$this->template->title = '会員登録';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/regist/temp_complete');
	}
	/*
	 * 本登録完了
	 */
	public function action_complete()
	{
		$this->template->title = '会員登録';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/regist/complete');
	}
}

