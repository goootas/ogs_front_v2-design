<?php
class Model_Db_Mailmagazine extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'email',
		'send_cnt',
		'bounce_cnt',
		'status',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_mailmagazine';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }

	public static function findEmail($email)
	{
		$data = static::find('first', array(
			'where' => array(
				array("email", $email),
			))
		);
		return $data;
	}
}
