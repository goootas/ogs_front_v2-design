<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="ja">
	<meta charset="utf-8">
	<link rel="shortcut icon" href="/<?php echo $shop_data["dir"];?>/assets/icon/<?php echo $shop_data["dir"];?>.ico">
	<?php if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) { ?>
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<?php } else { ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php } ?>
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
	<link href='https://fonts.googleapis.com/css?family=Luckiest+Guy' rel='stylesheet' type='text/css'>
	<?php
	echo Asset::css(
		array(
			Config::get("assets_url").'css/reset.css',
			Config::get("assets_url").'css/flexslider.css',
			Config::get("assets_url").'css/common.css',
			Config::get("assets_url").'css/'.$shop_data["dir"].'/'.$shop_data["dir"].'.css',
			Config::get("assets_url").'css/validationEngine.jquery.min.css',
		)
	);
	if(preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::css(array('common-ie.css'));
	}
	echo Asset::js(
		array(
			Config::get("assets_url").'js/jquery.1.8.2.min.js',
			Config::get("assets_url").'js/common.js',
			'jquery.validationEngine-ja.min.js',
			'jquery.validationEngine.min.js',
			Config::get("assets_url").'js/jquery.flexslider.js',
		)
	);
	if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
		echo Asset::js(
			array(
				Config::get("assets_url").'js/jquery.innerfade.js',
				Config::get("assets_url").'js/masonry.pkgd.min.js',
				Config::get("assets_url").'js/jquery.autopager-1.0.0.js',
			)
		);
	}
	?>
	<style>
		<!--
		.form-group input[type="tel"],.form-group input[type="email"],
		.form-group input[type="text"], .form-group input[type="password"], .form-group textarea {
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
			padding: 12px;
			width: 90%;
		}
		-->
	</style>
	<?php //TODO:OGタグの内容確認お願いします。 ?>
	<?php if(Request::active()->controller == 'Controller_Product' and in_array(Request::active()->action, array('detail'))) :?>
		<meta property="fb:app_id" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:title" content="<?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:description" content="<?php echo $shop_data["description"];?>"/>
		<meta property="og:url" content="https://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'] . "?" . $_SERVER['QUERY_STRING']; ?>"/>
		<meta property="og:image" content="<?php echo "https://" . $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/" . Config::get("aws.image_path") . $product->id . "/" . $product->id . "_000.jpg"; ?>"/>
		<meta property="og:site_name" content="<?php echo $shop_data["name"] ?>"/>
	<?php endif; ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $shop_data["ga_tag"]?>', 'auto');
		ga('require', 'linkid', 'linkid.js');
		ga('send', 'pageview');

	</script>
</head>
<body>
<div id="TOP" class="WRAPPER">
	<div class="HEADER">
	<div class="HEADER-INNER">
		<div class="MAINTITLE">
			<div class="TITLE">
				<h2><a href="/mixlemonedjelly/"><img id="logo" src="<?php echo Config::get("assets_url");?>img/<?= $shop_data["dir"]; ?>/title.png" alt="" /></a></h2>
			</div>
		</div>
		<a class="oac-menu" href="javascript:void(0);"><img id="logo" src="<?php echo Config::get("assets_url");?>img/<?= $shop_data["dir"]; ?>/UI_icon_menus.png" alt="MENU" /></a>
		<div class="NAVIGATION">
			<div class="GLOBAL">
				<div class="CONTAINER">
					<div class="buttons">
						<?php if(Session::get('user.id')) : ?>
							<?php echo Html::anchor('mypage', '<span>MYPAGE</span>', array("class" => "button own with-icon mypage green"), true); ?>
							<?php echo Html::anchor('logout', '<span>LOGOUT</span>', array("class" => "button own with-icon logout blue"), true); ?>
						<?php else: ?>
							<?php echo Html::anchor('regist', '<span>お客様情報登録</span>', array("class" => "button own regist blue"), true); ?>
							<?php echo Html::anchor('login', '<span>LOGIN</span>', array("class" => "button own with-icon login blue"), true); ?>
						<?php endif; ?>
						<?php echo Html::anchor('cart', '<span>CART</span>',array("class" => "button own with-icon cart green")); ?>
					</div>
				</div>
			</div>
			<div class="CATE">
				<div class="CONTAINER">
					<div class="navigation">
						<table>
							<tr>
								<td><?php echo Html::anchor('/', "<span>ALL</span>") ?></td>
 								<?php if($category1): ?>
									<?php foreach($category1 as $c1): ?>
										<td><?php echo Html::anchor('/product/list?c1='.$c1->id, "<span>".$c1->title."</span>") ?></td>
									<?php endforeach; ?>
								<?php endif; ?>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="CONTENTS">

		<?php if(Request::active()->controller == 'Controller_Top' ||
			(Request::active()->controller == 'Controller_Product' and Request::active()->action == 'list')) :?>
			<?php //echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
		<?php endif; ?>
		<div class="CONTAINER">
			<?php echo $content; ?>
		</div>
		<?php if(Request::active()->controller != 'Controller_Page') :?>
			<div style="background-color: #fffc00; padding: 20px;">
			<?php echo isset($shop_desc->pcsp_top2) ? $shop_desc->pcsp_top2 : "";?>
			</div>
		<?php endif; ?>

	</div>
	<div class="FOOTER">
		<a href="http://mixlemonedjelly.hide-city.com/pc/" target="_blank" class="WEBTITLE"><img src="<?php echo Config::get("assets_url");?>img/<?= $shop_data["dir"]; ?>/title-foot.png" alt="" /></a>
		<div class="LINKS">
			<div class="CONTAINER">
				<div class="navigation">
					<ul>
						<li><?php echo Html::anchor('/info/guide', '<span>SHOPPING GUIDE</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/inquiry', '<span>CONTACT</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/privacy', '<span>PRIVACY POLICY</span>', "", true) ?></li>
						<li><?php echo Html::anchor('/info/legal', '<span>特定商取引法に基づく表記</span>', "", true) ?></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="CREDIT">
			<div class="CONTAINER">
				<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI Co.,Ltd.";?> All Rights Reserved</p>
			</div>
		</div>
	</div>
</div>
</body>
</html>