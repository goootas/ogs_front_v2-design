<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<!-- 購入商品情報 -->
<?php //echo render($shop_data["dir"].'/order/_cart_view'); ?>

<?php if(!isset($user_data)) : ?>
	<!-- ログイン -->
	<?php echo Form::open(array('action' => '/login' ,'id' => "login" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
	<div class="login-panel">
		<div class="panel-body">
			<p class="description">お客様情報を登録されている方は、<br>こちらからログインするとお客様情報が自動入力されます。</p>
			<div class="form-group">
				<label class="col-sm-4 col-xs-5 control-label">メールアドレス</label>
				<div class="form-container">
					<div class="inner">
						<?php echo Form::input('email', Input::post('email', ''),
							array('class' => 'validate[required] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM','type' => 'email')); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-xs-5 control-label" for="form_password">パスワード</label>
				<div class="form-container">
					<div class="inner">
						<?php echo Form::password('password', Input::post('password', ''),
							array('class' => 'validate[required,minSize[3],maxSize[12]] form-control col-sm-4','placeholder' =>'3〜12文字の英数字')); ?>
					</div>
				</div>
			</div>
			<div class="buttons">
				<input class="button rect key size-L" name="exec" value="ログイン" type="submit" />
				<?php echo Html::anchor('/forget', 'パスワードを忘れた方はこちら', array("class" => "button text"), true) ?>
			</div>

			<div class="description notice">
				ログインアカウントは、当通販サイト(ラブライブ！サンシャイン!! Aqours 1st LIVE オフィシャルグッズストア)専用のものとなります。
			</div>
		</div>

	</div>
	<?php echo Form::close();?>
<?php endif;?>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<h4 class="headline">お客様情報</h4>
	<?php if(isset($user_data)):?>
		入力されているお客様情報確認後、変更がある場合は入力してください。<br><br>
	<?php else : ?>
		初めてお買い物をされるお客様は、こちらからお客様情報を入力してください。<br><br>
	<?php endif ;?>
	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須</span>', 'order_username',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_username_sei', Session::get("order_data.data1.order_username_sei") ? Session::get("order_data.data1.order_username_sei") : (isset($user_data) ? $user_data->username_sei : ""),
					array('class' => 'validate[required] form-control col-sm-4 col-xs-4',"placeholder" => "姓")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_username_mei', Session::get("order_data.data1.order_username_sei") ? Session::get("order_data.data1.order_username_mei") : (isset($user_data) ? $user_data->username_mei : ""),
					array('class' => 'validate[required] form-control col-sm-4 col-xs-4',"placeholder" => "名")); ?>
			</div>
		</div>
	</div>
	<div class="form-group count-2">
		<?php echo Form::label('ふりがな<span class="caption">※必須</span>', 'order_username_kana',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_username_sei_kana', Session::get("order_data.data1.order_username_sei_kana") ? Session::get("order_data.data1.order_username_sei_kana") : (isset($user_data) ? $user_data->username_sei_kana : ""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "せい")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_username_mei_kana', Session::get("order_data.data1.order_username_mei_kana") ? Session::get("order_data.data1.order_username_mei_kana") : (isset($user_data) ? $user_data->username_mei_kana : ""),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "めい")); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('メールアドレス<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_email',Session::get("order_data.data1.order_email") ? Session::get("order_data.data1.order_email") : (isset($user_data) ? $user_data->email : ""),
					array('class' => 'validate[required,custom[email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM' ,'id' => 'order_email','type' => 'email')); ?>
			</div>
			<div class="description notice">携帯電話会社等が提供しているメールアドレスは受信に関する規制が多く、注文確認等のメールが届かない場合があります。（ドメイン指定受信設定 / URL付きメール受信不可 / PCからのメール受信不可）</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('メールアドレス確認<span class="caption">※必須</span>', 'order_email',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_email2',Session::get("order_data.data1.order_email2") ? Session::get("order_data.data1.order_email2") : (isset($user_data) ? $user_data->email : ""),
					array('class' => 'validate[required,equals[order_email]] form-control col-sm-4','placeholder' =>'XXXXXXX@XXXXX.COM','type'=>'email')); ?>
			</div>
		</div>
	</div>
	<div class="form-group type-postal">
		<?php echo Form::label('郵便番号<span class="caption">※必須</span>', 'order_zip',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_zip',Session::get("order_data.data1.order_zip") ? Session::get("order_data.data1.order_zip") : (isset($user_data) ? $user_data->zip : ""),
					array('id' => 'zip','class' => 'validate[required,custom[onlyNumberSp],minSize[7],maxSize[7]] form-control col-sm-4',"placeholder" => "1234567",'type'=>'tel')); ?>
			</div>
			<div class="description">※ハイフン(-)は抜いてください </div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 col-xs-4 control-label">都道府県<span class="caption">※必須</span></label>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php echo Form::select('order_state', Session::get("order_data.data1.order_state") ? Session::get("order_data.data1.order_state") :
							(isset($user_data) ? $user_data->prefecture : ""),$prefecture_data,
							array('class' => 'validate[required] form-control col-md-5','id' => 'state')); ?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所１<span class="caption">※必須</span>', 'order_address1',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_address1', Session::get("order_data.data1.order_address1") ? Session::get("order_data.data1.order_address1") :
					(isset($user_data) ? mb_convert_kana($user_data->address1, "ASV") : ""),
					array('id' => 'address1' , 'class' => 'validate[required,ajax[ajaxZipAddress],maxSize[24]] form-control','placeholder' => '24文字まで')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('住所２', 'order_address2',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_address2',  Session::get("order_data.data1.order_address2") ? Session::get("order_data.data1.order_address2") :
					(isset($user_data) ? mb_convert_kana($user_data->address2, "ASV") : ""),
					array('id' => 'address2' ,'class' => 'validate[maxSize[24]] form-control',"placeholder" => "24文字まで")); ?>
			</div>
			<div class="">※「字」「大字」は省略してください。</div>
		</div>
	</div>
	<div class="form-group count-3">
		<?php echo Form::label('電話番号<span class="caption">※必須</span>', 'order_tel',array("class" => "col-sm-3 col-xs-12 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_tel1', Session::get("order_data.data1.order_tel1") ? Session::get("order_data.data1.order_tel1") :
					(isset($user_data) ? $user_data->tel1 : ""),
					array('id' => 'tel1' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_tel2', Session::get("order_data.data1.order_tel2") ? Session::get("order_data.data1.order_tel2") :
					(isset($user_data) ? $user_data->tel2 : ""),
					array('id' => 'tel2' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('order_tel3', Session::get("order_data.data1.order_tel3") ? Session::get("order_data.data1.order_tel3") :
					(isset($user_data) ? $user_data->tel3 : ""),
					array('id' => 'tel3' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('性別', 'order_sex',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<td><?php echo Form::radio('order_sex', '1',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>男性</td>
					<td><?php echo Form::radio('order_sex', '2',Session::get("order_data.data1.order_sex") ? Session::get("order_data.data1.order_sex") :
							(isset($user_data) ? $user_data->sex : "")); ?></td>
					<td>女性</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="form-group">
		<?php echo Form::label('生年月日', 'order_birthday',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('order_birthday', Session::get("order_data.data1.order_birthday") ? Session::get("order_data.data1.order_birthday") :
					(isset($user_data) ? $user_data->birthday : ""),
					array('class' => 'validate[custom[onlyNumberSp],minSize[8],maxSize[8]] form-control col-sm-4',"placeholder" => "19790929","type" => "tel")); ?>
			</div>
		</div>
	</div>

	<?php if ((!isset($user_data)) || (isset($user_data) && !$user_data->mailmagazine)) :?>
		<div class="form-group">
			<?php echo Form::label('お知らせメール<span class="caption">※必須</span>', 'mailmagazine_flg',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<table class="radio">
					<tr>
						<td><?php echo Form::radio('mailmagazine_flg', '1',Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") : "1",
								array('class' => 'validate[required]')); ?></td>
						<td>受信する</td>
						<td><?php echo Form::radio('mailmagazine_flg', '0',Session::get("order_data.data1.mailmagazine_flg") ? Session::get("order_data.data1.mailmagazine_flg") : "",
								array('class' => 'validate[required]')); ?></td>
						<td>受信しない</td>
					</tr>
				</table>
				<div class="description" style="margin-top:-5px;">
					※メールにて新商品のお知らせなどをさせていただく場合があります。
				</div>
			</div>
		</div>
	<?php endif;?>

	<?php if (!isset($user_data)) :?>

		<div class="form-group">
			<?php echo Form::label('お客様情報登録<span class="caption">※必須</span>', 'regist',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<table class="radio">
					<tr>
						<td><?php echo Form::radio('regist_flg', "1",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : "1",
								array('id'=> 'regist_on','class' => 'validate[required]')); ?></td>
						<td>登録する</td>
						<td><?php echo Form::radio('regist_flg', "0",Session::get("order_data.data1.regist_flg") ? Session::get("order_data.data1.regist_flg") : "",
								array('id'=> 'regist_off','class' => 'validate[required]')); ?></td>
						<td>登録しない</td>
					</tr>
				</table>
				<div class="description" style="margin-top:-5px;">
					※お客様情報を登録されますと、ご注文履歴の確認や、次回ご注文時に住所などの入力を省略できます。
				</div>
			</div>
		</div>
		<div class="form-group" id="regist_pass">
			<?php echo Form::label('パスワード', 'regist',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<div class="inner">
					<?php echo Form::password('order_password', Session::get("order_data.data1.order_password") ? Session::get("order_data.data1.order_password") : "",
						array('class' => 'validate[required,custom[onlyLetterNumber],minSize[3],maxSize[12]] form-control',"placeholder" => "3～12文字の英数字")); ?>
				</div>
			</div>
		</div>
	<?php endif;?>

	<div class="form-group">
		<?php echo Form::label('Aqours 1st LIVE 参加予定<span class="caption">※必須</span>', 'event_flg',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<table class="radio">
				<tr>
					<?php if (date("Ymd") >= "20161004" && date("Ymd") <= "20161024") :?>
						<td><?php echo Form::radio('order_event_flg', '1',Session::get("order_data.data1.order_event_flg") ? Session::get("order_data.data1.order_event_flg") :"", array('class' => 'validate[required]')) ?></td>
						<td>参加予定/未定</td>
					<?php endif;?>
					<td><?php echo Form::radio('order_event_flg', '0',Session::get("order_data.data1.order_event_flg") ? Session::get("order_data.data1.order_event_flg") :"", array('class' => 'validate[required]')) ?></td>
					<td>参加予定無し</td>
				</tr>
				<tr>
					<td colspan="6">※複数選択不可 / 回答必須</td>
				</tr>
			</table>
			<br>
			<div class="description" style="margin-top:-5px;">
				Aqours 1st LIVE 2017年2月25日,26日@横浜アリーナ公演参加予定について、ご回答お願いします。<br>
				※現状確定していない場合は「参加予定/未定」を選択してください。<br>
				※2日間のうち1日でも参加予定の方は「参加予定/未定」を選択してください。<br>
			</div>
		</div>
	</div>


	<div class="buttons count-2">
		<?php echo Html::anchor('/cart', 'カートに戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>

<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input2");
			$("#order").submit();
		});
		$("#login").validationEngine({promptPosition : "topLeft"});
		$("#order").validationEngine({
			promptPosition : "topLeft",
			scroll: false,
//			binded: false
		});
		regist_status = $("input[name='regist_flg']:checked").val();
		$("#regist_pass").hide();
		if(regist_status == 1){
			$("#regist_pass").show();
		}
		$('#regist_on').click(function() {
			$("#regist_pass").show();
		});
		$('#regist_off').click(function() {
			$("#regist_pass").hide();
		});
		// 検索データ受信時の処理
		function zipDataReceive(response, data) {
			response($.map(data, function (item) {
				// 住所
				var address1 = item.town + item.block;
				var address2 = item.street;
				// ラベル
				var label = item.zip_code + ' : ' + item.pref + address1 + address2;
				return {
					label: label,
					zip: item.zip_code,
					state: item.pref,
					address1: address1,
//					address2: item.biz == 1 ? item.street : '',
					address2: address2,
				}
			}));
		}

		// フォームの項目を更新
		function zipDataUpdate(ui) {
			$('#zip').val(ui.item.zip);
			$('#state').val(ui.item.state);
			$('#address1').val(ui.item.address1);
			$('#address2').val(ui.item.address2);
		}

		// 郵便番号の入力フィールドに Autocomplete を適用
		$('#zip').autocomplete({
			delay: 100,
			minLength: 6,
			source: function (request, response) {
				$.ajax({
					url: '/<?php echo $shop_data["dir"];?>/api/postal/address.json',
					dataType: 'json',
					data: {
						mode: 0,
						term: request.term,
						max_rows: 30,
						biz_mode: 2,
						sort: 0
					},
					success: function (data) {
						zipDataReceive(response, data);
					}
				});
			},
			select: function (event, ui) {
				zipDataUpdate(ui);
				return false;
			}
		});
	});
</script>
