<?php
//if (strripos(Input::user_agent(),"docomo") !== false) {
//	header('Content-Type: application/xhtml+xml');
//}
header('Content-Type: application/xhtml+xml');
echo '<?xml version="1.0" encoding="Shift_JIS" ?>';

if (strripos(Input::user_agent(),"docomo")){
	echo '<!DOCTYPE html PUBLIC "-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/1.1) 1.0//EN" "i-xhtml_4ja_10.dtd">';
}
if (strripos(Input::user_agent(),"kddi")){
	echo '<!DOCTYPE html PUBLIC "-//OPENWAVE//DTD XHTML 1.0//EN" "http://www.openwave.com/DTD/xhtml-basic.dtd">';
}
if ((strripos(Input::user_agent(),"vodafone")) || (strripos(Input::user_agent(),"softbank"))){
	echo '<!DOCTYPE html PUBLIC "-//J-PHONE//DTD XHTML Basic 1.0 Plus//EN""xhtml-basic10-plus.dtd">';
}

if (strripos(Input::user_agent(),"docomo") !== false ||
	strripos(Input::user_agent(),"kddi") !== false ||
	strripos(Input::user_agent(),"vodafone") !== false ||
	strripos(Input::user_agent(),"softbank") !== false) {
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-Type" content="application/xhtml+xml; charset=Shift_JIS" />
	<meta name="description" content="<?php echo $shop_data["description"];?>">
	<meta name="keywords" content="<?php echo $shop_data["keywords"];?>">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Cache-Control" content="max-age=0" />
	<meta http-equiv="Expires" content="0">
	<title><?php echo isset($title) ? $title . " - " : ""; ?><?php echo $shop_data["name"] ?></title>
</head>
<body style="background-color:#FFFFFF; color:#000000;" link="#000000" vlink="#000000" alink="#000000">
<style type="text/css">
	<![CDATA[
	a:link{color:#999999;}
	a:visited{color:#999999;}
	a:focus{color:#999999;}
	]]>
</style>
<div style="font-size:x-small;"><a id="top" name="top"></a>
	<div align="center">
		<?php echo Asset::img($shop_data["dir"].'/fp/header.gif',array("width" => "100%"));?>
	</div>
	<hr>
	<div align="center">STORE</div>
	<hr>

	<?php if(Request::active()->controller !== 'Controller_Order') :?>

		<div align="center">
			<?php echo Html::anchor('/'.$session_get_param, '<span><font color="#000000">ALL</font></span>', array("class" => "button text all"), true); ?>
			<?php if($category1): ?>
				<?php foreach($category1 as $c1): ?>
					<?php echo Html::anchor('/product/list/'.$session_get_param.'&c1='.$c1->id, "<span><font color='#000000'>".$c1->title."</font></span>") ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>

		<br>
		<div align="center">

			<?php echo Form::open(array("action" => '/cart' , "method" => "get"));?>
			<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
			<div align="center">
				<?php echo Form::submit('cart', 'カートを見る'); ?>
			</div>
			<?php echo Form::close();?>
			<!--		--><?php //echo Html::anchor('cart'.$session_get_param, Asset::img($shop_data["dir"].'/fp/button1.jpg',array("width"=>"80"))); ?>
			<!--		--><?php //echo Html::anchor('cart'.$session_get_param, "カートを見る"); ?>
		</div>
		<br>


		<div align="center" style="background-color: #DFDFDF">
			<?php if(Session::get('user.id')) : ?>
				<?php echo Html::anchor('mypage/'.$session_get_param,Asset::img($shop_data["dir"].'/fp/button2.gif',array("width"=>"80"))); ?>
				<?php echo Html::anchor('logout/'.$session_get_param,Asset::img($shop_data["dir"].'/fp/button3.gif',array("width"=>"80"))); ?>
			<?php else: ?>
				<?php echo Html::anchor('login'.$session_get_param, '<font color="#000000">ログイン</font>'); ?>
			<?php endif; ?>
		</div>

	<?php endif; ?>

	<div>
		<?php echo $content; ?>
	</div>

	<br>

<!--	<div align="center" style="background-color: #FFFFFF">-->
<!--		--><?php //echo Asset::img($shop_data["dir"].'/fp/banner_fp_dummy.gif',array("width"=>"90%")); ?>
<!--		<!--		--><?php ////echo Html::anchor('/'.$session_get_param,Asset::img($shop_data["dir"].'/fp/banner_fp_dummy.gif',array("width"=>"90%"))); ?>
<!--	</div>-->


	<table width="100%" bgcolor="#000">
		<tr>
			<td width="100%" align="center" bgcolor="#000">
				<font color="#FFFFFF">◆商品のお届けについて◆</font><br>
				<br>
				<font size="-2" color="#FFFFFF">
					2015/7/10(金)0:00～2015/7/26(日)23:59<br>→8/6(金)までのお届け<br>
					2015/7/27(月)0:00～2015/8/7(金)23:59<br>→8/10(金)頃より順次発送予定<br>
					2015/8/8(土)0:00～<br>→ご注文日から3〜5営業日以内で発送<br>
					<br>
				</font>
			</td>
		</tr>
	</table>
		<div>
			<br>
			■<a href="#top"><font color='#000000'>ページの先頭へ戻る</font></a><br>
			■<?php echo Html::anchor('/'.$session_get_param, "<font color='#000000'>ホームに戻る</font>") ?><br>
			■<?php echo Html::anchor('/cart'.$session_get_param, "<font color='#000000'>カートを見る</font>") ?><br>
			■<?php echo Html::anchor('/mypage'.$session_get_param, "<font color='#000000'>マイページ</font>") ?><br>
			<!--			<div align="right">-->
<!--				<a href="#top"><font color='#000000'>ページの先頭へ戻る→</font></a>&nbsp;-->
<!--			</div>-->
<!--			--><?php //if(Request::active()->controller !== 'Controller_Top' and
//			!(Request::active()->controller == 'Controller_Product' and Request::active()->action == 'list')) :?>
<!---->
<!--			<div align="right">-->
<!--				--><?php //echo Html::anchor('/'.$session_get_param, "<font color='#EA6900'>ショップへ戻る→</font>") ?><!--&nbsp;-->
<!--			</div>-->
<!--			--><?php //endif; ?>
		</div>

	<hr>

	※商品代金に加えて、全国一律（北海道・沖縄含む）配送料540円（tax in）を頂きます。なお、<?php echo number_format($shop_data["free_deliver_price"]);?>円以上のお買い上げで送料が無料となります。<br>
	※決済方法により、別途、決済手数料が発生する場合がございます。ご購入の際にご確認ください。<br>
	※ご注文後の変更、キャンセルは一切お受けできません。<br>
	※別々にいただいたご注文は、一つにまとめて出荷（同梱出荷）することはできません。<br>
	※商品には限りがあります。お申し込み多数の際は、受付期間内でも品切れとなる場合がございます。<br>
	※注文確認メールが届かない場合は、マイページ注文履歴をご確認ください。<br>
	※画像と実際の商品は色合い等、若干異なる場合がございますので、ご了承ください。<br>
	※各商品表示サイズをお確かめの上、お申し込みください。<br>
	※衣料商品は、特製上表示サイズと若干の誤差が生じる場合がございますので、あらかじめご了承ください。<br>
	※悪天候や交通事情等でお届けが遅れる場合がございます。<br>
	※通販会社（株）RENIにて販売代行しております。<br>
	<br>

	<!--	--><?php //if ($term): ?>
	<!--		--><?php //foreach ($term as $data): ?>
	<!--			<p>--><?php //echo $data->title ; ?><!--</p>-->
	<!--			<p>--><?php //echo $data->deliver_start_text ; ?><!--</p>-->
	<!--		--><?php //endforeach; ?>
	<!--	--><?php //endif; ?>


	<div style="background-color: #DFDFDF">
		<br>
		&nbsp; <?php echo Html::anchor('/fpinfo/guide/'.$session_get_param, '<font color="#000000">> ショッピングガイド</font>') ?><br />
		&nbsp; <?php echo Html::anchor('/fpinfo/privacy/'.$session_get_param, '<font color="#000000">> プライバシーポリシー</font>') ?><br />
		&nbsp; <?php echo Html::anchor('/fpinfo/legal/'.$session_get_param, '<font color="#000000">> 特定商取引法に基づく表記</font>') ?><br />
		&nbsp; <?php echo Html::anchor('/inquiry/'.$session_get_param, '<font color="#000000">> お問い合わせ</font>') ?><br />
		<br>
	</div>
	<div align="center">
		<p>Copyright (c) <?php echo date("Y");?> <?php echo $shop_data["copyright"] ? $shop_data["copyright"] : "RENI Co.,Ltd.";?> All Rights Reserved</p>
	</div>
</div>
</body>
</html>

