<p><?php echo implode('</p><p>', (array) Session::get_flash('error')); ?></p>
<div class="cart-list">
	<table>

		<?php
		$total = 0;
		$total_tax = 0;
		?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="total">小計<br />&nbsp;</th>
			<th class="delete"></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			?>
			<tr class="list">
				<td>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".Config::get("aws.image_path").$data["id"]."/".$data["id"]."_000.jpg" . '" alt="'.$data["title"]. '" />' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"]) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td class="countcontllor">
					<span><?php echo number_format($data["num"]);?></span>
					<a href="#" onclick="changeCart(<?php echo $data["did"];?>,1);">+</a>
					<?php if($data["num"] > 1): ?>
						<a href="#" onclick="changeCart(<?php echo $data["did"];?> ,-1);">-</a>
					<?php endif; ?>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
				<td>
					<div class="buttons">
						<?php echo Form::button('del', '削除', array('value' => $data["did"],'id' => 'del','class' => 'button rect size-S')); ?>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
		<tr class="fixed">
			<td></td>
			<td></td>
			<td>商品合計</td>
			<td>¥<?php echo number_format(intval($total_tax));?></td>
			<td></td>
		</tr>
		<?php else: ?>
			<div class="description" id="cookie_error"></div>
			現在、カートには商品がございません。商品をお選びください。
		<?php endif; ?>
		</tbody>
	</table>
	<div class="description">
		<p>
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
					送料無料です<br>
				<?php else :?>
					あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
				<?php endif; ?>
			<?php endif; ?>
		</p>
		<p>
			<?php if(Session::get("payment_ctl_cnt") < 3 && Session::get("payment_ctl_cnt") > 0):?>
				決済方法：<?php echo implode(" / ",Session::get("payment_ctl_data"));?><br>
			<?php endif; ?><br>
			※ご注文確定するまでは、カートに商品が入っていても在庫は確保されていません。予め以下ご了承ください。<br>
			・在庫の少ない商品は、ご注文手続き中に在庫切れとなる場合があります。<br>
			・ご注文手続き中に、ご注文商品の販売期間が終了してしまった場合、ご注文はできません。<br>
		</p>

		<?php if(date("Ymd") <= "20160103"):?>
			<br>
			<p>
				※年内のご注文商品の発送<br>
				年内発送：12/27(日)までのご注文<br>
				・12/28(月)以降のご注文：翌年1/5(火)以降順次発送<br>
				・12/28(月)～1/3(日)の間：お届け日指定不可<br>
			</p>
		<?php endif; ?>

	</div>
</div>

<?php if($total > 0 ): ?>
	<div class="buttons count-2">
		<?php if( Session::get("change") && Session::get("change") == 1) :?>
			<?php echo Html::anchor('/', '買い物を続ける',array('class' => 'button rect size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('/', '買い物を続ける',array('class' => 'button rect size-L'),true); ?>
		<?php endif;?>
		<?php if( Session::get("change") && Session::get("change") == 1) :?>
			<?php echo Html::anchor('/order/confirm', '変更',array('class' => 'button key rect size-L'),true); ?>
		<?php else:?>
			<?php echo Html::anchor('/order/input1', 'レジへ進む',array('class' => 'button key rect size-L'),true); ?>
		<?php endif;?>
	</div>
<?php endif; ?>

<?php echo Form::open(array('id' => 'delform' ,'action' => '/cart/del'));?>
<?php echo Form::hidden('did','',array('id' => 'del_did')); ?>
<?php echo Form::close();?>

<?php echo Form::open(array('id' => 'changeform' ,'action' => '/cart/add'));?>
<?php echo Form::hidden('did', '',array('id' =>"change_did")); ?>
<?php echo Form::hidden('num', '',array('id' =>"change_num")); ?>
<?php echo Form::close();?>
<script>
	$(function(){
		$("[id=del]").click(function() {
			$("#del_did").val($(this).val());
			$("#delform").submit();
		});
		// クッキー の有効/無効を取得
		if (navigator.cookieEnabled) {
			if ($('#alert').is('*')) {
				$('#alert').remove();
			}
		} else {
			$('#cookie_error').append('<div id="alert"><font color="red">続行するには、Webブラウザのcookieを有効にしてください<br>Cookieの設定方法については、ご利用のWebブラウザのツールバーにある「ヘルプ」をご覧ください。</font></div>');
		}
	});
	function changeCart(id,num){
		$("#change_did").val(id);
		$("#change_num").val(num);
		$("#changeform").submit();
	}
	$(document).ready(function(){
		(function(){
			var ans; //1つ前のページが同一ドメインかどうか
			var bs  = false; //unloadイベントが発生したかどうか
			var ref = document.referrer;
			$(window).bind("unload beforeunload",function(){
				bs = true;
			});
			re = new RegExp(location.hostname,"i");
			if(ref.match(re)){
				ans = true;
			}else{
				ans = false;
			}
			$('.historyback').bind("click",function(){
				var that = this;
				if(ans){
					history.back();
					setTimeout(function(){
						if(!bs){
							location.href = $(that).attr("href");
						}
					},100);
				}else{
					location.href = $(this).attr("href");
				}
				return false;
			});
		})();
	});
</script>

