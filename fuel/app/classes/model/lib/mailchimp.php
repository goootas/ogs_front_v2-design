<?php
use Fuel\Core\Log;
use Fuel\Core\Config;
use Fuel\Core\Session;

// メルマガ設定時、subscribed
// メルマガ解除時、unsubscribed
class Model_Lib_MailChimp extends \Model
{
	public static function get_member($apikey,$listid,$email)
	{
		$auth = base64_encode( 'user:'.$apikey );
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$listid.'/members/'.md5($email));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
		curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$result = curl_exec($ch);
		$obj = json_decode( $result , false ) ;
//		Log::error(print_r($obj,true));

		return $obj;
	}
	public static function subscribed($apikey,$listid,$email,$fname,$lname)
	{
		$mailchimp_status = "subscribed";
		//確認してあれば、更新。なければ登録
		$obj = self::get_member($apikey,$listid,$email);
		if(!isset($obj->status)){
			return true;
		}

		$auth = base64_encode( 'user:'.$apikey );
		$data = array(
			'apikey'        => $apikey,
			'email_address' => $email,
			'status'        => $mailchimp_status,
			'merge_fields'  => array(
				'FNAME' => $fname,
				'LNAME' => $lname
			)
		);

		$json_data = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
		curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		//確認してあれば、更新。なければ登録
		if($obj->status == "404")
		{
			curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$listid.'/members/');
			curl_setopt($ch, CURLOPT_POST, true);
		}else
		{
			curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$listid.'/members/'.md5($email));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		}

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);


//		Log::error(print_r($json_data,true));
//		Log::error(print_r(curl_getinfo($ch),true));
		$result = curl_exec($ch);
//		Log::error(print_r($result,true));
		return true;
	}
	public static function unsubscribed($apikey,$listid,$email)
	{
		//確認してあれば、更新。なければ登録
		$obj = self::get_member($apikey,$listid,$email);

		if(!isset($obj->status)){
			return true;
		}
		if($obj->status == "404")
		{
			return true;
		}

		$mailchimp_status = "unsubscribed";
		$auth = base64_encode( 'user:'.$apikey );
		$data = array(
			'status'        => $mailchimp_status
		);
		$json_data = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$listid.'/members/'.md5($email));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
		curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		//確認してあれば、更新。なければ登録
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

		$result = curl_exec($ch);
		Log::error(print_r($result,true));
		return true;
	}
}

