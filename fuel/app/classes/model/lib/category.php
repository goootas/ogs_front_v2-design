<?php
namespace Model\lib;

use Model_Db_Category1;
use Model_Db_Category2;
use Fuel\Core\Config;
use Fuel\Core\Log;

class Category extends \Model
{
	/*
	 * ID,TITLEを返す
	 * 該当レコードがなければNULLを返す。
	 */
	public static function getCategory1($shop_id)
	{
		$search_wheres = array();
		$search_wheres[] = array("shop_id", $shop_id);
		$search_wheres[] = array("status", "!=",Config::get('status_value.deleted'));
		$search_order = array();
		$search_order[] = array("sort","asc");
		$category = Model_Db_Category1::findListTool($search_wheres,$search_order);
		$data = array();
		$data = array("" => "未設定");
		if($category){
			foreach ($category as $value) {
				$data = $data + array($value->id => $value->title);
			}
		}
//		Log::warning(print_r($data,true));
		return $data;
	}
	/*
	 * ID,TITLEを返す
	 * 該当レコードがなければNULLを返す。
	 */
	public static function getCategory2($shop_id,$category1)
	{
		$search_wheres = array();
		$search_wheres[] = array("category1_id",$category1);
		$search_wheres[] = array("status",Config::get('status_value.enable'));
		$search_wheres[] = array("category1.status",Config::get('status_value.enable'));
		$search_wheres[] = array("category1.shop_id", $shop_id);
		$search_order = array();
		$search_order[] = array("sort","asc");
		$category = Model_Db_Category2::findDetailFront($search_wheres,$search_order);
		$data = array();
//		$data = array("" => "未設定");
		foreach ($category as $value) {
			//$data = array_merge($data,array($value->id => $value->title));
			$data = $data + array($value->id => $value->title);
		}
		return $data;
	}
}