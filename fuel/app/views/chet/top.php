<div class="HEADLINE">
	<div class="CONTAINER">
		<div class="mainslider">
			<img class="mainvisual photo-01" src="/assets/img/chet/IMG_main_02.jpg" alt="">
			<img class="mainvisual photo-02" src="/assets/img/chet/IMG_main_04.jpg" alt="">
			<img class="mainvisual photo-03" src="/assets/img/chet/IMG_main_02.jpg" alt="">
			<img class="mainvisual photo-04" src="/assets/img/chet/IMG_main_04.jpg" alt="">
		</div>
		<div class="TOPICS">
			<div class="title">
				<h2>NEW TOPICS</h2>
			</div>
			<ul class="slides">

				<?php if (isset($topics)): ?>
					<?php foreach ($topics as $key => $line): ?>
						<li>
							<?php $tag = '<dl><dt class="date">'.$line["view_date"].'</dt><dt class="text">'.$line["title"].'</dt></dl>'; ?>
							<?php echo Html::anchor('/topic/detail/'.$line["id"], $tag) ?>
						</li>
					<?php endforeach; ?>
				<?php else: ?>
				<?php endif; ?>

			</ul>
			<?php echo Html::anchor('/topic', "SHOW MORE", array('class' => 'btn primary')) ?>
		</div>
	</div>
</div>


<style>
	.CONTAINER2 {
		margin: 0 auto;
		overflow: hidden;
		padding-bottom: 20px;
		width: 800px;
	}
	@media screen and (max-width: 1024px) {
		.CONTAINER2 {
			min-width: 280px;
			/*padding: 80px 0;*/
			padding: 15px 10px 5px;
			width: auto;
		}
	}
	.youtube {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px;
		height: 0;
		overflow: hidden;
	}
	.youtube iframe {
		position: absolute;
		top: 0;
		left: 0;
		width: 100% !important;
		height: 100% !important;
	}
</style>

<div class="LINEUP">

	<div class="CONTAINER2">
		<div class="youtube">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLRzZIfIRGYHSmuQe0BvBNypYiHo9qc0iL" frameborder="0" allowfullscreen></iframe>
<!--			<iframe width="560" height="315" src="https://www.youtube.com/embed/lyR7ZkxXUUM?list=PLRzZIfIRGYHSmuQe0BvBNypYiHo9qc0iL&rel=0" frameborder="0" allowfullscreen></iframe>-->
<!--			<iframe width="560" height="315" src="https://www.youtube.com/embed?listType=user_uploads&list=UUPLRzZIfIRGYHSmuQe0BvBNypYiHo9qc0iL&rel=0" frameborder="0" allowfullscreen></iframe>-->
<!--			<iframe width="560" height="315" src="https://www.youtube.com/embed/EptzTZXv2Rc?list=PLRzZIfIRGYHSmuQe0BvBNypYiHo9qc0iL&rel=0" frameborder="0" allowfullscreen></iframe>-->
		</div>
	</div>


	<div class="CONTAINER">
		<div class="content-head">
			<div class="content-headline">
				<h1>LINEUP</h1>
				<hr>
			</div>
		</div>
		<div class="content-body">

			<?php if ($products): ?>
				<?php foreach ($products as $key => $line): ?>
					<?php
					$tag = '<div class="innner">';
					$tag .= '<figure>';
//					$tag .= '<img src="//'.$shop_data["s3bucket"].'.s3-ap-northeast-1.amazonaws.com/'.$line["imgs"][0]."?".time().'" alt="'.$line["title"].'">';
					$tag .= '<img src="//'.$shop_data["s3bucket"].'.s3-ap-northeast-1.amazonaws.com/'.$line["imgs"][0].'" alt="'.$line["title"].'">';
					$tag .= $line["body_list"];
					$tag .= '</figure>';
					$tag .= '<span class="btn primary">SHOW MORE</span>';
					$tag .= '</div>';
					?>
					<?php //echo $tag; ?>
					<?php echo Html::anchor('/product/detail/'.$line["id"], $tag) ?>

				<?php endforeach; ?>
			<?php else: ?>
			<?php endif; ?>

		</div>
	</div>
</div>

<div class="CONSEPT">
	<div class="CONTAINER">
		<div class="content-head">
			<div class="content-headline">
				<h1>CONCEPT</h1>
				<hr>
			</div>
			<p class="text-headline">IT IS ALL ABOUT<br>THE STYLE FOR ME! </p>
		</div>
		<div class="content-body">
			<img src="/assets/img/chet/IMG_concept.jpg" alt="">
			<?php echo Html::anchor('/concept', "SHOW MORE" ,array('class' => 'btn primary')) ?>
		</div>
	</div>
</div>

