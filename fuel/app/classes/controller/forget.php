<?php
use Fuel\Core\Cache;
use Fuel\Core\Config;
use Fuel\Core\Log;

/*
 * パスワードの再発行
 */
class Controller_Forget extends Controller_Basefront
{
	public function action_index()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Db_User::validate('forget_email');
			if ($val->run())
			{
				$user = Model_Db_User::findEmail2($this->shop_data["id"],Input::post("email"));
				if($user){
					$forget_param = Crypt::encode(Input::post("email"),  Config::get('cypt_key'));

					$forget = Model_Db_Forget::forge();
					$forget->key = Input::server('SERVER_NAME')."_".$this->shop_data["dir"]."_".$forget_param;
					$forget->value = strtotime("+ 1 hour");
					$forget->save();
//					Cache::set(Input::server('SERVER_NAME')."_".$this->shop_data["dir"]."_".$forget_param,strtotime("+ 1 hour"));

					//===================================================
					// メール送信
					//===================================================
					$shop_data = Model_Db_Shop::find($this->shop_data["id"]);
					Model_Lib_Mail::sendmail($shop_data,Config::get('mail.value.forget'),
						Input::post('email'),array(
							'forget_param' => $forget_param,
							'expire_time' => date("Y年m月d日 H時i分s秒",strtotime("+ 1 hour")),
						));
					Response::redirect('/forget/complete'.$this->session_get_param);
				}else{
					Session::set_flash('error', 'メールアドレスを再度ご確認の上、ご入力ください。');
				}
			}else{
				Session::set_flash('error', $val->error());
				Log::error('error', __FILE__ . __LINE__ . print_r($val->error(),true));
			}
		}
		$this->template->title = "パスワードの再発行";
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/forget/input_temp');
	}

	public function action_reset($email = null)
	{
		is_null($email) and Response::redirect('/forget'.$this->session_get_param);
//		try
		{
			//有効期限切れのデータを削除する。
			Model_Db_Forget::delExpired();
			//有効期間チェック用
//			Cache::get(Input::server('SERVER_NAME')."_".$this->shop_data["dir"]."_".$email);

			$forget = Model_Db_Forget::findForgetKey(Input::server('SERVER_NAME')."_".$this->shop_data["dir"]."_".$email);
			if(!$forget){
				Response::redirect('/forget/error'.$this->session_get_param);
			}

			Session::set("forget", array('email' => Crypt::decode($email,  Config::get('cypt_key'))));
			if (Input::method() == 'POST')
			{
				$users = Model_Db_User::findEmail2($this->shop_data["id"],Session::get('forget.email'));
				$users->password = Input::post('password');
				if ($users and $users->save()){
					Cache::delete(Input::server('SERVER_NAME')."_".$this->shop_data["dir"]."_".$email);
					Response::redirect('/forget/complete/1'.$this->session_get_param);
				}
			}
		}
//		catch (\CacheNotFoundException $e)
//		{
//			Response::redirect('/forget/error'.$this->session_get_param);
//		}
		$this->template->title = 'パスワード再発行';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/forget/input');
	}
	public function action_error($type=0)
	{
		$this->template->set_global('type', $type, false);

		// エラーページ
		$this->template->title = 'パスワード再発行';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/forget/complete');
	}
	public function action_complete($type=0)
	{
		$this->template->set_global('type', $type, false);

		// 完了ページ
		$this->template->title = 'パスワード再発行';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/forget/complete');
	}

}

