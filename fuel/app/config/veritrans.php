<?php
return array(
    //決済種別
    'status' => array(
        //ステータス1 決済種別
        'payment' => array(
            'var' =>array(
                'card'	=> 1, // クレカ
                'cvs'	=> 2, // コンビニ
                'cod'	=> 3, // 代引き
                'ato'	=> 4, // 後払い
                'npato'	=> 5, // 後払い
            ),
            'val' => array(
                1	=> 'card',
                2	=> 'cvs',
                3	=> 'cod',
                4	=> 'ato',
                5	=> 'npato',
            ),
        ),
        //ステータス2 実施したアクション
        'action' => array(
            'var' =>array(
                'authorize'		=> 1,
                'reauthorize'	=> 2,
                'capture'		=> 3,
                'cancel'		=> 4,
                'publish'		=> 5, // コンビニ決済用
                'cod'           => 6, // 代引き用
            ),
            'val' => array(
                1	=> 'authorize',
                2	=> 'reauthorize',
                3	=> 'capture',
                4	=> 'cancel',
                5	=> 'publish', // コンビニ決済用
                6	=> 'cod', // 代引き用
            ),
        ),
        'result' => array(
            'var' =>array(
                'failure'	=> 0,
                'success'	=> 1,
                'pending'	=> 9,
            ),
            'val' => array(
                0	=> 'failure',
                1	=> 'success',
                9	=> 'pending',
            ),
        ),
    ),
    //決済種別
    'payment' => array(
        1 => 'クレジットカード',
        2 => 'コンビニ決済',
        3 => '代金引換',
        4 => '後払い',
		5 => 'NP後払い',
		6 => '楽天ID決済',
    ),
    //コンビニ決済種別
    'cvs' => array(
        'sej1' => 'セブンイレブン',
        'econ1' => 'ローソン',
        'econ2' => 'ファミリーマート',
        'econ3' => 'ミニストップ',
        'other1' => 'サークルKサンクス',
        'other2' => 'デイリーヤマザキ',
        'econ4' => 'セイコーマート',
    ),
    //コンビニ決済手数料 TODO:店舗毎に手数料設定する可能性あり
    'cvs_fee' => array(
		array(
			'min' => 1,
			'max' => 2999,
			'fee' => 146,
		),
		array(
			'min' => 3000,
			'max' => 9999,
			'fee' => 178,
		),
		array(
			'min' => 10000,
			'max' => 29999,
			'fee' => 254,
		),
		array(
			'min' => 30000,
			'max' => 99999,
			'fee' => 545,
		),
		array(
			'min' => 100000,
			'max' => 299999,
			'fee' => 567,
		)
    ),
    //代引き手数料 TODO:店舗毎に手数料設定する可能性あり
    'cod_fee' => array(
        array(
            'min' => 1,
            'max' => 10000,
            'fee' => 324,
        ),
        array(
            'min' => 10001,
            'max' => 30000,
            'fee' => 432,
        ),
        array(
            'min' => 30001,
            'max' => 100000,
            'fee' => 648,
        ),
        array(
            'min' => 100001,
            'max' => 300000,
            'fee' => 1080,
        ),
        array(
            'min' => 300001,
            'max' => 500000,
            'fee' => 2160,
        )
    ),
    //後払い手数料 TODO:店舗毎に手数料設定する可能性あり
    'ato_fee' => array(
        array(
            'min' => 1,
            'max' => 999999999999,
            'fee' => 205,
        ),
    ),
    //クレジット分割支払
    'jpo' => array(
        '10' => '一括払い',
        '61C03' => '３回払い',
        '61C06' => '６回払い',
        '61C12' => '１２回払い',
        '61C24' => '２４回払い',
        '80' => 'リボ払い',
    ),
);
