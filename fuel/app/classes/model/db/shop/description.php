<?php
class Model_Db_Shop_Description extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'pcsp_top1',
		'pcsp_top2',
//		'pcsp_rule',
		'pcsp_privacy',
		'pcsp_legal',
		'pcsp_guide',
		'pcsp_faq',
		'fp_top1',
		'fp_top2',
//		'fp_rule',
		'fp_privacy',
		'fp_legal',
		'fp_guide',
		'fp_faq',
		'status',
		'insert_date',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_shop_description';

	public static function validate($factory)
	{
		$val = Validation::forge($factory);

		return $val;
	}

	public static function findListFront($where="")
	{
		$data = static::find('first', array(
				'where'		=> $where,
			)
		);
		return $data;
	}
}
