<?php // TODO: JQueryを使わないで処理する方法に変更　?>

<h4>会員ログイン</h4>
<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<div class="col-md-12">
	<div class="">
		<p>ログインID（メールアドレス）とパスワードを入力してください。</p>
		<?php echo Html::anchor('/regist'.$session_get_param, '<font color="#000000">新規会員登録はこちら</font>'); ?><br>
		<?php echo Html::anchor('/forget'.$session_get_param, '<font color="#000000">パスワードを忘れた方はこちら</font>') ?><br>
	</div>

	<?php echo Form::open();?>
	メールアドレス
	<div>
		<?php echo Form::input('email', Input::post('email', ''), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>
	パスワード
	<div>
		<?php echo Form::password('password', Input::post('password', ''), array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>

	<div>
		<?php echo Form::submit('exec', 'ログイン'); ?><br>
	</div>
	<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

	<font color="red">通販サイトリニューアルに伴い、2015年7月以前に会員登録されたお客様は、大変お手数ですが、再度新規会員登録をお願いいたします。</font><br>


	<?php echo Form::close();?>
</div>
