<div class="cart-list">
	<table>
		<?php
		$total = 0;
		$total_tax = 0;
		?>
		<?php if(count($cart) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="total">小計<br />&nbsp;</th>
			<th class="delete"></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($cart as $data): ?>
			<?php
			$total = $total + ($data["price"] * $data["num"]);
			$total_tax = $total_tax + (floor($data["price"] * $tax) * $data["num"]);
			$anchor_txt = $data["title"] ."&nbsp;". $data["op1"] ."&nbsp;". $data["op2"];
			?>
			<tr class="list">
				<td>
					<table>
						<tr>
							<td><?php echo '<img class="thumbnail" src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$data["imgs_pc"][0] . '" alt="'.$data["title"]. '" width="50px">' ?></td>
							<td><?php echo Html::anchor('/product/detail/'.$data["id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax));?></td>
				<td><?php echo number_format($data["num"]);?>&nbsp;&nbsp;</td>
				<td>¥<?php echo number_format(floor($data["price"] * $tax )* $data["num"]);?></td>
				<td></td>
			</tr>
		<?php endforeach; ?>
		<tr class="fixed">
			<td></td>
			<td></td>
			<td>商品合計</td>
			<td>¥<?php echo number_format(intval($total_tax));?></td>
			<td></td>
		</tr>
		<?php endif; ?>
		</tbody>
	</table>
	<?php if ($receives_count == 0 ) : ?>
	<div class="description">
		<p>
			<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
				<?php if (($total_tax) >= $shop_data["free_deliver_price"]) : ?>
					送料無料です<br>
				<?php else :?>
					あと¥<?php echo number_format($shop_data["free_deliver_price"] - ($total_tax));?>以上のご購入で送料無料です<br>
				<?php endif; ?>
			<?php endif; ?>
		</p>
	</div>
	<?php endif; ?>
</div>
