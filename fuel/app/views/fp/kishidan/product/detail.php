<div>
	<h4><?php echo isset($product) ? $product->title : '';?></h4>
</div>
<?php if(isset($images) && count($images) > 0): ?>
	<?php foreach ($images as $image): ?>
		<center><?php echo '<img src="/'.$shop_data["dir"].'/fpimg?img=https://'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$image.'" width="100%">';?></center>
	<?php endforeach; ?>
<?php endif; ?>
<div>
	<!-- 商品基本 -->
	商品コード:<br><?php echo isset($product) ? $product->code : '';?><br /><br />
	価格:<br>¥<?php echo isset($product) ? number_format(floor($product->price_sale * $tax)).'(tax in)' : '';?>
	<br>
	<br>
	<?php if($kikangai_flg) :?>
		<font color="red">現在販売しておりません</font><br>
		<br>
	<?php else :?>
		<?php echo Form::open(array('action' => '/cart/add'));?>
		<?php if(isset($product)): ?>


			<?php $stock_cnt = 0;?>
			<?php foreach ($product->detail as $detail): ?>
				<?php $stock_cnt = $stock_cnt + $detail->stock;?>
			<?php endforeach; ?>
			<?php if($stock_cnt <= 0 ): ?>

				<b><font color="red">SOLD OUT</font></b>
				<br>

			<?php else: ?>
				<?php foreach ($product->detail as $detail): ?>
					<?php
					if ($detail->stock <= 0 ){
						echo "在庫切れ";
					}else{
//				if ($product->publish_flg){?>
						<input type="radio" name="did" value="<?php echo $detail->id;?>">
						<?php
//				}
					}?>
					<?php echo $detail->option1_name ." ".$detail->option2_name; ?><br>
				<?php endforeach; ?>
				<br>
				<font color="red">※ご注文数量の増減は「購入する」ボタン押下後、カート内にて変更できます。</font>
				<br>
				<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
				<br>
				<?php echo Form::submit('buy', 'カートに入れる'); ?>
				<?php echo Form::hidden('num', '1'); ?>
				<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
			<?php endif; ?>

		<?php endif; ?>

		<?php echo Form::close();?>

	<?php endif; ?>

	<br>
	<?php echo isset($product) ? nl2br($product->comment) : '';?>

</div>
