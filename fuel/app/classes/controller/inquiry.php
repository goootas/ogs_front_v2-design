<?php
require_once APPPATH.'vendor/shuber/curl/curl.php';
require_once APPPATH.'vendor/HelpScout/ApiClient.php';

use HelpScout\ApiClient;

use Fuel\Core\Config;
use Fuel\Core\Log;

/**
 * 問い合わせ
 */
class Controller_Inquiry extends Controller_Basefront
{
	/*
	 * 問い合わせ入力・処理
	 */
	public function action_index()
	{
		if (Input::method() == 'POST')
		{
			//入力チェック
			$val = Model_Db_Inquiry::validate('create');
			if ($val->run())
			{
				$client = ApiClient::getInstance();
				$client->setKey($this->shop_data["helpscout_apikey"]);

				$cs_id = "";
				$cs_email = Input::post('email');
				$cs_firstname = Input::post('firstname');
				$cs_lastname = Input::post('lastname');

				// カスタマー取得及び作成
				$cs = $client->searchCustomersByEmail($cs_email);
				if($cs->getCount() == 0 ){
					// カスタマー新規作成
					$customer = new \HelpScout\model\Customer();
					$customer->setFirstName($cs_firstname);
					$customer->setLastName($cs_lastname);

					$customeremail = new \HelpScout\model\customer\EmailEntry();
					$customeremail->setValue($cs_email);
					$customer->setEmails($customeremail);

					$client->createCustomer($customer);
					$cs_id = $customer->getId();
				}else{
					// カスタマーID取得
					foreach($cs->getItems() as $val){
						$cs_id = $val->getId();
						if($cs_id){
							break;
						}
					}
					// カスタマー更新
					$customer = $client->getCustomer($cs_id);
					$customer->setFirstName($cs_firstname);
					$customer->setLastName($cs_lastname);
					$client->updateCustomer($customer);

				}

				$customerRef = $client->getCustomerRefProxy($cs_id, $cs_email);

				$conversation = new \HelpScout\model\Conversation();
				$conversation->setType     ('email');
				$conversation->setSubject  ("【".$this->shop_data["name"] . '】お問い合わせ');
				$conversation->setCustomer ($customerRef);
				$conversation->setCreatedBy($customerRef);

				$conversation->setMailbox  ($client->getMailboxProxy($this->shop_data["helpscout_mailboxno"]));

				$thread = new \HelpScout\model\thread\Customer();
				$thread->setBody(Input::post('message')."\n\n".Input::user_agent());

				$thread->setCreatedBy($customerRef);
				$conversation->addLineItem($thread);
				$client->createConversation($conversation);

//				$inquiry = Model_Db_Inquiry::forge(array(
//					'shop_id'		=> $this->shop_data["id"],
//					'user_id'		=> Session::get('user.id'),
//					'username'		=> Input::post('username'),
//					'email'			=> Input::post('email'),
//					'message'		=> Input::post('message')."\n\n".Input::user_agent(),
//					'status'		=> Config::get('inquiry.value.send'),
//				));
//				if ($inquiry and $inquiry->save())
//				{
//					//===================================================
//					// 問い合わせ内容メール送信（to ユーザー  bcc 運営）
//					//===================================================
//					$shop_data = Model_Db_Shop::find($this->shop_data["id"]);
//					Model_Lib_Mail::sendmail($shop_data,Config::get('mail.value.inquiry'),Input::post('email'),
//						array(
//							'username' => Input::post('username'),
//							'user_id' => Session::get('user.id'),
//							'email' => Input::post('email'),
//							'message' => Input::post('message')."\n\n".Input::user_agent(),
//						)
//					);
//
//					Response::redirect('/inquiry/complete'.$this->session_get_param);
//				}
				Response::redirect('/inquiry/complete'.$this->session_get_param);

			}else{
				Session::set_flash('error', $val->error());
				Log::error(__FILE__ . __LINE__ . print_r($val->error(),true));
			}
		}
		$this->template->title = 'お問い合わせ';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/inquiry/input');
	}
	/*
	 * 問い合わせ完了
	 */
	public function action_complete()
	{
		$this->template->title = 'お問い合わせ';
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/inquiry/complete');
	}

}

