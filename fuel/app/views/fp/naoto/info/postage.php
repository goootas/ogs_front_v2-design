<?php if ($postage): ?>
	<?php foreach ($postage as $base): ?>
		<div>
			<?php echo $base->prefectures->name; ?>
			¥<?php echo number_format($base->price); ?>
		</div>
	<?php endforeach; ?>
<?php else: ?>
<?php endif; ?>
<div>
	<hr>
	※以下の郵便番号への配送は、離島料金が適用されます。
</div>
<?php if ($rito): ?>
	<?php foreach ($rito as $base): ?>
		<div class="col-xs-3">
			〒<?php echo substr($base->zip, 0,3)."-".substr($base->zip, 3,4); ?>
			&nbsp;&nbsp;¥<?php echo number_format($base->price); ?>
		</div>
	<?php endforeach; ?>
<?php else: ?>
<?php endif; ?>
