<?php
class Model_Db_Group extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'shop_id',
		'parent_id',
		'title',
		'sort',
		'status',
		'insert_id',
		'insert_date',
		'update_id',
		'update_date',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
			'property' => 'insert_date',
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => true,
			'property' => 'update_date',
		),
	);
	protected static $_table_name = 'tbl_group';

	public static function validate($factory)
    {
        $val = Validation::forge($factory);

        return $val;
    }

	public static function findListTool($where="",$sort="")
	{
		$data = static::find('all', array(
			'where'		=> $where,
			'order_by'	=> $sort,
			)
		);
		return $data;
	}

	public static function get_list()
	{
		$data = static::find('all', array(
			'where' => array(
				array("status", '!=' , Config::get('status_value.deleted')),
				array("shop_id", '=' , Session::get('staff_info.shop_id')),
			))
		);
		return $data;
	}

	/*
	 * SELECT部品用
	 */
	public static function get_list_form_select()
	{
		$data = static::find('all', array(
			'select' => array(
				"id",
				"title",
				),
			'where' => array(
				array("status", '!=' , Config::get('status_value.deleted')),
				array("shop_id", '=' , Session::get('staff_info.shop_id')),

				),
			'order_by' => array(
				'sort' => 'asc'
				),
			)
		);

		$results = array();
		$results[""] = "未指定";
		foreach ($data as $value) {
			$results[$value->id] = $value->title;
		}
		return $results;
	}

}
