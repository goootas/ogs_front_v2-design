<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<div class="cart-list">
	<h4 class="headline">ご注文内容詳細</h4>

	<table>
		<?php $total_tax = 0; ?>
		<?php if(count($order["detail"]) > 0): ?>
		<thead>
		<tr>
			<th class="item">商品名<br />&nbsp;</th>
			<th class="price">販売価格<br />(tax in)</th>
			<th class="count">数量<br />&nbsp;</th>
			<th class="delete"></th>
			<th class="total">小計<br />&nbsp;</th>
		</tr>
		</thead>
		<tbody id="table-tbody">
		<?php foreach ($order["detail"] as $data): ?>
			<?php $product = $data["product"];?>
			<?php $total_tax = $total_tax + $data["price"];?>
			<tr class="list">
				<td>
					<?php
					$anchor_txt = "";
					$anchor_txt .= $product["base"]["title"] . $product["option1_name"] . $product["option2_name"];
					?>
					<table>
						<tr>
							<td>
								<?php if(isset($product["imgs_pc"][0])){ ?>
									<?php echo '<img src="//' . $shop_data['s3bucket'] . ".s3-ap-northeast-1.amazonaws.com/".$product["imgs_pc"][0] . '" alt="'.$product["base"]["title"]. '" width="50px">' ?>
								<?php } ?>
							</td>
							<td><?php echo Html::anchor('/product/detail/'.$product["product_id"],$anchor_txt) ?></td>
						</tr>
					</table>
				</td>
				<td>¥<?php echo number_format(floor($data["price"]/$data["num"]));?></td>
				<td>
					<?php echo number_format($data["num"]);?>&nbsp;&nbsp;
				</td>
				<td></td>
				<td>¥<?php echo number_format($data["price"]);?></td>
			</tr>
		<?php endforeach; ?>
		<?php endif; ?>
		</tbody>
	</table>
	<div class="totals">
		<ul>
			<li>
				<dl><dt>商品合計</dt><dd>¥<?php echo number_format(floor($total_tax));?></dd></dl>
			</li>
			<li>
				<dl><dt>送料</dt><dd>¥<?php echo number_format(intval($order["postage"]));?></dd></dl>
			</li>
			<li>
				<dl><dt>決済手数料</dt><dd>¥<?php echo number_format(intval($order["fee"]));?></dd></dl>
			</li>
			<li class="all">
				<dl><dt>総合計</dt><dd>¥<?php echo number_format(intval(floor($total_tax) + $order["postage"] + $order["fee"]));?></dd></dl>
			</li>
		</ul>
	</div>
</div>

<div class="fixed-data">
	<h4 class="headline">ご注文情報</h4>
	<table>
		<tr>
			<td class="head">受注番号</td>
			<td class="body"><?php echo $order["order_id"];?></td>
		</tr>
		<tr>
			<td class="head">注文日</td>
			<td class="body"><?php echo $order["insert_date"];?></td>
		</tr>
		<?php if( !$receives) :?>
			<tr>
				<td class="head">配送状況</td>
				<td class="body">
					<?php if($order["delivery_date"]):?>
						出荷済み(佐川急便お問い合わせNo:<a href="http://k2k.sagawa-exp.co.jp/p/web/okurijosearch.do?okurijoNo=<?php echo $order["tracking_number"];?>" target="_blank" style="text-decoration: underline;"><?php echo $order["tracking_number"];?></a>)
					<?php else:?>
						<?php if($order["cancel_date"]):?>
							出荷取消
						<?php else :?>
							出荷準備中
						<?php endif;?>
					<?php endif;?>
				</td>
			</tr>
		<?php endif;?>
		<tr>
			<td class="head">その他</td>
			<td class="body">
				<?php if($order["cancel_date"]):?>
					注文キャンセル
				<?php endif;?>
			</td>
		</tr>
	</table>
</div>

<?php if( $receives) :?>
	<div class="fixed-data">
		<h4 class="headline">商品受取日時</h4>

		<table>
			<tr>
				<td class="head">受取日時</td>
				<td class="body"><?php echo $receives["title1"].$receives["title2"]?></td>
			</tr>
		</table>
	</div>
<?php endif;?>

<div class="fixed-data">
	<h4 class="headline">お客様情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo $order["order_username_sei"].$order["order_username_mei"]?> 様</td>
		</tr>
		<tr>
			<td class="head">ふりがな</td>
			<td class="body"><?php echo $order["order_username_sei_kana"].$order["order_username_mei_kana"]?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr($order["order_zip"], 0,3);?>-<?php echo substr($order["order_zip"], 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo $order["order_prefecture"];?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo $order["order_address1"];?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo $order["order_address2"];?></td>
		</tr>
		<tr>
			<td class="head">メールアドレス</td>
			<td class="body"><?php echo $order["order_email"];?></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo $order["order_tel1"];?>-<?php echo $order["order_tel2"];?>-<?php echo $order["order_tel3"];?></td>
		</tr>
		<tr>
			<td class="head">性別</td>
			<td class="body">
				<?php if ($order["order_sex"]=="1"):?>男性
				<?php elseif ($order["order_sex"]=="2"):?>女性
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
		<tr>
			<td class="head">生年月日</td>
			<td class="body">
				<?php if ($order["order_birthday"]):?>
					<?php echo substr($order["order_birthday"], 0,4);?>年<?php echo substr($order["order_birthday"], 4,2);?>月<?php echo substr($order["order_birthday"], 6,2);?>日
				<?php else:?>指定なし
				<?php endif;?>
			</td>
		</tr>
	</table>
</div>


<div class="fixed-data">
	<h4 class="headline">お届け先情報</h4>
	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo $order["deliver_username_sei"];?> <?php echo $order["deliver_username_mei"];?> 様</td>
		</tr>
		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo substr($order["deliver_zip"], 0,3);?>-<?php echo substr($order["deliver_zip"], 3,4);?></td>
		</tr>
		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo $order["deliver_prefecture"];?></td>
		</tr>
		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo $order["deliver_address1"];?></td>
		</tr>
		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo $order["deliver_address2"];?></td>
		</tr>
		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo $order["deliver_tel1"];?>-<?php echo $order["deliver_tel2"];?>-<?php echo $order["deliver_tel3"];?></td>
		</tr>

		<?php if( !$receives) :?>
			<tr>
				<td class="head">お届け日</td>
				<td class="body">
					<?php if(!$order["deliver_day"]) : ?>
						指定無し
					<?php else : ?>
						<?php echo sprintf("%04d年%02d月%02d日",
							substr($order["deliver_day"],0,4),
							substr($order["deliver_day"],4,2),
							substr($order["deliver_day"],6,2));?>
					<?php endif; ?>
					<?php if(date("Ymd") < "20160816" ) : ?>
						<br>受注日翌日から2日以内で発送（土日祝日除く / 8/11(木・祝)以降のご注文：8/17(水)以降順次発送）
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="head">お届け希望時間帯</td>
				<td class="body">
					<?php if(!$order["deliver_time"]) : ?>
						指定無し
					<?php else : ?>
						<?php if($order["deliver_time"] == "AM") : ?>
							<?php echo "午前中";?>
						<?php else : ?>
							<?php $dt = explode("-",$order["deliver_time"]);?>
							<?php echo sprintf("%d時〜%d時",$dt[0],$dt[1]);?>
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endif;?>

	</table>
</div>

<div class="fixed-data">
	<h4 class="headline">お支払い情報</h4>
	<table>
		<tr>
			<td class="head">支払方法</td>
			<td class="body"><?php echo Config::get("payment.status_user.".$order["payment"]);?><?php if($order["payment"] == 2) echo Config::get("cvs.".$order["cvs_type"]);?></td>
		</tr>
	</table>
</div>

<div class="buttons count-2">
	<?php echo Html::anchor('/mypage', '戻る',array('class' => 'button rect key size-L'),true); ?>
</div>
