<?php
/*
 * リダイレクトページ
 * 期間終了後に削除予定(RIP専用)
 */
class Controller_qa extends Controller_Basefront
{
	public function action_index()
	{
		// FPはサイトTOPへ、それ以外はGoogleフォームへリダイレクト
		$url = "/";
		if($this->agent_dir == "")
		{
			$url = "http://goo.gl/forms/nepj6sYijz";
		}
		Response::redirect($url);
		exit;
	}
}

