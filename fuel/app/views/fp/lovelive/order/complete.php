<h4>注文完了</h4>

<div>
	<p>
		ご注文いただき誠にありがとうございました。<br>
		受注番号：<?php echo $order->order_id;?><br><br>

		ご注文内容を確認する場合は、「マイページ」より上記受注番号からご確認ください。（お客様情報登録された方のみ）<br /><br />
		また、注文確認のメールを「<?php echo $order->order_email;?>」宛に送信しておりますので、お客様情報登録されてない方はこちらで注文内容をご確認いただけます。<br /><br />

		<?php if ($order->payment == 1) : ?>
			※クレジットカード請求明細の請求屋号は「official-goods-store.jp」になります。<br /><br />
		<?php endif;?>

		<?php if ($order->payment == 2) : ?>
			■支払方法<br />
			オンライン決済番号または受付番号を控えて、ご指定いただいたコンビニエンスストアにて下記の支払い期限までにお支払いください。<br>
			<br>
			【支払い先コンビニ】<br><?php echo Config::get("cvs.".$order->cvs_type);?><br><br>

			<?php if (substr($order->cvs_type ,0,-1) == "econ") : ?>
				【受付番号】<br><?php echo $order->cvs_receipt_no;?><br><br>
			<?php elseif (substr($order->cvs_type ,0,-1) == "sej") : ?>
				【オンライン決済番号】<br><?php echo $order->cvs_receipt_no;?><br><br>
			<?php elseif (substr($order->cvs_type ,0,-1) == "other") : ?>
				【オンライン決済番号】<br><?php echo $order->cvs_receipt_no;?><br><br>
			<?php endif;?>

			<?php if ($order->cvs_haraikomi_url) : ?>
				【払込票URL】<a href="<?php echo $order->cvs_haraikomi_url;?>" target="_blank" style="text-decoration: underline"><?php echo $order->cvs_haraikomi_url;?></a><br>
			<?php endif;?>

			【支払期限】<br><?php echo $order->cvs_limit_date;?><br><br>

			【店頭でのお支払い方法】<br>
			<?php if ($order->cvs_type == "sej1") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/711_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/711_m.html</a><br>
			<?php elseif ($order->cvs_type == "econ1") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/lawson_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/lawson_m.html</a><br>
			<?php elseif ($order->cvs_type == "econ2") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/famima2_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/famima2_m.html</a><br>
			<?php elseif ($order->cvs_type == "econ3") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/ministop_loppi_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/ministop_loppi_m.html</a><br>
			<?php elseif ($order->cvs_type == "econ4") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/seicomart_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/seicomart_m.html</a><br>
			<?php elseif ($order->cvs_type == "other1") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/circleksunkus_econ_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/circleksunkus_econ_m.html</a><br>
			<?php elseif ($order->cvs_type == "other2") : ?>
				<a href="https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/dailyamazaki_m.html" style="text-decoration: underline" target="_blank" >https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/dailyamazaki_m.html</a><br>
			<?php endif;?>
			<br>
			※上記期限までにお支払いいただけなかった場合、ご注文はキャンセルとさせていただきます。<br>
			<br />
		<?php endif;?>

		<?php if ($order->payment == 4) : ?>
			※代金のお支払いについて<br>
			・請求書は商品に同封されておりますので、請求書到着後14日以内にお支払いください。<br>
			・銀行 / 郵便局 / コンビニでお支払いいただけます。<br>
			・登録ご住所・電話番号等の不備、限度額超過、請求期限切れ未払い取引があるなどの理由により後払い決済与信審査がNGとなる場合がございます。その場合別途メールにてご連絡いたしますが、ご注文はキャンセル扱いとなりますので、予めご了承ください。<br>
			<?php if ($order->result != 1) : ?>
				・お届け先の住所 / 電話番号を間違って登録してしまった場合、「後払い.com」の審査が通らないことがあります。その場合は別途メールにてご連絡いたしますので、メール内容ご確認ください。<br>
			<?php endif;?>
		<?php endif;?>

		<?php if ($order->payment == 5) : ?>
			<br />
			※後払い決済をご利用のお客様<br />
			ご注文確定後、後払い決済与信審査を実施いたしますが、登録ご住所 / 電話番号等の不備、限度額超過、請求期限切れ未払い取引があるなどの理由により後払い決済与信審査がNGとなる場合がございます。その場合別途メールにてご連絡いたしますが、ご注文はキャンセル扱いとなりますので、予めご了承ください。(ご注文された商品は販売用在庫に戻ります)<br />
		<?php endif;?>

	</p>
	<br>
	※注文確認メールが届かない場合<br>
	注文確認メールが届かない場合でも、正常にご注文をお受けさせていただいております。<br>
	<br>
	＊注文確認メールが届かない原因<br>
	(!) メールアドレス間違い<br>
	(!) 迷惑メールボックスに注文確認メールが入っている<br>
	(!) スマートフォンやフィーチャーフォンをご利用の場合、「PCからのメールを受信しない」「なりすまし設定を有効にしている」等のメール受信設定に引っかかっている（ドメイン指定受信設定されている方は「**@official-goods-store.jp」からのメールを受信できるようにしてください。）<br>
	<br>


</div>


