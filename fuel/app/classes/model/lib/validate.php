<?php
namespace Model\lib;

use Fuel\Core\DB;
use Fuel\Core\Log;

class Validate extends \Model
{
	public static function checkZip($zip)
	{
		if(strlen($zip) != 7) return false;

		$data = Postal::getPostalZip($zip);
		$status = true;
		if(!$data){
			$status = false;
		}
		return $status;
	}

	public static function checkZipAddress($zip,$state,$address1,$address2)
	{
		if(strlen($zip) != 7) return false;

		$data = Postal::getPostalZip($zip);
		if(!$data){
			return false;
		}

		$hit = 0;
		foreach ($data as $value) {
//			$needle = $value["pref"].$value["town"].$value["block"].$value["street"];
			$needle = $value["pref"].$value["town"].$value["block"];
			$haystack = $state.$address1.$address2;
			$res = mb_strpos($haystack,$needle);
			if($res === 0 ){
				$hit++;
			}
		}

		$status = false;
		if($hit > 0){
			$status = true;
		}else{
			Log::error("POSTAL ERROR > ".print_r($zip.":".$state.":".$address1.":".$address2,true));
		}
		return $status;
	}

	public static function checkTel($tel)
	{
		// 数値以外はNG
		if (!preg_match("/^[0-9]+$/", $tel)) {
			return false;
		}

		$tel = trim($tel);

		switch (substr($tel,0,3)) {
			case "020":
			case "050":
			case "060":
			case "070":
			case "080":
			case "090":
				$num = 11;
				break;
			default:
				$num = 10;
				break;
		}

		$status = true;
		if(strlen($tel) != $num){
			$status = false;
		}
		return $status;
	}

	/*
	 * 個人認証用チェックロジック
	 * （氏名＋電話番号）or（氏名＋メールアドレス）で同一期間中に購入済みかチェックを行う
	 */
	public static function checkPersonalAuthAlreadyOrder($shop_id, $w_order1,$w_order2,$type="")
	{
		$w_error_cnt = 0;

		// 会場購入以外もチェック対象にする場合
		if($type){
			if(!\Model\lib\Validate::_checkPersonalAuthAlreadyNormalOrder($shop_id,
				$w_order1["order_username_sei"], $w_order1["order_username_mei"],
				$w_order1["order_state"], $w_order1["order_address1"],$w_order1["order_address2"],
				$w_order1["order_tel1"], $w_order1["order_tel2"], $w_order1["order_tel3"],
				$w_order1["order_email"])){
				$w_error_cnt++;
			}
			if(!\Model\lib\Validate::_checkPersonalAuthAlreadyNormalOrder($shop_id,
				$w_order2["deliver_username_sei"], $w_order2["deliver_username_mei"],
				$w_order2["deliver_state"], $w_order2["deliver_address1"],$w_order2["deliver_address2"],
				$w_order2["deliver_tel1"], $w_order2["deliver_tel2"], $w_order2["deliver_tel3"])){
				$w_error_cnt++;
			}
		}
		// 会場購入のみチェック対象にする場合
		else{
			if(!\Model\lib\Validate::_checkPersonalAuthAlreadyOrder($shop_id,
				$w_order1["order_username_sei"], $w_order1["order_username_mei"],
				$w_order1["order_state"], $w_order1["order_address1"],$w_order1["order_address2"],
				$w_order1["order_tel1"], $w_order1["order_tel2"], $w_order1["order_tel3"],
				$w_order1["order_email"])){
				$w_error_cnt++;
			}
			if(!\Model\lib\Validate::_checkPersonalAuthAlreadyOrder($shop_id,
				$w_order2["deliver_username_sei"], $w_order2["deliver_username_mei"],
				$w_order2["deliver_state"], $w_order2["deliver_address1"],$w_order2["deliver_address2"],
				$w_order2["deliver_tel1"], $w_order2["deliver_tel2"], $w_order2["deliver_tel3"])){
				$w_error_cnt++;
			}
		}

		return $w_error_cnt;
	}
	/*
	 * 個人認証用チェックロジック
	 * （氏名＋電話番号）or（氏名＋メールアドレス）で同一期間中に購入済みかチェックを行う
	 *
	 * 住所、電話番号、メールアドレスいずれか２つヒットした場合、falseを返す
	 *
	 */
	public static function _checkPersonalAuthAlreadyNormalOrder($shop_id,$sei,$mei,$pref,$addr1,$addr2,$tel1,$tel2,$tel3,$mail = "")
	{
		if(!$shop_id) return false;
		if(!$sei) return false;
		if(!$mei) return false;
		if(!$pref) return false;
		if(!$addr1) return false;
		if(!$addr2) return false;
		if(!$tel1) return false;
		if(!$tel2) return false;
		if(!$tel3) return false;

		// 現在の販売期間を取得(対象期間を取得)
		$sql = "select start_date,finish_date from tbl_product_term where 1 = 1 ";
		$sql .= "and start_date <= now() and finish_date >= now() and status = 1 ";
		$sql .= "and shop_id = ".$shop_id;
		Log::debug(print_r($sql,true));
		$query = DB::query($sql);
		$term = $query->execute()->current();
		if(!$term){
			return false;
		}

		$counter = 0 ;

		// メールアドレス
		$sql = "";
		$sql .= "select count(order_id) as cnt from tbl_order ";
		$sql .= " where 1 = 1 ";
		$sql .= " and shop_id = ".$shop_id;
		$sql .= " and cancel_date is null ";
		$sql .= " and insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and order_email = :email";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'email'	=> trim($mail),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		// 氏名
		$sql = "";
		$sql .= "select count(order_id) as cnt from tbl_order ";
		$sql .= " where 1 = 1 ";
		$sql .= " and shop_id = ".$shop_id;
		$sql .= " and cancel_date is null ";
		$sql .= " and insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " order_username_sei = :sei";
		$sql .= " and order_username_mei = :mei";
		$sql .= " ) or ( ";
		$sql .= " deliver_username_sei = :sei";
		$sql .= " and deliver_username_mei = :mei";
		$sql .= ")) ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'sei'	=> trim($sei),
				'mei'	=> trim($mei),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		// 電話番号
		$sql = "";
		$sql .= "select count(order_id) as cnt from tbl_order ";
		$sql .= " where 1 = 1 ";
		$sql .= " and shop_id = ".$shop_id;
		$sql .= " and cancel_date is null ";
		$sql .= " and insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " order_tel1 = :tel1";
		$sql .= " and order_tel2 = :tel2";
		$sql .= " and order_tel3 = :tel3";
		$sql .= " ) or ( ";
		$sql .= " deliver_tel1 = :tel1";
		$sql .= " and deliver_tel2 = :tel2";
		$sql .= " and deliver_tel3 = :tel3";
		$sql .= ")) ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'tel1'	=> trim($tel1),
				'tel2'	=> trim($tel2),
				'tel3'	=> trim($tel3),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		if($counter > 2){
			return false;
		}

		// 住所
		$sql = "";
		$sql .= "select count(order_id) as cnt from tbl_order ";
		$sql .= " where 1 = 1 ";
		$sql .= " and shop_id = ".$shop_id;
		$sql .= " and cancel_date is null ";
		$sql .= " and insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " order_prefecture = :prefecture";
		$sql .= " and order_address1 = :address1";
		$sql .= " and order_address2 = :address2";
		$sql .= " ) or ( ";
		$sql .= " deliver_prefecture = :prefecture";
		$sql .= " and deliver_address1 = :address1";
		$sql .= " and deliver_address2 = :address2";
		$sql .= ")) ";
		$query = DB::query($sql);

		$query->parameters(
			array(
				'prefecture'	=> trim($pref),
				'address1'		=> trim(mb_convert_kana($addr1, "ASV")),
				'address2'		=> trim(mb_convert_kana($addr2, "ASV")),
			)
		);

		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		if($counter > 2){
			return false;
		}
		return true;
	}
	/*
	 * 個人認証用チェックロジック（会場受取用）
	 * （氏名＋電話番号）or（氏名＋メールアドレス）で同一期間中に購入済みかチェックを行う
	 */
	public static function _checkPersonalAuthAlreadyOrder($shop_id,$sei,$mei,$pref,$addr1,$addr2,$tel1,$tel2,$tel3,$mail = "")
	{
		if(!$shop_id) return false;
		if(!$sei) return false;
		if(!$mei) return false;
		if(!$pref) return false;
		if(!$addr1) return false;
		if(!$addr2) return false;
		if(!$tel1) return false;
		if(!$tel2) return false;
		if(!$tel3) return false;

		// 現在の販売期間を取得(対象期間を取得)
		$sql = "select start_date,finish_date from tbl_product_term where 1 = 1 ";
		$sql .= "and start_date <= now() and finish_date >= now() and status = 1 ";
		$sql .= "and shop_id = ".$shop_id;
		Log::debug(print_r($sql,true));
		$query = DB::query($sql);
		$term = $query->execute()->current();
		if(!$term){
			return false;
		}

		$counter = 0 ;

		// メールアドレス
		$sql = "";
		$sql .= "select count(tbl_order.order_id) as cnt from tbl_local_receives_order ";
		$sql .= " left join tbl_order on tbl_order.order_id = tbl_local_receives_order.order_id where 1 = 1 ";
		$sql .= " and tbl_order.shop_id = ".$shop_id;
		$sql .= " and tbl_order.cancel_date is null ";
		$sql .= " and tbl_order.insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and tbl_order.order_email = :email";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'email'	=> trim($mail),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		// 氏名
		$sql = "";
		$sql .= "select count(tbl_order.order_id) as cnt from tbl_local_receives_order ";
		$sql .= " left join tbl_order on tbl_order.order_id = tbl_local_receives_order.order_id where 1 = 1 ";
		$sql .= " and tbl_order.shop_id = ".$shop_id;
		$sql .= " and tbl_order.cancel_date is null ";
		$sql .= " and tbl_order.insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " tbl_order.order_username_sei = :sei";
		$sql .= " and tbl_order.order_username_mei = :mei";
		$sql .= " ) or ( ";
		$sql .= " tbl_order.deliver_username_sei = :sei";
		$sql .= " and tbl_order.deliver_username_mei = :mei";
		$sql .= ")) ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'sei'	=> trim($sei),
				'mei'	=> trim($mei),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		// 電話番号
		$sql = "";
		$sql .= "select count(tbl_order.order_id) as cnt from tbl_local_receives_order ";
		$sql .= " left join tbl_order on tbl_order.order_id = tbl_local_receives_order.order_id where 1 = 1 ";
		$sql .= " and tbl_order.shop_id = ".$shop_id;
		$sql .= " and tbl_order.cancel_date is null ";
		$sql .= " and tbl_order.insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " tbl_order.order_tel1 = :tel1";
		$sql .= " and tbl_order.order_tel2 = :tel2";
		$sql .= " and tbl_order.order_tel3 = :tel3";
		$sql .= " ) or ( ";
		$sql .= " tbl_order.deliver_tel1 = :tel1";
		$sql .= " and tbl_order.deliver_tel2 = :tel2";
		$sql .= " and tbl_order.deliver_tel3 = :tel3";
		$sql .= ")) ";
		$query = DB::query($sql);
		$query->parameters(
			array(
				'tel1'	=> trim($tel1),
				'tel2'	=> trim($tel2),
				'tel3'	=> trim($tel3),
			)
		);
		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		if($counter > 2){
			return false;
		}

		// 住所
		$sql = "";
		$sql .= "select count(tbl_order.order_id) as cnt from tbl_local_receives_order ";
		$sql .= " left join tbl_order on tbl_order.order_id = tbl_local_receives_order.order_id where 1 = 1 ";
		$sql .= " and tbl_order.shop_id = ".$shop_id;
		$sql .= " and tbl_order.cancel_date is null ";
		$sql .= " and tbl_order.insert_date between '".$term["start_date"]."' and '".$term["finish_date"]."' ";
		$sql .= " and (( ";
		$sql .= " tbl_order.order_prefecture = :prefecture";
		$sql .= " and tbl_order.order_address1 = :address1";
		$sql .= " and tbl_order.order_address2 = :address2";
		$sql .= " ) or ( ";
		$sql .= " tbl_order.deliver_prefecture = :prefecture";
		$sql .= " and tbl_order.deliver_address1 = :address1";
		$sql .= " and tbl_order.deliver_address2 = :address2";
		$sql .= ")) ";
		$query = DB::query($sql);

		$query->parameters(
			array(
				'prefecture'	=> trim($pref),
				'address1'		=> trim(mb_convert_kana($addr1, "ASV")),
				'address2'		=> trim(mb_convert_kana($addr2, "ASV")),
			)
		);

		$order = $query->execute()->current();
		if($order && $order["cnt"] > 0){
			$counter++;
		}

		if($counter > 2){
			return false;
		}

		return true;
	}
}
