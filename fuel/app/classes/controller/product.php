<?php
use Model\lib\Image;

class Controller_Product extends Controller_Basefront
{
	/*
	 * 商品TOP
	 */
	public function action_index()
	{
		Response::redirect("/product/list/".$this->session_get_param);
	}

	/*
	 * 商品一覧
	 */
	public function action_list()
	{
		//----------------------------------
		// 販売期間情報取得
		//----------------------------------
		$term_ids = array();
		foreach($this->term_data as $val){
			$term_ids[] = $val->id;
		}

		//----------------------------------
		// 商品情報取得
		//----------------------------------
		$wheres["shop_id"] = $this->shop_data["id"];
		$wheres["term_ids"] = implode(",",$term_ids);
		$page = Input::get("p") ? Input::get("p") : 1;
		if(Input::get("c1")) $wheres["c1"] = Input::get("c1");
//		if(Input::get("c2")) $wheres["c2"] = Input::get("c2");
//		if(Input::get("g")) $wheres["g"] = Input::get("g");

		$param = Input::server("QUERY_STRING") ? "?".Input::server("QUERY_STRING") : "";

		$page_limit_num = 12;
		if($this->agent_dir !== ""){
			$page_limit_num = 6;
		}

		$product_total = Model_Db_Product::findProductList($wheres);
		$paginations = Pagination::forge('list',
			array(
				'pagination_url'    => ($this->shop_data["type"] ? "/".$this->shop_data["dir"] : "").'/product/list/'.$param,
				'total_items'       => count($product_total),
				'per_page'          => $page_limit_num,
				'uri_segment'       => "p",
				'current_page'      => $page,
				'num_links'         => 0,
			)
		);

		// 最大件数がページング数に見たない場合は処理は行わない。
		if($page_limit_num > count($product_total)){
			$product_list = $product_total;
		}else{
			$product_list = Model_Db_Product::findProductList($wheres,$paginations);
		}

		$this->template->set_global('paginations', $paginations->render(true), false);
		$this->template->set_global('total_pages', Pagination::instance('list')->total_pages, false);


		//----------------------------------
		// 商品画像（キャッシュ化）
		//----------------------------------
		foreach($product_list as $key => $product){
			if($this->agent_dir == ""){
				$imgs = Image::getImage($product["id"]);
			}else{
				$imgs = Image::getImageFp($product["id"]);
			}
			$product_list[$key]["imgs"] = $imgs;
		}

		$this->template->set_global('products', $product_list, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/top');
	}

	/*
	 * 商品詳細
	 */
	public function action_detail($id = null)
	{
		is_null($id) and Response::redirect('/product/list'.$this->session_get_param);
		//----------------------------------
		// 商品情報取得
		//----------------------------------
		try
		{
			$product_base = Cache::get("product_base_".$this->shop_data["id"]."_".$id);
		}
		catch (\CacheNotFoundException $e)
		{
			$product_base 	= Model_Db_Product::findDetail($this->shop_data["id"],$id);
			Cache::set("product_base_".$this->shop_data["id"]."_".$id, $product_base, 60 * 1);
		}
		$product_detail = Model_Db_Product_Detail::findList($id);

		$matrix_x = array();
		$matrix_y = array();
		foreach ($product_detail as $dtl) {
			$matrix_x[] = $dtl->option1_name;
			$matrix_y[] = $dtl->option2_name;
		}
		$matrix_x = array_unique($matrix_x);
		$matrix_y = array_unique($matrix_y);
		$matrix_stock = array();
		foreach ($matrix_x as $x) {
			foreach ($matrix_y as $y) {
				$matrix_stock[$x][$y] = "-";
				foreach ($product_detail as $dtl) {
					if($dtl->option1_name == $x && $dtl->option2_name == $y){
//						$matrix_stock[$x][$y] = $dtl->stock;
						if ($dtl->stock > 0 ){
							if ($dtl->stock <= 5 ){
								$matrix_stock[$x][$y] = "△";
							}else{
								$matrix_stock[$x][$y] = "○";
							}
						}else{
							$matrix_stock[$x][$y] = "×";
						}
					}
				}
			}
		}

		$stock_status = "";
		$stock_status .= "<table class=\"matrix_stock\">";
		$stock_status .= "<tr>";
		$stock_status .= "<th></th>";
		foreach ($matrix_stock as $xkey => $xval) {
			foreach ($xval as $ykey => $yval) {
				$stock_status .= "<th>".$ykey."</th>";
			}
			break;
		}
		$stock_status .= "</tr>";
		foreach ($matrix_stock as $xkey => $xval) {
			$stock_status .= "<tr>";
			$stock_status .= "<th>".$xkey."</th>";
			foreach ($xval as $ykey => $yval) {
				$stock_status .= "<td>".$yval."</td>";
			}
			$stock_status .= "</tr>";
		}
		$stock_status .= "</table>";
		$product_base["comment"] = str_replace("%%STOCK_STATUS%%",$stock_status, $product_base["comment"]);

		\Fuel\Core\Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@",true));
		\Fuel\Core\Log::debug(print_r($matrix_stock,true));
		$this->template->set_global('matrix_stock', $matrix_stock, false);

		$product = (object)$product_base;
		$product->detail = $product_detail;

		\Fuel\Core\Log::debug(print_r("@@@@@@@@@@@@@@@@@@@@@@@@@@@@",true));
		\Fuel\Core\Log::debug(print_r($product,true));

		// 商品が無効、存在しない場合は、TOPへリダイレクトする。
		if(!$product){
			Response::redirect("/".$this->session_get_param);
		}

		// 予約販売、通常販売、期間外情報取得
		// 予約販売：予約販売期間テーブルを参照し、期間に該当するものがあれば予約
		// 通常販売：原則予約販売とはかぶらないが、被った場合は、予約販売が優先となる。
		// 上記以外は期間外とする。

		// 予約販売商品判定
		$yoyaku_flg = 0;
		foreach($this->term_data as $term){
			$wheres = array();
			$wheres[] = array("term_id",$term->id);
			$wheres[] = array("product_id",$id);

			try
			{
				$term_item = Cache::get("term_item_".$term->id."_".$id);
			}
			catch (\CacheNotFoundException $e)
			{
				$term_item = Model_Db_Product_Term_Relation::findDetailSimple($wheres);
				Cache::set("term_item_".$term->id."_".$id, $term_item, 60 * 1);
			}

			if($term_item){
				$yoyaku_flg = 1;
			}
		}

		// 通常販売商品判定
		$tsujyo_flg = 0;
		if(!$yoyaku_flg){
			if(isset($product->term_start_date) && isset($product->term_end_date)) {
				if($product->term_start_date <= date("Y-m-d H:i:s") &&
					$product->term_end_date >= date("Y-m-d H:i:s")
				){
					$tsujyo_flg= 1;
				}
			}
		}
		// 期間外表示判定
		$kikangai_flg = 0;
		if(isset($product->publish_flg)){
			if(!$yoyaku_flg && !$tsujyo_flg && $product->publish_flg){
				$kikangai_flg= 1;
			}
		}

		// 予約でも通常でも期間外表示でもない商品詳細へのアクセスはNG
		if(!$yoyaku_flg && !$tsujyo_flg && !$kikangai_flg){
			Response::redirect("/".$this->session_get_param);
		}
		$this->template->set_global('yoyaku_flg', $yoyaku_flg, false);
		$this->template->set_global('tsujyo_flg', $tsujyo_flg, false);
		$this->template->set_global('kikangai_flg', $kikangai_flg, false);
		$this->template->set_global('product', $product, false);

		//----------------------------------
		// 商品画像
		//----------------------------------
		$imgs = "";
		if($this->agent_dir){
			$imgs = Image::getImageFp($product->id);
		}else{
			$imgs = Image::getImage($product->id);
		}
		$this->template->set_global('images', $imgs, false);

		$this->template->title = $product->title;
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/product/detail');
	}

	/*
	 * 確認用ページ
	 * ステータスは確認しません。※無効な商品も表示されます。
	 */
	public function action_predetail($id = null)
	{
		is_null($id) and Response::redirect('/product/list'.$this->session_get_param);

		if (!isset($_SERVER['PHP_AUTH_USER'])){
			header('WWW-Authenticate: Basic realm="Private Page"');
			header('HTTP/1.0 401 Unauthorized');
			die('このページを見るにはログインが必要です');
		}else{
			if ($_SERVER['PHP_AUTH_USER'] != Config::get("basic.user")
				|| $_SERVER['PHP_AUTH_PW'] != Config::get("basic.pass")){
				header('WWW-Authenticate: Basic realm="Private Page"');
				header('HTTP/1.0 401 Unauthorized');
				die('このページを見るにはログインが必要です');
			}
		}

		$search_wheres[] = array("price_sale", ">",0);
		$search_wheres[] = array("id", $id);

		$product = Model_Db_Product::findDetailFront($search_wheres);
		$this->template->set_global('product', $product, false);


		// 予約販売商品判定
		$yoyaku_flg = 0;
		foreach($this->term_data as $term){
			$wheres = array();
			$wheres[] = array("term_id",$term->id);
			$wheres[] = array("product_id",$id);
			$term_item = Model_Db_Product_Term_Relation::findDetailSimple($wheres);
			if($term_item){
				$yoyaku_flg = 1;
			}
		}

		// 通常販売商品判定
		$tsujyo_flg = 0;
		if(!$yoyaku_flg){
			if(isset($product->term_start_date) && isset($product->term_end_date)) {
				if($product->term_start_date <= date("Y-m-d H:i:s") &&
					$product->term_end_date >= date("Y-m-d H:i:s")
				){
					$tsujyo_flg= 1;
				}
			}
		}
		// 期間外表示判定
		$kikangai_flg = 0;
		if(isset($product->publish_flg)){
			if(!$yoyaku_flg && !$tsujyo_flg && $product->publish_flg){
				$kikangai_flg= 1;
			}
		}

		// 予約でも通常でも期間外表示でもない商品詳細へのアクセスはNG
//		if(!$yoyaku_flg && !$tsujyo_flg && !$kikangai_flg){
//			Response::redirect("/".$this->session_get_param);
//		}
		$this->template->set_global('yoyaku_flg', $yoyaku_flg, false);
		$this->template->set_global('tsujyo_flg', $tsujyo_flg, false);
		$this->template->set_global('kikangai_flg', $kikangai_flg, false);

		$imgs = "";
		if($this->agent_dir){
			$imgs = Image::getImageFp($product->id);
		}else{
			$imgs = Image::getImage($product->id);
		}
		$this->template->set_global('images', $imgs, false);

		$this->template->title = $product->title;
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/product/detail');
	}

}

