<?php
return array(
	"rakuten" => array(
		"public_key"		=> "sandbox_public_774d76833d945142d19dad2a1c55df6051b55dbc9df035c6fca369420bc27675caac3de3d46a473e9fef5f74ce241b9b",
		"private_key"		=> "sandbox_private_48dd0a83b99fd8fddcd66cf9c84580b551b55dbc9df035c6fca369420bc27675caac3de3d46a473e9fef5f74ce241b9b",
		"js_url"			=> 'https://lite.checkout.rakuten.co.jp/s/js/checkout-lite-v1.js',
		"api_url"			=> "https://api.lite.checkout.rakuten.co.jp",
		"api_action"		=> array(
			"capture"			=> "/sandbox/v1/charges/%%CHARGE_ID%%/capture",
			"cancel"			=> "/sandbox/v1/charges/%%CHARGE_ID%%/refund",
		),
	),
	's3base_url' => "https://reni-ec-dev.s3-ap-northeast-1.amazonaws.com/",
	'assets_url' => "//reni-ec-dev.s3-ap-northeast-1.amazonaws.com/assets/",
	'banner_url' => "//reni-ec-dev.s3-ap-northeast-1.amazonaws.com/banner/",
	"basic" => array(
		'front_mode' => 1,
		'tool_mode' => 1,
		'user' => "admin",
		'pass' => "pass",
	),
	"atobarai_maintenance" => array(
		"start"		=> 20151101000000,
		"finish"	=> 20151201110000,
	),

	// 写真集名入れ商品ID
	"print_product_ids" => array(
		1191,1232
	),
	// 写真集名入れ商品詳細ID
	"print_product_detail_ids" => array(
		1091,1191,1232
	),

	"naire_product_ids" => array(
		1191,1232
	),
	"naire_max_length" => array(
		1191 => 17,
		1232 => 15,
	),
	"order_limit" => array(
	),
	"order_limit1" => array(
	),
	"order_limit2" => array(
	),
);
