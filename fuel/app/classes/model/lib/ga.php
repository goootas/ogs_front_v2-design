<?php
namespace Model\lib;

class Ga extends \Model
{
	/*
	 * カート情報取得
	 */
	public static function getFeatureTag($tid="",$ptitle="")
	{
		$cid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,
			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);

		$data['v']   = '1';
		$data['tid'] = $tid; // UAのコード
		$data['cid'] = $cid;
		$data['t']   = 'pageview';
		$data['dh']  = $_SERVER['HTTP_HOST'];
		$data['dp']  = $_SERVER['REQUEST_URI'];
		$data['dt']  = urlencode("FP ".$ptitle); // ページのタイトル

		$url  = 'https://www.google-analytics.com/collect';
		$url .= '?v='.$data['v'];
		$url .= '&tid='.$data['tid'];
		$url .= '&cid='.$data['cid'];
		$url .= '&t='.$data['t'];
		$url .= '&dh='.$data['dh'];
//		$url .= '&dl='.$data['dl'];
		$url .= '&dl=';
//		$url .= '&dr='.$data['dr'];
		$url .= '&dr=';
//		$url .= '&ds=call%20center';
		$url .= '&dp='.$data['dp'];
		$url .= '&dt='.$data['dt'];
//		$url .= '&ua='.urlencode($_SERVER['HTTP_USER_AGENT']);
		return $url;
	}

}