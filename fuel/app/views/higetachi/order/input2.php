<?php echo Asset::css(array('jquery-ui.css')); ?>
<?php echo Asset::js(array('jquery-ui.min.js')); ?>

<!-- 購入商品情報 -->
<?php echo render($shop_data["dir"].'/order/_cart_view'); ?>

<?php if( $receives_count == 0) :?>
	<div class="login-panel">
		<div class="panel-body">
			<p class="heading">配送方法 / 送料について</p>
			<p>
				■配送<br>
				佐川急便での配送になります。（日本国内のみ）
				<br>
				■送料<br>
				全国一律¥540（tax in / 1回のご注文につき）<br />
				<?php if ($shop_data["free_deliver_price"] >= 0 ) : ?>
					※１回のご注文合計金額が¥<?php echo number_format($shop_data["free_deliver_price"]);?>(tax in)以上のお客様は送料無料とさせていただきます
				<?php endif; ?>
				<br>
			</p>
		</div>
	</div>
<?php endif;?>

<?php echo Form::open(array('id' => "order" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<h4 class="headline">お届け先情報</h4>
	<div class="form-group count-2">
		<?php echo Form::label('お名前<span class="caption">※必須</span>', 'deliver_username',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_username_sei', Session::get("order_data.data2.deliver_username_sei") ? Session::get("order_data.data2.deliver_username_sei") : Session::get("order_data.data1.order_username_sei"),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "姓")); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_username_mei', Session::get("order_data.data2.deliver_username_mei") ? Session::get("order_data.data2.deliver_username_mei") : Session::get("order_data.data1.order_username_mei"),
					array('class' => 'validate[required] form-control col-sm-4',"placeholder" => "名")); ?>
			</div>
		</div>
	</div>

	<div class="form-group type-postal">
		<?php echo Form::label('郵便番号<span class="caption">※必須</span>', 'deliver_zip',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_zip', Session::get("order_data.data2.deliver_zip") ? Session::get("order_data.data2.deliver_zip") : Session::get("order_data.data1.order_zip"),
					array('id' => 'zip','class' => 'validate[required,custom[onlyNumberSp],minSize[7],maxSize[7]] form-control col-sm-4',"placeholder" => "1234567",'type'=>'tel')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 col-xs-4 control-label">都道府県<span class="caption">※必須</span></label>
		<div class="form-container">
			<table class="select">
				<tr>
					<td>
						<?php echo Form::select('deliver_state', Session::get("order_data.data2.deliver_state") ? Session::get("order_data.data2.deliver_state") : Session::get("order_data.data1.order_state"),$prefecture_data,
							array('class' => 'validate[required] form-control col-md-5','id' => 'state'));?>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所１<span class="caption">※必須</span>', 'deliver_address1',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_address1', Session::get("order_data.data2.deliver_address1") ? Session::get("order_data.data2.deliver_address1") : Session::get("order_data.data1.order_address1"),
					array('id' => 'address1' , 'class' => 'validate[required,ajax[ajaxZipAddress],maxSize[24]] form-control','placeholder' => '市区町村※24文字まで')); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Form::label('住所２', 'deliver_address2',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_address2', Session::get("order_data.data2.deliver_address2") ? Session::get("order_data.data2.deliver_address2") : Session::get("order_data.data1.order_address2"),
					array('id' => 'address2' ,'class' => 'validate[maxSize[24]] form-control',"placeholder" => "番地、建物名など※24文字まで")); ?>
			</div>
		</div>
	</div>

	<div class="form-group count-3">
		<?php echo Form::label('電話番号<span class="caption">※必須</span>', 'deliver_tel',array("class" => "col-sm-3 col-xs-12 control-label")); ?>
		<div class="form-container">
			<div class="inner">
				<?php echo Form::input('deliver_tel1', Session::get("order_data.data2.deliver_tel1") ? Session::get("order_data.data2.deliver_tel1") : Session::get("order_data.data1.order_tel1"),
					array('id' => 'tel1' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_tel2', Session::get("order_data.data2.deliver_tel2") ? Session::get("order_data.data2.deliver_tel2") : Session::get("order_data.data1.order_tel2"),
					array('id' => 'tel2' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>
			<div class="inner">
				<?php echo Form::input('deliver_tel3', Session::get("order_data.data2.deliver_tel3") ? Session::get("order_data.data2.deliver_tel3") : Session::get("order_data.data1.order_tel3"),
					array('id' => 'tel3' ,'class' => 'validate[required,custom[onlyNumberSp],maxSize[4]] form-control',"placeholder" => "半角数字",'type'=>'tel')); ?>
			</div>

		</div>
	</div>

	<?php if( $receives_count == 0) :?>
		<div class="form-group">
			<?php echo Form::label('お届け日', 'delivery_date',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<?php
				if(isset($deliver_term_date)){
					echo $deliver_term_date->deliver_start_text;
				}else{
					echo Form::select('delivery_date', Session::get("order_data.data2.delivery_date"),$delivery_date, array('class' => 'form-control col-md-5',));
//				echo "受注日翌日から2日以内で発送（土日祝日除く / 発送からお届けは1〜3日）";
					echo '<div class="description">※注文状況の混み具合により、お届け日が異なる場合がございますことを予めご了承ください。</div>';
				}
				?>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('お届け希望時間帯', 'delivery_time',array("class" => "col-sm-3 col-xs-4 control-label")); ?>
			<div class="form-container">
				<table class="select">
					<tr>
						<td>
							<?php
							if(isset($deliver_term_date)){
								echo "指定不可";
							}else {
								?>
								<?php echo Form::select('delivery_time', Session::get("order_data.data2.delivery_time"), Config::get("delivery_time"),
									array('class' => 'form-control col-md-5',));
							}?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	<?php endif;?>

	<div class="buttons count-2">
		<?php echo Html::anchor('/order/input1', '前に戻る',array('class' => 'button rect size-L'),true); ?>
		<?php if( $change == 1) :?>
			<?php echo Html::anchor('#', '変更',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php else :?>
			<?php echo Html::anchor('#', '次へ進む',array('id' => 'next','class' => 'button rect key size-L'),true); ?>
		<?php endif;?>
	</div>

</div>

<?php echo Form::hidden('delivery_comment',""); ?>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::close();?>

<script type="text/javascript">
	$(function(){
		$("#order").validationEngine({
			promptPosition : "topLeft",
			scroll: false,
//			binded: false
		});
		// 次へ
		$("[id=next]").click(function(){
			$("#order").attr("action","/<?php echo $shop_data["dir"];?>/order/input3");
			$("#order").submit();
		});
		// 検索データ受信時の処理
		function zipDataReceive(response, data) {
			response($.map(data, function (item) {
				// 住所
				var address1 = item.town + item.block;
				var address2 = item.street;
				// ラベル
				var label = item.zip_code + ' : ' + item.pref + address1 + address2;
				return {
					label: label,
					zip: item.zip_code,
					state: item.pref,
					address1: address1,
					address2: address2,
				}
			}));
		}

		// フォームの項目を更新
		function zipDataUpdate(ui) {
			$('#zip').val(ui.item.zip);
			$('#state').val(ui.item.state);
			$('#address1').val(ui.item.address1);
			$('#address2').val(ui.item.address2);
		}

		// 郵便番号の入力フィールドに Autocomplete を適用
		$('#zip').autocomplete({
			delay: 100,
			minLength: 6,
			source: function (request, response) {
				$.ajax({
					url: '/<?php echo $shop_data["dir"];?>/api/postal/address.json',
					dataType: 'json',
					data: {
						mode: 0,
						term: request.term,
						max_rows: 30,
						biz_mode: 2,
						sort: 0
					},
					success: function (data) {
						zipDataReceive(response, data);
					}
				});
			},
			select: function (event, ui) {
				zipDataUpdate(ui);
				return false;
			}
		});
	});
</script>