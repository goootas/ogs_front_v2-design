<?php
return array(
    "order_mail" => array(
        "title" =>
<<< EOM
■受注番号
%%ORDER_ID%%
EOM
,
        "orders" =>
<<< EOM


■お客様情報
【お名前】%%ORDER_NAME%% 様
【メールアドレス】%%ORDER_EMAIL%%
【郵便番号】%%ORDER_ZIP%%
【ご住所】%%ORDER_ADDRESS%%
【電話番号】%%ORDER_TEL%%
【注文日】%%ORDER_DATE%%
【決済方法】%%ORDER_PAYMENT%%
EOM
    ,
        "delivers" =>
<<< EOM


■配送先情報
【お名前】%%DELIVER_NAME%% 様
【郵便番号】%%DELIVER_ZIP%%
【ご住所】%%DELIVER_ADDRESS%%
【電話番号】%%DELIVER_TEL%%
【お届け日】%%DELIVER_DAY%%
【お届け希望時間帯】%%DELIVER_TIME%%
【配送会社】佐川急便
EOM
    ,
        "products_title" =>
<<< EOM


■商品詳細
EOM
    ,
        "products" =>
<<< EOM

【商品ＩＤ】%%PRODUCT_ID%%
【商品番号】%%PRODUCT_CODE%%
【商品名】%%PRODUCT_TITLE%%
【価格】%%PRODUCT_PRICE%%円（税込）
【数量】%%PRODUCT_NUM%%
【小計】%%SUBTOTAL%%円
----------------------------------------------------------------
EOM
    ,
        "delivers_total" =>
<<< EOM
EOM
    ,
        "total" =>
<<< EOM


■総合計
【商品合計】%%SUBTOTAL%%円（税込）
【送料合計】%%POSTAGE%%円（税込）
【決済手数料】%%FEE%%円（税込）
【総合計】%%TOTAL%%円
EOM
    ,
        "cvs_data" =>
            <<< EOM


■コンビニ決済でご購入のお客様
【支払い先コンビニ】%%CVS_TITLE%%
【オンライン決済番号・受付番号】%%CVS_NO%%
【支払期限】%%CVS_LIMIT%%
【店頭でのお支払い方法】%%CVS_SHOP_URL%%
※上記期限までにお支払いいただけなかった場合、ご注文はキャンセルとさせていただきます。

EOM
    ,
        "cvs_data_7" =>
            <<< EOM

【払込票URL】%%CVS_URL%%
EOM
    ,
    ),
);
