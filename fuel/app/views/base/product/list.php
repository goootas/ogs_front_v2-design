
<script type="text/javascript">
	$(function($){
		//タイルレイアウト実行
		function marsonryDo(){
			var windowW = $(window).width()
			var parentW = $(".CONTENTS .marsonry").width()
			if(windowW < 728){
				var boxW = (parentW) / 2
			} else {
				var boxW = (parentW) / 3
			}
			$('.box').each(function(i){
				if($(this).hasClass("pickup")){
					$(this).css({
						"width" : boxW * 2+ "px",
						"height" : boxW * 2 + "px"
					});
				} else {
					$(this).css({
						"width" : boxW + "px",
						"height" : boxW + "px"
					});
				}
				var thumbNailSize = $(this).width()
				$(this).find(".inner").css({
					"width" : (thumbNailSize - 10) + "px",
					"height" : (thumbNailSize - 10) + "px"
				});
			});
			clossFadeDo()
			setTimeout(function(){
				$('.marsonry ul').masonry({
					itemSelector: '.box',
					columnWidth: boxW,
					isFitWidth: false
				});
			},100);
		}
		//クロスフェード
		function clossFadeDo(){
			setTimeout(function(){
				$('.box:odd .thumbnail').innerfade({
					speed: 'slow',
					timeout: 6000,
					type: 'random_start'
				});
			},3000);
			$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
				speed: 'slow',
				timeout: 6000,
				type: 'random_start'
			});
		}

		function autoLoad(){
			var maxpage = 99;
			$('#loading').css('display', 'none');
			$.autopager({
				content: '.marsonry ul',
				link: '#NEXT a',
				autoLoad: true,

				start: function(current, next){
					$('#loading').css('display', 'block');
					$('#NEXT a').css('display', 'none');
				},
				load: function(current, next){
					$('#loading').css('display', 'none');
					$('#NEXT a').css('display', 'block');
					if( current.page >= maxpage ){
						marsonryDo()//パネル実行
						$('#NEXT a').hide();
						return false;
					}else{
						marsonryDo()//パネル実行
					}
				}
			});
			$('#NEXT a').click(function(){
				$.autopager('load');
				return false;
			});
		}


		//読み込み直後実行
		$(window).load(function () {
			marsonryDo()
			autoLoad()
		});
		//リサイズ時実行
		var timer = false;
		$(window).resize(function() {
			if (timer !== false) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				marsonryDo()
			}, 1000);
		});});
</script>


<?php //if ($term): ?>
<!--	--><?php //foreach ($term as $data): ?>
<!--		<p>--><?php //echo $data->title ; ?><!--</p>-->
<!--		<p>--><?php //echo $data->deliver_start_text ; ?><!--</p>-->
<!--	--><?php //endforeach; ?>
<?php //endif; ?>

<?php if ($products): ?>
	<div class="marsonry">
		<ul>
			<?php foreach ($products as $line): ?>
				<li class="box">
					<?php
					$html = '<div class="inner">';
					$html .= '<div class="alpha"></div>';
					$html .= '<div class="thumbnail"><img src="//'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".Config::get("aws.image_path").$line["id"]."/".$line["id"]."_000.jpg?".time().'"></div>';
					$html .= '<div class="text">';
					$html .= '<dl>';
					$html .= '<dt>'.$line["code"].'</dt>';
					$html .= '<dt>'.$line["title"].'</dt>';
					$html .= '<dd>¥'.number_format($line["price_sale"] * $tax).'</dd>';
					$html .= '</dl>';
					$html .= '</div>';
					$html .= '</div>';
					?>
					<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>

				</li>
			<?php endforeach; ?>

		</ul>
	</div>


	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>

		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>

<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>

<?php //if ($products): ?>
<!--	--><?php //foreach ($products as $line): ?>
<!--		<div class="col-xs-6 col-md-4">-->
<!--			<figure>-->
<!--				--><?php
//				$img_html = '<img src="//'.$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".Config::get("aws.image_path").$line["id"]."/".$line["id"]."_000.jpg?".time().'">';
//				$new_html = "";
//				if($line["new"]) {
//					$new_html =  "<span class='new_tag'>NEW</span>";
//				}
//				$stock_html = "";
//				if($line["stock"] == 0) {
//					$stock_html = "<span class='sold_out_tag'>SOLD OUT</span>";
//				}
//				$caption_html = '';
//				$caption_html .= '<figcaption><div class="cap">';
//				$caption_html .= '<p>'.$line["title"].'<br>'.$line["code"].'<br>'.number_format($line["price_sale"] * $tax).'円</p>';
//				$caption_html .= '</div></figcaption>';
//				?>
<!--				--><?php //echo Html::anchor('/product/detail/'.$line["id"], $img_html.$new_html.$stock_html.$caption_html, array("class"=>"thumbnail"), true) ?>
<!--			</figure>-->
<!--		</div>-->
<!--	--><?php //endforeach; ?>
<!--	<div class="col-xs-12 col-md-12 text-center">-->
<!--		<ul class="list-inline">-->
<!--			--><?php //echo Pagination::instance('list')->previous("BACK"); ?>
<!--			<li>PAGE --><?php //echo Pagination::instance('list')->current_page; ?><!-- OF --><?php //echo Pagination::instance('list')->total_pages; ?><!--</li>-->
<!--			--><?php //echo Pagination::instance('list')->next("NEXT"); ?>
<!--		</ul>-->
<!--	</div>-->
<?php //endif; ?>
