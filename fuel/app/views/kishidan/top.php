<style>
	.CONTENTS {
		margin-top: 0;
	}
	.CONTENTS .CONTAINER {
		max-width: 100%;
		padding: 0;
		width: 100%;
		border-radius: 0;
	}
	.outer{
		height: 100%;
		margin: 0 auto;
		max-width: 960px;
		overflow: hidden;
		padding: 0 10px;
	}
	.headline{
		margin: 80px 15px 40px;
	}
	.headline::before {
		background-color: #fff;
	}
	.headline::after {
		background-color: #fff;
	}
	@media screen and (max-width: 1024px) {
		.CONTENTS .CONTAINER {
			padding: 0 !important;
			width: auto;
		}
		.outer{
			padding:0px;
		}
	}
</style>
<?php if ($products): ?>
	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="MAINSLIDER">
			<img src="/assets/img/kishidan/IMG_headline_info01.jpg" class="PC_CHANGE">
			<img src="/assets/img/kishidan/sp/IMG_headline_info01_SP.jpg" class="SP_CHANGE">
		</div>
	<?php endif; ?>

	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="FREE-TEXT HEAD BANNER02">
			<?php
			if($banner):
				$two = array();
				foreach($banner as $b):
					$banner_tag = "";
					$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
					$array = get_headers($img_path);
					if(strpos($array[0],'OK')){
						switch ($b->sort){
							case 1:
							case 2:
							case 3:
							case 4:
								$banner_tag = <<< EOM
									<div class="banner-one">
									<a href="{$b->url}"><img src="{$img_path}" class=""></a>
									</div>
EOM;
								break;
							default:
								$two[$b->sort] = $b;
								break;
						}
					}
					echo $banner_tag;
				endforeach;
				?>
				<?php if(isset($two[5]) || isset($two[6])): ?>
				<div class="banner-two">
					<?php if(isset($two[5])): ?>
						<?php $data = $two[5]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[6])): ?>
						<?php $data = $two[6]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
				<?php if(isset($two[7]) || isset($two[8])): ?>
				<div class="banner-two">
					<?php if(isset($two[7])): ?>
						<?php $data = $two[7]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[8])): ?>
						<?php $data = $two[8]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
				<?php if(isset($two[9]) || isset($two[10])): ?>
				<div class="banner-two">
					<?php if(isset($two[9])): ?>
						<?php $data = $two[9]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[10])): ?>
						<?php $data = $two[10]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="UP">
		<?php echo isset($shop_desc->pcsp_top1) ? $shop_desc->pcsp_top1 : "";?>
	</div>

	<div class="outer">
		<!--<h4 class="headline">ITEM</h4>-->
		<div class="marsonry">
			<ul>
				<?php foreach ($products as $key => $line): ?>
					<li class="box">
						<?php
						$html = '<div class="inner imgchange">';
						$html .= '<div class=""></div><div class="thumbnail">';
						if(isset($line["imgs"][0])){
							$path_front = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
							$path_back = $shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$line["imgs"][0];
						}else{
							$path_front = "";
							$path_back = "";
						}
						$img_tag2 = <<< EOM
<img src="//{$path_front}">
EOM;
						$html .= $img_tag2;
						$html .= '</div>';
						$html .= '<figcaption>';
						$html .= '<strong>';
						$html .=$line["body_list"];
						$html .= '</strong>';
						$html .= '</figcaption>';
//						$html .= '<div class="text">';
//						if($agent == "sp"){
//							$html .= '<dl style="background: linear-gradient(to bottom, rgba(229,0,17,0) 0%,rgba(0, 0, 0, 0.59) 100%);">';
//						}else{
//							$html .= '<dl>';
//						}
//						$html .= '<dt>'.$line["title"].'</dt>';
//						$html .= '<dd>¥'.number_format(floor($line["price_sale"] * $tax)).'</dd>';
//						$html .= '</dl>';
//						$html .= '</div>';
						$html .= '</div>';
						?>
						<?php echo Html::anchor('/product/detail/'.$line["id"], $html) ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>

	<?php if(Request::active()->controller == 'Controller_Top') :?>
		<div class="FREE-TEXT HEAD BANNER02">
			<?php
			if($banner):
				$two = array();
				foreach($banner as $b):
					$banner_tag = "";
					$img_path = "https:".Config::get("banner_url").$b->id.'_pc.jpg';
					$array = get_headers($img_path);
					if(strpos($array[0],'OK')){
						switch ($b->sort){
							case 11:
							case 12:
							case 13:
							case 14:
								$banner_tag = <<< EOM
									<div class="banner-one">
									<a href="{$b->url}"><img src="{$img_path}" class=""></a>
									</div>
EOM;
								break;
							default:
								$two[$b->sort] = $b;
								break;
						}
					}
					echo $banner_tag;
				endforeach;
				?>
				<?php if(isset($two[15]) || isset($two[16])): ?>
				<div class="banner-two">
					<?php if(isset($two[15])): ?>
						<?php $data = $two[15]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[16])): ?>
						<?php $data = $two[16]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>

				<?php if(isset($two[17]) || isset($two[18])): ?>
				<div class="banner-two">
					<?php if(isset($two[17])): ?>
						<?php $data = $two[17]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[18])): ?>
						<?php $data = $two[18]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if(isset($two[19]) || isset($two[20])): ?>
				<div class="banner-two">
					<?php if(isset($two[19])): ?>
						<?php $data = $two[19]; ?>
						<div class="banners">
							<div class="banners-inner fst">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
					<?php if(isset($two[20])): ?>
						<?php $data = $two[20]; ?>
						<div class="banners">
							<div class="banners-inner sec">
								<a href="<?php echo $data->url;?>"><img src="<?php echo "https:".Config::get("banner_url").$data->id.'_pc.jpg';?>" class=""></a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php if(Pagination::instance('list')->current_page != Pagination::instance('list')->total_pages): ?>
		<div id="NEXT" class="buttons">
			<?php echo Pagination::instance('list')->next(); ?>
			<?php echo Asset::img($shop_data["dir"].'/loader.gif', array('id' => 'loading','alt' => '読み込み中','width' => '29','height' => '29',));?>
		</div>
	<?php endif; ?>
<?php else: ?>
	<p>NO ITEM</p>
<?php endif; ?>

<script type="text/javascript">
	$(function($){
		$('.HEADER').removeClass('MIN');
	});
	$(document).on({
		"mouseenter": function(){
			$(this).find("img").attr("src",$(this).find("#out").val());
		},
		"mouseleave": function(){
			$(this).find("img").attr("src",$(this).find("#hover").val());
		}
	}, ".imgchange");

</script>
<?php
$ua = $_SERVER['HTTP_USER_AGENT'];
if(!preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT'])) {
	?>
	<script type="text/javascript">
		$(function($){
			//タイルレイアウト実行
			function marsonryDo(){
				var windowW = $(window).width()
				var parentW = $(".CONTENTS .marsonry").width()
				if(windowW < 450){
					var boxW = (parentW) / 2
					var Hnum = 1.35
				}  else if(windowW < 700){
					var boxW = (parentW) / 2
					var Hnum = 1.2
//					$('.box:nth-child(5n+1)').addClass("pickup");
				}
				else {
					var rand = Math.floor(Math.random()*2 + 1) ;
					var boxW = (parentW) / 3
					var Hnum = 1.2
				}

				$('.box').each(function(i){
					if($(this).hasClass("pickup")){
						$(this).css({
							"width" : boxW * 2+ "px",
							// "height" : boxW * 2 + "px"
						});
					} else {
						$(this).css({
							"width" : boxW + "px",
							// "height" : boxW * Hnum + "px"
						});
					}
					var thumbNailSize = $(this).width()
					$(this).find(".inner").css({
						"width" : (thumbNailSize - 10) + "px",
						// "height" : (thumbNailSize * Hnum - 10) + "px"
					});
				});
				clossFadeDo()
				setTimeout(function(){
					$('.marsonry ul').masonry({
						itemSelector: '.box',
						columnWidth: boxW,
						isFitWidth: false
					});
					$('.box').css({
						opacity: 1,
					});
				},100);
			}
			//クロスフェード
			function clossFadeDo(){
				setTimeout(function(){
					$('.box:odd .thumbnail').innerfade({
						speed: 500,
						timeout: 9000,
						type: 'random_start'
					});
				},6000);
				$('.box:nth-child(1) .thumbnail , .box:even .thumbnail').innerfade({
					speed: 500,
					timeout: 9000,
					type: 'random_start'
				});
			}

			function autoLoad(){
				var maxpage = <?php echo $total_pages ?>;
				$('#loading').css('display', 'none');
				$.autopager({
					content: '.marsonry ul',
					link: '#NEXT a',
					autoLoad: true,

					start: function(current, next){
						$('#loading').css('display', 'block');
						$('#NEXT a').css('display', 'none');
					},
					load: function(current, next){
						$('#loading').css('display', 'none');
						$('#NEXT a').css('display', 'block');
						if( current.page >= maxpage ){
							marsonryDo()//パネル実行
							$('#NEXT a').hide();
							return false;
						}else{
							$('.marsonry').imagesLoaded(function(){
								marsonryDo()
							});
						}
					}
				});
				$('#NEXT a').click(function(){
					$.autopager('load');
					return false;
				});
			}
			//読み込み直後実行
			$(window).load(function () {
				$('.marsonry').imagesLoaded(function(){
					marsonryDo()
				});
				autoLoad()
				$('.flexslider').flexslider({
					animation: "slide",
					directionNav: true,
					prevText: "",
					nextText: ""
				});
			});
			//リサイズ時実行
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					marsonryDo()
				}, 1000);
			});});
	</script>
<?php } ?>
