<?php
use Fuel\Core\Log;
use Fuel\Core\Config;
use Fuel\Core\Session;

class Model_Lib_Mail extends \Model
{
	public static function sendmail($shop_data ,$type, $address, $sending_data="",$yoyaku="",$name="",$receives="")
	{
		Config::load("mail_text");// 注文確認メールテンプレート（一部）
		Config::load("veritrans");// 決済文言

		$template = Model_Db_Shop_Mail_Template::findType($shop_data->id,$type);

		// メールテンプレート情報が取得出来ない場合はreturnする。
		if(!isset($template))return true;

		$title = $template->title;
		$title = str_replace("%%SHOP_TITLE%%", $shop_data->name, $title);

		$message = $template->message;
		$custom = "";
		switch($type){
			case Config::get('mail.value.leave'):
				$custom = self::mail_leave($message,$sending_data);
				break;
			case Config::get('mail.value.forget'):
				$custom = self::mail_forget($message,$sending_data);
				break;
			case Config::get('mail.value.order'):
				$custom = self::mail_order($message,$sending_data,$yoyaku);
				$custom = str_replace("%%NAMES%%", $name, $custom);
				$custom = str_replace("%%RECEIVES%%", $receives, $custom);
				break;
			default:
				return true;
				break;
		}
		//$message = str_replace("%%CUSTOM%%", $custom, $message);
		$message = $custom;
		$message = str_replace("%%SHOP_TITLE%%", $shop_data->name, $message);
		$message = str_replace("%%USER_EMAIL%%", $address, $message);
		$message = str_replace("%%SHOP_URL%%", "https://".$shop_data->domain . ($shop_data->type ? "/".$shop_data->dir : ""), $message);
		$message = str_replace("%%SHOP_EMAIL%%", $shop_data->mail, $message);

		$email = Email::forge();
		$email->from($template->mail_sender,$shop_data->name);
		$email->return_path($shop_data->mail);
		$email->to($address);
		$email->subject($title);
		$email->body($message."\n");

		try
		{
			$email->send();
		}catch(\EmailValidationFailedException $e)
		{
			// バリデーションが失敗したとき
			Log::warning('error', __FILE__ . __LINE__ . print_r($email,true));
			Log::warning('error', __FILE__ . __LINE__ . print_r($e,true));
			throw new HttpServerErrorException;
		}catch(\EmailSendingFailedException $e)
		{
			// ドライバがメールを送信できなかったとき
			Log::warning('error', __FILE__ . __LINE__ . print_r($email,true));
			Log::warning('error', __FILE__ . __LINE__ .print_r($e,true));
			throw new HttpServerErrorException;
		}
		return true;
	}
	/*
	 * 受注
	 */
	public static function mail_order($message,$data="",$yoyaku="")
	{
		$orders = $data["order_data"];
		$orders2 = $data["order_data2"];
		Log::debug(print_r($orders,true));
		Log::debug(print_r($orders2,true));

		$title = Config::get("order_mail.title");
		$title = str_replace("%%ORDER_ID%%", $orders["order_id"], $title);

		$order_user = Config::get("order_mail.orders");
		$order_user = str_replace("%%ORDER_NAME%%", $orders["order_username_sei"]." ". $orders["order_username_mei"]." (". $orders["order_username_sei_kana"]." ". $orders["order_username_mei_kana"].")", $order_user);
		$order_user = str_replace("%%ORDER_EMAIL%%", $orders["order_email"], $order_user);
		$order_user = str_replace("%%ORDER_ZIP%%", $orders["order_zip"], $order_user);
		$order_user = str_replace("%%ORDER_ADDRESS%%", $orders["order_state"]." ". $orders["order_address1"]." ". $orders["order_address2"], $order_user);
		$order_user = str_replace("%%ORDER_TEL%%", $orders["order_tel1"]."-". $orders["order_tel2"]."-". $orders["order_tel3"], $order_user);
		$order_user = str_replace("%%ORDER_DATE%%", date("Y-m-d H:i:s"), $order_user);
		$payment = Config::get("payment.status_user.".$orders["payment"]);
		if($orders["payment"] == 1) {
			$payment .= " ".Config::get("jpo.".$orders["credit_jpo"]);
		}
		if($orders["payment"] == 2) {
			$payment .= " ".Config::get("cvs.".$orders["cvs"]);
		}
		$order_user = str_replace("%%ORDER_PAYMENT%%", $payment, $order_user);

		$deliver_user = Config::get("order_mail.delivers");
		$deliver_user = str_replace("%%DELIVER_NAME%%", $orders["deliver_username_sei"]." ". $orders["deliver_username_mei"], $deliver_user);
		$deliver_user = str_replace("%%DELIVER_ZIP%%", $orders["deliver_zip"], $deliver_user);
		$deliver_user = str_replace("%%DELIVER_ADDRESS%%", $orders["deliver_state"]." ". $orders["deliver_address1"]." ". $orders["deliver_address2"], $deliver_user);
		$deliver_user = str_replace("%%DELIVER_TEL%%", $orders["deliver_tel1"]."-". $orders["deliver_tel2"]."-". $orders["deliver_tel3"], $deliver_user);

		if($yoyaku){//予約商品の場合
			$deliver_day = $yoyaku->deliver_start_text;
			$deliver_user = str_replace("%%DELIVER_DAY%%", $deliver_day, $deliver_user);
			$deliver_time = "指定不可";
			$deliver_user = str_replace("%%DELIVER_TIME%%",$deliver_time, $deliver_user);
		}else{
			$deliver_day = "最短で発送（土日祝日除く / 発送からお届けは1〜3日）";
			if(isset($orders["delivery_date"]) && $orders["delivery_date"] != "" && $orders["delivery_date"] != "00000000"){
				$deliver_day = sprintf("%04d年%02d月%02d日", substr($orders["delivery_date"],0,4), substr($orders["delivery_date"],4,2), substr($orders["delivery_date"],6,2));
			}
			$deliver_user = str_replace("%%DELIVER_DAY%%", $deliver_day, $deliver_user);

			$deliver_time = "指定無し";
			if(isset($orders["delivery_time"]) && $orders["delivery_time"]){
				if($orders["delivery_time"] == "AM"){
					$deliver_time = "午前中";
				}else{
					$dt = explode("-",$orders["delivery_time"]);
					$deliver_time = sprintf("%d時〜%d時",$dt[0],$dt[1]);
				}
			}
			$deliver_user = str_replace("%%DELIVER_TIME%%",$deliver_time, $deliver_user);
		}
		$deliver_user = str_replace("%%DELIVER_COMMENT%%", $orders["delivery_comment"], $deliver_user);

		$products_total_price = 0;
		// ----------------------------繰り返し
		$products = Config::get("order_mail.products_title");
		$products_temp = Config::get("order_mail.products");
		foreach ($orders["products"] as $details) {
			$temp = str_replace("%%PRODUCT_ID%%", $details["did"], $products_temp);
			$temp = str_replace("%%PRODUCT_CODE%%", $details["data"]->base->code, $temp);
			$temp = str_replace("%%PRODUCT_TITLE%%", $details["data"]->base->title ." ". $details["data"]->option1_name ." ". $details["data"]->option2_name, $temp);
			$temp = str_replace("%%PRODUCT_PRICE%%", number_format(intval($details["price"] / $details["num"])), $temp);
			$temp = str_replace("%%PRODUCT_NUM%%", $details["num"].$details["data"]->base->unit, $temp);
			$temp = str_replace("%%SUBTOTAL%%", number_format($details["price"]), $temp);
			$products .= $temp;
			$products_total_price = $products_total_price +$details["price"];
		}

		$delivers_total = Config::get("order_mail.delivers_total");
		$delivers_total = str_replace("%%POSTAGE%%", number_format($orders["deliver_postage"]["price"]), $delivers_total);

		$total = Config::get("order_mail.total");
		$total = str_replace("%%SUBTOTAL%%", number_format($products_total_price), $total);
		$total = str_replace("%%POSTAGE%%", number_format($orders["postage"]), $total);
		$total = str_replace("%%FEE%%", number_format($orders["fee"]), $total);
		$total = str_replace("%%TOTAL%%", number_format($products_total_price + $orders["postage"] + $orders["fee"]), $total);

		$cvs = "";
		$cvs2 = "";
		if($orders["payment"]==2){
			$cvs = Config::get("order_mail.cvs_data");
			$cvs = str_replace("%%CVS_TITLE%%", Config::get("cvs.".$orders2->cvs_type), $cvs);
			$cvs = str_replace("%%CVS_NO%%", $orders2->cvs_receipt_no, $cvs);
			$cvs = str_replace("%%CVS_LIMIT%%", $orders2->cvs_limit_date, $cvs);

			if ($orders2->cvs_type == "sej1") {
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/711_m.html", $cvs);
			}elseif ($orders2->cvs_type == "econ1"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/lawson_m.html", $cvs);
			}elseif ($orders2->cvs_type == "econ2"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/famima2_m.html", $cvs);
			}elseif ($orders2->cvs_type == "econ3"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/ministop_loppi_m.html", $cvs);
			}elseif ($orders2->cvs_type == "econ4"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/seicomart_m.html", $cvs);
			}elseif ($orders2->cvs_type == "other1"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/circleksunkus_econ_m.html", $cvs);
			}elseif ($orders2->cvs_type == "other2"){
				$cvs = str_replace("%%CVS_SHOP_URL%%", "https://bs.veritrans.co.jp/support/docs/3g/consumer/cvs/mobile/dailyamazaki_m.html", $cvs);
			}

			if ($orders2->cvs_haraikomi_url){
				$cvs2 = Config::get("order_mail.cvs_data_7");
				$cvs2 = str_replace("%%CVS_URL%%", $orders2->cvs_haraikomi_url, $cvs2);
			}

			$total = str_replace("%%TOTAL%%", number_format($products_total_price + $orders["postage"] + $orders["fee"]), $total);
		}

		$order_data = $title . $order_user . $deliver_user . $products . $delivers_total . $total .$cvs .$cvs2;
//		Log::debug(print_r($order_data,true));

		$message = str_replace("%%ORDER_DATA%%", $order_data, $message);
		if(isset($data["username"])) $message = str_replace("%%USER_NAME%%", $data["username"], $message);
		return $message;
	}
	/*
	 * パスワード忘れ
	 */
	public static function mail_forget($message,$data="")
	{
		if($data["forget_param"]) $message = str_replace("%%FORGET_PARAM%%", $data["forget_param"], $message);
		if($data["expire_time"]) $message = str_replace("%%EXPIRE_TIME%%", $data["expire_time"], $message);
		if(isset($data["username"])) $message = str_replace("%%USER_NAME%%", $data["username"], $message);
		return $message;
	}
	/*
	 * 退会
	 */
	public static function mail_leave($message,$data="")
	{
		if($data["username"]) $message = str_replace("%%USER_NAME%%", $data["username"], $message);
		return $message;
	}
}

