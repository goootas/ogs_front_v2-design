<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<div>
	<p>新規会員登録を行います。登録情報を入力してください。</p>
</div>
<?php echo Form::open();?>
<div>
	お名前※必須
	<div>
		<?php echo Form::input('username_sei', Input::post('username_sei', ''), array('size' => '10')); ?>
		<?php echo Form::input('username_mei', Input::post('username_mei', ''), array('size' => '10')); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	ふりがな※必須
	<div>
		<?php echo Form::input('username_sei_kana', Input::post('username_sei_kana', ''), array('size' => '10')); ?>
		<?php echo Form::input('username_mei_kana', Input::post('username_mei_kana', ''), array('size' => '10')); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	メールアドレス
	<div><?php echo Session::get('regist.email'); ?></div>
</div>
<br>
<div>
	パスワード※必須
	<div>
		<?php echo Form::password('password',"", array("size"=>"10","istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	郵便番号※必須
	<div>
		<?php echo Form::input('zip',Input::post('zip', ''), array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		ハイフン(-)は抜いてください
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	都道府県※必須
	<div>
		<?php echo Form::select('state', Input::post('state', ''),$prefecture_data);?>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	住所１※必須
	<div>
		<?php echo Form::input('address1', Input::post('address1', '')); ?><br>
		市区町村

	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	住所２※必須
	<div>
		<?php echo Form::input('address2',Input::post('address2', '')); ?><br>
		番地 / 建物名など
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	電話番号※必須
	<div>
		<?php echo Form::input('tel1', Input::post('tel1', ''), array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('tel2', Input::post('tel2', ''), array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('tel3', Input::post('tel3', ''), array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
	</div>
	<font style="color: #FF0000"><?php echo implode('</font><br><font style="color: #FF0000">', (array) Session::get_flash('e_name')); ?></font>
</div>
<br>
<div>
	性別
	<div>
		<?php echo Form::radio('sex', '1',""); ?>男性
		<?php echo Form::radio('sex', '2',""); ?>女性
	</div>
</div>
<br>
<div>
	生年月日
	<div>
		<?php echo Form::input('birthday', Input::post('birthday', ''), array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
	</div>
</div>
<br>
<div>
	お知らせメール
	<div>
		<?php echo Form::radio('mailmagazine', 1,1); ?>受信する
		<?php echo Form::radio('mailmagazine', 0); ?>受信しない
	</div>
</div>
<br>
<div>
	<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
	<?php echo Form::submit('exec', '登録'); ?>
</div>
<?php echo Form::close();?>
