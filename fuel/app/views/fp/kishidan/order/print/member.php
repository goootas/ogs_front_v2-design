<p style="color: #FF0000;text-align: center"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>

<?php echo Form::open(array('id' => "print" ,'class' => 'form-horizontal','autocomplete'=>'off'));?>
<div class="forms">
	<div style="text-align: center">
		「LAST VISUALIVE ドキュメンタリー写真集」(刻印入り)をご注文のお客様は、G&LOVERS(ファンクラブ)登録情報をご入力ください。
	</div>
	G&LOVERS会員番号※必須
	<div class="form-group">
		<?php echo Form::input('club_no',isset($club_data["club_no"]) ? $club_data["club_no"] : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>
	<br>
<!--	お名前※必須-->
<!--	<div class="form-group">-->
<!--		--><?php //echo Form::input('name_sei', isset($club_data["name_sei"]) ? $club_data["name_sei"] : ""); ?>
<!--		--><?php //echo Form::input('name_mei', isset($club_data["name_mei"]) ? $club_data["name_mei"] : ""); ?>
<!--	</div>-->
<!--	<br>-->
	G&LOVERS登録電話番号※必須
	<div class="form-group">
		<?php echo Form::input('tel',isset($club_data["tel"]) ? $club_data["tel"] : "",array("istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>
	<br>
	<div style="style="color: #FF0000">
		G&LOVERSで「LAST VISUALIVE」のチケットを購入された方以外はご注文できません。
	</div>
	<br>
	<div>
		<?php if( $change == 1) :?>
			<?php echo Form::submit('next', '変更'); ?>
		<?php else:?>
			<?php echo Form::submit('next', '次へ進む'); ?>
		<?php endif;?>
	</div>
	<br>
	<div>
		<?php echo Html::anchor('/cart'.$session_get_param, '<font color="#FFFFFF">カートに戻る</font>'); ?>
	</div>
</div>
<?php echo Form::hidden('change_flg',$change); ?>
<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>

<?php echo Form::close();?>
