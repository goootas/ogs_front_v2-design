<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<p style="color: #000000"><?php echo implode('</p><p style="color: #000000">', (array) Session::get_flash('success')); ?></p>

<div class="fixed-data">

	<h4 class="headline">登録情報</h4>

	<table>
		<tr>
			<td class="head">お名前</td>
			<td class="body"><?php echo $user->username_sei.$user->username_mei; ?></td>
		</tr>

		<tr>
			<td class="head">ふりがな</td>
			<td class="body"><?php echo $user->username_sei_kana.$user->username_mei_kana; ?></td>
		</tr>

		<tr>
			<td class="head">郵便番号</td>
			<td class="body"><?php echo $user->zip; ?></td>
		</tr>

		<tr>
			<td class="head">都道府県</td>
			<td class="body"><?php echo $user->prefecture; ?></td>
		</tr>

		<tr>
			<td class="head">住所１</td>
			<td class="body"><?php echo $user->address1; ?></td>
		</tr>

		<tr>
			<td class="head">住所２</td>
			<td class="body"><?php echo $user->address2; ?></td>
		</tr>

		<tr>
			<td class="head">メールアドレス</td>
			<td class="body"><?php echo $user->email; ?></td>
		</tr>

		<tr>
			<td class="head">電話番号</td>
			<td class="body"><?php echo $user->tel1."-".$user->tel2."-".$user->tel3; ?></td>
		</tr>

		<tr>
			<td class="head">性別</td>
			<td class="body"><?php if($user->sex == 1 ) :?>男性
				<?php elseif($user->sex == 2 ) :?>女性
				<?php else :?>未設定
				<?php endif;?></td>
		</tr>

		<tr>
			<td class="head">生年月日</td>
			<td class="body"><?php echo substr($user->birthday,0,4)."年".substr($user->birthday,4,2)."月".substr($user->birthday,6,2)."日"; ?></td>
		</tr>
		<tr>
			<td class="head">メルマガ</td>
			<td class="body">
				<?php if($user->mailmagazine == 1 ) :?>受信する
				<?php else :?>受信しない
				<?php endif;?>
			</td>
		</tr>
	</table>
	<div class="buttons change count-2">
		<?php echo Html::anchor('javascript:void(0);', '退会する',array("id" => "leave",'class' =>'button rect'),true); ?>
		<?php echo Html::anchor('/mypage/profile','登録情報変更',array('class' => 'button rect key')) ?>
	</div>
</div>

<div class="buy-list">
	<h4 class="headline">注文履歴</h4>
	<table class="">
		<tr>
			<th class="number">受注番号</th>
			<th class="date">注文日</th>
			<th class="price">金額</th>
		</tr>
		<?php if(isset($order)):?>
			<?php foreach($order as $data):?>
				<tr>
					<td><?php echo Html::anchor('/mypage/order/detail/'.$data["order_id"],$data["order_id"],array('class' => 'button text')) ?></td>
					<td><?php echo $data["insert_date"];?></td>
					<td>¥<?php echo number_format($data["d_total"] + $data["fee"] + $data["postage"]);?></td>
				</tr>
			<?php endforeach;?>
		<?php endif;?>
	</table>
</div>

<form class="form-inline" role="form" autocomplete="off" id="mypage">
	<?php echo Form::close();?>


	<script>
		$(function(){
			$("#leave").click(function() {
				if(window.confirm('退会します。よろしいですか？')){
					$("#mypage").attr("action","/<?php echo $shop_data["dir"];?>/mypage/leave");
					$("#mypage").submit();
				}
			});
		});
	</script>


