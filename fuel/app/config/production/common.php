<?php
return array(
	"rakuten" => array(
		"public_key"		=> "live_public_f226a461a841d0571b5fbdd7a21d9cebb7e949506004b1989334b5107430860141ba0729521371410128626cf31d438c",
		"private_key"		=> "live_private_76978d0ba3f0a64d170bf293b4043e7ab7e949506004b1989334b5107430860141ba0729521371410128626cf31d438c",
		"js_url"			=> 'https://lite.checkout.rakuten.co.jp/s/js/checkout-lite-v1.js',
		"api_url"			=> "https://api.lite.checkout.rakuten.co.jp",
		"api_action"		=> array(
			"capture"			=> "/v1/charges/%%CHARGE_ID%%/capture",
			"cancel"			=> "/v1/charges/%%CHARGE_ID%%/refund",
		),
	),
	's3base_url' => "https://reni-ec-pro.s3-ap-northeast-1.amazonaws.com/",
	'assets_url' => "//reni-ec-pro.s3-ap-northeast-1.amazonaws.com/assets/",
	'banner_url' => "//reni-ec-pro.s3-ap-northeast-1.amazonaws.com/banner/",
	"basic" => array(
		'front_mode' => 0,
		'tool_mode' => 1,
		'user' => "admin",
		'pass' => "pass",
	),
	"atobarai_maintenance" => array(
//		"start"		=> 20151201000000,
//		"finish"	=> 20151201110000,
//		"start"		=> 20160927000000,
//		"finish"	=> 20160927120000,
//		"start"		=> 20161007160000,
//		"finish"	=> 20161007180000,
//		"start"		=> 20161019015000,
//		"finish"	=> 20161019043000,
		"start"		=> 20161116020000,
		"finish"	=> 20161116060000,
	),
	// 写真集名入れ商品ID
	"print_product_ids" => array(
		489
	),
	// 写真集名入れ商品詳細ID
	"print_product_detail_ids" => array(
		1922
	),

	"naire_product_ids" => array(
		191,236
	),
	"naire_max_length" => array(
		191 => 17,
		236 => 15,
	),
	"order_limit" => array(
		1310, 1311, 1312, 1313,
		1536,
		1759, 1760, 1761, 1762,
		1799
	),
	"order_limit1" => array(
		1921,1920,1911,1912,1913,1914,1915,1916,1917,1918,1919,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900,1897,1898,1899,1896,1894,1893,1892,1891,1890
	),
	"order_limit2" => array(
		1705,1706,1895
	),
	"apbankfes2016_category1" => 111,
	"apbankfes2016_category1old" => 112,
);
