<?php
/*
 * コンセプトページ
 */
class Controller_Concept extends Controller_Basefront
{
	public function action_index()
	{
		$wheres = array();
		$wheres["shop_id"] = $this->shop_data["id"];
		$wheres["page_id"] = 1;

		// 該当レコードのページ取得
		$page = Model_Db_Cms_Page::findDetail($wheres);
		if(isset($page))
		{
			if($page["redirect_url"])
			{
				Response::redirect($page["redirect_url"]);
				exit;
			}
		}else
		{
			Response::redirect("/");
			exit;
		}

		// 該当レコードのセル取得
		if($this->agent_dir){
			// 1 PC/SP 2 FP
			$wheres["device_type"] = 2;
		}else
		{
			$wheres["device_type"] = 1;
		}

		$cms = "";
		$cells = Model_Db_Cms_Cell::findList($wheres);
		foreach ($cells as $val) {
			$cms .= $val["body"];
		}

		if($cms == "")
		{
			Response::redirect("/");
			exit;
		}

		$this->template->set_global('cms', $cms, false);
		$this->template->content = View::forge($this->agent_dir.$this->shop_data["dir"].'/concept/detail');
	}
}
