<p style="color: #FF0000"><?php echo implode('</p><p style="color: #FF0000">', (array) Session::get_flash('error')); ?></p>
<h4>登録情報</h4>
<?php echo Form::open(array('action' => '/mypage/profile'));?>
<div>
	お名前お名前
	<div>
		<?php echo Form::input('username_sei', $user->username_sei, array("size"=>"10")); ?>
		<?php echo Form::input('username_mei', $user->username_mei, array("size"=>"10")); ?>
	</div>
</div>
<br>
<div>
	ふりがな※必須
	<div>
		<?php echo Form::input('username_sei_kana', $user->username_sei_kana, array("size"=>"10")); ?>
		<?php echo Form::input('username_mei_kana', $user->username_mei_kana, array("size"=>"10")); ?>
	</div>
</div>
<br>
<div>
	メールアドレス※必須
	<div>
		<?php echo Form::input('email', $user->email, array("istyle"=>"3", "format"=>"*x", "MODE"=>"alphabet")); ?>
	</div>
</div>
<br>
<div>
	パスワード※必須
	<div>
		<?php echo Form::password('password',$user->password); ?>
	</div>
</div>
<br>
<div>
	郵便番号※必須
	<div>
		<?php echo Form::input('zip',$user->zip, array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
		ハイフン(-)は抜いてください
	</div>
</div>
<br>
<div>
	都道府県※必須
	<div>
		<?php echo Form::select('state', $user->prefecture,$prefecture_data);?>
	</div>
</div>
<br>
<div>
	住所１※必須
	<div>
		<?php echo Form::input('address1', $user->address1); ?>
	</div>
</div>
<br>
<div>
	住所２※必須
	<div>
		<?php echo Form::input('address2',$user->address2); ?>
	</div>
</div>
<br>
<div>
	電話番号※必須
	<div>
		<?php echo Form::input('tel1', $user->tel1,
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('tel2', $user->tel2,
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
		<?php echo Form::input('tel3', $user->tel3,
			array("size"=>"5","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?>
	</div>
</div>
<br>
<div>
	性別
	<div>
		<?php echo Form::radio('sex', '1',$user->sex); ?>男性
		<?php echo Form::radio('sex', '2',$user->sex); ?>女性
	</div>
</div>
<br>
<div>
	生年月日
	<div>
		<?php echo Form::input('birthday', $user->birthday, array("size"=>"10","istyle"=>"4", "format"=>"*N", "MODE"=>"numeric")); ?><br>
	</div>
</div>
<br>
<div>
	お知らせメール
	<div>
		<?php echo Form::radio('mailmagazine', 1,$user->mailmagazine); ?>受信する
		<?php echo Form::radio('mailmagazine', 0,$user->mailmagazine); ?>受信しない<br>
		メールにて新商品のお知らせなどをさせていただく場合があります

	</div>
</div>
<br>
<div>
	<?php echo Form::hidden(Config::get("session.".Config::get("session.driver").".cookie_name"), Session::key()); ?>
	<?php echo Form::submit('exec', '登録情報変更'); ?>
</div>
<br>
<div>
	<?php echo Html::anchor('mypage'.$session_get_param, '<font color="#000000">前に戻る</font>'); ?>
</div>
<?php echo Form::close();?>
