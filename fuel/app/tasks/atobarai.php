<?php
/**
 * Created by IntelliJ IDEA.
 * Date: 14/11/25
 * Time: 15:46
 * キャッチボール側のメンテナンスによる一時対応となります。
 *
 * 検証 cd /var/www/ogs_front/public ; FUEL_ENV=development php ../oil r atobarai ;
 * 本番 cd /var/www/ogs_front/public ; FUEL_ENV=production php ../oil r atobarai ;
 *
 */
namespace Fuel\Tasks;

use Fuel\Core\Log;
use Fuel\Core\Config;

class Atobarai
{
	public static function run()
	{
		Config::load("common");// 注文確認メールテンプレート（一部）

		$wheres = array();
		$wheres[] = array("payment", Config::get('payment.value.ato'));// 後払い決済
		$wheres[] = array("atobarai_code", "XXXXXXXXX");
		$wheres[] = array("result", 0);// 与信中
		$order_data = \Model_Db_Order::find('all', array(
				'where' => $wheres,
			)
		);

		foreach ($order_data as $val) {

			usleep(100000);// 0.1秒

			//===================================================
			// 店舗情報取得
			//===================================================
			$shop_data = \Model_Db_Shop::findShopDetail($val->shop_id);

			//===================================================
			// 注文者情報取得（整形含む）
			//===================================================
			$order = $val;
			//===================================================
			// 注文内容情報取得（整形含む）
			//===================================================
			$wheres = array();
			$wheres[] = array("shop_id", $val->shop_id);
			$wheres[] = array("order_id", $val->order_id);
			$wheres[] = array("cancel_date","IS",NULL);
			$order_data = \Model_Db_Order::findListFront($wheres)->to_array();
			$total = $val["postage"] + $val["fee"];
			$products = array();
			foreach($order_data["detail"] as $key => $base ){
				$wheres = array();
				$wheres[] = array("id", $base["did"]);
				$tmp = $base;
				$tmp["data"] = \Model_Db_Product_Detail::findProductDetailOne($wheres);
				$products[] = $tmp;
				$total = $total + $base["price"];
			}
			$order["total"] = $total;
			$order["products"] = $products;

			Log::error(print_r($order,true));

			//===================================================
			// 後払いAPI 注文登録
			//===================================================
			$res = \Model_Lib_Payment_Atobarai_Cvs::authorize_exec2($shop_data,$order);

			//===================================================
			// 注文情報更新 後払い注文IDのみ更新 NG処理はバッチで行う。
			//===================================================
			$order_data = \Model_Db_Order::find($val->id);
			$order_data->atobarai_code	= $res["order_id"];
			$order_data->update_id		= 9998;
			$order_data->save();

		}
	}
}
