<?php echo Asset::css(array(Config::get("assets_url").'css/fotorama.css')); ?>
<?php echo Asset::js(array(Config::get("assets_url").'js/fotorama.js')); ?>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<div class="product-photo">
	<div class="fotorama" data-width="100%" data-ratio="4/4" data-nav="thumbs" data-thumbheight="70">
		<?php if(isset($images) && count($images) > 0): ?>
			<?php foreach ($images as $image): ?>
				<?php echo Asset::img("//".$shop_data["s3bucket"] . ".s3-ap-northeast-1.amazonaws.com/".$image."?".time());?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>

<div class="product-texts">

	<div class="product-social">
		<div class="twitter">
			<a href="//twitter.com/share" class="twitter-share-button" data-url="https://<?php echo Input::server("SERVER_NAME").Input::server("REQUEST_URI");?>" data-text="<?php echo $product->title; ?>" data-lang="ja" data-hashtags="<?php echo Config::get("tw.hashtag");?>">ツイート</a>
		</div>
		<div class="facebook">
			<iframe src="//www.facebook.com/plugins/like.php?href=https://<?php echo Input::server("SERVER_NAME")."/".Input::server("REQUEST_URI");?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=10&amp;appId=<?php echo Config::get("fb.app_id");?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
		</div>
	</div>

	<fieldset>
		<!-- 商品基本 -->
		<ul class="list">
			<li class="name"><h3><?php echo isset($product) ? $product->title : '';?></h3></li>
<!--			<li class="code">商品コード:<span>--><?php //echo isset($product) ? $product->code : '';?><!--</span></li>-->
			<li class="price">価格:<span>¥<?php echo isset($product) ? number_format(floor($product->price_sale * $tax)).'(税込)' : '';?></span></li>
		</ul>


		<?php if($kikangai_flg) :?>

			<div class="description notice">
				<br>
				現在販売しておりません<br>
				<br>
			</div>
			<br>

		<?php else :?>

		<form>
			<?php if(isset($product)): ?>
				<?php // 全SKU在庫確認　0の場合は、selectフォームは表示しない ?>
				<?php $stock_cnt = 0;?>
				<?php foreach ($product->detail as $detail): ?>
					<?php $stock_cnt = $stock_cnt + $detail->stock;?>
				<?php endforeach; ?>
				<?php if($stock_cnt <= 0 ): ?>
					<div class="description notice" style="text-align: center ; font-weight: bold;font-size: x-large;">
						<b>SOLD OUT</b>
					</div>
					<br>
				<?php else: ?>
					<select name="select" id="cart_num">
						<?php if(count($product->detail) > 1): ?>
							<option value="">商品種/カラー/サイズ等選択</option>
						<?php endif; ?>
						<?php foreach ($product->detail as $detail): ?>
							<?php if($detail->option1_name || $detail->option2_name): ?>
								<option value="<?php echo $detail->id;?>" <?php if ($detail->stock <= 0){echo "disabled";}?>>
									<?php
									echo $detail->option1_name ."  ".$detail->option2_name.":";
									if ($detail->stock > 0 ){
										if ($detail->stock <= 5 ){
											echo "残りわずか";
										}else{
											echo "在庫あり";
										}
									}else{
										echo "規定数終了";
									}
									?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
					<div class="description notice" style="text-align: left">
						※ご注文数量変更<br>
						購入するボタン押下後増減可能<br>
						(注文点数制限商品の場合不可)<br>
					</div>

					<div class="buttons">
						<a href="javascript:void(0)" class="button key rect size-L" onclick="selectitem();  return false;" >購入する</a>
					</div>

				<?php endif; ?>
			<?php endif; ?>

		<?php echo Form::close();?>

		<?php endif; ?>

		<ul class="freebox">
			<li><?php echo isset($product) ? nl2br($product->comment) : '';?></li>
		</ul>
	</fieldset>
</div>

<?php echo Form::open(array('action' => '/cart/add', 'id' => "cart"));?>
<?php echo Form::hidden('did', '',array('id' =>"did")); ?>
<?php echo Form::hidden('num', '1',array('id' =>"num")); ?>
<?php echo Form::close();?>


<script>
	function selectitem(){
		if($("#cart_num").val() != ""){
			$("#did").val($("select[name='select']").val());
			$("#num").val(1);
			$("#cart").submit();
		}else{
			alert("購入アイテムを選択してください");
		}
	}
</script>
